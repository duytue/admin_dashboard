import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  InputLabel,
  MenuItem,
  Select,
  Checkbox,
  ListItemText,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getOneRole, updateRole, updatePermissionRole } from 'src/actions/roleActions';
import { getListPermissions } from 'src/actions/permissionActions';

const useStyles = makeStyles((theme) => ({
  rootPageUpdateRole: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectEmpty: {
    width: "100%",
  },
}));

function RoleEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const roleId = _.trim(window.location.pathname.slice(22, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const { role } = useSelector((state) => {
    return state.role
  });
  const { permissions } = useSelector((state) => {
    return state.permission
  })

  useEffect(() => {
    dispatch(getOneRole(roleId));
  }, [dispatch, roleId]);

  useEffect(() => {
    dispatch(getListPermissions());
  }, [dispatch])

  const handleAddPermission = (valuePermissions) => {
    try {
      dispatch(updatePermissionRole({ role_id: roleId, permissions_ids: valuePermissions })).then(() => {
        enqueueSnackbar('Role updated Permission', {
          variant: 'success',
          action: <Button>See all</Button>
        });
      })
    } catch (error) {
      enqueueSnackbar('Role update failed', {
        variant: 'error',
        action: <Button>See all</Button>
      });
    }
  }

  if (_.size(role) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        name: role.name || '',
        description: role.description || '',
        permissionsRole: _.map(role.permissions.data, 'id') || [],
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        description: Yup.string().max(255).required('Description is required'),
        permissionsRole: Yup.array(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(updateRole({ ...values, id: roleId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Role updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push("/app/management/roles")
          });

        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageUpdateRole, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Grid container spacing={3}>
              <Grid item xs={8}>
                <Card>
                  <CardContent>
                    <Grid
                      container
                      spacing={3}
                    >
                      <Grid
                        item
                        md={12}
                        xs={24}
                      >
                        <TextField
                          error={Boolean(touched.name && errors.name)}
                          fullWidth
                          helperText={touched.name && errors.name}
                          label="Name"
                          name="name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        md={12}
                        xs={24}
                      >
                        <TextField
                          error={Boolean(touched.description && errors.description)}
                          fullWidth
                          helperText={touched.description && errors.description}
                          label="Description"
                          name="description"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          multiline
                          rows={4}
                          value={values.description}
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        Update role
                </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={4}>
                <Card>
                  <CardContent>
                    <InputLabel shrink id="Permissions_Role">
                      Permission Role
                    </InputLabel>
                    <Select
                      labelId="Permissions_Role"
                      id="Permissions_Role"
                      label="Permission Role"
                      name="permissionsRole"
                      multiple
                      value={values.permissionsRole}
                      onChange={handleChange}
                      renderValue={(selected) => {
                        const itemAfterSelected = selected.map(item => _.get(_.find(permissions, i => i.id === item), 'name'))
                        return itemAfterSelected.join(', ')
                      }}
                      className={clsx(classes.selectEmpty)}
                    >
                      {
                        permissions && permissions.map((item) => (
                          <MenuItem key={item.id} value={item.id}>
                            <Checkbox checked={values.permissionsRole.indexOf(item.id) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))
                      }
                    </Select>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => handleAddPermission(values.permissionsRole)}
                      >
                        Add Permission
                      </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </form>
        )}
    </Formik>
  );
}

RoleEditForm.propTypes = {
  className: PropTypes.string,
  role: PropTypes.object.isRequired
};

export default RoleEditForm;
