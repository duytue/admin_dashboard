import React, {
  // useState,
  // useCallback,
  // useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import PlatformEditForm from './PlatformEditForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PlatformsEditView() {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Platforms Edit"
    >
      <Container maxWidth="lg">
        <Header />
        <Box mt={3}>
          <PlatformEditForm />
        </Box>
      </Container>
    </Page>
  );
}

export default PlatformsEditView;
