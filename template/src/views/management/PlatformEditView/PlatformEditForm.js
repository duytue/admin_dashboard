import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
  Switch,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getOnePlatform, updatePlatform } from 'src/actions/platformActions';

const useStyles = makeStyles(() => ({
  root: {}
}));

function PlatformEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const platformId = _.trim(window.location.pathname.slice(25, -4), '/');
  const dispatch = useDispatch();
  const { get_one } = useSelector((state) => {
    return state.platform
  });
  const history = useHistory();

  useEffect(() => {
    dispatch(getOnePlatform(platformId));
  }, [dispatch, platformId]);


  if (_.size(get_one) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        name: get_one.name || '',
        code: get_one.code || '',
        description: get_one.description || '',
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        code: Yup.string().max(255).required('Code is required'),
        description: Yup.string().max(255).required('Description is required'),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(updatePlatform({ ...values, id: platformId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Platform updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/platform"))
          });

        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.code && errors.code)}
                      fullWidth
                      helperText={touched.code && errors.code}
                      label="Code"
                      name="code"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.code}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Update platform
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

PlatformEditForm.propTypes = {
  className: PropTypes.string,
  platform: PropTypes.object.isRequired
};

export default PlatformEditForm;
