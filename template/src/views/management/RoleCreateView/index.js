import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import RoleCreateForm from './RoleCreateForm';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function RoleCreateView() {
  const classes = useStyles();
  return (
    <Page
      className={classes.rootPageCreateView}
      title="Role Create"
    >
      <Container maxWidth={false}>
        <Header />
        <RoleCreateForm />
      </Container>
    </Page>
  );
}

export default RoleCreateView;
