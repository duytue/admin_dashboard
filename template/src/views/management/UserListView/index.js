import React, {
  // useState,
  useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getUsers } from 'src/actions/userActions';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function UserListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { users } = useSelector((state) => {
    return state.user
  });

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  if (!users) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="User List"
    >
      <Container maxWidth={false}>
        <Header />
        {users && (
          <Box mt={3}>
            <Results users={users} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default UserListView;
