import React, {
  // useState,
  useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getRoles } from 'src/actions/roleActions';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function RoleListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { roles } = useSelector((state) => {
    return state.role
  });

  useEffect(() => {
    dispatch(getRoles());
  }, [dispatch]);

  if (!roles) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Role List"
    >
      <Container maxWidth={false}>
        <Header />
        {roles && (
          <Box mt={3}>
            <Results users={roles} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default RoleListView;
