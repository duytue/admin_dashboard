import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
  Select,
  Switch,
  makeStyles
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getOneUser, updateUser, updateRoleUser } from 'src/actions/userActions';
import { getRoles } from 'src/actions/roleActions';

const useStyles = makeStyles(() => ({
  root: {},
  selectEmpty: {
    width: "100%",
  },
}));

function UserEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const userId = _.trim(window.location.pathname.slice(22, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const { user } = useSelector((state) => {
    return state.user
  });
  const { roles } = useSelector((state) => {
    return state.role
  })

  useEffect(() => {
    dispatch(getOneUser(userId))
  }, [dispatch, userId]);

  useEffect(() => {
    dispatch(getRoles());
  }, [dispatch])

  const handleAddRole = (valueRoles) => {
    try {
      dispatch(updateRoleUser({ user_id: userId, roles_ids: valueRoles })).then(() => {
        enqueueSnackbar('User updated Role', {
          variant: 'success',
          action: <Button>See all</Button>
        });
      })
    } catch (error) {
      enqueueSnackbar('User updated Failed', {
        variant: 'error',
        action: <Button>See all</Button>
      });
    }
  }

  if (_.size(user) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        email: user.email || '',
        first_name: user.first_name || '',
        last_name: user.last_name || '',
        phone: user.phone || '',
        active: user.active || false,
        birthday: new Date(user.birthday) || new Date(),
        rolesUser: _.map(user.roles.data, 'id') || [],
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
        first_name: Yup.string().max(255).required('First name is required'),
        last_name: Yup.string().max(255).required('Last name is required'),
        phone: Yup.string().max(15),
        birthday: Yup.date(),
        active: Yup.bool(),
        rolesUser: Yup.array(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting,
      }) => {
        try {
          // Make API request
          dispatch(updateUser({ ...values, id: userId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('User updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/users"))
          });

        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Grid container spacing={3}>
              <Grid item xs={8}>
                <Card>
                  <CardContent>
                    <Grid
                      container
                      spacing={3}
                    >
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.email && errors.email)}
                          fullWidth
                          helperText={touched.email && errors.email}
                          label="Email address"
                          name="email"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.email}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.first_name && errors.first_name)}
                          fullWidth
                          helperText={touched.first_name && errors.first_name}
                          label="First name"
                          name="first_name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.first_name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.last_name && errors.last_name)}
                          fullWidth
                          helperText={touched.last_name && errors.last_name}
                          label="Last name"
                          name="last_name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.last_name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <TextField
                          error={Boolean(touched.phone && errors.phone)}
                          fullWidth
                          helperText={touched.phone && errors.phone}
                          label="Phone number"
                          name="phone"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.phone}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="DD/MM/YYYY"
                          name="birthday"
                          margin="normal"
                          id="Date Of Birth"
                          label="Date Of Birth"
                          value={values.birthday}
                          onChange={val => {
                            setFieldValue("birthday", val);
                          }}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                      >
                        <Typography
                          variant="h5"
                          color="textPrimary"
                        >
                          User Active
                        </Typography>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                        >
                          This will give the user is active to work
                        </Typography>
                        <Switch
                          checked={values.active}
                          color="secondary"
                          edge="start"
                          name="active"
                          onChange={handleChange}
                          value={values.active}
                        />
                      </Grid>
                    </Grid>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        Update user
                      </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={4}>
                <Card>
                  <CardContent>
                    <InputLabel shrink id="Roles_User">
                      Roles User
                    </InputLabel>
                    <Select
                      labelId="Roles_User"
                      id="Roles_User"
                      label="Roles User"
                      name="rolesUser"
                      multiple
                      value={values.rolesUser}
                      onChange={handleChange}
                      renderValue={(selected) => {
                        const itemAfterSelected = selected.map(item => _.get(_.find(roles, i => i.id === item), 'name'))
                        return itemAfterSelected.join(', ')
                      }}
                      className={clsx(classes.selectEmpty)}
                    >
                      {
                        roles && roles.map((item) => (
                          <MenuItem key={item.id} value={item.id}>
                            <Checkbox checked={values.rolesUser.indexOf(item.id) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))
                      }
                    </Select>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => handleAddRole(values.rolesUser)}
                      >
                        Add Role
                      </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </form>
        )}
    </Formik>
  );
}

UserEditForm.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object.isRequired
};

export default UserEditForm;
