import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  makeStyles
} from '@material-ui/core';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import PersonIcon from '@material-ui/icons/PersonOutline';
import Label from 'src/components/Label';

const useStyles = makeStyles(theme => ({
  root: {},
  fontWeightMedium: {
    fontWeight: theme.typography.fontWeightMedium
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

function CustomerInfo({ get_one, className, ...rest }) {
  const classes = useStyles();

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardHeader title="Platform info" />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>Name</TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {get_one.name}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>Code</TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {get_one.code}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Description
            </TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {get_one.description}
              </Typography>
            </TableCell>
          </TableRow>
          {/* <TableRow>
            <TableCell className={classes.fontWeightMedium}>Country</TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {get_one.country}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Address 1
            </TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {customer.address1}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Address 2
            </TableCell>
            <TableCell>
              <Typography variant="body2" color="textSecondary">
                {customer.address2}
              </Typography>
            </TableCell>
          </TableRow> */}
        </TableBody>
      </Table>
      {/* <Box p={1} display="flex" flexDirection="column" alignItems="flex-start">
        <Button>
          <LockOpenIcon className={classes.actionIcon} />
          Reset &amp; Send Password
        </Button>
        <Button>
          <PersonIcon className={classes.actionIcon} />
          Login as Customer
        </Button>
      </Box> */}
    </Card>
  );
}

CustomerInfo.propTypes = {
  className: PropTypes.string,
  get_one: PropTypes.object.isRequired
};

export default CustomerInfo;
