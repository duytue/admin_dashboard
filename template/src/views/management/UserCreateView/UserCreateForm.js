import React, { useState } from 'react';
import {
  useDispatch,
  // useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Switch,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useHistory } from "react-router-dom";
import { createUser } from 'src/actions/userActions';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [selectBirthDay, setSelectBirthDay] = useState(new Date());

  const handleChangeBirthDay = value => {
    setSelectBirthDay(value)
  }

  return (
    <Formik
      initialValues={{
        email: '',
        first_name: '',
        last_name: '',
        phone: '',
        active: true,
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
        first_name: Yup.string().max(255).required('First name is required'),
        last_name: Yup.string().max(255).required('Last name is required'),
        phone: Yup.string().max(15),
        active: Yup.bool()
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(createUser({ ...values, birthday: selectBirthDay })).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('User created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/users"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email}
                      label="Email address"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.first_name && errors.first_name)}
                      fullWidth
                      helperText={touched.first_name && errors.first_name}
                      label="First name"
                      name="first_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.first_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.last_name && errors.last_name)}
                      fullWidth
                      helperText={touched.last_name && errors.last_name}
                      label="Last name"
                      name="last_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.last_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.phone && errors.phone)}
                      fullWidth
                      helperText={touched.phone && errors.phone}
                      label="Phone number"
                      name="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.phone}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <KeyboardDatePicker
                      disableToolbar
                      variant="inline"
                      format="DD/MM/YYYY"
                      margin="normal"
                      id="Date Of Birth"
                      label="Date Of Birth"
                      value={selectBirthDay}
                      onChange={handleChangeBirthDay}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Typography
                      variant="h5"
                      color="textPrimary"
                    >
                      User Active
                  </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                    >
                      This will give the user is active to work
                  </Typography>
                    <Switch
                      checked={values.active}
                      color="secondary"
                      edge="start"
                      name="active"
                      onChange={handleChange}
                      value={values.active}
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create User
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CustomerEditForm.propTypes = {
  className: PropTypes.string,
};

export default CustomerEditForm;
