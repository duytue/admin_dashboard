import React from 'react';
import { Chip } from '@material-ui/core';
import {
  Briefcase as BriefcaseIcon,
  Calendar as CalendarIcon,
  ShoppingCart as ShoppingCartIcon,
  Folder as FolderIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  UserPlus as UserPlusIcon,
  Shield as ShieldIcon,
  AlertCircle as AlertCircleIcon,
  Trello as TrelloIcon,
  User as UserIcon,
  Layout as LayoutIcon,
  Edit as EditIcon,
  DollarSign as DollarSignIcon,
  Mail as MailIcon,
  MessageCircle as MessageCircleIcon,
  PieChart as PieChartIcon,
  Share2 as ShareIcon,
  Users as UsersIcon,
  Tablet as TabletIcon,
  Aperture as ApertureIcon,
  Layers as LayersIcon,
  Activity as ActivityIcon
} from 'react-feather';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';

export default [
  // {
  //     subheader: 'Reports',
  //     iconHeader: PieChartIcon,
  //     hideInMenu: true,
  //     items: [
  //         {
  //             title: 'Dashboard New',
  //             icon: TabletIcon,
  //             href: '/app/reports/dashboard_new'
  //         },
  //         {
  //             title: 'Dashboard',
  //             icon: PieChartIcon,
  //             href: '/app/reports/dashboard'
  //         },
  //         {
  //             title: 'Dashboard Alternative',
  //             icon: BarChartIcon,
  //             href: '/app/reports/dashboard-alternative'
  //         }
  //     ]
  // },
  {
    subheader: 'Management',
    iconHeader: UsersIcon,
    items: [
      // {
      //     title: 'Customers',
      //     icon: UsersIcon,
      //     href: '/app/management/customers',
      //     items: [
      //         {
      //             title: 'List Customers',
      //             href: '/app/management/customers'
      //         },
      //         {
      //             title: 'View Customer',
      //             href: '/app/management/customers/1'
      //         },
      //         {
      //             title: 'Edit Customer',
      //             href: '/app/management/customers/1/edit'
      //         }
      //     ]
      // },
      // {
      //     title: 'Products',
      //     icon: ShoppingCartIcon,
      //     href: '/app/management/products',
      //     items: [
      //         {
      //             title: 'List Products',
      //             href: '/app/management/products'
      //         },
      //         {
      //             title: 'Create Product',
      //             href: '/app/management/products/create'
      //         }
      //     ]
      // },
      // {
      //     title: 'Orders',
      //     icon: FolderIcon,
      //     href: '/app/management/orders',
      //     items: [
      //         {
      //             title: 'List Orders',
      //             href: '/app/management/orders'
      //         },
      //         {
      //             title: 'View Order',
      //             href: '/app/management/orders/1'
      //         }
      //     ]
      // },
      // {
      //     title: 'Invoices',
      //     icon: ReceiptIcon,
      //     href: '/app/management/invoices',
      //     items: [
      //         {
      //             title: 'List Invoices',
      //             href: '/app/management/invoices'
      //         },
      //         {
      //             title: 'View Invoice',
      //             href: '/app/management/invoices/1'
      //         }
      //     ]
      // },
      {
        title: 'Users',
        icon: UsersIcon,
        href: '/app/management/users',
        items: [
          {
            title: 'List Users',
            href: '/app/management/users'
          },
          {
            title: 'Create User',
            href: '/app/management/users/create'
          }
        ]
      },
      {
        title: 'Roles',
        icon: ApertureIcon,
        href: '/app/management/roles',
        items: [
          {
            title: 'List Roles',
            href: '/app/management/roles'
          },
          {
            title: 'Create Roles',
            href: '/app/management/roles/create'
          }
        ]
      },
      {
        title: 'Permissions',
        icon: LayersIcon,
        href: '/app/management/permissions',
        items: [
          {
            title: 'List Permissions',
            href: '/app/management/permissions'
          },
          {
            title: 'Create Permission',
            href: '/app/management/permissions/create'
          }
        ]
      },
      {
        title: 'Platform',
        icon: ActivityIcon,
        href: '/app/management/platform',
        items: [
          {
            title: 'List Platform',
            href: '/app/management/platform'
          },
          {
            title: 'Create Platform',
            href: '/app/management/platform/create'
          }
        ]
      }
    ]
  }
  // {
  //     subheader: 'Applications',
  //     iconHeader: BriefcaseIcon,
  //     items: [
  //         {
  //             title: 'Projects Platform',
  //             href: '/app/projects',
  //             icon: BriefcaseIcon,
  //             items: [
  //                 {
  //                     title: 'Overview',
  //                     href: '/app/projects/overview'
  //                 },
  //                 {
  //                     title: 'Browse Projects',
  //                     href: '/app/projects/browse'
  //                 },
  //                 {
  //                     title: 'Create Project',
  //                     href: '/app/projects/create'
  //                 },
  //                 {
  //                     title: 'View Project',
  //                     href: '/app/projects/1'
  //                 }
  //             ]
  //         },
  //         {
  //             title: 'Social Platform',
  //             href: '/app/social',
  //             icon: ShareIcon,
  //             items: [
  //                 {
  //                     title: 'Profile',
  //                     href: '/app/social/profile'
  //                 },
  //                 {
  //                     title: 'Feed',
  //                     href: '/app/social/feed'
  //                 }
  //             ]
  //         },
  //         {
  //             title: 'Kanban',
  //             href: '/app/kanban',
  //             icon: TrelloIcon
  //         },
  //         {
  //             title: 'Mail',
  //             href: '/app/mail',
  //             icon: MailIcon,
  //             info: () => (
  //                 <Chip
  //                     color="secondary"
  //                     size="small"
  //                     label="Updated"
  //                 />
  //             )
  //         },
  //         {
  //             title: 'Chat',
  //             href: '/app/chat',
  //             icon: MessageCircleIcon,
  //             info: () => (
  //                 <Chip
  //                     color="secondary"
  //                     size="small"
  //                     label="Updated"
  //                 />
  //             )
  //         },
  //         {
  //             title: 'Calendar',
  //             href: '/app/calendar',
  //             icon: CalendarIcon
  //         },
  //     ]
  // },
  // {
  //     subheader: 'Auth',
  //     iconHeader: LockIcon,
  //     items: [
  //         {
  //             title: 'Login',
  //             href: '/login-unprotected',
  //             icon: LockIcon
  //         },
  //         {
  //             title: 'Register',
  //             href: '/register-unprotected',
  //             icon: UserPlusIcon
  //         },
  //         {
  //             title: 'Login: Guest Protected',
  //             href: '/login',
  //             icon: ShieldIcon
  //         },
  //         {
  //             title: 'Register: Guest Protected',
  //             href: '/register',
  //             icon: ShieldIcon
  //         }
  //     ]
  // },
  // {
  //     subheader: 'Pages',
  //     iconHeader: UserIcon,
  //     href: '/app/pages',
  //     items: [
  //         {
  //             title: 'Account',
  //             href: '/app/account',
  //             icon: UserIcon
  //         },
  //         {
  //             title: 'Error',
  //             href: '/404',
  //             icon: AlertCircleIcon
  //         },
  //         {
  //             title: 'Pricing',
  //             href: '/pricing',
  //             icon: DollarSignIcon
  //         }
  //     ]
  // },
  // {
  //     subheader: 'Extra',
  //     iconHeader: BarChartIcon,
  //     items: [
  //         {
  //             title: 'Charts',
  //             href: '/app/extra/charts',
  //             icon: BarChartIcon,
  //             items: [
  //                 {
  //                     title: 'Apex Charts',
  //                     href: '/app/extra/charts/apex'
  //                 }
  //             ]
  //         },
  //         {
  //             title: 'Forms',
  //             href: '/app/extra/forms',
  //             icon: EditIcon,
  //             items: [
  //                 {
  //                     title: 'Formik',
  //                     href: '/app/extra/forms/formik'
  //                 },
  //                 {
  //                     title: 'Redux Forms',
  //                     href: '/app/extra/forms/redux'
  //                 },
  //             ]
  //         },
  //         {
  //             title: 'Editors',
  //             href: '/app/extra/editors',
  //             icon: LayoutIcon,
  //             items: [
  //                 {
  //                     title: 'DraftJS Editor',
  //                     href: '/app/extra/editors/draft-js'
  //                 },
  //                 {
  //                     title: 'Quill Editor',
  //                     href: '/app/extra/editors/quill'
  //                 }
  //             ]
  //         }
  //     ]
  // }
];
