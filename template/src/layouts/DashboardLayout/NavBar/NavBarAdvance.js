import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { useLocation, matchPath } from 'react-router';
import {
    Mail as MailIcon,
    MoveToInbox as InboxIcon
} from '@material-ui/icons';
import {
    ShoppingCart as ShoppingCartIcon,
    HelpCircle as HelpCircleIcon,
} from 'react-feather';
import {
    ListItemText, ListItemIcon, ListItem, Divider, List,
    Button, Drawer, Link, Box, Hidden, Avatar, Backdrop,
    ListSubheader,
} from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Link as RouterLink } from 'react-router-dom';
import Logo from 'src/components/Logo';
import navConfig from './NavBarTree';
import NavItem from './NavItem';

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    drawerBar: {
        width: 100,
        marginTop: 60,
        boxSizing: 'border-box',
        backgroundColor: '#ffffff'
    },
});

export default function DrawerAdvanced() {
    const classes = useStyles();
    const [openDrawer, setOpenDrawer] = React.useState(false)
    const [dataDrawerActive, setDataDrawerActive] = React.useState([])

    const location = useLocation();
    const { user } = useSelector((state) => state.account);

    const toggleDrawer = (open, dataNav) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setOpenDrawer(open)
        if (open) setDataDrawerActive(dataNav.items)
    };

    function renderNavItems({ items, ...rest }) {
        return (
            <List disablePadding>
                {items.reduce(
                    (acc, item) => reduceChildRoutes({ acc, item, ...rest }),
                    []
                )}
            </List>
        )
    }

    function reduceChildRoutes({
        acc,
        pathname,
        item,
        depth = 0
    }) {
        const key = item.title + depth;
        if (item.items) {
            const open = matchPath(pathname, {
                path: item.href,
                exact: false
            });
            acc.push(
                <NavItem
                    depth={depth}
                    icon={item.icon}
                    key={key}
                    info={item.info}
                    open={Boolean(open)}
                    title={item.title}
                >
                    {renderNavItems({
                        depth: depth + 1,
                        pathname,
                        items: item.items
                    })}
                </NavItem>
            );
        } else {
            acc.push(
                <NavItem
                    depth={depth}
                    href={item.href}
                    icon={item.icon}
                    key={key}
                    info={item.info}
                    title={item.title}
                />
            );
        }

        return acc;
    }

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            // onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
        >
            <List>
                {renderNavItems({ items: dataDrawerActive, pathname: location.pathname })}
            </List>
        </div>
    );

    return (
        <Box className={classes.drawerBar}>
            <Drawer
                anchor={'left'}
                open={openDrawer}
                onClose={toggleDrawer(false, {})}
            >
                {list('left')}
            </Drawer>
            <PerfectScrollbar options={{ suppressScrollX: true }}>
                <Hidden lgUp>
                    <Box
                        p={2}
                        display="flex"
                        justifyContent="center"
                    >
                        <RouterLink to="/">
                            <Logo />
                        </RouterLink>
                    </Box>
                </Hidden>
                <Box p={2}>
                    <Box
                        display="flex"
                        justifyContent="center"
                    >
                        <RouterLink to="/app/account">
                            <Avatar
                                alt="User"
                                className={classes.avatar}
                                src={user.avatar}
                            />
                        </RouterLink>
                    </Box>
                </Box>
                <Divider />
                <Box p={2}>
                    {navConfig.map((config) => {
                        const IconName = config.iconHeader;
                        return (
                            <React.Fragment key={config.subheader}>
                                <Button onClick={toggleDrawer(true, config)} >
                                    {config.iconHeader ? <IconName /> : <ShoppingCartIcon />}
                                </Button>
                            </React.Fragment>
                        )
                    })
                    }
                </Box>
                <Divider />
                <Box p={2}>
                    <Box p={2}>
                        <Link
                            variant="subtitle1"
                            color="secondary"
                            component={RouterLink}
                            to="/docs"
                        >
                            <HelpCircleIcon />
                            Help
                        </Link>
                    </Box>
                </Box>
            </PerfectScrollbar>
        </Box>
    );
}
