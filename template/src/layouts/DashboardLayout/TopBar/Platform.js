import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import {
  Menu,
  Box,
  IconButton,
  Button,
  Link,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  MenuItem,
  Popover,
  SvgIcon,
  Tooltip,
  Typography,
  makeStyles
} from '@material-ui/core';
import { ChevronDown as ChevronDownIcon } from 'react-feather';
import OnlineIndicator from 'src/components/OnlineIndicator';
import { getContacts } from 'src/actions/chatActions';

const useStyles = makeStyles((theme) => ({
  popover: {
    width: 320,
    padding: theme.spacing(2)
  },
  list: {
    padding: theme.spacing(1, 3)
  },
  listItemText: {
    marginRight: theme.spacing(1)
  },
  lastActivity: {
    whiteSpace: 'nowrap'
  }
}));

const INTINIAL_PLATFORM = [
  { id: 1, name: 'all' },
  { id: 2, name: 'mobile' },
  { id: 3, name: 'web' }
];

function PlatformSelect() {
  const classes = useStyles();
  const ref = useRef(null);
  const dispatch = useDispatch();
  const { contacts } = useSelector((state) => state.chat);
  const [isOpen, setOpen] = useState(false);
  const [selectedVal, setSelectedVal] = useState('');
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(getContacts());
  }, [dispatch]);

  return (
    <>
      <Tooltip title="Contacts">
        <IconButton color="inherit" onClick={handleOpen} ref={ref}>
          <Typography color="textPrimary" variant="h5">
            Platform Select
          </Typography>
          <SvgIcon fontSize="small">
            <ChevronDownIcon />
          </SvgIcon>
        </IconButton>
      </Tooltip>
      <Popover
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        classes={{ paper: classes.popover }}
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
      >
        <Box mt={2}>
          <Menu
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            keepMounted
            PaperProps={{ className: classes.popover }}
            getContentAnchorEl={null}
            anchorEl={ref.current}
            open={isOpen}
          >
            {INTINIAL_PLATFORM.map((platform) => {
              return (
                <MenuItem
                  component={RouterLink}
                  // to="/app/social/profile"
                >
                  {platform.name}
                </MenuItem>
              );
            })}
          </Menu>
        </Box>
      </Popover>
    </>
  );
}

export default PlatformSelect;
