import {
  GET_ROLES,
  CREATE_ROLE,
  GET_ONE_ROLE,
  UPDATE_ROLE,
} from 'src/actions/roleActions';
import _ from 'lodash';

const initialState = {
  roles: [],
  role: {},
};

const roleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ROLES: {
      return {
        ...state,
        roles: action.payload
      }
    }

    case CREATE_ROLE: {
      return {
        ...state,
        roles: state.roles.concat(action.payload)
      }
    }

    case GET_ONE_ROLE: {
      return {
        ...state,
        role: action.payload,
      }
    }

    case UPDATE_ROLE: {
      const updateRole = _.map(state.roles, i => {
        if (i.id === action.payload.id) return action.payload;
        return i;
      })
      if (updateRole !== undefined && updateRole.length > 0) {
        return {
          ...state,
          roles: updateRole,
        }
      } else return {
        ...state,
        roles: action.payload
      }

    }

    default: {
      return state;
    }
  }
};

export default roleReducer;
