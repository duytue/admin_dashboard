/* eslint-disable no-param-reassign */
import produce from 'immer';
import {
  LIST_PLATFORM_REQUEST,
  LIST_PLATFORM_SUCCESS,
  LIST_PLATFORM_FAILURE,
  CREATE_PLATFORM_REQUEST,
  CREATE_PLATFORM_SUCCESS,
  CREATE_PLATFORM_FAILURE,
  GET_ONE_PLATFORM_REQUEST,
  GET_ONE_PLATFORM_SUCCESS,
  GET_ONE_PLATFORM_FAILURE
} from 'src/actions/platformActions';

const initialState = {
  platform: null,
  create_platform: null,
  get_one: null
};

const platformReducer = (state = initialState, action) => {
  switch (action.type) {
    case LIST_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.platform = null;
      });
    }

    case LIST_PLATFORM_SUCCESS: {
      const { platform } = action.payload;
      return produce(state, draft => {
        draft.platform = platform;
      });
    }

    case LIST_PLATFORM_FAILURE: {
      return produce(state, () => {
        // Maybe store error
      });
    }

    case CREATE_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.create_platform = null;
      });
    }

    case CREATE_PLATFORM_SUCCESS: {
      const { platform } = action.payload;
      return produce(state, draft => {
        draft.create_platform = platform;
      });
    }

    case CREATE_PLATFORM_FAILURE: {
      return produce(state, () => {
        // Maybe store error
      });
    }

    case GET_ONE_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.get_one = null;
      });
    }

    case GET_ONE_PLATFORM_SUCCESS: {
      const { result } = action.payload;
      return produce(state, draft => {
        draft.get_one = result;
        console.log('draft', draft, result);
      });
    }

    case GET_ONE_PLATFORM_FAILURE: {
      return produce(state, () => {
        // Maybe store error
      });
    }

    default: {
      return state;
    }
  }
};

export default platformReducer;
