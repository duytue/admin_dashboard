import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import accountReducer from './accountReducer';
import notificationsReducer from './notificationsReducer';
import chatReducer from './chatReducer';
import mailReducer from './mailReducer';
import kanbanReducer from './kanbanReducer';
import userReducer from './userReducer';
import roleReducer from './roleReducer';
import permissionReducer from './permissionReducer';
import platformReducer from './platformReducer';

const rootReducer = combineReducers({
  account: accountReducer,
  notifications: notificationsReducer,
  chat: chatReducer,
  mail: mailReducer,
  kanban: kanbanReducer,
  form: formReducer,
  user: userReducer,
  role: roleReducer,
  permission: permissionReducer,
  platform: platformReducer,
});

export default rootReducer;
