import {
  GET_PERMISSIONS,
  CREATE_PERMISSION,
  GET_ONE_PERMISSION,
  UPDATE_PERMISSION,
} from 'src/actions/permissionActions';
import _ from 'lodash';

const initialState = {
  permissions: [],
  permission: {},
};

const permissionReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PERMISSIONS: {
      return {
        ...state,
        permissions: action.payload
      }
    }

    case CREATE_PERMISSION: {
      return {
        ...state,
        permissions: state.permissions.concat(action.payload)
      }
    }

    case GET_ONE_PERMISSION: {
      return {
        ...state,
        permission: action.payload,
      }
    }

    case UPDATE_PERMISSION: {
      const updateRole = _.map(state.permissions, i => {
        if (i.id === action.payload.id) return action.payload;
        return i;
      })
      return {
        ...state,
        permission: updateRole,
      }
    }

    default: {
      return state;
    }
  }
};

export default permissionReducer;
