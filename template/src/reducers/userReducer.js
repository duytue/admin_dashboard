import {
  GET_USERS,
  CREATE_USER,
  GET_ONE_USER,
  UPDATE_USER,
  UPDATE_ACTIVE_USER,
} from 'src/actions/userActions';
import _ from 'lodash';

const initialState = {
  users: [],
  user: {},
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS: {
      return {
        ...state,
        users: action.payload
      }
    }

    case CREATE_USER: {
      return {
        ...state,
        users: state.users.concat(action.payload)
      }
    }

    case GET_ONE_USER: {
      return {
        ...state,
        user: action.payload,
      }
    }

    case UPDATE_USER: {
      const updateUser = _.map(state.users, i => {
        if (i.id === action.payload.id) return action.payload;
        return i;
      })
      if (updateUser !== undefined && updateUser.length > 0) {
        return {
          ...state,
          users: updateUser,
        }
      } else return {
        ...state,
        users: action.payload
      }
    }

    case UPDATE_ACTIVE_USER: {
      const updateUser = _.map(state.users, i => {
        if (i.id === action.payload.id) return action.payload;
        return i;
      })
      return {
        ...state,
        users: updateUser,
      }
    }

    default: {
      return state;
    }
  }
};

export default userReducer;
