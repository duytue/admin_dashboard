import jwtDecode from 'jwt-decode';
import { API_BASE_URL } from '../config';
var access_token = localStorage.getItem('accessToken') || undefined;
var axios = require('axios');

class PlatFormService {
  getListPlatForm = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_URL}/api/platforms?size=5&page=1`,
        headers: {
          Authorization: `Bearer ${access_token}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOnePlatForm = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_BASE_URL}/api/platform`,
        headers: {
          Authorization: `Bearer ${access_token}`
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOnePlatForm = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_URL}/api/platform/${id}`,
        headers: {
          Authorization: `Bearer ${access_token}`
        }
      };

      axios(config)
        .then(response => {
          console.log('getOnePlatForm', response);
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updatePlatform = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'put',
        url: `${API_BASE_URL}/api/platform/${data.id}`,
        headers: {
          Authorization: `Bearer ${access_token}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const platFormService = new PlatFormService();

export default platFormService;
