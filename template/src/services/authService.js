import jwtDecode from 'jwt-decode';
import axios from 'src/utils/axios';

class AuthService {
  setAxiosInterceptors = ({ onLogout }) => {
    axios.interceptors.response.use(
      (response) => response,
      (error) => {
        if (error.response && error.response.status === 401) {
          this.setSession(null);

          if (onLogout) {
            onLogout();
          }
        }

        return Promise.reject(error);
      }
    );
  };

  handleAuthentication() {
    const accessToken = this.getAccessToken();

    if (!accessToken) {
      return;
    }

    if (this.isValidToken(accessToken)) {
      this.setSession(accessToken);
    } else {
      this.setSession(null);
    }
  }

  loginWithEmailAndPassword = (email, password) => new Promise((resolve, reject) => {
    const instance = require('axios');
    const data = JSON.stringify({ "email": email, "password": password });

    const config = {
      method: 'post',
      url: 'https://api-stg-cen.jamalex.net/api/login',
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    instance(config)
      .then((response) => {
        if (response.data.data.user_info) {
          this.setSession(response.data.data.token);
          resolve(response.data.data.user_info);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  })

  loginInWithToken = () => new Promise((resolve, reject) => {
    const instance = require('axios');
    const access_token = localStorage.getItem('accessToken') || undefined;
    instance.get('https://api-stg-cen.jamalex.net/ms-user/api/user/3d387547-2c92-40b0-a8b5-9977f4f55cfb', {
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    })
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
          this.logout();
        }
      })
      .catch((error) => {
        reject(error);
      });
  })

  logout = () => {
    this.setSession(null);
  }

  setSession = (accessToken) => {
    if (accessToken) {
      localStorage.setItem('accessToken', accessToken);
      axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    } else {
      localStorage.removeItem('accessToken');
      delete axios.defaults.headers.common.Authorization;
    }
  }

  getAccessToken = () => localStorage.getItem('accessToken');

  isValidToken = (accessToken) => {
    if (!accessToken) {
      return false;
    }

    const decoded = jwtDecode(accessToken);
    const currentTime = Date.now() / 1000;

    return decoded.exp > currentTime;
  }

  isAuthenticated = () => !!this.getAccessToken()
}

const authService = new AuthService();

export default authService;
