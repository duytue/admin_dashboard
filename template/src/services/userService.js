import jwtDecode from 'jwt-decode';
// import axios from 'src/utils/axios';
var axios = require('axios');

const instance = axios.create({
    baseURL: 'https://api-stg-cen.jamalex.net/ms-user',
    headers: {
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
        Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTIwNTE4NzMsImp0aSI6Ikg4WHh0OENfckMzUlFUR0ZQd1VPSGdzb2hyQTdxd1V5eG9nRjNYaUplRExIakk0M1ZTb010ZERxYTVHdHEwTC1CZ290ekE9PSJ9.RbBtl3KBLNwfLsGkP9IzL93x3sYS1bK24sgnPIV2B24"
    },
});
class UserService {
    getAccessToken = () => localStorage.getItem('accessToken');
    getOneUser(id) {
        var config = {
            method: 'get',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${id}`,
            headers: {
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTIxNTE1NTQsImp0aSI6IkRIS1YtMEh6a0RnTFcxSnV0T3BZQ19FSmpTcmlLUTFDa3ViUGFQVmhfM21CWGxyUm9PVEtYblBFMDJoUkRlRjd0MVhoTGc9PSJ9.3BrGkyudJMKVaO3fBJZF0h9Y8LdPE2quVPdJaGn1QmI'
            }
        };
        axios(config)
            .then(function (response) {
                if (response.data.platform) {
                    resolve(response.data);
                } else {
                    reject(response.data.error);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    };
}

const UserService = new UserService();

export default UserService;
