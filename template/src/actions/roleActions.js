import axios from 'axios';

export const GET_ROLES = '@role/get-roles';
export const CREATE_ROLE = '@role/create-roles';
export const GET_ONE_ROLE = '@role/get-one-role';
export const UPDATE_ROLE = '@role/update-role';

const instance = axios.create({
  baseURL: 'https://api-stg-cen.jamalex.net/ms-user',
  headers: {
    "Access-Control-Allow-Origin": "http://localhost:3000",
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
  },
});

export function getRoles() {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const request = instance.get('/api/roles', {
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      size: 100,
      page: 1
    }
  });
  return (dispatch) => {
    request.then((response) => {
      return dispatch({
        type: GET_ROLES,
        payload: response.data.data
      })
    }).catch((error) => {
      console.log('error', error);
    })
  };
}

export function createRole(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataCreate = JSON.stringify({
    ...data,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = await instance.post('/api/role', dataCreate, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: CREATE_ROLE,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function getOneRole(id) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  return async function action(dispatch) {
    dispatch({ type: GET_ONE_ROLE })
    const request = instance.get(`/api/role/${id}`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: GET_ONE_ROLE,
        payload: response.data.data,
      });
    }
    catch (error) {
      console.log('error', error);
    }
  };
}

export function updateRole(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify({
    ...data,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = instance.put(`/api/role/${data.id}`, dataUpdate, {
      headers: {
        'x-user-id': data.id,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_ROLE,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function updatePermissionRole(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify(data)
  return async function action(dispatch) {
    const request = instance.post(`/api/role/permissions`, dataUpdate, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_ROLE,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}
