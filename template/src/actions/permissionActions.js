import axios from 'axios';

export const GET_PERMISSIONS = '@permission/get-list-permissions';
export const CREATE_PERMISSION = '@permission/create-permissions';
export const GET_ONE_PERMISSION = '@permission/get-one-permission';
export const UPDATE_PERMISSION = '@permission/update-permission';

const instance = axios.create({
  baseURL: 'https://api-stg-cen.jamalex.net/ms-user',
  headers: {
    "Access-Control-Allow-Origin": "http://localhost:3000",
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
  },
});

export function getListPermissions() {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const request = instance.get('/api/permissions', {
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      size: 100,
      page: 1
    }
  });
  return (dispatch) => {
    request.then((response) => {
      return dispatch({
        type: GET_PERMISSIONS,
        payload: response.data.data
      })
    }).catch((error) => {
      console.log('error', error);
    })
  };
}

export function createPermission(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataCreate = JSON.stringify({
    ...data,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = await instance.post('/api/permission', dataCreate, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: CREATE_PERMISSION,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function getOnePermission(id) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  return async function action(dispatch) {
    dispatch({ type: GET_ONE_PERMISSION })
    const request = instance.get(`/api/permission/${id}`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: GET_ONE_PERMISSION,
        payload: response.data.data,
      });
    }
    catch (error) {
      console.log('error', error);
    }
  };
}

export function updatePermission(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify({
    ...data,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = instance.put(`/api/permission/${data.id}`, dataUpdate, {
      headers: {
        'x-user-id': data.id,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_PERMISSION,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}
