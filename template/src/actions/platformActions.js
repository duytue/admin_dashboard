import axios from 'src/utils/axios';
import platformService from 'src/services/platformService';

export const LIST_PLATFORM_REQUEST = '@platform/list-platform-request';
export const LIST_PLATFORM_SUCCESS = '@platform/list-platform-success';
export const LIST_PLATFORM_FAILURE = '@platform/list-platform-failure';

export const CREATE_PLATFORM_REQUEST = '@platform/create-platform-request';
export const CREATE_PLATFORM_SUCCESS = '@platform/create-platform-success';
export const CREATE_PLATFORM_FAILURE = '@platform/create-platform-failure';

export const GET_ONE_PLATFORM_REQUEST = '@platform/get_one-platform-request';
export const GET_ONE_PLATFORM_SUCCESS = '@platform/get_one-platform-success';
export const GET_ONE_PLATFORM_FAILURE = '@platform/get_one-platform-failure';

export const UPDATE_PLATFORM_REQUEST = '@platform/update-platform-request';
export const UPDATE_PLATFORM_SUCCESS = '@platform/update-platform-success';
export const UPDATE_PLATFORM_FAILURE = '@platform/update-platform-failure';

export function getListPlatform() {
  return async dispatch => {
    try {
      dispatch({ type: LIST_PLATFORM_REQUEST });
      const platform = await platformService.getListPlatForm();
      dispatch({
        type: LIST_PLATFORM_SUCCESS,
        payload: {
          platform
        }
      });
    } catch (error) {
      dispatch({ type: LIST_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function createPlatform(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_PLATFORM_REQUEST });
      const platform = await platformService.createOnePlatForm(data);
      dispatch({
        type: CREATE_PLATFORM_SUCCESS,
        payload: {
          platform
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function getOnePlatform(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_PLATFORM_REQUEST });
      const result = await platformService.getOnePlatForm(data);
      dispatch({
        type: GET_ONE_PLATFORM_SUCCESS,
        payload: {
          result,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function updatePlatform(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_PLATFORM_REQUEST });
      const result_update = await platformService.updatePlatform(data);
      console.log('result_update', result_update)
      dispatch({
        type: UPDATE_PLATFORM_SUCCESS,
        payload: {
          result_update
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PLATFORM_FAILURE });
      throw error;
    }
  };
}
