import axios from 'axios';

export const GET_USERS = '@user/get-users';
export const CREATE_USER = '@user/create-users';
export const GET_ONE_USER = '@user/get-one-user';
export const UPDATE_USER = '@user/update-user';
export const UPDATE_ACTIVE_USER = '@user/update-active-user';

const instance = axios.create({
  baseURL: 'https://api-stg-cen.jamalex.net/ms-user',
  headers: {
    "Access-Control-Allow-Origin": "http://localhost:3000",
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
  },
});

export function getUsers() {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const request = instance.get('/api/users', {
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      size: 100,
      page: 1
    }
  });
  return (dispatch) => {
    request.then((response) => {
      return dispatch({
        type: GET_USERS,
        payload: response.data.data
      })
    }).catch((error) => {
      console.log('error', error);
    })
  };
}

export function createUser(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataCreate = JSON.stringify({
    ...data,
    active: true,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = await instance.post('/api/user', dataCreate, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: CREATE_USER,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function getOneUser(id) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  return async function action(dispatch) {
    dispatch({ type: GET_ONE_USER })
    const request = instance.get(`/api/user/${id}`, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: GET_ONE_USER,
        payload: response.data.data,
      });
    }
    catch (error) {
      console.log('error', error);
    }
  };
}

export function updateUser(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify({
    ...data,
    active: true,
    platform_code: "centralweb"
  })
  return async function action(dispatch) {
    const request = instance.put(`/api/user/${data.id}`, dataUpdate, {
      headers: {
        'x-user-id': data.id,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_USER,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function updateRoleUser(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify(data)
  return async function action(dispatch) {
    const request = instance.post(`/api/user/roles`, dataUpdate, {
      headers: {
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_USER,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function updateActiveUser(data) {
  const access_token = localStorage.getItem('accessToken') || undefined;
  const dataUpdate = JSON.stringify({
    ...data,
    active: !data.active,
  })
  return async function action(dispatch) {
    const request = instance.put(`/api/user/${data.id}`, dataUpdate, {
      headers: {
        'x-user-id': data.id,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`
      },
    });

    try {
      const response = await request;
      return dispatch({
        type: UPDATE_ACTIVE_USER,
        payload: response.data.data
      })
    } catch (error) {
      console.log('error', error);
    }
  };
}
