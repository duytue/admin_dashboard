/* eslint-disable react/no-array-index-key */
import React, { lazy, Suspense, Fragment } from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import DocsLayout from 'src/layouts/DocsLayout';
import MainLayout from 'src/layouts/MainLayout';
import HomeView from 'src/views/pages/HomeView';
import LoadingScreen from 'src/components/LoadingScreen';
import AuthGuard from 'src/components/AuthGuard';
import GuestGuard from 'src/components/GuestGuard';

const routesConfig = [
  {
    exact: true,
    path: '/',
    component: () => <Redirect to="/login" />
  },
  {
    exact: true,
    path: '/404',
    component: lazy(() => import('src/views/pages/Error404View'))
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/login',
    component: lazy(() => import('src/views/auth/LoginView'))
  },
  {
    exact: true,
    path: '/login-unprotected',
    component: lazy(() => import('src/views/auth/LoginView'))
  },
  {
    exact: true,
    guard: GuestGuard,
    path: '/register',
    component: lazy(() => import('src/views/auth/RegisterView'))
  },
  {
    exact: true,
    path: '/register-unprotected',
    component: lazy(() => import('src/views/auth/RegisterView'))
  },
  {
    path: '/app',
    guard: AuthGuard,
    layout: DashboardLayout,
    routes: [
      {
        exact: true,
        path: '/app',
        component: () => <Redirect to="/app/management/users" />
      },
      {
        exact: true,
        path: '/app/account',
        component: lazy(() => import('src/views/pages/AccountView'))
      },
      {
        exact: true,
        path: '/app/reports/dashboard',
        component: lazy(() => import('src/views/reports/DashboardView'))
      },
      {
        exact: true,
        path: '/app/reports/dashboard_new',
        component: lazy(() => import('src/views/reports/DashboardViewCustom'))
      },
      {
        exact: true,
        path: '/app/reports/dashboard-alternative',
        component: lazy(() =>
          import('src/views/reports/DashboardAlternativeView')
        )
      },
      {
        exact: true,
        path: '/app/reports',
        component: () => <Redirect to="/app/reports/dashboard" />
      },
      {
        exact: true,
        path: '/app/management/customers',
        component: lazy(() => import('src/views/management/CustomerListView'))
      },
      {
        exact: true,
        path: '/app/management/customers/:customerId',
        component: lazy(() =>
          import('src/views/management/CustomerDetailsView')
        )
      },
      {
        exact: true,
        path: '/app/management/customers/:customerId/edit',
        component: lazy(() => import('src/views/management/CustomerEditView'))
      },
      {
        exact: true,
        path: '/app/management/customer/create',
        component: lazy(() => import('src/views/management/CustomerCreateView'))
      },
      {
        exact: true,
        path: '/app/management/users',
        component: lazy(() => import('src/views/management/UserListView'))
      },
      {
        exact: true,
        path: '/app/management/users/create',
        component: lazy(() => import('src/views/management/UserCreateView'))
      },
      {
        exact: true,
        path: '/app/management/users/:id',
        component: lazy(() => import('src/views/management/UserDetailsView'))
      },
      {
        exact: true,
        path: '/app/management/users/:id/edit',
        component: lazy(() => import('src/views/management/UserEditView'))
      },
      {
        exact: true,
        path: '/app/management/roles',
        component: lazy(() => import('src/views/management/RoleListView'))
      },
      {
        exact: true,
        path: '/app/management/roles/create',
        component: lazy(() => import('src/views/management/RoleCreateView'))
      },
      {
        exact: true,
        path: '/app/management/roles/:id/edit',
        component: lazy(() => import('src/views/management/RoleEditView'))
      },
      {
        exact: true,
        path: '/app/management/permissions',
        component: lazy(() => import('src/views/management/PermissionListView'))
      },
      {
        exact: true,
        path: '/app/management/permissions/create',
        component: lazy(() =>
          import('src/views/management/PermissionCreateView')
        )
      },
      {
        exact: true,
        path: '/app/management/permissions/:id/edit',
        component: lazy(() => import('src/views/management/PermissionEditView'))
      },
      {
        exact: true,
        path: '/app/management/platform',
        component: lazy(() => import('src/views/management/PlatformListView'))
      },
      {
        exact: true,
        path: '/app/management/platform/create',
        component: lazy(() => import('src/views/management/PlatformCreateView'))
      },
      {
        exact: true,
        path: '/app/management/platform/:id',
        component: lazy(() =>
          import('src/views/management/PlatformDetailsView')
        )
      },
      {
        exact: true,
        path: '/app/management/platform/:id/edit',
        component: lazy(() => import('src/views/management/PlatformEditView'))
      },
      {
        exact: true,
        path: '/app/controlpanel/auditlog',
        component: lazy(() => import('src/views/controlpanel/AuditLogListView'))
      },
      {
        exact: true,
        path: '/app/controlpanel/auditlog/:id',
        component: lazy(() =>
          import('src/views/controlpanel/AuditLogDetailView')
        )
      },
      {
        exact: true,
        path: '/app/management/languages',
        component: lazy(() => import('src/views/management/LanguageListView'))
      },
      {
        exact: true,
        path: '/app/management/languages/create',
        component: lazy(() => import('src/views/management/LanguageCreateView'))
      },
      {
        exact: true,
        path: '/app/management/languages/:id/edit',
        component: lazy(() => import('src/views/management/LanguageEditView'))
      },
      {
        exact: true,
        path: '/app/management/products',
        component: lazy(() => import('src/views/management/ProductListView'))
      },
      {
        exact: true,
        path: '/app/management/products/create',
        component: lazy(() => import('src/views/management/ProductCreateView'))
      },
      {
        exact: true,
        path: '/app/management/orders',
        component: lazy(() => import('src/views/management/OrderListView'))
      },
      {
        exact: true,
        path: '/app/management/orders/:orderId',
        component: lazy(() => import('src/views/management/OrderDetailsView'))
      },
      {
        exact: true,
        path: '/app/management/order/:orderId/edit',
        component: lazy(() => import('src/views/management/OrderEditView'))
      },
      {
        exact: true,
        path: '/app/management/invoices',
        component: lazy(() => import('src/views/management/InvoiceListView'))
      },
      {
        exact: true,
        path: '/app/management/invoices/:invoiceId',
        component: lazy(() => import('src/views/management/InvoiceDetailsView'))
      },
      {
        exact: true,
        path: '/app/management',
        component: () => <Redirect to="/app/management/users" />
      },
      {
        exact: true,
        path: '/app/masterdata/address',
        component: lazy(() => import('src/views/masterdata/AddressListView'))
      },
      {
        exact: true,
        path: '/app/masterdata',
        component: () => <Redirect to="/app/masterdata/address" />
      },
      {
        exact: true,
        path: '/app/calendar',
        component: lazy(() => import('src/views/calendar/CalendarView'))
      },
      {
        exact: true,
        path: '/app/kanban',
        component: lazy(() => import('src/views/kanban/KanbanView'))
      },
      {
        exact: true,
        path: ['/app/chat/new', '/app/chat/:threadKey'],
        component: lazy(() => import('src/views/chat/ChatView'))
      },
      {
        exact: true,
        path: '/app/chat',
        component: () => <Redirect to="/app/chat/new" />
      },
      {
        exact: true,
        path: [
          '/app/mail/label/:customLabel/:mailId?',
          '/app/mail/:systemLabel/:mailId?'
        ],
        component: lazy(() => import('src/views/mail/MailView'))
      },
      {
        exact: true,
        path: '/app/mail',
        component: () => <Redirect to="/app/mail/all" />
      },
      {
        exact: true,
        path: '/app/projects/overview',
        component: lazy(() => import('src/views/projects/OverviewView'))
      },
      {
        exact: true,
        path: '/app/projects/browse',
        component: lazy(() => import('src/views/projects/ProjectBrowseView'))
      },
      {
        exact: true,
        path: '/app/projects/create',
        component: lazy(() => import('src/views/projects/ProjectCreateView'))
      },
      {
        exact: true,
        path: '/app/projects/:id',
        component: lazy(() => import('src/views/projects/ProjectDetailsView'))
      },
      {
        exact: true,
        path: '/app/projects',
        component: () => <Redirect to="/app/projects/browse" />
      },
      {
        exact: true,
        path: '/app/social/feed',
        component: lazy(() => import('src/views/social/FeedView'))
      },
      {
        exact: true,
        path: '/app/social/profile',
        component: lazy(() => import('src/views/social/ProfileView'))
      },
      {
        exact: true,
        path: '/app/social',
        component: () => <Redirect to="/app/social/profile" />
      },
      {
        exact: true,
        path: '/app/extra/charts/apex',
        component: lazy(() => import('src/views/extra/charts/ApexChartsView'))
      },
      {
        exact: true,
        path: '/app/extra/forms/formik',
        component: lazy(() => import('src/views/extra/forms/FormikView'))
      },
      {
        exact: true,
        path: '/app/extra/forms/redux',
        component: lazy(() => import('src/views/extra/forms/ReduxFormView'))
      },
      {
        exact: true,
        path: '/app/extra/editors/draft-js',
        component: lazy(() => import('src/views/extra/editors/DraftEditorView'))
      },
      {
        exact: true,
        path: '/app/extra/editors/quill',
        component: lazy(() => import('src/views/extra/editors/QuillEditorView'))
      },
      {
        exact: true,
        path: '/app/management/station',
        component: lazy(() => import('src/views/management/StationListView'))
      },
      {
        exact: true,
        path: '/app/management/station/create',
        component: lazy(() => import('src/views/management/StationCreateView'))
      },
      {
        exact: true,
        path: '/app/management/station/:id/edit',
        component: lazy(() => import('src/views/management/StationEditView'))
      },
      {
        exact: true,
        path: '/app/management/stocks',
        component: lazy(() => import('src/views/management/StockListView'))
      },
      {
        exact: true,
        path: '/app/management/stocks/edit/:stockId',
        component: lazy(() => import('src/views/management/StockFormView'))
      },
      {
        exact: true,
        path: '/app/management/stocks/create',
        component: lazy(() => import('src/views/management/StockFormView'))
      },
      {
        exact: true,
        path: '/app/management/time-slot-capacity/create',
        component: lazy(() =>
          import('src/views/management/CapacityTimeSlotFormView')
        )
      },
      {
        exact: true,
        path: '/app/management/time-slot-capacity/edit/:slotId',
        component: lazy(() =>
          import('src/views/management/CapacityTimeSlotFormView')
        )
      },
      {
        exact: true,
        path: '/app/management/time-slot-capacity',
        component: lazy(() =>
          import('src/views/management/CapacityTimeSlotView')
        )
      },
      {
        exact: true,
        path: '/app/management/language-setting',
        component: lazy(() =>
          import('src/views/management/LanguageSettingListView')
        )
      },
      {
        exact: true,
        path: '/app/management/language-setting/create',
        component: lazy(() =>
          import('src/views/management/LanguageSettingCreateView')
        )
      },
      {
        exact: true,
        path: '/app/management/language-setting/:key/edit',
        component: lazy(() =>
          import('src/views/management/LanguageSettingEditView')
        )
      },
      {
        exact: true,
        path: '/app/management/reasons',
        component: lazy(() => import('src/views/management/ReasonListView'))
      },
      {
        exact: true,
        path: '/app/management/customer-fee',
        component: lazy(() => import('src/views/management/CustomerFeeView'))
      },
      {
        exact: true,
        path: '/app/management/customer-fee/create',
        component: lazy(() =>
          import('src/views/management/CustomerFeeFormView')
        )
      },
      {
        exact: true,
        path: '/app/management/customer-fee/edit/:feeId',
        component: lazy(() =>
          import('src/views/management/CustomerFeeFormView')
        )
      },
      {
        exact: true,
        path: '/app/management/suppliers',
        component: lazy(() => import('src/views/management/SupplierListView'))
      },
      {
        exact: true,
        path: '/app/management/supplier/create',
        component: lazy(() => import('src/views/management/SupplierCreateView'))
      },
      {
        exact: true,
        path: '/app/management/supplier/:id/edit',
        component: lazy(() => import('src/views/management/SupplierEditView'))
      },
      {
        exact: true,
        path: '/app/management/supplier/:id',
        component: lazy(() =>
          import('src/views/management/SupplierDetailsView')
        )
      },
      {
        exact: true,
        path: '/app/management/supplier/:id/clone',
        component: lazy(() => import('src/views/management/SupplierCloneView'))
      },
      {
        exact: true,
        path: '/app/content/comments',
        component: lazy(() => import('src/views/content/CommentListView'))
      },
      {
        exact: true,
        path: '/app/content/comments/:id/edit',
        component: lazy(() => import('src/views/content/CommentEditView'))
      },
      {
        exact: true,
        path: '/app/content/reviews',
        component: lazy(() => import('src/views/content/ReviewListView'))
      },
      {
        exact: true,
        path: '/app/content/review/:id/edit',
        component: lazy(() => import('src/views/content/ReviewEditView'))
      },
      {
        exact: true,
        path: '/app/content/variants',
        component: lazy(() => import('src/views/content/VariantListView'))
      },
      {
        exact: true,
        path: '/app/content/item-attributes',
        component: lazy(() => import('src/views/content/ItemAttributeListView'))
      },
      {
        exact: true,
        path: '/app/content/item-management',
        component: lazy(() =>
          import('src/views/content/ItemManagementListView')
        )
      },
      {
        exact: true,
        path: '/app/content/product-management',
        component: lazy(() => import('src/views/content/ProductManagementListView'))
      },
      {
        exact: true,
        path: '/app/content/product-management/edit/:productId',
        component: lazy(() => import('src/views/content/ProductFormView'))
      },
      {
        exact: true,
        path: '/app/content/product-management/create',
        component: lazy(() => import('src/views/content/ProductFormView'))
      },
      {
        exact: true,
        path: '/app/content/medias',
        component: lazy(() => import('src/views/content/MediaListView'))
      },
      {
        exact: true,
        path: '/app/content/media/create',
        component: lazy(() => import('src/views/content/MediaCreateView'))
      },
      {
        exact: true,
        path: '/app/content/media/:id/edit',
        component: lazy(() => import('src/views/content/MediaEditView'))
      },
      {
        exact: true,
        path: '/app/masterdata/policies',
        component: lazy(() => import('src/views/masterdata/PolicyListView'))
      },
      {
        exact: true,
        path: '/app/management/picklist',
        component: lazy(() => import('src/views/management/PicklistListView'))
      },
      {
        exact: true,
        path: '/app/management/picklist/search',
        component: lazy(() => import('src/views/management/PicklistSearchView'))
      },
      {
        exact: true,
        path: '/app/management/package',
        component: lazy(() => import('src/views/management/PackageListView'))
      },
      {
        exact: true,
        path: '/app/management/package/search',
        component: lazy(() => import('src/views/management/PackageSearchView'))
      },
      {
        exact: true,
        path: '/app/management/promotion',
        component: lazy(() => import('src/views/management/PromotionListView'))
      },
      {
        exact: true,
        path: '/app/management/promotion/create',
        component: lazy(() =>
          import('src/views/management/PromotionCreateView')
        )
      },
      {
        exact: true,
        path: '/app/management/promotion/:id/edit',
        component: lazy(() =>
          import('src/views/management/PromotionEditView')
        )
      },
      {
        exact: true,
        path: '/app/management/order-process',
        component: lazy(() =>
          import('src/views/management/OrderProcessListView')
        )
      },
      {
        exact: true,
        path: '/app/management/mass-upload',
        component: lazy(() => import('src/views/management/MassUploadView'))
      },
      {
        exact: true,
        path: '/app/management/pricing',
        component: lazy(() => import('src/views/management/PricingListView'))
      },
      {
        exact: true,
        path: '/app/management/notification',
        component: lazy(() => import('src/views/management/NotificationListView'))
      },
      {
        exact: true,
        path: '/app/management/order-verification',
        component: lazy(() => import('src/views/management/OrderVerificationListView'))
      },
      {
        exact: true,
        path: '/app/management/ticket',
        component: lazy(() => import('src/views/management/TicketListView'))
      },
      {
        exact: true,
        path: '/app/management/refund-transaction',
        component: lazy(() => import('src/views/management/RefundTransactionListView'))
      },
      {
        exact: true,
        path: '/app/management/metadata',
        component: lazy(() => import('src/views/management/MetadataListView'))
      },
      {
        component: () => <Redirect to="/404" />
      }
    ]
  },
  {
    path: '/docs',
    layout: DocsLayout,
    routes: [
      {
        exact: true,
        path: '/docs',
        component: () => <Redirect to="/docs/welcome" />
      },
      {
        exact: true,
        path: '/docs/welcome',
        component: lazy(() => import('src/views/docs/WelcomeView'))
      },
      {
        exact: true,
        path: '/docs/getting-started',
        component: lazy(() => import('src/views/docs/GettingStartedView'))
      },
      {
        exact: true,
        path: '/docs/environment-variables',
        component: lazy(() => import('src/views/docs/EnvironmentVariablesView'))
      },
      {
        exact: true,
        path: '/docs/deployment',
        component: lazy(() => import('src/views/docs/DeploymentView'))
      },
      {
        exact: true,
        path: '/docs/api-calls',
        component: lazy(() => import('src/views/docs/ApiCallsView'))
      },
      {
        exact: true,
        path: '/docs/analytics',
        component: lazy(() => import('src/views/docs/AnalyticsView'))
      },
      {
        exact: true,
        path: '/docs/authentication',
        component: lazy(() => import('src/views/docs/AuthenticationView'))
      },
      {
        exact: true,
        path: '/docs/routing',
        component: lazy(() => import('src/views/docs/RoutingView'))
      },
      {
        exact: true,
        path: '/docs/settings',
        component: lazy(() => import('src/views/docs/SettingsView'))
      },
      {
        exact: true,
        path: '/docs/state-management',
        component: lazy(() => import('src/views/docs/StateManagementView'))
      },
      {
        exact: true,
        path: '/docs/theming',
        component: lazy(() => import('src/views/docs/ThemingView'))
      },
      {
        exact: true,
        path: '/docs/support',
        component: lazy(() => import('src/views/docs/SupportView'))
      },
      {
        exact: true,
        path: '/docs/changelog',
        component: lazy(() => import('src/views/docs/ChangelogView'))
      },
      {
        component: () => <Redirect to="/404" />
      }
    ]
  },
  {
    path: '/content',
    layout: DashboardLayout,
    routes: [
      {
        exact: true,
        path: '/content/category-management',
        component: lazy(() =>
          import('src/views/content/Category/CategoryManagement')
        )
      },
      {
        exact: true,
        path: '/content/category-management/create',
        component: lazy(() =>
          import(
            'src/views/content/Category/CategoryManagement/CategoryCreateView'
          )
        )
      },
      {
        exact: true,
        path: '/content/category-management/:id/edit',
        component: lazy(() =>
          import(
            'src/views/content/Category/CategoryManagement/CategoryEditView'
          )
        )
      },
      {
        component: () => <Redirect to="/404" />
      }
    ]
  },
  {
    path: '*',
    layout: MainLayout,
    routes: [
      {
        exact: true,
        path: '/login',
        component: HomeView
      },
      {
        exact: true,
        path: '/pricing',
        component: lazy(() => import('src/views/pages/PricingView'))
      },
      {
        component: () => <Redirect to="/404" />
      }
    ]
  }
];

const renderRoutes = (routes) =>
  routes ? (
    <Suspense fallback={<LoadingScreen />}>
      <Switch>
        {routes.map((route, i) => {
          const Guard = route.guard || Fragment;
          const Layout = route.layout || Fragment;
          const Component = route.component;

          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={(props) => (
                <Guard>
                  <Layout>
                    {route.routes ? (
                      renderRoutes(route.routes)
                    ) : (
                        <Component {...props} />
                      )}
                  </Layout>
                </Guard>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  ) : null;

function Routes() {
  return renderRoutes(routesConfig);
}

export default Routes;
