/* eslint-disable no-param-reassign */
import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
  CREATE_CATEGORY_REQUEST,
  CREATE_CATEGORY_SUCCESS,
  CREATE_CATEGORY_FAILURE,
  GET_ONE_CATEGORY_REQUEST,
  GET_ONE_CATEGORY_SUCCESS,
  GET_ONE_CATEGORY_FAILURE,
  UPDATE_CATEGORY_REQUEST,
  UPDATE_CATEGORY_SUCCESS,
  UPDATE_CATEGORY_FAILURE,
  DELETE_CATEGORY_FAILURE,
  DELETE_CATEGORY_REQUEST,
  DELETE_CATEGORY_SUCCESS,
  GET_CATEGORIES_LEVEL_REQUEST,
  GET_CATEGORIES_LEVEL_SUCCESS,
  GET_CATEGORIES_LEVEL_FAILURE
} from 'src/actions/categoryActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  categories: [],
  category: {},
  categoriesByLevel: [],
  loadingGetCategories: false,
  loadingCreateCategory: false,
  loadingGetOneCategory: false,
  loadingUpdateCategory: false,
  loadingDeleteCategory: false
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Categories
    case GET_CATEGORIES_REQUEST: {
      return produce(state, draft => {
        draft.categories = [];
        draft.loadingGetCategories = true;
      });
    }

    case GET_CATEGORIES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {

        draft.categories = response;
        draft.loadingGetCategories = false;
      });
    }

    case GET_CATEGORIES_FAILURE: {
      return produce(state, draft => {
        // Maybe category error
        draft.loadingGetCategories = false;
      });
    }

    // Case Create Category
    case CREATE_CATEGORY_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateCategory = true;
      });
    }

    case CREATE_CATEGORY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.categories = draft.categories.concat(response)
        draft.loadingCreateCategory = false;
      });
    }

    case CREATE_CATEGORY_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateCategory = false;
        // Maybe category error
      });
    }

    // Case Get One Category
    case GET_ONE_CATEGORY_REQUEST: {
      return produce(state, draft => {
        draft.category = {};
        draft.loadingGetOneCategory = true;
      });
    }

    case GET_ONE_CATEGORY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.category = response;
        draft.loadingGetOneCategory = false;
      });
    }

    case GET_ONE_CATEGORY_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneCategory = false;
      });
    }

    // Case Update Category
    case UPDATE_CATEGORY_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateCategory = true;
      });
    }

    case UPDATE_CATEGORY_SUCCESS: {
      const { response } = action.payload;
      const updateCategory = _.map(state.categories, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateCategory !== undefined && updateCategory.length > 0) {
        return produce(state, draft => {
          draft.categories = updateCategory;
          draft.loadingUpdateCategory = false;
        });
      } else return produce(state, draft => {
        draft.categories = [response];
        draft.loadingUpdateCategory = false;
      });
    }

    case UPDATE_CATEGORY_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateCategory = false;
      });
    }

    // Case Delete Category
    case DELETE_CATEGORY_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteCategory = true;
      });
    }

    case DELETE_CATEGORY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        // let i = draft.categories.findIndex(item => item.id == response.id);
        draft.categories = draft.categories.filter(item => item.id !== response.id);
        draft.loadingDeleteCategory = false;
      });
    }

    case DELETE_CATEGORY_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteCategory = false;
        // Maybe category error
      });
    }

    case GET_CATEGORIES_LEVEL_REQUEST: {
      return produce(state, (draft) => {
        draft.categoriesByLevel = [];
        draft.loadingGetCategories = true;
      });
    }

    case GET_CATEGORIES_LEVEL_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.categoriesByLevel = response;
        draft.loadingGetCategories = false;
      });
    }

    case GET_CATEGORIES_LEVEL_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetCategories = false;
      });
    }

    default: {
      return state; // We return the default state here
    }
  }
};

export default categoryReducer;
