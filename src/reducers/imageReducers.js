import {
    UPLOAD_IMAGE_REQUEST,
    UPLOAD_IMAGE_SUCCESS,
    UPLOAD_IMAGE_FAILURE
} from 'src/actions/imageActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
    image: {},
    loadingUploadImage: false
};

const imageReducer = (state = initialState, action) => {
    switch (action.type) {
      // Case upload image
      case UPLOAD_IMAGE_REQUEST: {
        console.log('UPLOAD_IMAGE_REQUEST')
        return produce(state, draft => {
          draft.image = {};
          draft.loadingUploadImage = true;
        });
      }
  
      case UPLOAD_IMAGE_SUCCESS: {
        console.log('UPLOAD_IMAGE_SUCCESS')
        const { response } = action.payload;
        return produce(state, draft => {
          draft.image = response;
          draft.loadingUploadImage = false;
        });
      }
  
      case UPLOAD_IMAGE_FAILURE: {
        return produce(state, draft => {
          draft.loadingUploadImage = false;
        });
      }

      default: {
        return state // We return the default state here
      }
    }
}

export default imageReducer;