/* eslint-disable no-param-reassign */
import {
  GET_REASON_TYPE_REQUEST,
  GET_REASON_TYPE_SUCCESS,
  GET_REASON_TYPE_FAILURE,
  GET_REASON_LIST_REQUEST,
  GET_REASON_LIST_SUCCESS,
  GET_REASON_LIST_FAILURE,
  CREATE_REASON_REQUEST,
  CREATE_REASON_SUCCESS,
  CREATE_REASON_FAILURE,
  GET_REASON_REQUEST,
  GET_REASON_SUCCESS,
  GET_REASON_FAILURE,
} from 'src/actions/reasonActions';
import _ from 'lodash';
import produce from 'immer';
import { UPDATE_USER_FAILURE, UPDATE_USER_SUCCESS } from '../actions/userActions';

const initialState = {
  reasonTypeList: [],
  reasonList: [],
  reason: {},
  loadingGetReasonList: false,
};

const reasonReducer = (state = initialState, action) => {
  switch (action.type) {

    case GET_REASON_TYPE_REQUEST: {
      return produce(state, (draft) => {
        draft.reasonTypeList = [];
      });
    }

    case GET_REASON_TYPE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.reasonTypeList = response;
      });
    }

    case GET_REASON_TYPE_FAILURE: {
      return produce(state, (draft) => {
        // Maybe user error
      });
    }

    case GET_REASON_LIST_REQUEST: {
      return produce(state, (draft) => {
        draft.reasonList = [];
      });
    }

    case GET_REASON_LIST_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.reasonList = response;
      });
    }

    case GET_REASON_LIST_FAILURE: {
      return produce(state, (draft) => {
      });
    }

    case GET_REASON_REQUEST: {
      return produce(state, (draft) => {
        draft.reason = {};
      });
    }

    case GET_REASON_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.reason = response;
      });
    }

    case GET_REASON_FAILURE: {
      return produce(state, (draft) => {
      });
    }


    default: {
      return state; // We return the default state here
    }
  }
};

export default reasonReducer;
