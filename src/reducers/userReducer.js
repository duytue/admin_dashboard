import {
  GET_USERS_REQUEST,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILURE,
  GET_ONE_USER_REQUEST,
  GET_ONE_USER_SUCCESS,
  GET_ONE_USER_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  UPDATE_ROLE_USER_REQUEST,
  UPDATE_ROLE_USER_SUCCESS,
  UPDATE_ROLE_USER_FAILURE,
  UPDATE_PERMISSION_USER_REQUEST,
  UPDATE_PERMISSION_USER_SUCCESS,
  UPDATE_PERMISSION_USER_FAILURE,
  UPDATE_ACTIVE_USER_REQUEST,
  UPDATE_ACTIVE_USER_SUCCESS,
  UPDATE_ACTIVE_USER_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE
} from 'src/actions/userActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  users: [],
  user: {},
  loadingGetUsers: false,
  loadingCreateUser: false,
  loadingGetOneUser: false,
  loadingUpdateUser: false,
  loadingUpdateRoleOfUser: false,
  loadingUpdatePermissionOfUser: false,
  loadingUpdateActiveUser: false,
  loadingDeleteUser: false
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Users
    case GET_USERS_REQUEST: {
      return produce(state, draft => {
        draft.users = [];
        draft.loadingGetUsers = true;
      });
    }

    case GET_USERS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.users = response;
        draft.loadingGetUsers = false;
      });
    }

    case GET_USERS_FAILURE: {
      return produce(state, draft => {
        // Maybe user error
        draft.loadingGetUsers = false;
      });
    }

    // Case Create User
    case CREATE_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateUser = true;
      });
    }

    case CREATE_USER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.users = draft.users.concat(response)
        draft.loadingCreateUser = false;
      });
    }

    case CREATE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateUser = false;
        // Maybe user error
      });
    }

    // Case Get One User
    case GET_ONE_USER_REQUEST: {
      return produce(state, draft => {
        draft.user = {};
        draft.loadingGetOneUser = true;
      });
    }

    case GET_ONE_USER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.user = response;
        draft.loadingGetOneUser = false;
      });
    }

    case GET_ONE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneUser = false;
      });
    }

    // Case Update User
    case UPDATE_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateUser = true;
      });
    }

    case UPDATE_USER_SUCCESS: {
      const { response } = action.payload;
      const updateUser = _.map(state.users, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateUser !== undefined && updateUser.length > 0) {
        return produce(state, draft => {
          draft.users = updateUser;
          draft.loadingUpdateUser = false;
        });
      } else return produce(state, draft => {
        draft.users = [response];
        draft.loadingUpdateUser = false;
      });
    }

    case UPDATE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateUser = false;
      });
    }

    // Case Update Role User
    case UPDATE_ROLE_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateRoleOfUser = true;
      });
    }

    case UPDATE_ROLE_USER_SUCCESS: {
      const { response } = action.payload;
      const updateUser = _.map(state.users, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateUser !== undefined && updateUser.length > 0) {
        return produce(state, draft => {
          draft.users = updateUser;
          draft.loadingUpdateRoleOfUser = false;
        });
      } else return produce(state, draft => {
        draft.users = [response];
        draft.loadingUpdateRoleOfUser = false;
      });
    }

    case UPDATE_ROLE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateRoleOfUser = false;
      });
    }

    // Case Update Permission User
    case UPDATE_PERMISSION_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdatePermissionOfUser = true;
      });
    }

    case UPDATE_PERMISSION_USER_SUCCESS: {
      const { response } = action.payload;
      const updateUser = _.map(state.users, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateUser !== undefined && updateUser.length > 0) {
        return produce(state, draft => {
          draft.users = updateUser;
          draft.loadingUpdatePermissionOfUser = false;
        });
      } else return produce(state, draft => {
        draft.users = [response];
        draft.loadingUpdatePermissionOfUser = false;
      });
    }

    case UPDATE_PERMISSION_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateRoleOfUser = false;
      });
    }

    // Case Update Active User
    case UPDATE_ACTIVE_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateActiveUser = true;
      });
    }

    case UPDATE_ACTIVE_USER_SUCCESS: {
      const { response } = action.payload;
      const updateUser = _.map(state.users, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateUser !== undefined && updateUser.length > 0) {
        return produce(state, draft => {
          draft.users = updateUser;
          draft.loadingUpdateActiveUser = false;
        });
      } else return produce(state, draft => {
        draft.users = [response];
        draft.loadingUpdateActiveUser = false;
      });
    }

    case UPDATE_ACTIVE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateActiveUser = false;
      });
    }

    // Case Delete Category
    case DELETE_USER_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteUser = true;
      });
    }

    case DELETE_USER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        // let i = draft.categories.findIndex(item => item.id == response.id);
        draft.users = draft.users.filter(item => item.id !== response.id);
        draft.loadingDeleteUser = false;
      });
    }

    case DELETE_USER_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteUser = false;
        // Maybe category error
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default userReducer;
