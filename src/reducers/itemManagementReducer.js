/* eslint-disable no-param-reassign */
import _ from 'lodash';
import produce from 'immer';
import {
  GET_ITEMS_REQUEST,
  GET_ITEMS_SUCCESS,
  GET_ITEMS_FAILURE,
  ADD_ITEM_REQUEST,
  ADD_ITEM_SUCCESS,
  ADD_ITEM_FAILURE,
  GET_ITEM_REQUEST,
  GET_ITEM_SUCCESS,
  GET_ITEM_FAILURE,
  UPDATE_ITEM_REQUEST,
  UPDATE_ITEM_SUCCESS,
  UPDATE_ITEM_FAILURE
} from 'src/actions/itemManagementAction';

const initialState = {
  items: [],
  item: {},
  loadingGetItems: false,
  loadingCreateItem: false,
  loadingGetItem: false,
  loadingUpdateItem: false
};

const itemManagementReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEMS_REQUEST: {
      return produce(state, (draft) => {
        draft.items = [];
        draft.loadingGetItems = true;
      });
    }

    case GET_ITEMS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.items = response;
        draft.loadingGetItems = false;
      });
    }

    case GET_ITEMS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetItems = false;
      });
    }

    case ADD_ITEM_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateItem = true;
      });
    }

    case ADD_ITEM_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.items = draft.items.concat(response);
        draft.loadingCreateItem = false;
      });
    }

    case ADD_ITEM_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateItem = false;
      });
    }

    case GET_ITEM_REQUEST: {
      return produce(state, (draft) => {
        draft.item = {};
        draft.loadingGetItem = true;
      });
    }

    case GET_ITEM_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.item = response;
        draft.loadingGetItem = false;
      });
    }

    case GET_ITEM_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetItem = false;
      });
    }

    case UPDATE_ITEM_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdateItem = true;
      });
    }

    case UPDATE_ITEM_SUCCESS: {
      const { response } = action.payload;
      const updateItem = _.map(state.items, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateItem !== undefined && updateItem.length > 0) {
        return produce(state, (draft) => {
          draft.items = updateItem;
          draft.loadingUpdateItem = false;
        });
      } return produce(state, (draft) => {
        draft.items = [response];
        draft.loadingUpdateItem = false;
      });
    }

    case UPDATE_ITEM_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdateItem = false;
      });
    }

    default: {
      return state;
    }
  }
};

export default itemManagementReducer;
