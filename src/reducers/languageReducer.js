import {
  GET_LANGUAGES_REQUEST,
  GET_LANGUAGES_SUCCESS,
  GET_LANGUAGES_FAILURE,
  CREATE_LANGUAGE_REQUEST,
  CREATE_LANGUAGE_SUCCESS,
  CREATE_LANGUAGE_FAILURE,
  GET_ONE_LANGUAGE_REQUEST,
  GET_ONE_LANGUAGE_SUCCESS,
  GET_ONE_LANGUAGE_FAILURE,
  UPDATE_LANGUAGE_REQUEST,
  UPDATE_LANGUAGE_SUCCESS,
  UPDATE_LANGUAGE_FAILURE,
  DELETE_LANGUAGE_REQUEST,
  DELETE_LANGUAGE_SUCCESS,
  DELETE_LANGUAGE_FAILURE
} from 'src/actions/languageActions';
import _ from 'lodash';
import produce from 'immer';
  
const initialState = {
  languages: [],
  language: {},
  loadingGetLanguages: false,
  loadingCreateLanguage: false,
  loadingGetOneLanguage: false,
  loadingUpdateLanguage: false,
  deletedSuccess: false
};

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Languages
    case GET_LANGUAGES_REQUEST: {
      return produce(state, draft => {
        draft.languages = [];
        draft.loadingGetLanguages = true;
      });
    }

    case GET_LANGUAGES_SUCCESS: {
      const { response } = action.payload;
      console.log('GET_LANGUAGES_SUCCESS', action);
      return produce(state, draft => {
        draft.languages = response;
        draft.loadingGetLanguages = false;
      });
    }
  
    case GET_LANGUAGES_FAILURE: {
      return produce(state, draft => {
        // Maybe language error
        draft.loadingGetLanguages = false;
      });
    }

    case GET_ONE_LANGUAGE_REQUEST: {
      return produce(state, draft => {
        draft.language = {};
        draft.loadingGetOneLanguage = true;
      });
    }

    case GET_ONE_LANGUAGE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.language = response;
        draft.loadingGetOneLanguage = false;
      });
    }

    case DELETE_LANGUAGE_REQUEST: {
      return produce(state, draft => {
        draft.deletedSuccess = false;
      });
    }

    case DELETE_LANGUAGE_SUCCESS: {
      return produce(state, draft => {
        draft.deletedSuccess = true;
      });
    }

    case DELETE_LANGUAGE_FAILURE: {
      return produce(state, draft => {
        draft.deletedSuccess = false;
      });
    }



    // case GET_ONE_LANGUAGE_FAILURE: {
    //   return produce(state, draft => {
    //     draft.loadingGetOneLanguage = false;
    //   });
    // }

    // // Case Update Language
    // case UPDATE_LANGUAGE_REQUEST: {
    //   return produce(state, draft => {
    //     draft.loadingUpdateLanguage = true;
    //   });
    // }

    // case UPDATE_LANGUAGE_SUCCESS: {
    //   const { response } = action.payload;
    //   const updateLanguage = _.map(state.languages, i => {
    //     if (i.id === response.id) return response;
    //     return i;
    //   })
    //   if (updateLanguage !== undefined && updateLanguage.length > 0) {
    //     return produce(state, draft => {
    //       draft.languages = updateLanguage;
    //       draft.loadingUpdateLanguage = false;
    //     });
    //   } else return produce(state, draft => {
    //     draft.languages = [response];
    //     draft.loadingUpdateLanguage = false;
    //   });
    // }

    // case UPDATE_LANGUAGE_FAILURE: {
    //   return produce(state, draft => {
    //     draft.loadingUpdateLanguage = false;
    //   });
    // }

    default: {
      return state // We return the default state here
    }
  }
};

export default languageReducer;
