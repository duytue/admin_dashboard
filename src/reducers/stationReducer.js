/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import produce from 'immer';
import {
  GET_STATIONS_REQUEST, GET_STATIONS_SUCCESS, GET_STATIONS_FAILURE, ADD_STATION_REQUEST, ADD_STATION_SUCCESS, ADD_STATION_FAILURE, GET_STATION_REQUEST, GET_STATION_SUCCESS, GET_STATION_FAILURE, GET_STATIONFUNCTION_REQUEST, GET_STATIONFUNCTION_SUCCESS, GET_STATIONFUNCTION_FAILURE, DELETE_STATION_REQUEST, DELETE_STATION_SUCCESS, DELETE_STATION_FAILURE
} from 'src/actions/stationActions';

const initialState = {
  stations: [],
  station: {},
  stationFunctions: [],
  loadingGetStations: false,
  loadingCreateStation: false,
  loadingUpdateStation: false,
  loadingGetStation: false,
  loadingDeleteStation: false
};

const stationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_STATIONS_REQUEST: {
      console.log('GET_STATIONS_REQUEST');

      return produce(state, (draft) => {
        draft.stations = [];
        draft.loadingGetStations = true;
      });
    }

    case GET_STATIONS_SUCCESS: {
      const { response } = action.payload;
      console.log('GET_STATIONS_SUCCESS', response);
      return produce(state, (draft) => {
        draft.stations = response;
        draft.loadingGetStations = false;
      });
    }

    case GET_STATIONS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetStations = false;
      });
    }

    case ADD_STATION_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateStation = true;
      });
    }

    case ADD_STATION_SUCCESS: {
      const { response } = action.payload;
      console.log('ADD_STATION_SUCCESS', action);
      return produce(state, (draft) => {
        draft.stations = draft.stations.concat(response);
        draft.loadingCreateStation = false;
      });
    }

    case ADD_STATION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateStation = false;
      });
    }

    case GET_STATION_REQUEST: {
      console.log('GET_STATION_REQUEST');

      return produce(state, (draft) => {
        draft.station = {};
        draft.loadingGetStation = true;
      });
    }

    case GET_STATION_SUCCESS: {
      const { response } = action.payload;
      console.log('GET_STATION_SUCCESS', action);
      return produce(state, (draft) => {
        draft.station = response;
        draft.loadingGetStation = false;
      });
    }

    case GET_STATION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetStation = false;
      });
    }

    case GET_STATIONFUNCTION_REQUEST: {
      console.log('GET_STATIONFUNCTION_REQUEST');
      return produce(state, (draft) => {
        draft.stationFunctions = [];
      });
    }

    case GET_STATIONFUNCTION_SUCCESS: {
      const { response } = action.payload;
      console.log('GET_STATIONFUNCTION_SUCCESS', action);
      return produce(state, (draft) => {
        draft.stationFunctions = response;
      });
    }

    case GET_STATIONFUNCTION_FAILURE: {
      return produce(state, (draft) => {
        // error
      });
    }

    case DELETE_STATION_REQUEST: {
      console.log('DELETE_STATION_REQUEST');
      return produce(state, (draft) => {
        draft.stationFunctions = [];
      });
    }

    case DELETE_STATION_SUCCESS: {
      const { station } = action.payload;
      console.log('DELETE_STATION_SUCCESS', station);

      return produce(state, (draft) => {
        const { stations } = draft;
        let filteredStation;
        if (typeof station === 'object') {
          filteredStation = stations.filter((item) => item.id !== station.id);
        } else {
          filteredStation = stations.filter((item) => item.id !== station);
        }

        draft.stations = filteredStation;
      });
    }

    case DELETE_STATION_FAILURE: {
      return produce(state, (draft) => {
        // error
      });
    }

    default: {
      return state;
    }
  }
};

export default stationReducer;
