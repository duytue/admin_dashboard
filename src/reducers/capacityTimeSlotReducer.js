/* eslint-disable no-param-reassign */
import {
  GET_CAPACITIES_REQUEST,
  GET_CAPACITIES_SUCCESS,
  GET_CAPACITIES_FAILURE,
  GET_CAPACITY_REQUEST,
  GET_CAPACITY_SUCCESS,
  GET_CAPACITY_FAILURE,
  ADD_CAPACITY_REQUEST,
  ADD_CAPACITY_SUCCESS,
  ADD_CAPACITY_FAILURE,
  UPDATE_CAPACITY_REQUEST,
  UPDATE_CAPACITY_SUCCESS,
  UPDATE_CAPACITY_FAILURE,
  DELETE_CAPACITY_REQUEST,
  DELETE_CAPACITY_SUCCESS,
  DELETE_CAPACITY_FAILURE,
  CLEAR_CAPACITY,
} from 'src/actions/capacityTimeSlotActions';
import produce from 'immer';

const initialState = {
  capacities: [],
  capacity: {},
  loading: false,
};

const capacityReducer = (state = initialState, action) => {
  switch (action.type) {
    // GET capacity list case
    case GET_CAPACITIES_REQUEST: {
      return produce(state, (draft) => {
        draft.capacities = [];
        draft.loading = true;
      });
    }

    case GET_CAPACITIES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.capacities = response;
        draft.loading = false;
      });
    }

    case GET_CAPACITIES_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // GET capacity detail case
    case GET_CAPACITY_REQUEST: {
      return produce(state, (draft) => {
        draft.capacity = {};
        draft.loading = true;
      });
    }

    case GET_CAPACITY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.capacity = response;
        draft.loading = false;
      });
    }

    case GET_CAPACITY_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // ADD case
    case ADD_CAPACITY_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case ADD_CAPACITY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stations = [...draft.capacities, response];
        draft.loading = false;
      });
    }

    case ADD_CAPACITY_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // UPDATE case
    case UPDATE_CAPACITY_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_CAPACITY_SUCCESS: {
      const { response } = action.payload;
      const updateObj = state.capacities.map((item) => {
        if (item.id === response.id) return response;
        return item;
      });

      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.capacities = updateObj;
          draft.loading = false;
        });
      }

      return produce(state, (draft) => {
        draft.capacities = [];
        draft.loading = false;
      });
    }

    case UPDATE_CAPACITY_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // DELETE case
    case DELETE_CAPACITY_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_CAPACITY_SUCCESS: {
      const { capacity } = action.payload;

      return produce(state, (draft) => {
        const { capacities } = draft;
        let filteredCapacities;
        if (typeof capacity === 'object') {
          filteredCapacities = capacities.filter((item) => item.id !== capacity.id);
        } else {
          filteredCapacities = capacities.filter((item) => item.id !== capacity);
        }

        draft.capacities = filteredCapacities;
        draft.loading = false;
      });
    }

    case DELETE_CAPACITY_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case CLEAR_CAPACITY: {
      return produce(state, (draft) => {
        draft.capacity = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default capacityReducer;
