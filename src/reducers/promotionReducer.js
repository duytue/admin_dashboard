import {
  GET_PROMOTIONS_REQUEST,
  GET_PROMOTIONS_SUCCESS,
  GET_PROMOTIONS_FAILURE,
  GET_PROMOTION_REQUEST,
  GET_PROMOTION_SUCCESS,
  GET_PROMOTION_FAILURE,
  ADD_PROMOTION_REQUEST,
  ADD_PROMOTION_SUCCESS,
  ADD_PROMOTION_FAILURE,
  UPDATE_PROMOTION_REQUEST,
  UPDATE_PROMOTION_SUCCESS,
  UPDATE_PROMOTION_FAILURE,
  DELETE_PROMOTION_REQUEST,
  DELETE_PROMOTION_SUCCESS,
  DELETE_PROMOTION_FAILURE,
} from 'src/actions/promotionActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  promotions: [],
  promotion: {},
  loadingGets: false,
  loadingGetOne: false,
  loadingUpdate: false,
  loadingUpdatePermission: false,
  loadingUpdateActive: false,
  loadingDelete: false,
};

const promotionReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get promotions
    case GET_PROMOTIONS_REQUEST: {
      return produce(state, draft => {
        draft.promotions = [];
        draft.loadingGets = true;
      });
    }

    case GET_PROMOTIONS_SUCCESS: {
      const { response } = action.payload;
      console.log('promotions', response);
      return produce(state, draft => {
        draft.promotions = response;
        draft.loadingGets = false;
      });
    }

    case GET_PROMOTIONS_FAILURE: {
      return produce(state, draft => {
        draft.loadingGets = false;
      });
    }

    // Case Get One
    case GET_PROMOTION_REQUEST: {
      return produce(state, draft => {
        draft.promotion = {};
        draft.loadingGetOne = true;
      });
    }

    case GET_PROMOTION_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.promotion = response;
        draft.loadingGetOne = false;
      });
    }

    case GET_PROMOTION_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOne = false;
      });
    }

    // Case Update promotion
    case UPDATE_PROMOTION_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdate = true;
      });
    }

    case UPDATE_PROMOTION_SUCCESS: {
      const { response } = action.payload;
      const updateObj = _.map(state.medias, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, draft => {
          draft.medias = updateObj;
          draft.loadingUpdate = false;
        });
      } else return produce(state, draft => {
        draft.medias = [response];
        draft.loadingUpdate = false;
      });
    }

    case UPDATE_PROMOTION_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdate = false;
      });
    }

    // // Case Update Active promotion
    // case UPDATE_ACTIVE_REQUEST: {
    //   return produce(state, draft => {
    //     draft.loadingUpdateActive = true;
    //   });
    // }
    //
    // case UPDATE_ACTIVE_SUCCESS: {
    //   console.log('UPDATE_ACTIVE_SUCCESS')
    //   const { response } = action.payload;
    //   const updateObj = _.map(state.medias, i => {
    //     if (i.id === response.id) return response;
    //     return i;
    //   })
    //   if (updateObj !== undefined && updateObj.length > 0) {
    //     return produce(state, draft => {
    //       draft.medias = updateObj;
    //       draft.loadingUpdateActive = false;
    //     });
    //   } else return produce(state, draft => {
    //     draft.medias = [response];
    //     draft.loadingUpdateActive = false;
    //   });
    // }
    //
    // case UPDATE_ACTIVE_FAILURE: {
    //   return produce(state, draft => {
    //     draft.loadingUpdateActive = false;
    //   });
    // }

    // Case Delete promotion
    case DELETE_PROMOTION_REQUEST: {
      return produce(state, draft => {
        draft.loadingDelete = true;
      });
    }

    case DELETE_PROMOTION_SUCCESS: {
      const { promotion } = action.payload;

      const deletePromotion = _.filter(state.promotions, i => {
        return (i.id !== promotion);
      })
      if (deletePromotion !== undefined && deletePromotion.length > 0) {
        return produce(state, draft => {
          draft.promotions = deletePromotion;
          draft.loadingDelete = false;
        });
      } else return produce(state, draft => {
        draft.promotions = [];
        draft.loadingDelete = false;
      });
    }


    case DELETE_PROMOTION_FAILURE: {
      return produce(state, draft => {
        draft.loadingDelete = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default promotionReducer;
