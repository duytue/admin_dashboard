import {
  GET_LANGUAGE_SETTINGS_REQUEST,
  GET_LANGUAGE_SETTINGS_SUCCESS,
  GET_LANGUAGE_SETTINGS_FAILURE,

  CREATE_LANGUAGE_SETTING_REQUEST,
  CREATE_LANGUAGE_SETTING_SUCCESS,
  CREATE_LANGUAGE_SETTING_FAILURE,

  GET_LANGUAGE_SETTING_REQUEST,
  GET_LANGUAGE_SETTING_SUCCESS,
  GET_LANGUAGE_SETTING_FAILURE,
  DELETE_LANGUAGE_SETTING_REQUEST,
  DELETE_LANGUAGE_SETTING_SUCCESS,

} from 'src/actions/languageSettingActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  language_settings: [],
  language_setting: {},
  loadingGetLanguageSettings: false,
  loadingCreateLanguageSetting: false,
  loadingUpdateLanguageSetting: false,
  loadingGetLanguageSetting: false,
  loadingDeleteLanguageSetting: false
};

const languageSettingReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get LanguageSettings
    case GET_LANGUAGE_SETTINGS_REQUEST: {

      return produce(state, draft => {
        draft.language_settings = [];
        draft.loadingGetLanguageSettings = true;
      });
    }

    case GET_LANGUAGE_SETTINGS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.language_settings = response;
        draft.loadingGetLanguageSettings = false;
      });
    }

    case GET_LANGUAGE_SETTINGS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetLanguageSettings = false;
      });
    }

    // Case Create LanguageSettings
    case CREATE_LANGUAGE_SETTING_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateLanguageSetting = true;
      });
    }

    case CREATE_LANGUAGE_SETTING_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.language_settings = draft.language_settings.concat(response);
        draft.loadingCreateLanguageSetting = false;
      });
    }

    case CREATE_LANGUAGE_SETTING_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateLanguageSetting = false;
      });
    }

    // Case Get One LanguageSetting
    case GET_LANGUAGE_SETTING_REQUEST: {
      return produce(state, draft => {
        draft.language_setting = {};
        draft.loadingGetLanguageSetting = true;
      });
    }

    case GET_LANGUAGE_SETTING_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.language_setting = response;
        draft.loadingGetLanguageSetting = false;
      });
    }

    case GET_LANGUAGE_SETTING_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetLanguageSetting = false;
      });
    }

    case DELETE_LANGUAGE_SETTING_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteLanguageSetting = true;
      });
    }

    case DELETE_LANGUAGE_SETTING_SUCCESS: {
      const { language_setting } = action.payload;

      return produce(state, (draft) => {
        const { language_settings } = draft;
        let filteredLanguageSettings;
        if (typeof language_setting === 'object') {
          filteredLanguageSettings = language_settings.filter((item) => item.id !== language_setting.id);
        } else {
          filteredLanguageSettings = language_settings.filter((item) => item.id !== language_setting);
        }

        draft.language_settings = filteredLanguageSettings;
        draft.loadingDeleteLanguageSetting = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default languageSettingReducer;
