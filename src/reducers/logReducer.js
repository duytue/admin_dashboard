import {
  GET_LOG_REQUEST,
  GET_LOG_SUCCESS,
  GET_LOG_FAILURE
} from 'src/actions/logAction';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  logs: [],
  loadingGetLogs: false
  // platform: {},
  // loadingGetPlatforms: false,
  // loadingCreatePlatform: false,
  // loadingGetOnePlatform: false,
  // loadingUpdatePlatform: false,
  // loadingUpdatePermissionOfPlatform: false,
  // loadingUpdateActivePlatform: false
};

const logReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Audit Log
    case GET_LOG_REQUEST: {
      return produce(state, draft => {
        draft.logs = [];
        draft.loadingGetLogs = true;
      });
    }

    case GET_LOG_SUCCESS: {
      const { response } = action.payload;
      console.log('logReducer -> response', response);
      return produce(state, draft => {
        draft.logs = response;
        draft.loadingGetLogs = false;
      });
    }

    case GET_LOG_FAILURE: {
      return produce(state, draft => {
        // Maybe Audit Log  failure
        draft.loadingGetLogs = false;
      });
    }

    default: {
      return state; // We return the default state here
    }
  }
};

export default logReducer;
