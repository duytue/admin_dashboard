/* eslint-disable no-param-reassign */
import _ from 'lodash';
import produce from 'immer';
import {
  SEARCH_PRODUCTS_REQUEST,
  SEARCH_PRODUCTS_SUCCESS,
  SEARCH_PRODUCTS_FAILURE,
  GET_PRODUCTS_REQUEST,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAILURE,
  ADD_PRODUCT_REQUEST,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_FAILURE,
  GET_PRODUCT_REQUEST,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_FAILURE,
  UPDATE_PRODUCT_REQUEST,
  UPDATE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_FAILURE,
  CLEAR_PRODUCT,
} from 'src/actions/productActions';

const initialState = {
  searchProductResult: [],
  items: [],
  item: {},
  total: 0,
  loadingGetItems: false,
  loadingCreateItem: false,
  loadingGetItem: false,
  loadingUpdateItem: false
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_PRODUCTS_REQUEST: {
      return produce(state, (draft) => {
        draft.searchProductResult = [];
        draft.loadingGetItems = true;
      });
    }

    case SEARCH_PRODUCTS_SUCCESS: {
      const { response, total } = action.payload;
      return produce(state, (draft) => {
        draft.searchProductResult = response;
        draft.loadingGetItems = false;
        draft.total = total;
      });
    }

    case SEARCH_PRODUCTS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetItems = false;
      });
    }

    case GET_PRODUCTS_REQUEST: {
      return produce(state, (draft) => {
        draft.items = [];
        draft.loadingGetItems = true;
      });
    }

    case GET_PRODUCTS_SUCCESS: {
      const { response, total } = action.payload;
      return produce(state, (draft) => {
        draft.items = response;
        draft.loadingGetItems = false;
        draft.total = total;
      });
    }

    case GET_PRODUCTS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetItems = false;
      });
    }

    case ADD_PRODUCT_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateItem = true;
      });
    }

    case ADD_PRODUCT_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.items = draft.items.concat(response);
        draft.loadingCreateItem = false;
      });
    }

    case ADD_PRODUCT_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateItem = false;
      });
    }

    case GET_PRODUCT_REQUEST: {
      return produce(state, (draft) => {
        draft.item = {};
        draft.loadingGetItem = true;
      });
    }

    case GET_PRODUCT_SUCCESS: {
      return produce(state, (draft) => {
        draft.item = action.payload;
        draft.loadingGetItem = false;
      });
    }

    case GET_PRODUCT_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetItem = false;
      });
    }

    case UPDATE_PRODUCT_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdateItem = true;
      });
    }

    case UPDATE_PRODUCT_SUCCESS: {
      const { response } = action.payload;
      const updateItem = _.map(state.items, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateItem !== undefined && updateItem.length > 0) {
        return produce(state, (draft) => {
          draft.items = updateItem;
          draft.loadingUpdateItem = false;
        });
      } return produce(state, (draft) => {
        draft.items = [response];
        draft.loadingUpdateItem = false;
      });
    }

    case UPDATE_PRODUCT_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdateItem = false;
      });
    }

    case CLEAR_PRODUCT: {
      return produce(state, (draft) => {
        draft.item = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default productReducer;
