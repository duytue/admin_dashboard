/* eslint-disable no-param-reassign */
import {
  GET_ROLES_REQUEST,
  GET_ROLES_SUCCESS,
  GET_ROLES_FAILURE,
  CREATE_ROLE_REQUEST,
  CREATE_ROLE_SUCCESS,
  CREATE_ROLE_FAILURE,
  GET_ONE_ROLE_REQUEST,
  GET_ONE_ROLE_SUCCESS,
  GET_ONE_ROLE_FAILURE,
  UPDATE_ROLE_REQUEST,
  UPDATE_ROLE_SUCCESS,
  UPDATE_ROLE_FAILURE,
  UPDATE_PERMISSION_ROLE_REQUEST,
  UPDATE_PERMISSION_ROLE_SUCCESS,
  UPDATE_PERMISSION_ROLE_FAILURE,
  UPDATE_ACTIVE_ROLE_REQUEST,
  UPDATE_ACTIVE_ROLE_SUCCESS,
  UPDATE_ACTIVE_ROLE_FAILURE,
  DELETE_ROLE_REQUEST,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_FAILURE,
} from 'src/actions/roleActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  roles: [],
  role: {},
  loadingGetRoles: false,
  loadingCreateRole: false,
  loadingGetOneRole: false,
  loadingUpdateRole: false,
  loadingUpdatePermissionOfRole: false,
  loadingUpdateActiveRole: false,
  loadingDeleteRole: false,
  loading: false,
};

const roleReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Roles
    case GET_ROLES_REQUEST: {
      return produce(state, draft => {
        draft.roles = [];
        draft.loadingGetRoles = true;
      });
    }

    case GET_ROLES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.roles = response;
        draft.loadingGetRoles = false;
      });
    }

    case GET_ROLES_FAILURE: {
      return produce(state, draft => {
        // Maybe role error
        draft.loadingGetRoles = false;
      });
    }

    // Case Create Role
    case CREATE_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateRole = true;
      });
    }

    case CREATE_ROLE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.roles = draft.roles.concat(response)
        draft.loadingCreateRole = false;
      });
    }

    case CREATE_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateRole = false;
        // Maybe role error
      });
    }

    // Case Get One Role
    case GET_ONE_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.role = {};
        draft.loadingGetOneRole = true;
      });
    }

    case GET_ONE_ROLE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.role = response;
        draft.loadingGetOneRole = false;
      });
    }

    case GET_ONE_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneRole = false;
      });
    }

    // Case Update Role
    case UPDATE_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateRole = true;
      });
    }

    case UPDATE_ROLE_SUCCESS: {
      const { response } = action.payload;
      const updateRole = _.map(state.roles, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateRole !== undefined && updateRole.length > 0) {
        return produce(state, draft => {
          draft.roles = updateRole;
          draft.loadingUpdateRole = false;
        });
      } else return produce(state, draft => {
        draft.roles = [response];
        draft.loadingUpdateRole = false;
      });
    }

    case UPDATE_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateRole = false;
      });
    }

    // Case Update Role Role
    case UPDATE_PERMISSION_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdatePermissionOfRole = true;
      });
    }

    case UPDATE_PERMISSION_ROLE_SUCCESS: {
      const { response } = action.payload;
      const updateRole = _.map(state.roles, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateRole !== undefined && updateRole.length > 0) {
        return produce(state, draft => {
          draft.roles = updateRole;
          draft.loadingUpdatePermissionOfRole = false;
        });
      } else return produce(state, draft => {
        draft.roles = [response];
        draft.loadingUpdatePermissionOfRole = false;
      });
    }

    case UPDATE_PERMISSION_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdatePermissionOfRole = false;
      });
    }

    // Case Update Active Role
    case UPDATE_ACTIVE_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateActiveRole = true;
      });
    }

    case UPDATE_ACTIVE_ROLE_SUCCESS: {
      const { response } = action.payload;
      const updateRole = _.map(state.roles, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateRole !== undefined && updateRole.length > 0) {
        return produce(state, draft => {
          draft.roles = updateRole;
          draft.loadingUpdateActiveRole = false;
        });
      } else return produce(state, draft => {
        draft.roles = [response];
        draft.loadingUpdateActiveRole = false;
      });
    }

    case UPDATE_ACTIVE_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateActiveRole = false;
      });
    }

    // Case Delete Role
    case DELETE_ROLE_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteRole = true;
      });
    }

    case DELETE_ROLE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.roles = draft.roles.filter(item => item.id !== response.id);
        draft.loadingDeleteRole = false;
      });
    }

    case DELETE_ROLE_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteRole = false;
        // Maybe role error
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default roleReducer;
