import {
  CREATE_POLICY_FAILURE,
  CREATE_POLICY_REQUEST,
  CREATE_POLICY_SUCCESS,
  CREATE_POLICY_TYPE_FAILURE,
  CREATE_POLICY_TYPE_REQUEST,
  CREATE_POLICY_TYPE_SUCCESS,
  GET_ONE_POLICY_FAILURE,
  GET_ONE_POLICY_REQUEST,
  GET_ONE_POLICY_SUCCESS,
  GET_ONE_POLICY_TYPE_FAILURE,
  GET_ONE_POLICY_TYPE_REQUEST,
  GET_ONE_POLICY_TYPE_SUCCESS,
  GET_POLICIES_FAILURE,
  GET_POLICIES_REQUEST,
  GET_POLICIES_SUCCESS,
  GET_POLICY_TYPES_FAILURE,
  GET_POLICY_TYPES_REQUEST,
  GET_POLICY_TYPES_SUCCESS,
  DELETE_POLICY_FAILURE,
  DELETE_POLICY_REQUEST,
  DELETE_POLICY_SUCCESS,
  UPDATE_POLICY_FAILURE,
  UPDATE_POLICY_REQUEST,
  UPDATE_POLICY_SUCCESS,
  UPDATE_POLICY_TYPE_FAILURE,
  UPDATE_POLICY_TYPE_REQUEST,
  UPDATE_POLICY_TYPE_SUCCESS
} from 'src/actions/policyActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  policies: [],
  policy: {},
  policyTypes: [],
  policyType: {},
  loadingGetPolicies: false,
  loadingCreatePolicy: false,
  loadingGetOnePolicy: false,
  loadingUpdatePolicy: false,
  loadingDeletePolicy: false,
  loadingGetPolicyTypes: false,
  loadingCreatePolicyType: false,
  loadingGetOnePolicyType: false,
  loadingUpdatePolicyType: false,
};

const policyReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Policies
    case GET_POLICIES_REQUEST: {
      return produce(state, draft => {
        draft.policies = [];
        draft.loadingGetPolicies = true;
      });
    }

    case GET_POLICIES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policies = response;
        draft.loadingGetPolicies = false;
      });
    }

    case GET_POLICIES_FAILURE: {
      return produce(state, draft => {
        // Maybe policy error
        draft.loadingGetPolicies = false;
      });
    }

    // Case Create Policy
    case CREATE_POLICY_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreatePolicy = true;
      });
    }

    case CREATE_POLICY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policies = draft.policies.concat(response)
        draft.loadingCreatePolicy = false;
      });
    }

    case CREATE_POLICY_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreatePolicy = false;
        // Maybe policy error
      });
    }

    // Case Get One Policy
    case GET_ONE_POLICY_REQUEST: {
      return produce(state, draft => {
        draft.policy = {};
        draft.loadingGetOnePolicy = true;
      });
    }

    case GET_ONE_POLICY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policy = response;
        draft.loadingGetOnePolicy = false;
      });
    }

    case GET_ONE_POLICY_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOnePolicy = false;
      });
    }

    // Case Update policy
    case UPDATE_POLICY_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdatePolicy = true;
      });
    }

    case UPDATE_POLICY_SUCCESS: {
      const { response } = action.payload;
      const updatePolicy = _.map(state.policies, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updatePolicy !== undefined && updatePolicy.length > 0) {
        return produce(state, draft => {
          draft.policies = updatePolicy;
          draft.loadingUpdatePolicy = false;
        });
      } else return produce(state, draft => {
        draft.policies = [response];
        draft.loadingUpdatePolicy = false;
      });
    }

    case UPDATE_POLICY_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdatePolicy = false;
      });
    }

    // Case Delete policy
    case DELETE_POLICY_REQUEST: {
      return produce(state, draft => {
        draft.policies = [];
        draft.loadingDeletePolicy = true;
      });
    }

    case DELETE_POLICY_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policies = response;
        draft.loadingDeletePolicy = false;
      });
    }

    case DELETE_POLICY_FAILURE: {
      return produce(state, draft => {
        // Maybe policy error
        draft.loadingDeletePolicy = false;
      });
    }

    // policyType

    // Case Get PolicyTypes
    case GET_POLICY_TYPES_REQUEST: {
      return produce(state, draft => {
        draft.policyTypes = [];
        draft.loadingGetPolicyTypes = true;
      });
    }

    case GET_POLICY_TYPES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policyTypes = response;
        draft.loadingGetPolicyTypes = false;
      });
    }

    case GET_POLICY_TYPES_FAILURE: {
      return produce(state, draft => {
        // Maybe policy type error
        draft.loadingGetPolicyTypes = false;
      });
    }

    // Case Create Policy Type
    case CREATE_POLICY_TYPE_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreatePolicyType = true;
      });
    }

    case CREATE_POLICY_TYPE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policyTypes = draft.policyTypes.concat(response)
        draft.loadingCreatePolicyType = false;
      });
    }

    case CREATE_POLICY_TYPE_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreatePolicyType = false;
        // Maybe policy error
      });
    }

    // Case Get One Policy
    case GET_ONE_POLICY_TYPE_REQUEST: {
      return produce(state, draft => {
        draft.policyType = {};
        draft.loadingGetOnePolicyType = true;
      });
    }

    case GET_ONE_POLICY_TYPE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.policyType = response;
        draft.loadingGetOnePolicyType = false;
      });
    }

    case GET_ONE_POLICY_TYPE_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOnePolicyType = false;
      });
    }

    // Case Update policy type
    case UPDATE_POLICY_TYPE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdatePolicyType = true;
      });
    }

    case UPDATE_POLICY_TYPE_SUCCESS: {
      const { response } = action.payload;
      const updatePolicyType = _.map(state.policyTypes, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updatePolicyType !== undefined && updatePolicyType.length > 0) {
        return produce(state, draft => {
          draft.policyTypes = updatePolicyType;
          draft.loadingUpdatePolicyType = false;
        });
      } else return produce(state, draft => {
        draft.policyTypes = [response];
        draft.loadingUpdatePolicyType = false;
      });
    }

    case UPDATE_POLICY_TYPE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdatePolicyType = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default policyReducer;
