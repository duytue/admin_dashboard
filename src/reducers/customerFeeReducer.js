/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import produce from 'immer';
import {
  GET_CUSTOMER_FEES_REQUEST,
  GET_CUSTOMER_FEES_SUCCESS,
  GET_CUSTOMER_FEES_FAILURE,
  GET_CUSTOMER_FEE_REQUEST,
  GET_CUSTOMER_FEE_SUCCESS,
  GET_CUSTOMER_FEE_FAILURE,
  ADD_CUSTOMER_FEE_REQUEST,
  ADD_CUSTOMER_FEE_SUCCESS,
  ADD_CUSTOMER_FEE_FAILURE,
  UPDATE_CUSTOMER_FEE_REQUEST,
  UPDATE_CUSTOMER_FEE_SUCCESS,
  UPDATE_CUSTOMER_FEE_FAILURE,
  DELETE_CUSTOMER_FEE_REQUEST,
  DELETE_CUSTOMER_FEE_SUCCESS,
  DELETE_CUSTOMER_FEE_FAILURE,
  CLEAR_CUSTOMER_FEE,
} from 'src/actions/customerFeeActions';

import map from 'lodash/map';

const initialState = {
  customerFees: [],
  fee: {},
  loading: false,
};

const customerFeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CUSTOMER_FEES_REQUEST: {
      return produce(state, (draft) => {
        draft.customerFees = [];
        draft.loading = true;
      });
    }

    case GET_CUSTOMER_FEES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.customerFees = response;
        draft.loading = false;
      });
    }

    case GET_CUSTOMER_FEES_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case ADD_CUSTOMER_FEE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case ADD_CUSTOMER_FEE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.customerFees = [...draft.customerFees, response];
        draft.loading = false;
      });
    }

    case ADD_CUSTOMER_FEE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case GET_CUSTOMER_FEE_REQUEST: {
      return produce(state, (draft) => {
        draft.fee = {};
        draft.loading = true;
      });
    }

    case GET_CUSTOMER_FEE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.fee = response;
        draft.loading = false;
      });
    }

    case GET_CUSTOMER_FEE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case UPDATE_CUSTOMER_FEE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_CUSTOMER_FEE_SUCCESS: {
      const { response } = action.payload;
      const updateObj = map(state.customerFees, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.customerFees = updateObj;
          draft.loading = false;
        });
      }
      return produce(state, (draft) => {
        draft.customerFees = [response];
        draft.loading = false;
      });
    }

    case UPDATE_CUSTOMER_FEE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case DELETE_CUSTOMER_FEE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_CUSTOMER_FEE_SUCCESS: {
      const { fee } = action.payload;

      return produce(state, (draft) => {
        const { customerFees } = state;
        const filteredFees = customerFees.filter((item) => item.id !== fee.id);
        draft.customerFees = filteredFees;
        draft.loading = false;
      });
    }

    case DELETE_CUSTOMER_FEE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case CLEAR_CUSTOMER_FEE: {
      return produce(state, (draft) => {
        draft.fee = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default customerFeeReducer;
