/* eslint-disable no-param-reassign */
import {
  GET_STOCKS_REQUEST,
  GET_STOCKS_SUCCESS,
  GET_STOCKS_FAILURE,
  GET_STOCK_REQUEST,
  GET_STOCK_SUCCESS,
  GET_STOCK_FAILURE,
  ADD_STOCK_REQUEST,
  ADD_STOCK_SUCCESS,
  ADD_STOCK_FAILURE,
  UPDATE_STOCK_REQUEST,
  UPDATE_STOCK_SUCCESS,
  UPDATE_STOCK_FAILURE,
  DELETE_STOCK_REQUEST,
  DELETE_STOCK_SUCCESS,
  DELETE_STOCK_FAILURE,
  CLEAR_STOCK,
} from 'src/actions/stockActions';
import produce from 'immer';

const initialState = {
  stocks: [],
  stock: {},
  loading: false,
};

const stockReducer = (state = initialState, action) => {
  switch (action.type) {
    // GET stock list case
    case GET_STOCKS_REQUEST: {
      return produce(state, (draft) => {
        draft.stocks = [];
        draft.loading = true;
      });
    }

    case GET_STOCKS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stocks = response;
        draft.loading = false;
      });
    }

    case GET_STOCKS_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // GET stock detail case
    case GET_STOCK_REQUEST: {
      return produce(state, (draft) => {
        draft.stock = {};
        draft.loading = true;
      });
    }

    case GET_STOCK_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stock = response;
        draft.loading = false;
      });
    }

    case GET_STOCK_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // ADD case
    case ADD_STOCK_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case ADD_STOCK_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stations = [...draft.stocks, response];
        draft.loading = false;
      });
    }

    case ADD_STOCK_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // UPDATE case
    case UPDATE_STOCK_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_STOCK_SUCCESS: {
      const { response } = action.payload;
      const updateObj = state.stocks.map((item) => {
        if (item.id === response.id) return response;
        return item;
      });

      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.stocks = updateObj;
          draft.loading = false;
        });
      }

      return produce(state, (draft) => {
        draft.stocks = [];
        draft.loading = false;
      });
    }

    case UPDATE_STOCK_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // DELETE case
    case DELETE_STOCK_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_STOCK_SUCCESS: {
      const { stock } = action.payload;

      return produce(state, (draft) => {
        const { stocks } = draft;
        let filteredStocks;
        if (typeof stock === 'object') {
          filteredStocks = stocks.filter((item) => item.id !== stock.id);
        } else {
          filteredStocks = stocks.filter((item) => item.id !== stock);
        }

        draft.stocks = filteredStocks;
        draft.loading = false;
      });
    }

    case DELETE_STOCK_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case CLEAR_STOCK: {
      return produce(state, (draft) => {
        draft.stock = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default stockReducer;
