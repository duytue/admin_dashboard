import {
  GET_REVIEWS_REQUEST,
  GET_REVIEWS_SUCCESS,
  GET_REVIEWS_FAILURE,
  GET_ONE_REVIEW_REQUEST,
  GET_ONE_REVIEW_SUCCESS,
  GET_ONE_REVIEW_FAILURE,
  UPDATE_REVIEW_REQUEST,
  UPDATE_REVIEW_SUCCESS,
  UPDATE_REVIEW_FAILURE,
  UPDATE_ACTIVE_REVIEW_REQUEST,
  UPDATE_ACTIVE_REVIEW_SUCCESS,
  UPDATE_ACTIVE_REVIEW_FAILURE,
  DELETE_REVIEW_REQUEST,
  DELETE_REVIEW_SUCCESS,
  DELETE_REVIEW_FAILURE,
} from 'src/actions/reviewActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  reviews: [],
  review: {},
  loadingGets: false,
  loadingGetOneComment: false,
  loadingUpdate: false,
  loadingUpdatePermissionOfComment: false,
  loadingUpdateActive: false,
  loadingDeleteComment: false,
};

const reviewReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get reviews
    case GET_REVIEWS_REQUEST: {
      return produce(state, draft => {
        draft.reviews = [];
        draft.loadingGets = true;
      });
    }

    case GET_REVIEWS_SUCCESS: {
      const { response } = action.payload;
      console.log('reviewReducer', response);
      return produce(state, draft => {
        draft.reviews = response;
        draft.loadingGets = false;
      });
    }

    case GET_REVIEWS_FAILURE: {
      return produce(state, draft => {
        // Maybe Comment error
        draft.loadingGets = false;
      });
    }

    // Case Get One Comment
    case GET_ONE_REVIEW_REQUEST: {
      return produce(state, draft => {
        draft.review = {};
        draft.loadingGetOneComment = true;
      });
    }

    case GET_ONE_REVIEW_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.review = response;
        draft.loadingGetOneComment = false;
      });
    }

    case GET_ONE_REVIEW_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneComment = false;
      });
    }

    // Case Update Comment
    case UPDATE_REVIEW_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdate = true;
      });
    }

    case UPDATE_REVIEW_SUCCESS: {
      console.log('UPDATE_REVIEW_SUCCESS')
      const { response } = action.payload;
      const updateObj = _.map(state.reviews, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, draft => {
          draft.reviews = updateObj;
          draft.loadingUpdate = false;
        });
      } else return produce(state, draft => {
        draft.reviews = [response];
        draft.loadingUpdate = false;
      });
    }

    case UPDATE_REVIEW_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdate = false;
      });
    }

    // Case Update Active Comment
    case UPDATE_ACTIVE_REVIEW_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateActive = true;
      });
    }

    case UPDATE_ACTIVE_REVIEW_SUCCESS: {
      console.log('UPDATE_ACTIVE_REVIEW_SUCCESS')
      const { response } = action.payload;
      const updateObj = _.map(state.reviews, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, draft => {
          draft.reviews = updateObj;
          draft.loadingUpdateActive = false;
        });
      } else return produce(state, draft => {
        draft.reviews = [response];
        draft.loadingUpdateActive = false;
      });
    }

    case UPDATE_ACTIVE_REVIEW_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateActive = false;
      });
    }

    // Case Delete Comment
    case DELETE_REVIEW_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteComment = true;
      });
    }

    case DELETE_REVIEW_SUCCESS: {
      const { response } = action.payload;
      const deleteComment = _.filter(state.reviews, i => {
        return (i.id !== response);
      })
      if (deleteComment !== undefined && deleteComment.length > 0) {
        return produce(state, draft => {
          draft.reviews = deleteComment;
          draft.loadingDeleteComment = false;
        });
      } else return produce(state, draft => {
        draft.reviews = [];
        draft.loadingDeleteComment = false;
      });
    }

    case DELETE_REVIEW_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteComment = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default reviewReducer;
