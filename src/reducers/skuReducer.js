/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import produce from 'immer';
import {
  GET_SKUS_REQUEST,
  GET_SKUS_SUCCESS,
  GET_SKUS_FAILURE,
  ADD_SKU_REQUEST,
  ADD_SKU_SUCCESS,
  ADD_SKU_FAILURE,
  UPDATE_SKU_REQUEST,
  UPDATE_SKU_SUCCESS,
  UPDATE_SKU_FAILURE
} from 'src/actions/skuActions';

const initialState = {
  skus: [],
  loadingCreateSku: false,
  loading: false,
};

const skuReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SKUS_REQUEST: {
      return produce(state, (draft) => {
        draft.skus = [];
        draft.loading = true;
      });
    }

    case GET_SKUS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.skus = response;
        draft.loading = false;
      });
    }

    case GET_SKUS_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case ADD_SKU_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateSku = true;
      });
    }

    case ADD_SKU_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.skus = draft.skus.concat(response);
        draft.loadingCreateSku = false;
      });
    }

    case ADD_SKU_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateSku = false;
      });
    }

    case UPDATE_SKU_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_SKU_SUCCESS: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case UPDATE_SKU_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    default: {
      return state;
    }
  }
};

export default skuReducer;
