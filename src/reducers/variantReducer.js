/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import produce from 'immer';
import {
  GET_VARIANTS_REQUEST,
  GET_VARIANTS_SUCCESS,
  GET_VARIANTS_FAILURE,
  SET_VARIANT,
  GET_VARIANT_REQUEST,
  GET_VARIANT_SUCCESS,
  GET_VARIANT_FAILURE,
  ADD_VARIANT_REQUEST,
  ADD_VARIANT_SUCCESS,
  ADD_VARIANT_FAILURE,
  UPDATE_VARIANT_REQUEST,
  UPDATE_VARIANT_SUCCESS,
  UPDATE_VARIANT_FAILURE,
  DELETE_VARIANT_REQUEST,
  DELETE_VARIANT_SUCCESS,
  DELETE_VARIANT_FAILURE,
  CLEAR_VARIANT,
} from 'src/actions/variantActions';

const initialState = {
  variants: [],
  variant: {},
  loading: false,
};

const variantReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_VARIANTS_REQUEST: {
      return produce(state, (draft) => {
        draft.variants = [];
        draft.loading = true;
      });
    }

    case GET_VARIANTS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.variants = response;
        draft.loading = false;
      });
    }

    case GET_VARIANTS_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case SET_VARIANT: {
      const { variantItem } = action.payload;
      return produce(state, (draft) => {
        draft.variant = variantItem;
      });
    }

    case GET_VARIANT_REQUEST: {
      return produce(state, (draft) => {
        draft.variant = {};
        draft.loading = true;
      });
    }

    case GET_VARIANT_SUCCESS: {
      return produce(state, (draft) => {
        draft.variant = action.payload;
        draft.loading = false;
      });
    }

    case GET_VARIANT_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // ADD case
    case ADD_VARIANT_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case ADD_VARIANT_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stations = [...draft.variants, response];
        draft.loading = false;
      });
    }

    case ADD_VARIANT_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // UPDATE case
    case UPDATE_VARIANT_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_VARIANT_SUCCESS: {
      const { response } = action.payload;
      const updateObj = state.variants.map((item) => {
        if (item.id === response.id) return response;
        return item;
      });

      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.variants = updateObj;
          draft.loading = false;
        });
      }

      return produce(state, (draft) => {
        draft.variants = [];
        draft.loading = false;
      });
    }

    case UPDATE_VARIANT_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // DELETE case
    case DELETE_VARIANT_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_VARIANT_SUCCESS: {
      const { variant } = action.payload;

      return produce(state, (draft) => {
        const { variants } = draft;
        let filteredVariants;
        if (typeof variant === 'object') {
          filteredVariants = variants.filter((item) => item.id !== variant.id);
        } else {
          filteredVariants = variants.filter((item) => item.id !== variant);
        }

        draft.variants = filteredVariants;
        draft.loading = false;
      });
    }

    case DELETE_VARIANT_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case CLEAR_VARIANT: {
      return produce(state, (draft) => {
        draft.variant = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default variantReducer;
