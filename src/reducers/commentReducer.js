import {
  GET_COMMENTS_REQUEST,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAILURE,
  GET_ONE_COMMENT_REQUEST,
  GET_ONE_COMMENT_SUCCESS,
  GET_ONE_COMMENT_FAILURE,
  UPDATE_COMMENT_REQUEST,
  UPDATE_COMMENT_SUCCESS,
  UPDATE_COMMENT_FAILURE,
  UPDATE_ACTIVE_COMMENT_REQUEST,
  UPDATE_ACTIVE_COMMENT_SUCCESS,
  UPDATE_ACTIVE_COMMENT_FAILURE,
  DELETE_COMMENT_REQUEST,
  DELETE_COMMENT_SUCCESS,
  DELETE_COMMENT_FAILURE,
} from 'src/actions/commentActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  comments: [],
  comment: {},
  loadingGetComments: false,
  loadingGetOneComment: false,
  loadingUpdateComment: false,
  loadingUpdatePermissionOfComment: false,
  loadingUpdateActiveComment: false,
  loadingDeleteComment: false,
};

const commentReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Comments
    case GET_COMMENTS_REQUEST: {
      return produce(state, draft => {
        draft.comments = [];
        draft.loadingGetComments = true;
      });
    }

    case GET_COMMENTS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.comments = response;
        draft.loadingGetComments = false;
      });
    }

    case GET_COMMENTS_FAILURE: {
      return produce(state, draft => {
        // Maybe Comment error
        draft.loadingGetComments = false;
      });
    }

    // Case Get One Comment
    case GET_ONE_COMMENT_REQUEST: {
      return produce(state, draft => {
        draft.comment = {};
        draft.loadingGetOneComment = true;
      });
    }

    case GET_ONE_COMMENT_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.comment = response;
        draft.loadingGetOneComment = false;
      });
    }

    case GET_ONE_COMMENT_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneComment = false;
      });
    }

    // Case Update Comment
    case UPDATE_COMMENT_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateComment = true;
      });
    }

    case UPDATE_COMMENT_SUCCESS: {
      const { response } = action.payload;
      console.log(response)
      const updateComment = _.map(state.comments, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateComment !== undefined && updateComment.length > 0) {
        return produce(state, draft => {
          draft.comments = updateComment;
          draft.loadingUpdateComment = false;
        });
      } else return produce(state, draft => {
        draft.comments = [response];
        draft.loadingUpdateComment = false;
      });
    }

    case UPDATE_COMMENT_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateComment = false;
      });
    }

    // Case Update Active Comment
    case UPDATE_ACTIVE_COMMENT_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateActiveComment = true;
      });
    }

    case UPDATE_ACTIVE_COMMENT_SUCCESS: {
      const { response } = action.payload;
      const updateComment = _.map(state.comments, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateComment !== undefined && updateComment.length > 0) {
        return produce(state, draft => {
          draft.comments = updateComment;
          draft.loadingUpdateActiveComment = false;
        });
      } else return produce(state, draft => {
        draft.comments = [response];
        draft.loadingUpdateActiveComment = false;
      });
    }

    case UPDATE_ACTIVE_COMMENT_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateActiveComment = false;
      });
    }

    // Case Delete Comment
    case DELETE_COMMENT_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteComment = true;
      });
    }

    case DELETE_COMMENT_SUCCESS: {
      const { response } = action.payload;
      const deleteComment = _.filter(state.comments, i => {
        return (i.id !== response);
      })
      if (deleteComment !== undefined && deleteComment.length > 0) {
        return produce(state, draft => {
          draft.comments = deleteComment;
          draft.loadingDeleteComment = false;
        });
      } else return produce(state, draft => {
        draft.comments = [];
        draft.loadingDeleteComment = false;
      });
    }

    case DELETE_COMMENT_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteComment = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default commentReducer;
