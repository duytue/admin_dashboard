import {
  GET_SUPPLIERS_REQUEST,
  GET_SUPPLIERS_SUCCESS,
  GET_SUPPLIERS_FAILURE,
  CREATE_SUPPLIER_REQUEST,
  CREATE_SUPPLIER_SUCCESS,
  CREATE_SUPPLIER_FAILURE,
  GET_ONE_SUPPLIER_REQUEST,
  GET_ONE_SUPPLIER_SUCCESS,
  GET_ONE_SUPPLIER_FAILURE,
  UPDATE_SUPPLIER_REQUEST,
  UPDATE_SUPPLIER_SUCCESS,
  UPDATE_SUPPLIER_FAILURE
} from 'src/actions/supplierActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  suppliers: [],
  supplier: {},
  loadingGetSuppliers: false,
  loadingCreateSupplier: false,
  loadingGetOneSupplier: false,
  loadingUpdateSupplier: false,
  loadingUpdatePermissionOfSuppliers: false,
  loadingUpdateActiveSuppliers: false,
};

const supplierReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Suppliers
    case GET_SUPPLIERS_REQUEST: {
      console.log('GET_SUPPLIERS_REQUEST')

      return produce(state, draft => {
        draft.suppliers = [];
        draft.loadingGetSuppliers = true;
      });
    }

    case GET_SUPPLIERS_SUCCESS: {
      const { response } = action.payload;
      console.log('Reducer', action)
      return produce(state, draft => {

        draft.suppliers = response;
        draft.loadingGetSuppliers = false;
      });
    }

    case GET_SUPPLIERS_FAILURE: {
      return produce(state, draft => {
        // Maybe supplier error
        draft.loadingGetSuppliers = false;
      });
    }

    // Case Create Suppliers
    case CREATE_SUPPLIER_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateSupplier = true;
      });
    }

    case CREATE_SUPPLIER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.suppliers = draft.suppliers.concat(response)
        draft.loadingCreateSupplier = false;
      });
    }

    case CREATE_SUPPLIER_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateSupplier = false;
      });
    }

    // Case Get One Suppliers
    case GET_ONE_SUPPLIER_REQUEST: {
      return produce(state, draft => {
        draft.supplier = {};
        draft.loadingGetOneSupplier = true;
      });
    }

    case GET_ONE_SUPPLIER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.supplier = response;
        draft.loadingGetOneSupplier = false;
      });
    }

    case GET_ONE_SUPPLIER_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneSupplier = false;
      });
    }

     // Case Update Suppliers
     case UPDATE_SUPPLIER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateSupplier = true;
      });
    }

    case UPDATE_SUPPLIER_SUCCESS: {
      const { response } = action.payload;
      const updateSupplier = _.map(state.suppliers, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateSupplier !== undefined && updateSupplier.length > 0) {
        return produce(state, draft => {
          draft.suppliers = updateSupplier;
          draft.loadingUpdateSupplier = false;
        });
      } else return produce(state, draft => {
        draft.suppliers = [response];
        draft.loadingUpdateSupplier = false;
      });
    }

    case UPDATE_SUPPLIER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateSupplier = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default supplierReducer;
