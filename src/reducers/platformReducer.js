import {
  GET_PLATFORMS_REQUEST,
  GET_PLATFORMS_SUCCESS,
  GET_PLATFORMS_FAILURE,
  CREATE_PLATFORM_REQUEST,
  CREATE_PLATFORM_SUCCESS,
  CREATE_PLATFORM_FAILURE,
  GET_ONE_PLATFORM_REQUEST,
  GET_ONE_PLATFORM_SUCCESS,
  GET_ONE_PLATFORM_FAILURE,
  UPDATE_PLATFORM_REQUEST,
  UPDATE_PLATFORM_SUCCESS,
  UPDATE_PLATFORM_FAILURE,
  DELETE_PLATFORM_REQUEST,
  DELETE_PLATFORM_SUCCESS,
  DELETE_PLATFORM_FAILURE
} from 'src/actions/platformActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  platforms: [],
  platform: {},
  loadingGetPlatforms: false,
  loadingCreatePlatform: false,
  loadingGetOnePlatform: false,
  loadingUpdatePlatform: false,
  loadingUpdatePermissionOfPlatform: false,
  loadingUpdateActivePlatform: false,
  loadingDeletePlatform: false
};

const platformReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Platforms
    case GET_PLATFORMS_REQUEST: {
      return produce(state, draft => {
        draft.platforms = [];
        draft.loadingGetPlatforms = true;
      });
    }

    case GET_PLATFORMS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.platforms = response;
        draft.loadingGetPlatforms = false;
      });
    }

    case GET_PLATFORMS_FAILURE: {
      return produce(state, draft => {
        // Maybe platform error
        draft.loadingGetPlatforms = false;
      });
    }

    // Case Create Platform
    case CREATE_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreatePlatform = true;
      });
    }

    case CREATE_PLATFORM_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.platforms = draft.platforms.concat(response)
        draft.loadingCreatePlatform = false;
      });
    }

    case CREATE_PLATFORM_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreatePlatform = false;
        // Maybe platform error
      });
    }

    // Case Get One Platform
    case GET_ONE_PLATFORM_REQUEST: {
      console.log('GET_ONE_PLATFORM_REQUEST')
      return produce(state, draft => {
        draft.platform = {};
        draft.loadingGetOnePlatform = true;
      });
    }

    case GET_ONE_PLATFORM_SUCCESS: {
      console.log('GET_ONE_PLATFORM_SUCCESS')
      const { response } = action.payload;
      return produce(state, draft => {
        draft.platform = response;
        draft.loadingGetOnePlatform = false;
      });
    }

    case GET_ONE_PLATFORM_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOnePlatform = false;
      });
    }

    // Case Update Platform
    case UPDATE_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdatePlatform = true;
      });
    }

    case UPDATE_PLATFORM_SUCCESS: {
      const { response } = action.payload;
      const updatePlatform = _.map(state.platforms, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updatePlatform !== undefined && updatePlatform.length > 0) {
        return produce(state, draft => {
          draft.platforms = updatePlatform;
          draft.loadingUpdatePlatform = false;
        });
      } else return produce(state, draft => {
        draft.platforms = [response];
        draft.loadingUpdatePlatform = false;
      });
    }

    case UPDATE_PLATFORM_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdatePlatform = false;
      });
    }

    // Case Delete Platform
    case DELETE_PLATFORM_REQUEST: {
      return produce(state, draft => {
        draft.loadingDeleteCategory = true;
      });
    }

    case DELETE_PLATFORM_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        // let i = draft.categories.findIndex(item => item.id == response.id);
        draft.platforms = draft.platforms.filter(item => item.id !== response.id);
        draft.loadingDeleteCategory = false;
      });
    }

    case DELETE_PLATFORM_FAILURE: {
      return produce(state, draft => {
        draft.loadingDeleteCategory = false;
        // Maybe category error
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default platformReducer;
