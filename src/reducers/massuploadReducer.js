/* eslint-disable no-param-reassign */
import produce from 'immer';
import { MASSUPLOAD_REQUEST, MASSUPLOAD_SUCCESS, MASSUPLOAD_FAILURE, MASSUPLOAD_CLEAR } from 'src/actions/massuploadActions';

const initialState = {
  massuploadResponse: null
};

const massuploadReducer = (state = initialState, action) => {
  switch (action.type) {
    case MASSUPLOAD_REQUEST: {
      return state;
    }

    case MASSUPLOAD_SUCCESS: {
      const response = action.payload.response;
      return produce(state, (draft) => {
        draft.massuploadResponse = response;
      })
    }

    case MASSUPLOAD_FAILURE: {
      return state;
    }

    case MASSUPLOAD_CLEAR: {
      return produce(state, (draft) => {
        draft.massuploadResponse = null;
      })
    }

    default: {
      return state;
    }
  }
};

export default massuploadReducer;
