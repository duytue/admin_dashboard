import {
  GET_UI_CONFIG_REQUEST,
  GET_UI_CONFIG_SUCCESS,
  GET_UI_CONFIG_FAILURE
} from 'src/actions/centralUIConfigActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  listUIConfig: [],
  loadingGetListUIConfig: false
};

const CentralUIConfigReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Users
    case GET_UI_CONFIG_REQUEST: {
      return produce(state, draft => {
        draft.users = [];
        draft.loadingGetListUIConfig = true;
      });
    }

    case GET_UI_CONFIG_SUCCESS: {
      const { list_ui_config } = action.payload;
      return produce(state, draft => {
        draft.listUIConfig = list_ui_config;
        draft.loadingGetListUIConfig = false;
      });
    }

    case GET_UI_CONFIG_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetListUIConfig = false;
      });
    }

    default: {
      return state; // We return the default state here
    }
  }
};

export default CentralUIConfigReducer;
