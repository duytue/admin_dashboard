import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import accountReducer from './accountReducer';
import notificationsReducer from './notificationsReducer';
import chatReducer from './chatReducer';
import mailReducer from './mailReducer';
import kanbanReducer from './kanbanReducer';
import userReducer from './userReducer';
import reasonReducer from './reasonReducer';
import roleReducer from './roleReducer';
import permissionReducer from './permissionReducer';
import platformReducer from './platformReducer';
import addressReducer from './addressReducer';
import centralUIConfigReducer from './centralUIConfigReducer';
import supplierReducer from './supplierReducer';
import customerReducer from './customerReducer';
import stationReducer from './stationReducer';
import logReducer from './logReducer';
import commentReducer from './commentReducer';
import reviewReducer from './reviewReducer';
import orderReducer from './orderReducer';
import stockReducer from './stockReducer';
import capacityTimeslotReducer from './capacityTimeSlotReducer';
import skuReducer from './skuReducer';
import customerFeeReducer from './customerFeeReducer';

import languageReducer from './languageReducer';
import languageSettingReducer from './languageSettingReducer';
import categoryReducer from './categoryReducer';
import mediaReducer from './mediaReducer';
import itemAttribute from './itemAttributeReducer';
import itemManagementReducer from './itemManagementReducer';
import productReducer from './productReducer';
import policyReducer from './policyReducer';
import picklistReducer from './picklistReducer';
import variantReducer from './variantReducer';
import massuploadReducer from "./massuploadReducer";
import promotionReducer from "./promotionReducer";

const rootReducer = combineReducers({
  account: accountReducer,
  notifications: notificationsReducer,
  chat: chatReducer,
  mail: mailReducer,
  kanban: kanbanReducer,
  form: formReducer,
  user: userReducer,
  reason: reasonReducer,
  role: roleReducer,
  permission: permissionReducer,
  platform: platformReducer,
  address: addressReducer,
  centralUIConfig: centralUIConfigReducer,
  supplier: supplierReducer,
  customer: customerReducer,
  station: stationReducer,
  auditLog: logReducer,
  comment: commentReducer,
  review: reviewReducer,
  order: orderReducer,
  language: languageReducer,
  language_setting: languageSettingReducer,
  itemAttribute: itemAttribute,
  item: itemManagementReducer,
  category: categoryReducer,
  media: mediaReducer,
  policy: policyReducer,
  stock: stockReducer,
  capacity_timeslot: capacityTimeslotReducer,
  customer: customerReducer,
  sku: skuReducer,
  customer_fee: customerFeeReducer,
  picklist: picklistReducer,
  variant: variantReducer,
  massuploadResponse: massuploadReducer,
  promotion: promotionReducer,
  product: productReducer,
});

export default rootReducer;
