/* eslint-disable no-param-reassign */
import {
  GET_ORDERS_REQUEST,
  GET_ORDERS_SUCCESS,
  GET_ORDERS_FAILURE,
  GET_ORDER_REQUEST,
  GET_ORDER_SUCCESS,
  GET_ORDER_FAILURE,
  ADD_ORDER_REQUEST,
  ADD_ORDER_SUCCESS,
  ADD_ORDER_FAILURE,
  UPDATE_ORDER_REQUEST,
  UPDATE_ORDER_SUCCESS,
  UPDATE_ORDER_FAILURE,
  DELETE_ORDER_REQUEST,
  DELETE_ORDER_SUCCESS,
  DELETE_ORDER_FAILURE,
  GET_ORDER_ITEMS_REQUEST,
  GET_ORDER_ITEMS_SUCCESS,
  GET_ORDER_ITEMS_FAILURE
} from 'src/actions/orderActions';
import produce from 'immer';

const initialState = {
  orders: [],
  orderItems: [],
  loading: false,
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    // GET order list case
    case GET_ORDERS_REQUEST: {
      return produce(state, (draft) => {
        draft.orders = [];
        draft.loading = true;
      });
    }

    case GET_ORDERS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.orders = response;
        draft.loading = false;
      });
    }

    case GET_ORDERS_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // GET order detail case
    case GET_ORDER_REQUEST: {
      return produce(state, (draft) => {
        draft.order = {};
        draft.loading = true;
      });
    }

    case GET_ORDER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.order = response;
        draft.loading = false;
      });
    }

    case GET_ORDER_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // ADD case
    case ADD_ORDER_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case ADD_ORDER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.stations = [...draft.orders, response];
        draft.loading = false;
      });
    }

    case ADD_ORDER_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // UPDATE case
    case UPDATE_ORDER_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_ORDER_SUCCESS: {
      const { response } = action.payload;
      const updateObj = state.reviews.map((item) => {
        if (item.id === response.id) return response;
        return item;
      });

      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.orders = updateObj;
          draft.loading = false;
        });
      }

      return produce(state, (draft) => {
        draft.orders = [];
        draft.loading = false;
      });
    }

    case UPDATE_ORDER_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // DELETE case
    case DELETE_ORDER_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_ORDER_SUCCESS: {
      const { order } = action.payload;

      return produce(state, (draft) => {
        const { orders } = draft;
        let filteredOrders;
        if (typeof order === 'object') {
          filteredOrders = orders.filter((item) => item.id !== order.id);
        } else {
          filteredOrders = orders.filter((item) => item.id !== order);
        }

        draft.orders = filteredOrders;
        draft.loading = false;
      });
    }

    case DELETE_ORDER_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    // GET order items case
    case GET_ORDER_ITEMS_REQUEST: {
      return produce(state, (draft) => {
        draft.orderItems = [];
        draft.loading = true;
      });
    }

    case GET_ORDER_ITEMS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.orderItems = response;
        draft.loading = false;
      });
    }

    case GET_ORDER_ITEMS_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    default: {
      return state;
    }
  }
};

export default orderReducer;
