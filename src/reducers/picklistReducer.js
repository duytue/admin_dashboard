/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import produce from 'immer';
import {
  GET_REQUEST,
  GET_SUCCESS,
  GET_FAILURE,
  GET_CHECK_CONSOLIDATION_REQUEST,
  GET_CHECK_CONSOLIDATION_SUCCESS,
  GET_CHECK_CONSOLIDATION_FAILURE,
  GET_ONE_REQUEST,
  GET_ONE_SUCCESS,
  GET_ONE_FAILURE,
  CREATE_REQUEST,
  CREATE_SUCCESS,
  CREATE_FAILURE,
  UPDATE_REQUEST,
  UPDATE_SUCCESS,
  UPDATE_FAILURE,
  UPDATE_IS_PRINTED_REQUEST,
  UPDATE_IS_PRINTED_SUCCESS,
  UPDATE_IS_PRINTED_FAILURE,
  UPDATE_ACTIVE_REQUEST,
  UPDATE_ACTIVE_SUCCESS,
  UPDATE_ACTIVE_FAILURE,
  DELETE_REQUEST,
  DELETE_SUCCESS,
  DELETE_FAILURE,
  UPDATE_BY_TRADE_ORDER_REQUEST,
  UPDATE_BY_TRADE_ORDER_SUCCESS,
  UPDATE_BY_TRADE_ORDER_FAILURE,
  CLEAR_CONSOLIDATION
} from 'src/actions/picklistActions';

import map from 'lodash/map';

const initialState = {
  picklists: [],
  picklist: {},
  consolidation: {},
  loading: false
};

const picklistReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REQUEST: {
      return produce(state, (draft) => {
        draft.picklists = [];
        draft.loading = true;
      });
    }

    case GET_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.picklists = response;
        draft.loading = false;
      });
    }

    case GET_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case GET_CHECK_CONSOLIDATION_REQUEST: {
      return produce(state, (draft) => {
        draft.consolidation = {};
        draft.loading = true;
      });
    }

    case GET_CHECK_CONSOLIDATION_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.consolidation = response;
        draft.loading = false;
      });
    }

    case GET_CHECK_CONSOLIDATION_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case CREATE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case CREATE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.picklists = [...draft.picklists, response];
        draft.loading = false;
      });
    }

    case CREATE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case GET_REQUEST: {
      return produce(state, (draft) => {
        draft.picklist = {};
        draft.loading = true;
      });
    }

    case GET_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.picklist = response;
        draft.loading = false;
      });
    }

    case GET_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case UPDATE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_SUCCESS: {
      const { response } = action.payload;
      const updateObj = map(state.picklists, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.picklists = updateObj;
          draft.loading = false;
        });
      }
      return produce(state, (draft) => {
        draft.picklists = [response];
        draft.loading = false;
      });
    }

    case UPDATE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case DELETE_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_SUCCESS: {
      const { picklistId } = action.payload;

      return produce(state, (draft) => {
        const { picklists } = draft;
        const filteredPicklists = picklists.filter(
          (item) => item.id !== picklistId
        );
        draft.picklists = filteredPicklists;
        draft.loading = false;
      });
    }

    case DELETE_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    case UPDATE_BY_TRADE_ORDER_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case UPDATE_BY_TRADE_ORDER_SUCCESS: {
      const { response } = action.payload;
      const updateObj = map(state.picklists, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, (draft) => {
          draft.picklists = updateObj;
          draft.consolidation = {};
          draft.loading = false;
        });
      }
      return produce(state, (draft) => {
        draft.picklists = [response];
        draft.consolidation = {};
        draft.loading = false;
      });
    }

    case UPDATE_BY_TRADE_ORDER_FAILURE: {
      return produce(state, (draft) => {
        draft.consolidation = {};
        draft.loading = false;
      });
    }

    case CLEAR_CONSOLIDATION: {
      return produce(state, (draft) => {
        draft.consolidation = {};
      });
    }

    default: {
      return state;
    }
  }
};

export default picklistReducer;
