import {
  GET_CUSTOMER_REQUEST,
  GET_CUSTOMER_SUCCESS,
  GET_CUSTOMER_FAILURE,
  GET_ONE_CUSTOMER_REQUEST,
  GET_ONE_CUSTOMER_SUCCESS,
  GET_ONE_CUSTOMER_FAILURE,
  CREATE_CUSTOMER_REQUEST,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_FAILURE,
  UPDATE_CUSTOMER_REQUEST,
  UPDATE_CUSTOMER_SUCCESS,
  UPDATE_CUSTOMER_FAILURE,
  GET_METADATA_REQUEST,
  GET_METADATA_SUCCESS,
  GET_METADATA_FAILURE,

} from 'src/actions/customerAction';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  customers: [],
  metadatas: [],
  customer: {},
  loadingGetCustomers: false,
  loadingCreateCustomer: false,
  loadingGetOneCustomer: false,
  loadingUpdateCustomer: false,
  loadingMetadata: false,
};

const customerReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Customer
    case GET_CUSTOMER_REQUEST: {
      return produce(state, draft => {
        draft.customers = [];
        draft.loadingGetCustomers = true;
      });
    }

    case GET_CUSTOMER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.customers = response;
        draft.loadingGetCustomers = false;
      });
    }

    case GET_CUSTOMER_FAILURE: {
      return produce(state, draft => {
        // Maybe user error
        draft.loadingGetCustomers = false;
      });
    }

    // Case Create Customer
    case CREATE_CUSTOMER_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateCustomer = true;
      });
    }

    case CREATE_CUSTOMER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.customers = draft.customers.concat(response)
        draft.loadingCreateCustomer = false;
      });
    }

    case CREATE_CUSTOMER_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateCustomer = false;
      });
    }
    // Case Get One customer
    case GET_ONE_CUSTOMER_REQUEST: {
      return produce(state, draft => {
        draft.customer = {};
        draft.loadingGetOneCustomer = true;
      });
    }

    case GET_ONE_CUSTOMER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.customer = response;
        draft.loadingGetOneCustomer = false;
      });
    }

    case GET_ONE_CUSTOMER_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOneCustomer = false;
      });
    }

    // Case Update Customer
    case UPDATE_CUSTOMER_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateCustomer = true;
      });
    }

    case UPDATE_CUSTOMER_SUCCESS: {
      const { response } = action.payload;
      const updateCustomer = _.map(state.customers, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateCustomer !== undefined && updateCustomer.length > 0) {
        return produce(state, draft => {
          draft.customers = updateCustomer;
          draft.loadingUpdateCustomer = false;
        });
      } else return produce(state, draft => {
        draft.customers = [response];
        draft.loadingUpdateCustomer = false;
      });
    }

    case UPDATE_CUSTOMER_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateCustomer = false;
      });
    }
    // Case Metadata
    case GET_METADATA_REQUEST: {
      return produce(state, draft => {
        draft.metadatas = [];
        draft.loadingMetadata = true;
      });
    }

    case GET_METADATA_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.metadatas = response;
        draft.loadingMetadata = false;
      });
    }

    case GET_METADATA_FAILURE: {
      return produce(state, draft => {
        draft.loadingMetadata = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default customerReducer;
