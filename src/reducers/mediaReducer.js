import {
  GET_REQUEST,
  GET_SUCCESS,
  GET_FAILURE,
  GET_ONE_REQUEST,
  GET_ONE_SUCCESS,
  GET_ONE_FAILURE,
  UPDATE_REQUEST,
  UPDATE_SUCCESS,
  UPDATE_FAILURE,
  UPDATE_ACTIVE_REQUEST,
  UPDATE_ACTIVE_SUCCESS,
  UPDATE_ACTIVE_FAILURE,
  DELETE_REQUEST,
  DELETE_SUCCESS,
  DELETE_FAILURE,
} from 'src/actions/mediaActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  medias: [],
  media: {},
  loadingGets: false,
  loadingGetOne: false,
  loadingUpdate: false,
  loadingUpdatePermission: false,
  loadingUpdateActive: false,
  loadingDelete: false,
};

const mediaReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get medias
    case GET_REQUEST: {
      return produce(state, draft => {
        draft.medias = [];
        draft.loadingGets = true;
      });
    }

    case GET_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        console.log('red', response);
        draft.medias = response;
        draft.loadingGets = false;
      });
    }

    case GET_FAILURE: {
      return produce(state, draft => {
        draft.loadingGets = false;
      });
    }

    // Case Get One
    case GET_ONE_REQUEST: {
      return produce(state, draft => {
        draft.media = {};
        draft.loadingGetOne = true;
      });
    }

    case GET_ONE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.media = response;
        draft.loadingGetOne = false;
      });
    }

    case GET_ONE_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetOne = false;
      });
    }

    // Case Update Comment
    case UPDATE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdate = true;
      });
    }

    case UPDATE_SUCCESS: {
      const { response } = action.payload;
      const updateObj = _.map(state.medias, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, draft => {
          draft.medias = updateObj;
          draft.loadingUpdate = false;
        });
      } else return produce(state, draft => {
        draft.medias = [response];
        draft.loadingUpdate = false;
      });
    }

    case UPDATE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdate = false;
      });
    }

    // Case Update Active Comment
    case UPDATE_ACTIVE_REQUEST: {
      return produce(state, draft => {
        draft.loadingUpdateActive = true;
      });
    }

    case UPDATE_ACTIVE_SUCCESS: {
      console.log('UPDATE_ACTIVE_SUCCESS')
      const { response } = action.payload;
      const updateObj = _.map(state.medias, i => {
        if (i.id === response.id) return response;
        return i;
      })
      if (updateObj !== undefined && updateObj.length > 0) {
        return produce(state, draft => {
          draft.medias = updateObj;
          draft.loadingUpdateActive = false;
        });
      } else return produce(state, draft => {
        draft.medias = [response];
        draft.loadingUpdateActive = false;
      });
    }

    case UPDATE_ACTIVE_FAILURE: {
      return produce(state, draft => {
        draft.loadingUpdateActive = false;
      });
    }

    // Case Delete Comment
    case DELETE_REQUEST: {
      return produce(state, draft => {
        draft.loadingDelete = true;
      });
    }

    case DELETE_SUCCESS: {
      const { response } = action.payload;
      const deleteComment = _.filter(state.medias, i => {
        return (i.id !== response);
      })
      if (deleteComment !== undefined && deleteComment.length > 0) {
        return produce(state, draft => {
          draft.medias = deleteComment;
          draft.loadingDelete = false;
        });
      } else return produce(state, draft => {
        draft.medias = [];
        draft.loadingDelete = false;
      });
    }

    case DELETE_FAILURE: {
      return produce(state, draft => {
        draft.loadingDelete = false;
      });
    }

    default: {
      return state // We return the default state here
    }
  }
};

export default mediaReducer;
