/* eslint-disable no-param-reassign */
import {
  GET_PERMISSIONS_REQUEST,
  GET_PERMISSIONS_SUCCESS,
  GET_PERMISSIONS_FAILURE,
  GET_PERMISSIONS_BY_USER_REQUEST,
  GET_PERMISSIONS_BY_USER_SUCCESS,
  GET_PERMISSIONS_BY_USER_FAILURE,
  GET_PERMISSIONS_OBJECT_REQUEST,
  GET_PERMISSIONS_OBJECT_SUCCESS,
  GET_PERMISSIONS_OBJECT_FAILURE,
  GET_PERMISSIONS_ACTION_REQUEST,
  GET_PERMISSIONS_ACTION_SUCCESS,
  GET_PERMISSIONS_ACTION_FAILURE,
  CREATE_PERMISSION_REQUEST,
  CREATE_PERMISSION_SUCCESS,
  CREATE_PERMISSION_FAILURE,
  GET_ONE_PERMISSION_REQUEST,
  GET_ONE_PERMISSION_SUCCESS,
  GET_ONE_PERMISSION_FAILURE,
  UPDATE_PERMISSION_REQUEST,
  UPDATE_PERMISSION_SUCCESS,
  UPDATE_PERMISSION_FAILURE,
  UPDATE_ROLE_OF_PERMISSION_REQUEST,
  UPDATE_ROLE_OF_PERMISSION_SUCCESS,
  UPDATE_ROLE_OF_PERMISSION_FAILURE,
  UPDATE_ACTIVE_PERMISSION_REQUEST,
  UPDATE_ACTIVE_PERMISSION_SUCCESS,
  UPDATE_ACTIVE_PERMISSION_FAILURE,
  DELETE_PERMISSION_REQUEST,
  DELETE_PERMISSION_SUCCESS,
  DELETE_PERMISSION_FAILURE,
} from 'src/actions/permissionActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  permissions: [],
  permissionsByUser: [],
  objects: [],
  actions: [],
  permission: {},
  loadingGetPermissions: false,
  loadingGetListPermissionsByUser: false,
  loadingGetObjectPermissions: false,
  loadingGetActionPermissions: false,
  loadingCreatePermission: false,
  loadingGetOnePermission: false,
  loadingUpdatePermission: false,
  loadingUpdateRoleOfPermission: false,
  loadingUpdateActivePermission: false,
  loading: false,
};

const permissionReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Permissions
    case GET_PERMISSIONS_REQUEST: {
      return produce(state, (draft) => {
        draft.permissions = [];
        draft.loadingGetPermissions = true;
      });
    }

    case GET_PERMISSIONS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.permissions = response;
        draft.loadingGetPermissions = false;
      });
    }

    case GET_PERMISSIONS_FAILURE: {
      return produce(state, (draft) => {
        // Maybe permission error
        draft.loadingGetPermissions = false;
      });
    }

    // Case Get List Permissions By User
    case GET_PERMISSIONS_BY_USER_REQUEST: {
      return produce(state, (draft) => {
        draft.permissionsByUser = [];
        draft.loadingGetListPermissionsByUser = true;
      });
    }

    case GET_PERMISSIONS_BY_USER_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.permissionsByUser = response.list_permission;
        draft.loadingGetListPermissionsByUser = false;
      });
    }

    case GET_PERMISSIONS_BY_USER_FAILURE: {
      return produce(state, (draft) => {
        // Maybe permission error
        draft.loadingGetListPermissionsByUser = false;
      });
    }

    // Case Get Object Permissions
    case GET_PERMISSIONS_OBJECT_REQUEST: {
      return produce(state, (draft) => {
        draft.objects = [];
        draft.loadingGetObjectPermissions = true;
      });
    }

    case GET_PERMISSIONS_OBJECT_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.objects = response;
        draft.loadingGetObjectPermissions = false;
      });
    }

    case GET_PERMISSIONS_OBJECT_FAILURE: {
      return produce(state, (draft) => {
        // Maybe permission error
        draft.loadingGetObjectPermissions = false;
      });
    }

    // Case Get Action Permissions
    case GET_PERMISSIONS_ACTION_REQUEST: {
      return produce(state, (draft) => {
        draft.actions = [];
        draft.loadingGetActionPermissions = true;
      });
    }

    case GET_PERMISSIONS_ACTION_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.actions = response;
        draft.loadingGetActionPermissions = false;
      });
    }

    case GET_PERMISSIONS_ACTION_FAILURE: {
      return produce(state, (draft) => {
        // Maybe permission error
        draft.loadingGetActionPermissions = false;
      });
    }

    // Case Create Permission
    case CREATE_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreatePermission = true;
      });
    }

    case CREATE_PERMISSION_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.permissions = draft.permissions.concat(response);
        draft.loadingCreatePermission = false;
      });
    }

    case CREATE_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreatePermission = false;
        // Maybe permission error
      });
    }

    // Case Get One Permission
    case GET_ONE_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.permission = {};
        draft.loadingGetOnePermission = true;
      });
    }

    case GET_ONE_PERMISSION_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.permission = response;
        draft.loadingGetOnePermission = false;
      });
    }

    case GET_ONE_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetOnePermission = false;
      });
    }

    // Case Update Permission
    case UPDATE_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdatePermission = true;
      });
    }

    case UPDATE_PERMISSION_SUCCESS: {
      const { response } = action.payload;
      const updatePermission = _.map(state.permissions, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updatePermission !== undefined && updatePermission.length > 0) {
        return produce(state, (draft) => {
          draft.permissions = updatePermission;
          draft.loadingUpdatePermission = false;
        });
      } return produce(state, (draft) => {
        draft.permissions = [response];
        draft.loadingUpdatePermission = false;
      });
    }

    case UPDATE_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdatePermission = false;
      });
    }

    // Case Update Permission Permission
    case UPDATE_ROLE_OF_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdateRoleOfPermission = true;
      });
    }

    case UPDATE_ROLE_OF_PERMISSION_SUCCESS: {
      const { response } = action.payload;
      const updatePermission = _.map(state.permissions, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updatePermission !== undefined && updatePermission.length > 0) {
        return produce(state, (draft) => {
          draft.permissions = updatePermission;
          draft.loadingUpdateRoleOfPermission = false;
        });
      } return produce(state, (draft) => {
        draft.permissions = [response];
        draft.loadingUpdateRoleOfPermission = false;
      });
    }

    case UPDATE_ROLE_OF_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdateRoleOfPermission = false;
      });
    }

    // Case Update Active Permission
    case UPDATE_ACTIVE_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdateActivePermission = true;
      });
    }

    case UPDATE_ACTIVE_PERMISSION_SUCCESS: {
      const { response } = action.payload;
      const updatePermission = _.map(state.permissions, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updatePermission !== undefined && updatePermission.length > 0) {
        return produce(state, (draft) => {
          draft.permissions = updatePermission;
          draft.loadingUpdateActivePermission = false;
        });
      } return produce(state, (draft) => {
        draft.permissions = [response];
        draft.loadingUpdateActivePermission = false;
      });
    }

    case UPDATE_ACTIVE_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdateActivePermission = false;
      });
    }

    // DELETE case
    case DELETE_PERMISSION_REQUEST: {
      return produce(state, (draft) => {
        draft.loading = true;
      });
    }

    case DELETE_PERMISSION_SUCCESS: {
      const { response } = action.payload;

      return produce(state, (draft) => {
        const { permissions } = draft;
        let filteredPermissions;
        if (typeof response === 'object') {
          filteredPermissions = permissions.filter((item) => item.id !== response.id);
        } else {
          filteredPermissions = permissions.filter((item) => item.id !== response);
        }

        draft.permissions = filteredPermissions;
        draft.loading = false;
      });
    }

    case DELETE_PERMISSION_FAILURE: {
      return produce(state, (draft) => {
        draft.loading = false;
      });
    }

    default: {
      return state; // We return the default state here
    }
  }
};

export default permissionReducer;
