import {
  GET_ADDRESSES_REQUEST,
  GET_ADDRESSES_SUCCESS,
  GET_ADDRESSES_FAILURE,
  GET_ADDRESSES_BY_LVL_1_REQUEST,
  GET_ADDRESSES_BY_LVL_1_SUCCESS,
  GET_ADDRESSES_BY_LVL_1_FAILURE,
  GET_ADDRESSES_BY_LVL_2_REQUEST,
  GET_ADDRESSES_BY_LVL_2_SUCCESS,
  GET_ADDRESSES_BY_LVL_2_FAILURE,
  CREATE_ADDRESS_REQUEST,
  CREATE_ADDRESS_SUCCESS,
  CREATE_ADDRESS_FAILURE,
  GET_ONE_ADDRESS_REQUEST,
  GET_ONE_ADDRESS_SUCCESS,
  GET_ONE_ADDRESS_FAILURE,
  UPDATE_ADDRESS_REQUEST,
  UPDATE_ADDRESS_SUCCESS,
  UPDATE_ADDRESS_FAILURE,
  GET_CITIES_REQUEST,
  GET_CITIES_SUCCESS,
  GET_CITIES_FAILURE
} from 'src/actions/addressActions';
import _ from 'lodash';
import produce from 'immer';

const initialState = {
  addresses: [],
  cities: [],
  address: {},
  city: {},
  loadingGetAddresses: false,
  loadingCreateAddress: false,
  loadingGetOneAddress: false,
  loadingUpdateAddress: false,
  loadingUpdatePermissionOfAddress: false,
  loadingUpdateActiveAddress: false,
  loadingGetCities: false
};

const addressReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case Get Addresses
    case GET_ADDRESSES_REQUEST: {
      return produce(state, (draft) => {
        draft.addresses = [];
        draft.loadingGetAddresses = true;
      });
    }

    case GET_ADDRESSES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.addresses = response;
        draft.loadingGetAddresses = false;
      });
    }

    case GET_ADDRESSES_FAILURE: {
      return produce(state, (draft) => {
        // Maybe address error
        draft.loadingGetAddresses = false;
      });
    }

    // Case Get Addresses by LOC LVL 1
    case GET_ADDRESSES_BY_LVL_1_REQUEST: {
      return produce(state, draft => {
        // draft.addresses = [];
        draft.loadingGetAddresses = true;
      });
    }

    case GET_ADDRESSES_BY_LVL_1_SUCCESS: {
      const { response, params } = action.payload;
      return produce(state, draft => {
        const indexNewAddresses = _.findIndex(
          state.addresses,
          i => i.short_id === params.parent_short_id,
        );
        draft.addresses[indexNewAddresses].data_lvl2 = response;
        draft.loadingGetAddresses = false;
      });
    }

    case GET_ADDRESSES_BY_LVL_1_FAILURE: {
      return produce(state, draft => {
        // Maybe address error
        draft.loadingGetAddresses = false;
      });
    }

    // Case Get Addresses by LOC LVL 2
    case GET_ADDRESSES_BY_LVL_2_REQUEST: {
      return produce(state, draft => {
        // draft.addresses = [];
        draft.loadingGetAddresses = true;
      });
    }

    case GET_ADDRESSES_BY_LVL_2_SUCCESS: {
      const { response, params } = action.payload;
      return produce(state, draft => {
        const indexLevel1Addresses = _.findIndex(
          state.addresses,
          i => i.short_id === params.short_id_lvl1,
        );
        const indexLevel2Addresses = _.findIndex(
          state.addresses[indexLevel1Addresses].data_lvl2,
          i => i.short_id === params.parent_short_id,
        );
        draft.addresses[indexLevel1Addresses].data_lvl2[indexLevel2Addresses].data_lvl3 = response;
        draft.address = state.addresses[indexLevel1Addresses].data_lvl2[indexLevel2Addresses];
        draft.loadingGetAddresses = false;
      });
    }

    case GET_ADDRESSES_BY_LVL_2_FAILURE: {
      return produce(state, draft => {
        // Maybe address error
        draft.loadingGetAddresses = false;
      });
    }

    // Case Create Address
    case CREATE_ADDRESS_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingCreateAddress = true;
      });
    }

    case CREATE_ADDRESS_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.addresses = draft.addresses.concat(response);
        draft.loadingCreateAddress = false;
      });
    }

    case CREATE_ADDRESS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingCreateAddress = false;
        // Maybe address error
      });
    }

    // Case Get One Address
    case GET_ONE_ADDRESS_REQUEST: {
      return produce(state, (draft) => {
        draft.address = {};
        draft.loadingGetOneAddress = true;
      });
    }

    case GET_ONE_ADDRESS_SUCCESS: {
      const { params } = action.payload;
      return produce(state, draft => {
        draft.address = params;
        draft.loadingGetOneAddress = false;
      });
    }

    case GET_ONE_ADDRESS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetOneAddress = false;
      });
    }

    // Case Update Address
    case UPDATE_ADDRESS_REQUEST: {
      return produce(state, (draft) => {
        draft.loadingUpdateAddress = true;
      });
    }

    case UPDATE_ADDRESS_SUCCESS: {
      const { response } = action.payload;
      const updateAddress = _.map(state.addresses, (i) => {
        if (i.id === response.id) return response;
        return i;
      });
      if (updateAddress !== undefined && updateAddress.length > 0) {
        return produce(state, (draft) => {
          draft.addresses = updateAddress;
          draft.loadingUpdateAddress = false;
        });
      }
      return produce(state, (draft) => {
        draft.addresses = [response];
        draft.loadingUpdateAddress = false;
      });
    }

    case UPDATE_ADDRESS_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingUpdateAddress = false;
      });
    }

    case GET_CITIES_REQUEST: {
      return produce(state, (draft) => {
        draft.cities = [];
        draft.loadingGetCities = true;
      });
    }

    case GET_CITIES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, (draft) => {
        draft.cities = response;
        draft.loadingGetCities = false;
      });
    }

    case GET_CITIES_FAILURE: {
      return produce(state, (draft) => {
        draft.loadingGetCities = false;
      });
    }

    default: {
      return state; // We return the default state here
    }
  }
};

export default addressReducer;
