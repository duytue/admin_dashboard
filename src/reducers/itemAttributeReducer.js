import {
  GET_ITEM_ATTRIBUTES_REQUEST,
  GET_ITEM_ATTRIBUTES_SUCCESS,
  GET_ITEM_ATTRIBUTES_FAILURE,
  ADD_ITEM_ATTRIBUTE_REQUEST,
  ADD_ITEM_ATTRIBUTE_SUCCESS,
  ADD_ITEM_ATTRIBUTE_FAILURE,
  GET_ITEM_ATTRIBUTE_REQUEST,
  GET_ITEM_ATTRIBUTE_SUCCESS,
  GET_ITEM_ATTRIBUTE_FAILURE,
  CREATE_ITEM_ATTRIBUTE_STEP1,
  CREATE_ITEM_ATTRIBUTE_STEP2
} from 'src/actions/itemAttributeAction';
import produce from 'immer';

const initialState = {
  attributes: [],
  attribute: {},
  loadingGetAttributes: false,
  loadingCreateAtribute: false,
  loadingGetAttribute: false,
  loadingUpdateUser: false,
  loadingUpdateRoleOfUser: false,
  loadingUpdatePermissionOfUser: false,
  loadingUpdateActiveUser: false,
  dataStep1: {},
  dataStep2: {}
};

const itemAttribute = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEM_ATTRIBUTES_REQUEST: {
      return produce(state, draft => {
        draft.attributes = [];
        draft.loadingGetAttributes = true;
      });
    }

    case GET_ITEM_ATTRIBUTES_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.attributes = response;
        draft.loadingGetAttributes = false;
      });
    }

    case GET_ITEM_ATTRIBUTES_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetAttributes = false;
      });
    }

    case ADD_ITEM_ATTRIBUTE_REQUEST: {
      return produce(state, draft => {
        draft.loadingCreateAtribute = true;
      });
    }

    case ADD_ITEM_ATTRIBUTE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.attributes = [response, ...draft.attributes];
        draft.loadingCreateAtribute = false;
      });
    }

    case ADD_ITEM_ATTRIBUTE_FAILURE: {
      return produce(state, draft => {
        draft.loadingCreateAtribute = false;
      });
    }

    case GET_ITEM_ATTRIBUTE_REQUEST: {
      return produce(state, draft => {
        draft.attribute = {};
        draft.loadingGetAttribute = true;
      });
    }

    case GET_ITEM_ATTRIBUTE_SUCCESS: {
      const { response } = action.payload;
      return produce(state, draft => {
        draft.user = response;
        draft.loadingGetAttribute = false;
      });
    }

    case GET_ITEM_ATTRIBUTE_FAILURE: {
      return produce(state, draft => {
        draft.loadingGetAttribute = false;
      });
    }

    case CREATE_ITEM_ATTRIBUTE_STEP1: {
      return produce(state, draft => {
        draft.dataStep1 = action.payload;
      });
    }

    case CREATE_ITEM_ATTRIBUTE_STEP2: {
      return produce(state, draft => {
        draft.dataStep2 = action.payload;
      });
    }

    //   // Case Update User
    //   case UPDATE_USER_REQUEST: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateUser = true;
    //     });
    //   }
    //
    //   case UPDATE_USER_SUCCESS: {
    //     const { response } = action.payload;
    //     const updateUser = _.map(state.users, i => {
    //       if (i.id === response.id) return response;
    //       return i;
    //     })
    //     if (updateUser !== undefined && updateUser.length > 0) {
    //       return produce(state, draft => {
    //         draft.users = updateUser;
    //         draft.loadingUpdateUser = false;
    //       });
    //     } else return produce(state, draft => {
    //       draft.users = [response];
    //       draft.loadingUpdateUser = false;
    //     });
    //   }
    //
    //   case UPDATE_USER_FAILURE: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateUser = false;
    //     });
    //   }
    //
    //   // Case Update Role User
    //   case UPDATE_ROLE_USER_REQUEST: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateRoleOfUser = true;
    //     });
    //   }
    //
    //   case UPDATE_ROLE_USER_SUCCESS: {
    //     const { response } = action.payload;
    //     const updateUser = _.map(state.users, i => {
    //       if (i.id === response.id) return response;
    //       return i;
    //     })
    //     if (updateUser !== undefined && updateUser.length > 0) {
    //       return produce(state, draft => {
    //         draft.users = updateUser;
    //         draft.loadingUpdateRoleOfUser = false;
    //       });
    //     } else return produce(state, draft => {
    //       draft.users = [response];
    //       draft.loadingUpdateRoleOfUser = false;
    //     });
    //   }
    //
    //   case UPDATE_ROLE_USER_FAILURE: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateRoleOfUser = false;
    //     });
    //   }
    //
    //   // Case Update Permission User
    //   case UPDATE_PERMISSION_USER_REQUEST: {
    //     return produce(state, draft => {
    //       draft.loadingUpdatePermissionOfUser = true;
    //     });
    //   }
    //
    //   case UPDATE_PERMISSION_USER_SUCCESS: {
    //     const { response } = action.payload;
    //     const updateUser = _.map(state.users, i => {
    //       if (i.id === response.id) return response;
    //       return i;
    //     })
    //     if (updateUser !== undefined && updateUser.length > 0) {
    //       return produce(state, draft => {
    //         draft.users = updateUser;
    //         draft.loadingUpdatePermissionOfUser = false;
    //       });
    //     } else return produce(state, draft => {
    //       draft.users = [response];
    //       draft.loadingUpdatePermissionOfUser = false;
    //     });
    //   }
    //
    //   case UPDATE_PERMISSION_USER_FAILURE: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateRoleOfUser = false;
    //     });
    //   }
    //
    //   // Case Update Active User
    //   case UPDATE_ACTIVE_USER_REQUEST: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateActiveUser = true;
    //     });
    //   }
    //
    //   case UPDATE_ACTIVE_USER_SUCCESS: {
    //     const { response } = action.payload;
    //     const updateUser = _.map(state.users, i => {
    //       if (i.id === response.id) return response;
    //       return i;
    //     })
    //     if (updateUser !== undefined && updateUser.length > 0) {
    //       return produce(state, draft => {
    //         draft.users = updateUser;
    //         draft.loadingUpdateActiveUser = false;
    //       });
    //     } else return produce(state, draft => {
    //       draft.users = [response];
    //       draft.loadingUpdateActiveUser = false;
    //     });
    //   }
    //
    //   case UPDATE_ACTIVE_USER_FAILURE: {
    //     return produce(state, draft => {
    //       draft.loadingUpdateActiveUser = false;
    //     });
    //   }
    //
    default: {
      return state;
    }
  }
};

export default itemAttribute;
