// or get from process.env.REACT_APP_{var} to handle PROD and DEV environments
export const APP_VERSION = '2.0.0';
export const BASE_URI = 'https://api-stg-cen.jx.com.vn';
export const API_BASE_URL = 'https://api-stg-cen.jx.com.vn/ms-user';
export const API_BASE_URL_MSSTATION =
  'https://api-stg-cen.jx.com.vn/ms-station';
export const API_DEPLOY_URL = 'https://api-stg-cen.jamalex.net/ms-user';
export const API_LOCATION_URL =
  'https://api-stg-cen.jx.com.vn/ms-location-tree';
export const API_BASE_LOG_URL = 'https://api-stg-cen.jx.com.vn/ms-logging';
export const API_BASE_URL_MSSKU = 'https://api-stg-cen.jx.com.vn/ms-sku';

export const API_BASE_URL_MSVARIANT = 'https://api-stg-cen.jx.com.vn/ms-variant';

// Comment API
export const API_COMMENT_URL = '/ms-comment/api';
// Review API
export const API_REVIEW_URL = '/ms-review/api';
// Media API
export const API_MEDIA_URL = '/ms-media/api/v1';
// Picklist API
export const API_ORDER_PROCESSING_URL = '/ms-order-processing/api';
// Package API
export const API_PACKAGE_URL = 'ms-package/api/v1';
// OrderProcess API
export const API_ORDER_PROCESS_URL = 'ms-order-process/api/v1';
// Massupload API
export const API_MASSUPLOAD_URL = 'ms-mass-upload/api';

export const API_POLICY_URL =
  'https://api-stg-cen.jamalex.net/ms-policy/api/v1';
export const API_SUPPLIER_URL = 'https://api-stg-cen.jamalex.net/ms-supplier';
export const API_CUSTOMER_URL = 'https://api-stg-cen.jamalex.net/ms-customer';
export const ENABLE_REDUX_LOGGER = false;

export const MAX_LEVEL = 5;

export default {};
