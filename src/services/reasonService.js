import jwtDecode from 'jwt-decode';
import instance from 'src/utils/axios';
import axios from 'axios';
const  REASON_URL = 'https://api-stg-cen.jx.com.vn/ms-setting/api'

class ReasonService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getReasonType = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${REASON_URL}/reason-type`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      params: {
        size: 100,
        page: 1
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getListReasons = (reasonType) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${REASON_URL}/reason`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      params: {
        txttype: reasonType,
        size: 100,
        page: 1
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createReason = data => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data)
    const config = {
      method: 'post',
      url: `${REASON_URL}/reason`,
      data: dataCreate,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  })

  getReason = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${REASON_URL}/reason/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateReason = (data) => new Promise((resolve, reject) => {
    const dataUpdate = JSON.stringify(data)
    const config = {
      method: 'put',
      url: `${REASON_URL}/reason/${data.id}`,
      data: dataUpdate,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

  deleteReason = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${REASON_URL}/reason/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then((response) => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const reasonService = new ReasonService();

export default reasonService;
