import { BASE_URI, API_MASSUPLOAD_URL } from '../config';
import axios from 'axios';

class MassuploadService {
  getAccessToken = () => localStorage.getItem('accessToken');

  upload = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'POST',
        url: `${BASE_URI}/${API_MASSUPLOAD_URL}/mass-upload/import-items`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data);
          } else {
            resolve(response.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
}

const massuploadService = new MassuploadService();

export default massuploadService;
