import axios from 'axios';
import { API_LOCATION_URL } from '../config';

class AddressService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListAddress = () =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_LOCATION_URL}/api/locations`,
        params: {
          loc_lvl: 1,
          size: 100,
          page: 1,
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
        },
      };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createOneAddress = (data) => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data);
    const config = {
      method: 'post',
      url: `${API_LOCATION_URL}/api/location`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
      data: dataCreate,
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getLocationByLvl = params => {
    return new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_LOCATION_URL}/api/locations`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
        },
        params,
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code === 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  };

  updateAddress = (data) => new Promise((resolve, reject) => {
    const config = {
      method: 'put',
      url: `${API_LOCATION_URL}/api/location/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
        'Content-Type': 'application/json',
      },
      data,
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getListCities = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_LOCATION_URL}/api/locations`,
      params: {
        loc_lvl: 1
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getDistrictByCityId = (cityId) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_LOCATION_URL}/api/locations`,
      params: {
        parent_short_id: cityId,
        loc_lvl: 2
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getWardsByDistrictId = (districtId) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_LOCATION_URL}/api/locations`,
      params: {
        parent_short_id: districtId,
        loc_lvl: 3
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const addressService = new AddressService();

export default addressService;
