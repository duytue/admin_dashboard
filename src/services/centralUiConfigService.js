import jwtDecode from 'jwt-decode';
import axios from 'src/utils/axios';
import instance from 'axios';
import { API_BASE_URL } from '../config';

class CentralUIConfigService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListUserConfig = list_permission =>
    new Promise((resolve, reject) => {
      var axios = require('axios');
      var data = JSON.stringify({
        permission_ids: list_permission
      });

      var config = {
        method: 'post',
        url:
          'https://api-stg-cen.jx.com.vn/ms-ui-config/api/ui-config/list-by-permission-ids',
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(function(response) {
          console.log('getListUserConfig', response)
          if (response.data && response.data.code == 'success') {
            localStorage.setItem(
              'listUIConfig',
              JSON.stringify(response.data.data)
            );
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(function(error) {
          reject(error);
        });
    });
}

const centralUIConfigService = new CentralUIConfigService();

export default centralUIConfigService;
