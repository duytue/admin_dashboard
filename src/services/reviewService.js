import { BASE_URI, API_REVIEW_URL } from '../config';
import axios from 'axios';

class ReviewService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getList = () =>
    new Promise((resolve, reject) => {
      console.log('ReviewService');
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_REVIEW_URL}/reviews`,
        params: {
          size: 100,
          page: 1,
          sort: 'created_at DESC'
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOne = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_REVIEW_URL}/review/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  update = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'PATCH',
        url: `${BASE_URI}${API_REVIEW_URL}/review/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteComment = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'DELETE',
        url: `${BASE_URI}${API_REVIEW_URL}/review/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: id
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(id);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const reviewService = new ReviewService();

export default reviewService;
