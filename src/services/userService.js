import jwtDecode from 'jwt-decode';
import instance from 'src/utils/axios';
import axios from 'axios';
class UserService {
    getAccessToken = () => localStorage.getItem('accessToken');

    getUsers = () => new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/users`,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
            params: {
                size: 100,
                page: 1
            }
        };
        return axios(config)
            .then((response) => {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    getOneUser = (id) => new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${id}`,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    createUser = data => new Promise((resolve, reject) => {
        const dataCreate = JSON.stringify(data)
        const config = {
            method: 'post',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user`,
            data: dataCreate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    })

    updateUser = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify(data)
        const config = {
            method: 'put',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${data.id}`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    updateRoleOfUser = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify(data)
        const config = {
            method: 'post',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/roles`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    updatePermissionOfUser = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify(data)
        const config = {
            method: 'post',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/permissions`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    updateActiveUser = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify({
            ...data,
            active: !data.active,
        })
        const config = {
            method: 'put',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${data.id}`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    deleteUser = data =>
      new Promise((resolve, reject) => {
        var config = {
          method: 'delete',
          url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${data.id}`,
          headers: {
            Authorization: `Bearer ${this.getAccessToken()}`
          }
        };
  
        axios(config)
          .then(response => {
            if (response.data && response.data.code == 'success') {
              resolve(data);
            } else {
              reject(response.data.data.error);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
}

const userService = new UserService();

export default userService;
