import jwtDecode from 'jwt-decode';
import instance from 'src/utils/axios';
import axios from 'axios';

const ITEM_URL = 'https://api-stg-cen.jx.com.vn/ms-product-attribute/api';
class ItemAttributeService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getAttributes = params =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${ITEM_URL}/product/attributes`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        params: {
          size: 100,
          page: 1,
          admin_name: params?.admin_name
        }
      };
      return axios(config)
        .then(response => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getItemAttribute = id =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${ITEM_URL}/product/attribute/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };
      axios(config)
        .then(response => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createItemAttribute = data =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'post',
        url: `${ITEM_URL}/product/attribute`,
        data,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };
      axios(config)
        .then(response => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updateItemAttribute = data =>
    new Promise((resolve, reject) => {
      const dataUpdate = JSON.stringify(data);
      const config = {
        method: 'patch',
        url: `${ITEM_URL}/product/attribute/${data.id}`,
        data: dataUpdate,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };
      axios(config)
        .then(response => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const itemAttributeService = new ItemAttributeService();

export default itemAttributeService;
