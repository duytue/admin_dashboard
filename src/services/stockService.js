import axios from 'axios';
import { BASE_URI } from '../config';

class StockService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListStocks = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-stock/api/stocks`,
      params: {
        size: 100,
        page: 1
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createStock = (data) => new Promise((resolve, reject) => {
    const dataCreate = data;
    dataCreate.selling_quantity = Number(dataCreate.selling_quantity);
    dataCreate.reserved_quantity = Number(dataCreate.reserved_quantity);
    dataCreate.status = 'Normal';

    const config = {
      method: 'post',
      url: `${BASE_URI}/ms-stock/api/stock`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateStock = (data) => new Promise((resolve, reject) => {
    const dataUpdate = data;
    dataUpdate.selling_quantity = Number(dataUpdate.selling_quantity);
    dataUpdate.reserved_quantity = Number(dataUpdate.reserved_quantity);

    const config = {
      method: 'patch',
      url: `${BASE_URI}/ms-stock/api/stock/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataUpdate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getStock = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-stock/api/stock/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteStock = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URI}/ms-stock/api/stock/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 1) {
          resolve(response.data.code);
        } else {
          reject(response.data.code);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const stockService = new StockService();

export default stockService;
