import axios from 'axios';
import { API_BASE_URL } from '../config';

class PermissionService {
    getAccessToken = () => localStorage.getItem('accessToken');

    getListPermissions = () => new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_BASE_URL}/api/permissions`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        params: {
          size: 100,
          page: 1
        }
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    getListPermissionsByUser = (id) => new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `https://api-stg-cen.jamalex.net/ms-user/api/user/${id}/permissions`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    getListObjectPermissions = () => new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_BASE_URL}/api/objects`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        params: {
          size: 100,
          page: 1
        }
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    getListActionPermissions = () => new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_BASE_URL}/api/action`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        params: {
          size: 100,
          page: 1
        }
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    getOnePermission = (id) => new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${API_BASE_URL}/api/permission/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    createPermission = (data) => new Promise((resolve, reject) => {
      const config = {
        method: 'post',
        url: `${API_BASE_URL}/api/permission`,
        data,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    })

    updatePermission = (data) => new Promise((resolve, reject) => {
      const dataUpdate = JSON.stringify({
        ...data,
        active: true,
        // platform_code: "centralweb"
      });
      const config = {
        method: 'put',
        url: `${API_BASE_URL}/api/permission/${data.id}`,
        data: dataUpdate,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    updateRoleOfPermission = (data) => new Promise((resolve, reject) => {
      const dataUpdate = JSON.stringify(data);
      const config = {
        method: 'post',
        url: `${API_BASE_URL}/api/permission/add-for-multi-role`,
        data: dataUpdate,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    updateActivePermission = (data) => new Promise((resolve, reject) => {
      const dataUpdate = JSON.stringify({
        ...data,
        active: !data.active,
      });
      const config = {
        method: 'put',
        url: `${API_BASE_URL}/api/permission/${data.id}`,
        data: dataUpdate,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then((response) => {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

    deletePermission = (id) => new Promise((resolve, reject) => {
      const config = {
        method: 'delete',
        url: `${API_BASE_URL}/api/permission/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data.code === 1) {
            resolve(response.data);
          } else {
            reject(response.data);
          }
        })
        .catch((error) => {
          reject(error.response.data);
        });
    });
}

const permissionService = new PermissionService();

export default permissionService;
