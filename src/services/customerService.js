import jwtDecode from 'jwt-decode';
import instance from 'src/utils/axios';
import { API_CUSTOMER_URL } from '../config';
import axios from 'axios';
class CustomerService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListCustomer = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `https://api-stg-cen.jx.com.vn/ms-customer/api/filter-customer?`,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
      params: {
        size: 100,
        page: 1
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });
  // create customer
  createCustomer = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_CUSTOMER_URL}/api/customer`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          console.log(response.data);
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOneCustomer = id => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `https://api-stg-cen.jamalex.net/ms-customer/api/customer/${id}`,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

  updateCustomer = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'put',
        url: `https://api-stg-cen.jamalex.net/ms-customer/api/customer/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == "success") {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  uploadImage = (presignedPostData, file) =>
    new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(presignedPostData.fields).forEach(key => {
        formData.append(key, presignedPostData.fields[key]);
      });

      // Actual file has to be appended last.
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", presignedPostData.url, true);
      xhr.send(formData);
      xhr.onload = function (response) {
        this.status === 204 ? resolve('success') : reject(null);
      };
    });

  preUpload = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jamalex.net/ms-media/api/v1/uploads/pre_up`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createUpload = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jx.com.vn/ms-media/api/v1/uploads/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  deleteOneCustomer = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'delete',
        url: `${API_CUSTOMER_URL}/api/customer/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        }
      };

      axios(config)
        .then(response => {
          console.log('response', response);
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteBulkCustomer = listCustomer =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jx.com.vn/ms-customer/api/customer`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: listCustomer
      };

      axios(config)
        .then(response => {
          console.log('response', response);
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  getListMetaData = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `https://api-stg-cen.jamalex.net/ms-customer/api/metadata?`,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
      params: {
        platform_key: 'centralweb',
        type: 'customer_group'
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

}

const customerService = new CustomerService();

export default customerService;
