import { BASE_URI, API_MEDIA_URL } from '../config';
import axios from 'axios';

class MediaService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getList = () =>
    new Promise((resolve, reject) => {
      console.log('mediaService');
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_MEDIA_URL}/uploads`,
        params: {
          size: 100,
          page: 1,
          sort: 'created_at DESC'
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOne = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_MEDIA_URL}/uploads/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  update = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'POST',
        url: `${BASE_URI}${API_MEDIA_URL}/uploads/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteMedia = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'DELETE',
        url: `${BASE_URI}${API_MEDIA_URL}/uploads/delete`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: {ids: [id] }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(id);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  uploadFile = (presignedPostData, file) =>
    new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(presignedPostData.fields).forEach(key => {
        formData.append(key, presignedPostData.fields[key]);
      });

      // Actual file has to be appended last.
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", presignedPostData.url, true);
      xhr.send(formData);
      xhr.onload = function (response) {
        this.status === 204 ? resolve('success') : reject(null);
      };
    });

  uploadUpdateFile = (presignedPostData, file) =>
    new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(presignedPostData.fields).forEach(key => {
        formData.append(key, presignedPostData.fields[key]);
      });

      // Actual file has to be appended last.
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", presignedPostData.url, true);
      xhr.send(formData);
      xhr.onload = function (response) {
        this.status === 204 ? resolve(response) : reject(null);
      };
    });


  preUpload = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jamalex.net/ms-media/api/v1/uploads/pre_up`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createUpload = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jx.com.vn/ms-media/api/v1/uploads/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const mediaService = new MediaService();

export default mediaService;
