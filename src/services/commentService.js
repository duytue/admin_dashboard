import { API_BASE_URL, BASE_URI, API_COMMENT_URL } from '../config';
import axios from 'axios';

class CommentService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListComment = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_COMMENT_URL}/comments`,
        params: {
          size: 100,
          page: 1,
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOneComment = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_COMMENT_URL}/comment/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updateComment = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'PATCH',
        url: `${BASE_URI}${API_COMMENT_URL}/comment/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteComment = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'DELETE',
        url: `${BASE_URI}${API_COMMENT_URL}/comment/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: id
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(id);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const commentService = new CommentService();

export default commentService;
