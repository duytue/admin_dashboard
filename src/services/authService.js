import jwtDecode from 'jwt-decode';
import axios from 'src/utils/axios';
import instance from 'axios';

class AuthService {
  setAxiosInterceptors = ({ onLogout }) => {
    axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response && error.response.status === 401) {
          this.setSession(null);

          if (onLogout) {
            onLogout();
          }
        }

        return Promise.reject(error);
      },
    );
  };

  handleAuthentication() {
    const accessToken = this.getAccessToken();

    if (!accessToken) {
      return;
    }

    if (this.isValidToken(accessToken)) {
      this.setSession(accessToken);
    } else {
      this.setSession(null);
    }
  }

  loginWithEmailAndPassword = (email, password) =>
    new Promise((resolve, reject) => {
      const data = JSON.stringify({ email, password });

      const config = {
        method: 'post',
        url: 'https://api-stg-cen.jamalex.net/api/login',
        headers: {
          'Content-Type': 'application/json',
        },
        data,
      };
      instance(config)
        .then(response => {
          if (response.data.data) {
            this.setSession(response.data.data.token);
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  loginInWithToken = () =>
    new Promise((resolve, reject) => {
      const access_token = localStorage.getItem('accessToken') || undefined;
      if (access_token) {
        instance
          .get(
            `https://api-stg-cen.jamalex.net/ms-user/api/user/64027cf4-8e98-45f8-8cfd-ced37681af29`,
            {
              headers: {
                Authorization: `Bearer ${access_token}`,
              },
            },
          )
          .then(response => {
            if (response.data.data) {
              resolve(response.data.data);
            } else {
              reject(response.data.data);
            }
          })
          .catch(error => {
            reject(error);
          });
      } else {
        this.logout();
      }
    });

  logout = () => {
    this.setSession(null);
  };

  setSession = accessToken => {
    if (accessToken) {
      localStorage.setItem('accessToken', accessToken);
      axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    } else {
      localStorage.removeItem('accessToken');
      delete axios.defaults.headers.common.Authorization;
    }
  };

  getAccessToken = () => localStorage.getItem('accessToken');

  isValidToken = accessToken => {
    if (!accessToken) {
      return false;
    }

    const decoded = jwtDecode(accessToken);
    const currentTime = Date.now() / 1000;

    return decoded.exp > currentTime;
  };

  isAuthenticated = () => !!this.getAccessToken();
}

const authService = new AuthService();

export default authService;
