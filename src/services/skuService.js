import axios from 'axios';
import { API_BASE_URL_MSSKU } from '../config';

class SkuService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListSkus = (params = {}) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_BASE_URL_MSSKU}/api/skus`,
      params: {
        ...params,
        size: 900,
        page: 1,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          console.log('list getListSkus response sucess', response.data);
          resolve(response.data.data);
        } else {
          console.log('list getListSkus response reject', response.data);
          reject(response.data.data);
        }
      })
      .catch((error) => {
        console.log('list getListSkus response error', error);
        reject(error);
      });
  });

  // JUST TEMPORARY
  getSkuById = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `https://api-stg-cen.jx.com.vn/ms-elasticsearch/api/sku/search?id=${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createSku = (data) => new Promise((resolve, reject) => {
    const config = {
      method: 'post',
      url: `${API_BASE_URL_MSSKU}/api/sku`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateSku = (data) => new Promise((resolve, reject) => {
    const config = {
      method: 'put',
      url: `${API_BASE_URL_MSSKU}/api/sku/${data.id}`,
      data,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const skuService = new SkuService();

export default skuService;
