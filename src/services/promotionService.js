import axios from 'axios';
import { BASE_URI } from '../config';

class PromotionService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getPromotions = () =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${BASE_URI}/ms-promotion/api/promotion`,
        params: {
          limit: 100,
          page: 1
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code === 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  createPromotion = (data) =>
    new Promise((resolve, reject) => {
      const dataCreate = data;
      dataCreate.max_size = parseInt(data.max_size);
      dataCreate.customer_size = parseInt(data.customer_size);
      dataCreate.value = parseInt(data.value);

      const config = {
        method: 'post',
        url: `${BASE_URI}/ms-promotion/api/promotion`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: dataCreate
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code === 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  updatePromotion = (data, id) =>
    new Promise((resolve, reject) => {

      const dataUpdate = data;
      dataUpdate.max_size = parseInt(data.max_size);
      dataUpdate.customer_size = parseInt(data.customer_size);
      dataUpdate.value = parseInt(data.value);

      const config = {
        method: 'PUT',
        url: `${BASE_URI}/ms-promotion/api/promotion/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        dataUpdate
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code === 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  getPromotionById = (promotionId) =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${BASE_URI}/ms-promotion/api/promotion/${promotionId}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code === 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  deletePromotion = (promotionId) =>
    new Promise((resolve, reject) => {
      const config = {
        method: 'delete',
        url: `${BASE_URI}/ms-promotion/api/promotion/${promotionId}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data.code === 'success') {

            resolve(promotionId);
          } else {
            reject(response.data.code);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
}

const promotionService = new PromotionService();

export default promotionService;
