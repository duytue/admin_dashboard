import { API_SUPPLIER_URL } from '../config';
import axios from 'axios';

class SupplierService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListSupplier = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_SUPPLIER_URL}/api/suppliers`,
        params: {
          size: 100,
          page: 1
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          // console.log('Hello', response.data.code)
          if (response.data && response.data.code == 1) {
            console.log('Hello', response.data.code);
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOneSupplier = data =>
    new Promise((resolve, reject) => {
      // const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_SUPPLIER_URL}/api/supplier`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOneSupplier = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_SUPPLIER_URL}/api/supplier/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updateSupplier = data =>
    new Promise((resolve, reject) => {
      console.log('data', data);
      var config = {
        method: 'patch',
        url: `${API_SUPPLIER_URL}/api/supplier/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response);
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteOneSupplier = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'delete',
        url: `${API_SUPPLIER_URL}/api/supplier/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        }
      };

      axios(config)
        .then(response => {
          console.log('response', response);
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteBulkSupplier = listSupplier =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jx.com.vn/ms-supplier/api/suppliers`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: listSupplier
      };

      axios(config)
        .then(response => {
          console.log('response', response);
          if (response.data && response.data.code == 1) {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const supplierService = new SupplierService();

export default supplierService;
