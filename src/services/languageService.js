import jwtDecode from 'jwt-decode';
import instance from 'src/utils/axios';
import axios from 'axios';
import { reject } from 'lodash';
import { API_BASE_URL } from '../config';
import querystring from 'querystring';

const BASE_URL_SETTING_SERVICE = 'https://api-stg-cen.jx.com.vn/ms-setting';

class LanguageService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getLanguages = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URL_SETTING_SERVICE}/api/language`,
      params: {
        size: 100,
        page: 1
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getOneLanguage = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URL_SETTING_SERVICE}/api/language/${id}`,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then(function(response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function(error) {
        reject(error);
      });
  });

  createLanguage = (data) => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data);
    const config = {
      method: 'post',
      url: `${BASE_URL_SETTING_SERVICE}/api/language`,
      data: dataCreate,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then((response) => {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateLanguage = (data) => new Promise((resolve, reject) => {
    const dataUpdate = JSON.stringify(data);
    const config = {
      method: 'put',
      url: `${BASE_URL_SETTING_SERVICE}/api/language/${data.id}`,
      data: dataUpdate,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then(function(response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function(error) {
        reject(error);
      });
  });

  deleteLanguage = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URL_SETTING_SERVICE}/api/language/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };
    axios(config)
      .then((response) => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const languageService = new LanguageService();

export default languageService;
