import axios from 'axios';
import { BASE_URI } from '../config';

const ITEM_URL = 'https://api-stg-cen.jx.com.vn/ms-item/api';
class ItemManagementService {
  getAccessToken = () => localStorage.getItem('accessToken');

  searchItem = (params = {}) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-list-sku-storage/api/search`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
      params: {
        size: 10,
        page: 1,
        ...params,
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getItems = (params = {}) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${ITEM_URL}/items`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
      },
      params: {
        size: 100,
        page: 1,
        ...params,
      }
    };
    return axios(config)
      .then((response) => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getItem = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${ITEM_URL}/item/${id}`,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

  createItem = data => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data)
    const config = {
      method: 'post',
      url: `${ITEM_URL}/item`,
      data: dataCreate,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  })

  updateItem = (data) => new Promise((resolve, reject) => {
    const dataUpdate = JSON.stringify(data)
    const config = {
      method: 'put',
      url: `${ITEM_URL}/item/${data.id}`,
      data: dataUpdate,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

}

const itemManagementService = new ItemManagementService();

export default itemManagementService;
