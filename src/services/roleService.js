import axios from 'axios';

class RoleService {
    getAccessToken = () => localStorage.getItem('accessToken');

    getRoles = () => new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/roles`,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
            params: {
                size: 100,
                page: 1
            }
        };
        return axios(config)
            .then((response) => {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    getOneRole = (id) => new Promise((resolve, reject) => {
        const config = {
            method: 'get',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/role/${id}`,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    createRole = data => new Promise((resolve, reject) => {
        const config = {
            method: 'post',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/role`,
            data: data,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    })

    updateRole = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify({
            ...data,
            active: true,
            platform_code: "centralweb"
        })
        const config = {
            method: 'put',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/role/${data.id}`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    updatePermissionOfRole = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify(data)
        const config = {
            method: 'post',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/role/permissions`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    updateActiveRole = (data) => new Promise((resolve, reject) => {
        const dataUpdate = JSON.stringify({
            ...data,
            active: !data.active,
        })
        const config = {
            method: 'put',
            url: `https://api-stg-cen.jamalex.net/ms-user/api/role/${data.id}`,
            data: dataUpdate,
            headers: {
                'Authorization': `Bearer ${this.getAccessToken()}`
            },
        };
        axios(config)
            .then(function (response) {
                if (response.data.data) {
                    resolve(response.data.data);
                } else {
                    reject(response);
                }
            })
            .catch(function (error) {
                reject(error);
            });
    });

    deleteRole = data =>
      new Promise((resolve, reject) => {
        var config = {
          method: 'delete',
          url: `https://api-stg-cen.jamalex.net/ms-user/api/role/${data.id}`,
          headers: {
            Authorization: `Bearer ${this.getAccessToken()}`
          }
        };
  
        axios(config)
          .then(response => {
            if (response.data && response.data.code == 'success') {
              resolve(data);
            } else {
              reject(response.data.data.error);
            }
          })
          .catch(error => {
            reject(error);
          });
    });
}

const roleService = new RoleService();

export default roleService;
