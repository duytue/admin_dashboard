import axios from 'axios';
import { BASE_URI } from '../config';

class CapacityTimeSlotService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListCapacities = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-capacity/api/capacities`,
      params: {
        size: 100,
        page: 1
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createCapacity = (data) => new Promise((resolve, reject) => {
    const dataCreate = data;
    dataCreate.position = Number(dataCreate.position);
    dataCreate.capacity = Number(dataCreate.capacity);

    const config = {
      method: 'post',
      url: `${BASE_URI}/ms-capacity/api/capacity`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateCapacity = (data) => new Promise((resolve, reject) => {
    const dataUpdate = data;
    dataUpdate.position = Number(dataUpdate.position);
    dataUpdate.capacity = Number(dataUpdate.capacity);

    const config = {
      method: 'patch',
      url: `${BASE_URI}/ms-capacity/api/capacity/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataUpdate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getCapacity = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-capacity/api/capacity/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteCapacity = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URI}/ms-capacity/api/capacity/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 1) {
          resolve(response.data.code);
        } else {
          reject(response.data.code);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const capacityTimeSlotService = new CapacityTimeSlotService();

export default capacityTimeSlotService;
