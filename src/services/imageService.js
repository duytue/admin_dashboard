// import { API_BASE_URL } from '../config';
import axios from 'axios';

const API_BASE_URL = 'http://cdn.jx.com.vn/api/v1/uploads'

class ImageService {
  getAccessToken = () => localStorage.getItem('accessToken');

  uploadImage = formData =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `${API_BASE_URL}/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: formData
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const imageService = new ImageService();

export default imageService;