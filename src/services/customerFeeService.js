import axios from 'axios';
import { BASE_URI } from '../config';

class CustomerFeeService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getCustomerFees = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-customer-fee/api/customer-fees`,
      params: {
        size: 100,
        page: 1,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createCustomerFee = (data) => new Promise((resolve, reject) => {
    const dataCreate = data;
    dataCreate.platform_key = 'centralweb';

    const config = {
      method: 'post',
      url: `${BASE_URI}/ms-customer-fee/api/customer-fee`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateCustomerFee = (data) => new Promise((resolve, reject) => {
    const config = {
      method: 'PATCH',
      url: `${BASE_URI}/ms-customer-fee/api/customer-fee/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`,
        'Content-Type': 'application/json'
      },
      data
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getCustomerFeeById = (feeId) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-customer-fee/api/customer-fee/${feeId}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteCustomerFee = (feeId) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URI}/ms-customer-fee/api/customer-fee/${feeId}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 1) {
          resolve(response.data.code);
        } else {
          reject(response.data.code);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const customerFeeService = new CustomerFeeService();

export default customerFeeService;
