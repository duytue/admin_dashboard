import axios from 'axios';
import { API_BASE_URL } from '../config';

const BASE_URL_SETTING_SERVICE = 'https://api-stg-cen.jx.com.vn/ms-setting'

class LanguageSettingService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getLanguageSettings = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URL_SETTING_SERVICE}/api/language-info`,
      params: {
        size: 100,
        page: 1,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createLanguageSetting = data => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data)
    const config = {
      method: 'post',
      url: `${BASE_URL_SETTING_SERVICE}/api/language-info`,
      data: dataCreate,
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        if (response.data.data) {
          resolve(response.data.data);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });

  updateLanguageSetting = (data) =>
    new Promise((resolve, reject) => {
      const dataUpdate = JSON.stringify(data);
      const config = {
        method: 'put',
        url: `${BASE_URL_SETTING_SERVICE}/api/language-info`,
        data: dataUpdate,
        headers: {
          'Authorization': `Bearer ${this.getAccessToken()}`
        },
      };
      axios(config)
        .then(function (response) {
          if (response.data.data) {
            resolve(response.data.data);
          } else {
            reject(response);
          }
        })
        .catch(function (error) {
          reject(error);
        });
    });

  getLanguageSetting = (key) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URL_SETTING_SERVICE}/api/language-info/${key}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteLanguageSetting = (key, platform_key) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URL_SETTING_SERVICE}/api/language-info`,
      data: { "key": key, "platform_key": platform_key },
      headers: {
        'Authorization': `Bearer ${this.getAccessToken()}`
      },
    };
    axios(config)
      .then(function (response) {
        console.log(response); // response.data.data là null
        if (response.data.message) {
          resolve(response.data.message);
        } else {
          reject(response);
        }
      })
      .catch(function (error) {
        reject(error);
      });
  });
}

const languageSettingService = new LanguageSettingService();

export default languageSettingService;
