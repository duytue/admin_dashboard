import { API_POLICY_URL } from '../config';
import axios from 'axios';

class PolicyService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListPolicyType = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_POLICY_URL}/policy-type`,
        params: {
          size: data.size || 100,
          page: data.page || 1,
          name: data.name,
          creator_id: data.creator_id
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOnePolicyType = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_POLICY_URL}/policy-type/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOnePolicyType = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_POLICY_URL}/policy-type/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updatePolicyType = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'post',
        url: `${API_POLICY_URL}/policy-type/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getListPolicy = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_POLICY_URL}/policy`,
        params: {
          size: 100,
          page: 1,
          type: data.type,
          platform_key: data.platform_key
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOnePolicy = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_POLICY_URL}/policy/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOnePolicy = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_POLICY_URL}/policy/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updatePolicy = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'post',
        url: `${API_POLICY_URL}/policy/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deletePolicy = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'delete',
        url: `${API_POLICY_URL}/policy/delete`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  uploadImage = (presignedPostData, file) =>
    new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(presignedPostData.fields).forEach(key => {
        formData.append(key, presignedPostData.fields[key]);
      });

      // Actual file has to be appended last.
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", presignedPostData.url, true);
      xhr.send(formData);
      xhr.onload = function (response) {
        this.status === 204 ? resolve('success') : reject(null);
      };
  });

  preUpload = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jamalex.net/ms-media/api/v1/uploads/pre_up`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

    createUpload = data =>
      new Promise((resolve, reject) => {
        console.log('data', data)
        var config = {
          method: 'post',
          url: `https://api-stg-cen.jx.com.vn/ms-media/api/v1/uploads/create`,
          headers: {
            Authorization: `Bearer ${this.getAccessToken()}`,
            'Content-Type': 'application/json'
          },
          data: data
        };
  
        axios(config)
          .then(response => {
            console.log('response', response)
            if (response.data && response.data.code == 'success') {
              resolve(response.data.data);
            } else {
              reject(response.data.data.error);
            }
          })
          .catch(error => {
            reject(error);
          });
      });

  }

const policyService = new PolicyService();

export default policyService;
