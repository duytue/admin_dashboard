import axios from 'axios';
import { BASE_URI } from '../config';

class VariantService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListVariants = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-variant/api/variants`,
      params: {
        size: 100,
        page: 1
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createVariant = (data) => new Promise((resolve, reject) => {
    const dataCreate = data;

    const config = {
      method: 'post',
      url: `${BASE_URI}/ms-variant/api/variant`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateVariant = (data) => new Promise((resolve, reject) => {
    const dataUpdate = data;

    const config = {
      method: 'PUT',
      url: `${BASE_URI}/ms-variant/api/variant/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataUpdate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getVariant = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-variant/api/variant/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteVariant = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URI}/ms-variant/api/variant/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 'success') {
          resolve(response.data.code);
        } else {
          reject(response.data.code);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const variantService = new VariantService();

export default variantService;
