import { API_BASE_LOG_URL } from '../config';
import axios from 'axios';

class LogService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListLog = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_LOG_URL}/api/logs`,
        params: {
          size: 100,
          page: 1
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          console.log('LogService -> response', response);
          if (response.data) {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const logService = new LogService();

export default logService;
