import { API_BASE_URL } from '../config';
import axios from 'axios';

class PlatFormService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListPlatForm = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_URL}/api/platforms`,
        params: {
          size: 100,
          page: 1,
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOnePlatForm = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_BASE_URL}/api/platform`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOnePlatForm = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_URL}/api/platform/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updatePlatform = data =>
    new Promise((resolve, reject) => {
      console.log('data', data)
      var config = {
        method: 'put',
        url: `${API_BASE_URL}/api/platform/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          console.log('response', response)
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deletePlatform = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'delete',
        url: `${API_BASE_URL}/api/platform/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
}

const platFormService = new PlatFormService();

export default platFormService;
