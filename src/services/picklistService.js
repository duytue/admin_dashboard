import { BASE_URI, API_ORDER_PROCESSING_URL } from '../config';
import axios from 'axios';
import { STATES } from 'src/constants';

class PicklistService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getList = (states, stationFrom) =>
    new Promise((resolve, reject) => {
      console.log('picklistService');
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/picklist`,
        params: {
          size: 100,
          page: 1,
          state: states,
          station_from: stationFrom
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  checkConsolidation = (id, state = 'checked_out') => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/check-consolidation`,
      params: {
        picklist_number: id,
        state,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getOne = (id) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/uploads/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  update = (picklistNumber, data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'PUT',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/picklist/${picklistNumber}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  deletePicklist = (id) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'DELETE',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/uploads/delete`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: { ids: [id] }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(id);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  updatePicklistIsPrinted = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'POST',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/uploads/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  updateByTradeOrderNumber = (tradeOrderNumber, data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'PUT',
        url: `${BASE_URI}${API_ORDER_PROCESSING_URL}/update-from-tradeorder/${tradeOrderNumber}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.message);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
}

const picklistService = new PicklistService();

export default picklistService;
