import { BASE_URI, API_ORDER_PROCESS_URL } from '../config';
import axios from 'axios';

class OrderProcessService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getList = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_ORDER_PROCESS_URL}/uploads`,
        params: {
          size: 100,
          page: 1,
          sort: 'created_at DESC'
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  getOne = (id) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${BASE_URI}${API_ORDER_PROCESS_URL}/uploads/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  update = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'POST',
        url: `${BASE_URI}${API_ORDER_PROCESS_URL}/uploads/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  deleteOrderProcess = (id) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'DELETE',
        url: `${BASE_URI}${API_ORDER_PROCESS_URL}/uploads/delete`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: { ids: [id] }
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(id);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });

  updateOrderProcessIsPrinted = (data) =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'POST',
        url: `${BASE_URI}${API_ORDER_PROCESS_URL}/uploads/update`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then((response) => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
}

const orderProcessService = new OrderProcessService();

export default orderProcessService;
