// import { API_BASE_URL } from '../config';
import axios from 'axios';

const API_BASE_URL = 'https://api-stg-cen.jamalex.net/ms-category'

class CategoryService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListCategory = () =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `https://api-stg-cen.jamalex.net/ms-category/api/categories`,
        params: {
          size: 800,
          page: 1,
        },
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createOneCategory = data =>
    new Promise((resolve, reject) => {
      const dataCreate = JSON.stringify(data);
      var config = {
        method: 'post',
        url: `${API_BASE_URL}/api/category`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        },
        data: dataCreate
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getOneCategory = id =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'get',
        url: `${API_BASE_URL}/api/category/${id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  updateCategory = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'put',
        url: `${API_BASE_URL}/api/category/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  deleteCategory = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'delete',
        url: `${API_BASE_URL}/api/category/${data.id}`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`
        }
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  uploadImage = (presignedPostData, file) =>
    new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(presignedPostData.fields).forEach(key => {
        formData.append(key, presignedPostData.fields[key]);
      });

      // Actual file has to be appended last.
      formData.append("file", file);

      const xhr = new XMLHttpRequest();
      xhr.open("POST", presignedPostData.url, true);
      xhr.send(formData);
      xhr.onload = function (response) {
        this.status === 204 ? resolve('success') : reject(null);
      };
    });

  preUpload = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jamalex.net/ms-media/api/v1/uploads/pre_up`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  createUpload = data =>
    new Promise((resolve, reject) => {
      var config = {
        method: 'post',
        url: `https://api-stg-cen.jx.com.vn/ms-media/api/v1/uploads/create`,
        headers: {
          Authorization: `Bearer ${this.getAccessToken()}`,
          'Content-Type': 'application/json'
        },
        data: data
      };

      axios(config)
        .then(response => {
          if (response.data && response.data.code == 'success') {
            resolve(response.data.data);
          } else {
            reject(response.data.data.error);
          }
        })
        .catch(error => {
          reject(error);
        });
    });

  getListCategoryByLevel = (level, type) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: 'https://api-stg-cen.jamalex.net/ms-category/api/categories',
      params: {
        size: 100,
        page: 1,
        level,
        type
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code == 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const categoryService = new CategoryService();

export default categoryService;
