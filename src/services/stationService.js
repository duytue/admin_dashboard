import axios from 'axios';
import { API_BASE_URL_MSSTATION } from '../config';

class StationService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListStations = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_BASE_URL_MSSTATION}/api/stations`,
      params: {
        size: 100,
        page: 1,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          console.log('list stations response sucess', response.data);
          resolve(response.data.data);
        } else {
          console.log('list stations response reject', response.data);
          reject(response.data.data);
        }
      })
      .catch((error) => {
        console.log('list stations response error', error);
        reject(error);
      });
  });

  createStation = (data) => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data);
    const config = {
      method: 'post',
      url: `${API_BASE_URL_MSSTATION}/api/station`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getStation = (stationId) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_BASE_URL_MSSTATION}/api/station/${stationId}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          console.log('get station response sucess', response.data);
          resolve(response.data.data);
        } else {
          console.log('get station response reject', response.data);
          reject(response.data.data);
        }
      })
      .catch((error) => {
        console.log('get station response error', error);
        reject(error);
      });
  });

  getListStationFunctions = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${API_BASE_URL_MSSTATION}/api/station-function`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 'success') {
          console.log('get station function response sucess', response.data);
          resolve(response.data.data);
        } else {
          console.log('get station function response reject', response.data);
          reject(response.data.data);
        }
      })
      .catch((error) => {
        console.log('get station function response error', error);
        reject(error);
      });
  });

  deleteStation = (stationId) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${API_BASE_URL_MSSTATION}/api/station/${stationId}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 'success') {
          console.log('delete station response sucess', response.data.code);
          resolve(response.data.code);
        } else {
          console.log('delete station response reject', response.data.code);
          reject(response.data.code);
        }
      })
      .catch((error) => {
        console.log('get station response error', error);
        reject(error);
      });
  });
}

const stationService = new StationService();

export default stationService;
