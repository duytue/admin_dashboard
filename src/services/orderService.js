import axios from 'axios';
import { BASE_URI } from '../config';

class OrderService {
  getAccessToken = () => localStorage.getItem('accessToken');

  getListOrders = () => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-order-management/api/orders`,
      params: {
        size: 100,
        page: 1,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  createOrder = (data) => new Promise((resolve, reject) => {
    const dataCreate = JSON.stringify(data);
    const config = {
      method: 'post',
      url: `${BASE_URI}/ms-order-management/api/order`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataCreate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  updateOrder = (data) => new Promise((resolve, reject) => {
    const dataUpdate = JSON.stringify(data);
    const config = {
      method: 'update',
      url: `${BASE_URI}/ms-order-management/api/order/${data.id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      },
      data: dataUpdate
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data.error);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getOrder = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-order-management/api/order/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  deleteOrder = (id) => new Promise((resolve, reject) => {
    const config = {
      method: 'delete',
      url: `${BASE_URI}/ms-order-management/api/order/${id}`,
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data.code === 1) {
          resolve(response.data.code);
        } else {
          reject(response.data.code);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });

  getOrderItems = (tradeOrderId) => new Promise((resolve, reject) => {
    const config = {
      method: 'get',
      url: `${BASE_URI}/ms-order-management/api/order-items`,
      params: {
        size: 10,
        page: 1,
        trade_order_id: tradeOrderId,
      },
      headers: {
        Authorization: `Bearer ${this.getAccessToken()}`
      }
    };

    axios(config)
      .then((response) => {
        if (response.data && response.data.code === 1) {
          resolve(response.data.data);
        } else {
          reject(response.data.data);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

const orderService = new OrderService();

export default orderService;
