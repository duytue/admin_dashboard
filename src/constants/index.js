/* eslint-disable import/prefer-default-export */
export const THEMES = {
  LIGHT: 'LIGHT',
  ONE_DARK: 'ONE_DARK',
  UNICORN: 'UNICORN',
  BLUE_OCEAN: 'BLUE_OCEAN'
};
export const STATES = {
  PICKING: 'picking',
  CHECKED_OUT: 'checked_out',
  NEED_TO_TRANSIT_FC: 'need_to_transit_fc',
  NEED_TO_CONSOLIDATE: 'need_to_consolidate',
  READY_TO_CONSOLIDATE: 'ready_to_consolidate',
  IN_TRANSIT_FC: 'in_transit_fc',
  CONSOLIDATED: 'consolidated'
};
export const BAG_PACKING_MODAL_MESSAGE = {
  READY_TO_CONSOLIDATE: {
    BASKET_NUMBER: 1,
    MESSAGE: 'Chờ gộp kiện ngay'
  },
  NEED_TO_CONSOLIDATE: {
    BASKET_NUMBER: 2,
    MESSAGE: 'Cần lưu tạm chờ gộp kiện'
  },
  NEED_TO_TRANSIT_FC: {
    BASKET_NUMBER: 3,
    MESSAGE: 'Chờ trung chuyển đến nơi gộp kiện'
  }
};
