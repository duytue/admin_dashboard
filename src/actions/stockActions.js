/* eslint-disable no-param-reassign */
import stockService from 'src/services/stockService';
import skuService from 'src/services/skuService';

export const GET_STOCKS_REQUEST = '@stock/get-stocks-request';
export const GET_STOCKS_SUCCESS = '@stock/get-stocks-success';
export const GET_STOCKS_FAILURE = '@stock/get-stocks-failure';

export const GET_STOCK_REQUEST = '@stock/get-stock-request';
export const GET_STOCK_SUCCESS = '@stock/get-stock-success';
export const GET_STOCK_FAILURE = '@stock/get-stock-failure';

export const ADD_STOCK_REQUEST = '@stock/add-stock-request';
export const ADD_STOCK_SUCCESS = '@stock/add-stock-success';
export const ADD_STOCK_FAILURE = '@stock/add-stock-failure';

export const UPDATE_STOCK_REQUEST = '@stock/udpate-stock-request';
export const UPDATE_STOCK_SUCCESS = '@stock/udpate-stock-success';
export const UPDATE_STOCK_FAILURE = '@stock/udpate-stock-failure';

export const DELETE_STOCK_REQUEST = '@stock/delete-stock-request';
export const DELETE_STOCK_SUCCESS = '@stock/delete-stock-success';
export const DELETE_STOCK_FAILURE = '@stock/delete-stock-failure';

export const CLEAR_STOCK = '@stock/clear-stock';

export function getListStocks() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_STOCKS_REQUEST });
      const response = await stockService.getListStocks();
      dispatch({
        type: GET_STOCKS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_STOCKS_FAILURE });
      throw error;
    }
  };
}

export function getStock(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_STOCK_REQUEST });
      const response = await stockService.getStock(id);

      // GET Product attributes
      const res = await skuService.getSkuById(response.sku_id);
      response.item = res.item;

      dispatch({
        type: GET_STOCK_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_STOCK_FAILURE });
      throw error;
    }
  };
}

export function addStock(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_STOCK_REQUEST });
      const response = await stockService.createStock(data);
      dispatch({
        type: ADD_STOCK_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_STOCK_FAILURE });
      throw error;
    }
  };
}

export function updateStock(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_STOCK_REQUEST });
      const response = await stockService.updateStock(data);
      dispatch({
        type: UPDATE_STOCK_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_STOCK_FAILURE });
      throw error;
    }
  };
}

export function deleteStock(stock) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_STOCK_REQUEST });
      if (typeof stock === 'object') {
        await stockService.deleteStock(stock.id);
      } else {
        await stockService.deleteStock(stock);
      }
      dispatch({
        type: DELETE_STOCK_SUCCESS,
        payload: {
          stock
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_STOCK_FAILURE });
      throw error;
    }
  };
}

export function clearStock() {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_STOCK
    });
  };
}
