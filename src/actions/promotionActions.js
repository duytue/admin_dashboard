import promotionService from 'src/services/promotionService';

export const GET_PROMOTIONS_REQUEST = '@promotion/get-promotions-request';
export const GET_PROMOTIONS_SUCCESS = '@promotion/get-promotions-success';
export const GET_PROMOTIONS_FAILURE = '@promotion/get-promotions-failure';

export const GET_PROMOTION_REQUEST = '@promotion/get-promotion-request';
export const GET_PROMOTION_SUCCESS = '@promotion/get-promotion-success';
export const GET_PROMOTION_FAILURE = '@promotion/get-promotion-failure';

export const ADD_PROMOTION_REQUEST = '@promotion/add-promotions-request';
export const ADD_PROMOTION_SUCCESS = '@promotion/add-promotions-success';
export const ADD_PROMOTION_FAILURE = '@promotion/add-promotions-failure';

export const UPDATE_PROMOTION_REQUEST = '@promotion/update-promotions-request';
export const UPDATE_PROMOTION_SUCCESS = '@promotion/update-promotions-success';
export const UPDATE_PROMOTION_FAILURE = '@promotion/update-promotions-failure';

export const DELETE_PROMOTION_REQUEST = '@promotion/delete-promotion-request';
export const DELETE_PROMOTION_SUCCESS = '@promotion/delete-promotion-success';
export const DELETE_PROMOTION_FAILURE = '@promotion/delete-promotion-failure';

export const CLEAR_PROMOTION = '@promotion/clear-promotion';

export function getPromotions() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PROMOTIONS_REQUEST });
      const response = await promotionService.getPromotions();
      dispatch({
        type: GET_PROMOTIONS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PROMOTIONS_FAILURE });
      throw error;
    }
  };
}

export function addPromotion(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_PROMOTION_REQUEST });
      const response = await promotionService.createPromotion(data);
      dispatch({
        type: ADD_PROMOTION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_PROMOTION_FAILURE });
      throw error;
    }
  };
}

export function updatePromotion(data, id) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_PROMOTION_REQUEST });
      const response = await promotionService.updatePromotion(data, id);
      dispatch({
        type: UPDATE_PROMOTION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PROMOTION_FAILURE });
      throw error;
    }
  };
}

export function getPromotionById(promotionId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PROMOTION_REQUEST });
      const response = await promotionService.getPromotionById(promotionId);
      dispatch({
        type: GET_PROMOTION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PROMOTION_FAILURE });
      throw error;
    }
  };
}

export function deletePromotion(promotion) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_PROMOTION_REQUEST });
      if (typeof promotion === 'object') {
        await promotionService.deletePromotion(promotion.id);
      } else {
        await promotionService.deletePromotion(promotion);
      }
      dispatch({
        type: DELETE_PROMOTION_SUCCESS,
        payload: {
          promotion
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_PROMOTION_FAILURE });
      throw error;
    }
  };
}

export function clearPromotion() {
  return (dispatch) => {
    dispatch({
      type: CLEAR_PROMOTION
    });
  };
}
