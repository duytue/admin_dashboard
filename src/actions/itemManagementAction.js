/* eslint-disable max-len */
import _ from 'lodash';
import itemManagementService from 'src/services/itemManagementService';
import skuService from 'src/services/skuService';

export const GET_ITEMS_REQUEST = '@item/get-items-request';
export const GET_ITEMS_SUCCESS = '@item/get-items-success';
export const GET_ITEMS_FAILURE = '@item/get-items-failure';

export const ADD_ITEM_REQUEST = '@item/add-item-request';
export const ADD_ITEM_SUCCESS = '@item/add-item-success';
export const ADD_ITEM_FAILURE = '@item/add-item-failure';

export const UPDATE_ITEM_REQUEST = '@item/update-item-request';
export const UPDATE_ITEM_SUCCESS = '@item/update-item-success';
export const UPDATE_ITEM_FAILURE = '@item/update-item-failure';

export const GET_ITEM_REQUEST = '@item/get-item-request';
export const GET_ITEM_SUCCESS = '@item/get-item-success';
export const GET_ITEM_FAILURE = '@item/get-item-failure';

export const DELETE_ITEM_REQUEST = '@item/delete-item-request';
export const DELETE_ITEM_SUCCESS = '@item/delete-item-success';
export const DELETE_ITEM_FAILURE = '@item/delete-item-failure';

export function getListItems(params = {}) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ITEMS_REQUEST });
      const itemsResponse = await itemManagementService.getItems(params);
      const skuResponse = await skuService.getListSkus();

      const response = _.map(itemsResponse.data, (item) => _.extend(item, { skuItems: [_.find(skuResponse, { item_id: item.id })] }));

      dispatch({
        type: GET_ITEMS_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ITEMS_FAILURE });
      throw error;
    }
  };
}

export function addItem(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_ITEM_REQUEST });
      const response = await itemManagementService.createItem(data);
      dispatch({
        type: ADD_ITEM_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: ADD_ITEM_FAILURE });
      throw error;
    }
  };
}

export function updateItem(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ITEM_REQUEST });
      const response = await itemManagementService.updateItem(data);
      dispatch({
        type: UPDATE_ITEM_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: UPDATE_ITEM_FAILURE });
      throw error;
    }
  };
}

export function getItem(itemId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ITEM_REQUEST });
      const response = await itemManagementService.getItem(itemId);
      dispatch({
        type: GET_ITEM_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ITEM_FAILURE });
      throw error;
    }
  };
}

// export function deleteItem(item) {
//   return async (dispatch) => {
//     try {
//       dispatch({ type: DELETE_ITEM_REQUEST });
//       console.log('deleteItem object', item);
//       if (typeof station === 'object') {
//         await itemManagementService.deleteStation(item.id);
//       } else {
//         await itemManagementService.deleteStation(item);
//       }
//       dispatch({
//         type: DELETE_ITEM_SUCCESS,
//         payload: {
//           item
//         }
//       });
//     } catch (error) {
//       dispatch({ type: DELETE_ITEM_FAILURE });
//       throw error;
//     }
//   };
// }
