import reasonService from '../services/reasonService';
import userService from '../services/userService';
import { UPDATE_USER_FAILURE, UPDATE_USER_REQUEST, UPDATE_USER_SUCCESS } from './userActions';

export const GET_REASON_TYPE_REQUEST = '@reason/get-reason-type-request';
export const GET_REASON_TYPE_SUCCESS = '@reason/get-reason-type-success';
export const GET_REASON_TYPE_FAILURE = '@reason/get-reason-type-failure';

export const GET_REASON_LIST_REQUEST = '@reason/get-reason-list-request';
export const GET_REASON_LIST_SUCCESS = '@reason/get-reason-list-success';
export const GET_REASON_LIST_FAILURE = '@reason/get-reason-list-failure';

export const CREATE_REASON_REQUEST = '@reason/create-reason-request';
export const CREATE_REASON_SUCCESS = '@reason/create-reason-success';
export const CREATE_REASON_FAILURE = '@reason/create-reason-failure';

export const GET_REASON_REQUEST = '@reason/get-reason-request';
export const GET_REASON_SUCCESS = '@reason/get-reason-success';
export const GET_REASON_FAILURE = '@reason/get-reason-failure';

export const UPDATE_REASON_REQUEST = '@reason/update-reason-request';
export const UPDATE_REASON_SUCCESS = '@reason/update-reason-success';
export const UPDATE_REASON_FAILURE = '@reason/update-reason-failure';

export function getReasonType() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REASON_TYPE_REQUEST });
      const response = await reasonService.getReasonType();
      dispatch({
        type: GET_REASON_TYPE_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: GET_REASON_TYPE_FAILURE });
      throw error;
    }
  };
}
export function getListReasons(reasonType = '') {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REASON_LIST_REQUEST });
      const response = await reasonService.getListReasons(reasonType);
      dispatch({
        type: GET_REASON_LIST_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_REASON_LIST_FAILURE });
      throw error;
    }
  };
}

export function createReason(data) {
  return async (dispatch) => {
    try {
      const response = await reasonService.createReason(data);
      dispatch({
        type: CREATE_REASON_REQUEST,
        payload: {
          response
        }
      })
    } catch (error) {
      dispatch({ type: CREATE_REASON_FAILURE });
      throw error;
    }
  };
}

export function getReason(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REASON_REQUEST });
      const response = await reasonService.getReason(id);
      dispatch({
        type: GET_REASON_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_REASON_FAILURE });
      throw error;
    }
  };
}


export function updateReason(data) {
  return async (dispatch) => {
    try {
      const response = await reasonService.updateReason(data);
    } catch (error) {
      throw error;
    }
  };
}

export function deleteReason(id) {
  return async (dispatch) => {
    try {
      const response = await reasonService.deleteReason(id);
    } catch (error) {
      throw error;
    }
  };
}
