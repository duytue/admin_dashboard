import customerService from '../services/customerService';

export const GET_CUSTOMER_REQUEST = '@customer/get-customer-request';
export const GET_CUSTOMER_SUCCESS = '@customer/get-customer-success';
export const GET_CUSTOMER_FAILURE = '@customer/get-customer-failure';

// export const CREATE_CUSTOMER_REQUEST = '@customer/create-customer-request';
// export const CREATE_CUSTOMER_SUCCESS = '@customer/create-customer-success';
// export const CREATE_CUSTOMER_FAILURE = '@customer/create-customer-failure';

export const GET_ONE_CUSTOMER_REQUEST = '@customer/get_one-customer-request';
export const GET_ONE_CUSTOMER_SUCCESS = '@customer/get_one-customer-success';
export const GET_ONE_CUSTOMER_FAILURE = '@customer/get_one-customer-failure';

export const UPDATE_CUSTOMER_REQUEST = '@customer/update-customer-request';
export const UPDATE_CUSTOMER_SUCCESS = '@customer/update-customer-success';
export const UPDATE_CUSTOMER_FAILURE = '@customer/update-customer-failure';

export const CREATE_CUSTOMER_REQUEST = '@customer/create-customer-request';
export const CREATE_CUSTOMER_SUCCESS = '@customer/create-customer-success';
export const CREATE_CUSTOMER_FAILURE = '@customer/create-customer-failure';

export const GET_METADATA_REQUEST = '@customer/get-metadata-request';
export const GET_METADATA_SUCCESS = '@customer/get-metadata-success';
export const GET_METADATA_FAILURE = '@customer/get-metadata-failure';

export const DELETE_CUSTOMER_REQUEST = '@customer/delete-customer-request';
export const DELETE_CUSTOMER_SUCCESS = '@customer/delete-customer-success';
export const DELETE_CUSTOMER_FAILURE = '@customer/delete-customer-failure';

export const DELETE_BULK_CUSTOMER_REQUEST = '@customer/delete_bulk-customer-request';
export const DELETE_BULK_CUSTOMER_SUCCESS = '@customer/delete_bulk-customer-success';
export const DELETE_BULK_CUSTOMER_FAILURE = '@customer/delete_bulk-customer-failure';

export function getListCustomer() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CUSTOMER_REQUEST });
      const response = await customerService.getListCustomer();
      dispatch({
        type: GET_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CUSTOMER_FAILURE });
      throw error;
    }
  };
}
export function createCustomer(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_CUSTOMER_REQUEST });
      const response = await customerService.createCustomer(data);
      dispatch({
        type: CREATE_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_CUSTOMER_FAILURE });
      throw error;
    }
  };
}


export function getOneCustomer(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_CUSTOMER_REQUEST });
      const response = await customerService.getOneCustomer(id);
      dispatch({
        type: GET_ONE_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_CUSTOMER_FAILURE });
      throw error;
    }
  };
}

export function updateCustomer(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_CUSTOMER_REQUEST });
      const response = await customerService.updateCustomer(data);
      dispatch({
        type: UPDATE_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_CUSTOMER_FAILURE });
      throw error;
    }
  };
}
export function deleteOneCustomer(id) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_CUSTOMER_REQUEST });
      const response = await customerService.deleteOneCustomer(id);
      dispatch({
        type: DELETE_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_CUSTOMER_FAILURE });
      throw error;
    }
  };
}

export function deleteBulkCustomer(listCustomer) {
  // supplierService.deleteBulkSupplier(listSupplier);
  return async dispatch => {
    try {
      dispatch({ type: DELETE_BULK_CUSTOMER_REQUEST });
      debugger;
      const response = await customerService.deleteBulkCustomer(listCustomer);
      dispatch({
        type: DELETE_BULK_CUSTOMER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_BULK_CUSTOMER_FAILURE });
      throw error;
    }
  };
}
export function getListMetaData() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_METADATA_REQUEST });
      const response = await customerService.getListMetaData();
      dispatch({
        type: GET_METADATA_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_METADATA_FAILURE });
      throw error;
    }
  };
}
