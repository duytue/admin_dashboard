import categoryService from 'src/services/categoryService';
import { GET_VARIANTS_FAILURE } from './variantActions';

export const GET_CATEGORIES_REQUEST = '@categories/get-categories-request';
export const GET_CATEGORIES_SUCCESS = '@categories/get-categories-success';
export const GET_CATEGORIES_FAILURE = '@categories/get-categories-failure';

export const CREATE_CATEGORY_REQUEST = '@category/create-category-request';
export const CREATE_CATEGORY_SUCCESS = '@category/create-category-success';
export const CREATE_CATEGORY_FAILURE = '@category/create-category-failure';

export const GET_ONE_CATEGORY_REQUEST = '@category/get_one-category-request';
export const GET_ONE_CATEGORY_SUCCESS = '@category/get_one-category-success';
export const GET_ONE_CATEGORY_FAILURE = '@category/get_one-category-failure';

export const UPDATE_CATEGORY_REQUEST = '@category/update-category-request';
export const UPDATE_CATEGORY_SUCCESS = '@category/update-category-success';
export const UPDATE_CATEGORY_FAILURE = '@category/update-category-failure';

export const DELETE_CATEGORY_REQUEST = '@category/delete-category-request';
export const DELETE_CATEGORY_SUCCESS = '@category/delete-category-success';
export const DELETE_CATEGORY_FAILURE = '@category/delete-category-failure';

export const GET_CATEGORIES_LEVEL_REQUEST = '@category/get-level-categories-request';
export const GET_CATEGORIES_LEVEL_SUCCESS = '@category/get-level-categories-success';
export const GET_CATEGORIES_LEVEL_FAILURE = '@category/get-level-categories-failure';

export function getListCategory() {
  return async dispatch => {
    try {
      dispatch({ type: GET_CATEGORIES_REQUEST });
      const response = await categoryService.getListCategory();
      dispatch({
        type: GET_CATEGORIES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CATEGORIES_FAILURE });
      throw error;
    }
  };
}

export function createCategory(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_CATEGORY_REQUEST });
      const response = await categoryService.createOneCategory(data);
      dispatch({
        type: CREATE_CATEGORY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_CATEGORY_FAILURE });
      throw error;
    }
  };
}

export function getOneCategory(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_CATEGORY_REQUEST });
      const response = await categoryService.getOneCategory(data);
      dispatch({
        type: GET_ONE_CATEGORY_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_CATEGORY_FAILURE });
      throw error;
    }
  };
}

export function updateCategory(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_CATEGORY_REQUEST });
      const response = await categoryService.updateCategory(data);
      dispatch({
        type: UPDATE_CATEGORY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_CATEGORY_FAILURE });
      throw error;
    }
  };
}

export function deleteCategory(data) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_CATEGORY_REQUEST });
      const response = await categoryService.deleteCategory(data);
      dispatch({
        type: DELETE_CATEGORY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_CATEGORY_FAILURE });
      throw error;
    }
  };
}

export function getListCategoriesByLevel(level, type) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CATEGORIES_LEVEL_REQUEST });
      const response = await categoryService.getListCategoryByLevel(level, type);
      dispatch({
        type: GET_CATEGORIES_LEVEL_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CATEGORIES_LEVEL_FAILURE });
      throw error;
    }
  };
}
