import customerFeeService from 'src/services/customerFeeService';

export const GET_CUSTOMER_FEES_REQUEST = '@customer-fee/get-customer-fees-request';
export const GET_CUSTOMER_FEES_SUCCESS = '@customer-fee/get-customer-fees-success';
export const GET_CUSTOMER_FEES_FAILURE = '@customer-fee/get-customer-fees-failure';

export const GET_CUSTOMER_FEE_REQUEST = '@customer-fee/get-customer-fee-request';
export const GET_CUSTOMER_FEE_SUCCESS = '@customer-fee/get-customer-fee-success';
export const GET_CUSTOMER_FEE_FAILURE = '@customer-fee/get-customer-fee-failure';

export const ADD_CUSTOMER_FEE_REQUEST = '@customer-fee/add-customer-fees-request';
export const ADD_CUSTOMER_FEE_SUCCESS = '@customer-fee/add-customer-fees-success';
export const ADD_CUSTOMER_FEE_FAILURE = '@customer-fee/add-customer-fees-failure';

export const UPDATE_CUSTOMER_FEE_REQUEST = '@customer-fee/update-customer-fees-request';
export const UPDATE_CUSTOMER_FEE_SUCCESS = '@customer-fee/update-customer-fees-success';
export const UPDATE_CUSTOMER_FEE_FAILURE = '@customer-fee/update-customer-fees-failure';

export const DELETE_CUSTOMER_FEE_REQUEST = '@customer-fee/delete-customer-fee-request';
export const DELETE_CUSTOMER_FEE_SUCCESS = '@customer-fee/delete-customer-fee-success';
export const DELETE_CUSTOMER_FEE_FAILURE = '@customer-fee/delete-customer-fee-failure';

export const CLEAR_CUSTOMER_FEE = '@customer-fee/clear-customer-fee';

export function getCustomerFees() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CUSTOMER_FEES_REQUEST });
      const response = await customerFeeService.getCustomerFees();
      dispatch({
        type: GET_CUSTOMER_FEES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CUSTOMER_FEES_FAILURE });
      throw error;
    }
  };
}

export function addCustomerFee(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_CUSTOMER_FEE_REQUEST });
      const response = await customerFeeService.createCustomerFee(data);
      dispatch({
        type: ADD_CUSTOMER_FEE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_CUSTOMER_FEE_FAILURE });
      throw error;
    }
  };
}

export function updateCustomerFee(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_CUSTOMER_FEE_REQUEST });
      const response = await customerFeeService.updateCustomerFee(data);
      dispatch({
        type: UPDATE_CUSTOMER_FEE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_CUSTOMER_FEE_FAILURE });
      throw error;
    }
  };
}

export function getCustomerFeeById(feeId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CUSTOMER_FEE_REQUEST });
      const response = await customerFeeService.getCustomerFeeById(feeId);
      dispatch({
        type: GET_CUSTOMER_FEE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CUSTOMER_FEE_FAILURE });
      throw error;
    }
  };
}

export function deleteCustomerFee(fee) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_CUSTOMER_FEE_REQUEST });
      if (typeof fee === 'object') {
        await customerFeeService.deleteCustomerFee(fee.id);
      } else {
        await customerFeeService.deleteCustomerFee(fee);
      }
      dispatch({
        type: DELETE_CUSTOMER_FEE_SUCCESS,
        payload: {
          fee
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_CUSTOMER_FEE_FAILURE });
      throw error;
    }
  };
}

export function clearCustomerFee() {
  return (dispatch) => {
    dispatch({
      type: CLEAR_CUSTOMER_FEE,
    });
  };
}
