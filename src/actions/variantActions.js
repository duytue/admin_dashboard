/* eslint-disable no-param-reassign */
import variantService from 'src/services/variantService';

export const GET_VARIANTS_REQUEST = '@variant/get-variants-request';
export const GET_VARIANTS_SUCCESS = '@variant/get-variants-success';
export const GET_VARIANTS_FAILURE = '@variant/get-variants-failure';

export const GET_VARIANT_REQUEST = '@variant/get-variant-request';
export const GET_VARIANT_SUCCESS = '@variant/get-variant-success';
export const GET_VARIANT_FAILURE = '@variant/get-variant-failure';

export const ADD_VARIANT_REQUEST = '@variant/add-variant-request';
export const ADD_VARIANT_SUCCESS = '@variant/add-variant-success';
export const ADD_VARIANT_FAILURE = '@variant/add-variant-failure';

export const UPDATE_VARIANT_REQUEST = '@variant/udpate-variant-request';
export const UPDATE_VARIANT_SUCCESS = '@variant/udpate-variant-success';
export const UPDATE_VARIANT_FAILURE = '@variant/udpate-variant-failure';

export const DELETE_VARIANT_REQUEST = '@variant/delete-variant-request';
export const DELETE_VARIANT_SUCCESS = '@variant/delete-variant-success';
export const DELETE_VARIANT_FAILURE = '@variant/delete-variant-failure';

export const SET_VARIANT = '@variant/set-variant';

export const CLEAR_VARIANT = '@variant/clear-variant';

export function getListVariants() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_VARIANTS_REQUEST });
      const response = await variantService.getListVariants();
      dispatch({
        type: GET_VARIANTS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_VARIANTS_FAILURE });
      throw error;
    }
  };
}

export function getVariant(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_VARIANT_REQUEST });
      const response = await variantService.getVariant(id);

      dispatch({
        type: GET_VARIANT_SUCCESS,
        payload: response,
      });
    } catch (error) {
      dispatch({ type: GET_VARIANT_FAILURE });
      throw error;
    }
  };
}

export function addVariant(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_VARIANT_REQUEST });
      const response = await variantService.createVariant(data);
      dispatch({
        type: ADD_VARIANT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_VARIANT_FAILURE });
      throw error;
    }
  };
}

export function updateVariant(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_VARIANT_REQUEST });
      const response = await variantService.updateVariant(data);
      dispatch({
        type: UPDATE_VARIANT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_VARIANT_FAILURE });
      throw error;
    }
  };
}

export function deleteVariant(variant) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_VARIANT_REQUEST });
      if (typeof variant === 'object') {
        await variantService.deleteVariant(variant.id);
      } else {
        await variantService.deleteVariant(variant);
      }
      dispatch({
        type: DELETE_VARIANT_SUCCESS,
        payload: {
          variant
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_VARIANT_FAILURE });
      throw error;
    }
  };
}

export function clearVariant() {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_VARIANT
    });
  };
}

export function setVariant(variantItem) {
  return async (dispatch) => {
    dispatch({
      type: SET_VARIANT,
      payload: {
        variantItem
      }
    });
  };
}
