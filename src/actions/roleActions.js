import roleService from '../services/roleService';

export const GET_ROLES_REQUEST = '@role/get-roles-request';
export const GET_ROLES_SUCCESS = '@role/get-roles-success';
export const GET_ROLES_FAILURE = '@role/get-roles-failure';

export const CREATE_ROLE_REQUEST = '@role/create-role-request';
export const CREATE_ROLE_SUCCESS = '@role/create-role-success';
export const CREATE_ROLE_FAILURE = '@role/create-role-failure';

export const GET_ONE_ROLE_REQUEST = '@role/get-one-role-request';
export const GET_ONE_ROLE_SUCCESS = '@role/get-one-role-success';
export const GET_ONE_ROLE_FAILURE = '@role/get-one-role-failure';

export const UPDATE_ROLE_REQUEST = '@role/update-role-request';
export const UPDATE_ROLE_SUCCESS = '@role/update-role-success';
export const UPDATE_ROLE_FAILURE = '@role/update-role-failure';

export const UPDATE_PERMISSION_ROLE_REQUEST = '@role/update-role-role-request';
export const UPDATE_PERMISSION_ROLE_SUCCESS = '@role/update-role-role-success';
export const UPDATE_PERMISSION_ROLE_FAILURE = '@role/update-role-role-failure';

export const UPDATE_ACTIVE_ROLE_REQUEST = '@role/update-active-role-request';
export const UPDATE_ACTIVE_ROLE_SUCCESS = '@role/update-active-role-success';
export const UPDATE_ACTIVE_ROLE_FAILURE = '@role/update-active-role-failure';

export const DELETE_ROLE_REQUEST = '@role/delete-role-request';
export const DELETE_ROLE_SUCCESS = '@role/delete-role-success';
export const DELETE_ROLE_FAILURE = '@role/delete-role-failure';

export function getRoles() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ROLES_REQUEST });
      const response = await roleService.getRoles();
      dispatch({
        type: GET_ROLES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ROLES_FAILURE });
      throw error;
    }
  };
}

export function createRole(data) {
  return async (dispatch) => {
    try {
      const response = await roleService.createRole(data);
      dispatch({
        type: CREATE_ROLE_REQUEST,
        payload: {
          response
        }
      })
    } catch (error) {
      dispatch({ type: GET_ONE_ROLE_FAILURE });
      throw error;
    }
  };
}

export function getOneRole(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_ROLE_REQUEST });
      const response = await roleService.getOneRole(id);
      dispatch({
        type: GET_ONE_ROLE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_ROLE_FAILURE });
      throw error;
    }
  };
}

export function updateRole(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ROLE_REQUEST });
      const response = await roleService.updateRole(data);
      dispatch({
        type: UPDATE_ROLE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ROLE_FAILURE });
      throw error;
    }
  };
}

export function updatePermissionOfRole(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_PERMISSION_ROLE_REQUEST });
      const response = await roleService.updatePermissionOfRole(data);
      dispatch({
        type: UPDATE_PERMISSION_ROLE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PERMISSION_ROLE_FAILURE });
      throw error;
    }
  };
}

export function updateActiveRole(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ACTIVE_ROLE_REQUEST });
      const response = await roleService.updateActiveRole(data);
      dispatch({
        type: UPDATE_ACTIVE_ROLE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ACTIVE_ROLE_FAILURE });
      throw error;
    }
  };
}

export function deleteRole(data) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_ROLE_REQUEST });
      const response = await roleService.deleteRole(data);
      dispatch({
        type: DELETE_ROLE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_ROLE_FAILURE });
      throw error;
    }
  };
}
