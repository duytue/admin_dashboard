import stationService from 'src/services/stationService';

export const GET_STATIONS_REQUEST = '@station/get-stations-request';
export const GET_STATIONS_SUCCESS = '@station/get-stations-success';
export const GET_STATIONS_FAILURE = '@station/get-stations-failure';

export const GET_STATION_REQUEST = '@station/get-station-request';
export const GET_STATION_SUCCESS = '@station/get-station-success';
export const GET_STATION_FAILURE = '@station/get-station-failure';

export const ADD_STATION_REQUEST = '@station/add-stations-request';
export const ADD_STATION_SUCCESS = '@station/add-stations-success';
export const ADD_STATION_FAILURE = '@station/add-stations-failure';

export const GET_STATIONFUNCTION_REQUEST = '@station/get-station-function-request';
export const GET_STATIONFUNCTION_SUCCESS = '@station/get-station-function-success';
export const GET_STATIONFUNCTION_FAILURE = '@station/get-station-function-failure';

export const DELETE_STATION_REQUEST = '@station/delete-station-request';
export const DELETE_STATION_SUCCESS = '@station/delete-station-success';
export const DELETE_STATION_FAILURE = '@station/delete-station-failure';

export function getListStations() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_STATIONS_REQUEST });
      const response = await stationService.getListStations();
      dispatch({
        type: GET_STATIONS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_STATIONS_FAILURE });
      throw error;
    }
  };
}

export function addStation(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_STATION_REQUEST });
      const response = await stationService.createStation(data);
      dispatch({
        type: ADD_STATION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_STATION_FAILURE });
      throw error;
    }
  };
}

export function getStation(stationId) {
  console.log('inside getStation action', stationId);
  return async (dispatch) => {
    try {
      dispatch({ type: GET_STATION_REQUEST });
      const response = await stationService.getStation(stationId);
      dispatch({
        type: GET_STATION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_STATION_FAILURE });
      throw error;
    }
  };
}

export function getListStationFunctions() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_STATIONFUNCTION_REQUEST });
      const response = await stationService.getListStationFunctions();
      console.log('action getListStationFunctions', response);
      dispatch({
        type: GET_STATIONFUNCTION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_STATIONFUNCTION_FAILURE });
      throw error;
    }
  };
}

export function deleteStation(station) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_STATION_REQUEST });
      console.log('deleteStation object', station);
      if (typeof station === 'object') {
        await stationService.deleteStation(station.id);
      } else {
        await stationService.deleteStation(station);
      }
      dispatch({
        type: DELETE_STATION_SUCCESS,
        payload: {
          station
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_STATION_FAILURE });
      throw error;
    }
  };
}
