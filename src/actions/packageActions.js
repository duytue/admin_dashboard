import packageService from 'src/services/packageService';
import platformService from '../services/platformService';
import {
  CREATE_PLATFORM_FAILURE,
  CREATE_PLATFORM_REQUEST,
  CREATE_PLATFORM_SUCCESS
} from './platformActions';

export const GET_REQUEST = '@package/get-request';
export const GET_SUCCESS = '@package/get-success';
export const GET_FAILURE = '@package/get-failure';

export const GET_ONE_REQUEST = '@package/get_one-request';
export const GET_ONE_SUCCESS = '@package/get_one-success';
export const GET_ONE_FAILURE = '@package/get_one-failure';

export const CREATE_REQUEST = '@package/create-request';
export const CREATE_SUCCESS = '@package/create-success';
export const CREATE_FAILURE = '@package/create-failure';

export const UPDATE_REQUEST = '@package/update-request';
export const UPDATE_SUCCESS = '@package/update-success';
export const UPDATE_FAILURE = '@package/update-failure';

export const UPDATE_IS_PRINTED_REQUEST = '@package/update-is-printed-request';
export const UPDATE_IS_PRINTED_SUCCESS = '@package/update-is-printed-success';
export const UPDATE_IS_PRINTED_FAILURE = '@package/update-is-printed-failure';

export const UPDATE_ACTIVE_REQUEST = '@package/update-active-request';
export const UPDATE_ACTIVE_SUCCESS = '@package/update-active-success';
export const UPDATE_ACTIVE_FAILURE = '@package/update-active-failure';

export const DELETE_REQUEST = '@package/delete-request';
export const DELETE_SUCCESS = '@package/delete-success';
export const DELETE_FAILURE = '@package/delete-failure';

export function getList() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REQUEST });
      const response = await packageService.getList();
      console.log('getList action', response);
      dispatch({
        type: GET_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_FAILURE });
      throw error;
    }
  };
}

export function getOne(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_REQUEST });
      const response = await packageService.getOne(data);
      dispatch({
        type: GET_ONE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_FAILURE });
      throw error;
    }
  };
}

export function create(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: CREATE_REQUEST });
      const response = await packageService.createUpload(data);
      dispatch({
        type: CREATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_FAILURE });
      throw error;
    }
  };
}

export function update(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_REQUEST });
      const response = await packageService.update(data);
      dispatch({
        type: UPDATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_FAILURE });
      throw error;
    }
  };
}

export function deletePackage(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_REQUEST });
      const response = await packageService.deletePackage(data);
      dispatch({
        type: DELETE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_FAILURE });
      throw error;
    }
  };
}

export function updatePackageIsPrinted(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_IS_PRINTED_REQUEST });
      const response = await packageService.updatePackageIsPrinted(id);
      dispatch({
        type: UPDATE_IS_PRINTED_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_IS_PRINTED_FAILURE });
      throw error;
    }
  };
}
