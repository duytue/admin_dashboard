import picklistService from 'src/services/picklistService';
import platformService from '../services/platformService';
import {
  CREATE_PLATFORM_FAILURE,
  CREATE_PLATFORM_REQUEST,
  CREATE_PLATFORM_SUCCESS
} from './platformActions';

export const GET_REQUEST = '@picklist/get-request';
export const GET_SUCCESS = '@picklist/get-success';
export const GET_FAILURE = '@picklist/get-failure';

export const GET_ONE_REQUEST = '@picklist/get_one-request';
export const GET_ONE_SUCCESS = '@picklist/get_one-success';
export const GET_ONE_FAILURE = '@picklist/get_one-failure';

export const CREATE_REQUEST = '@picklist/create-request';
export const CREATE_SUCCESS = '@picklist/create-success';
export const CREATE_FAILURE = '@picklist/create-failure';

export const UPDATE_REQUEST = '@picklist/update-request';
export const UPDATE_SUCCESS = '@picklist/update-success';
export const UPDATE_FAILURE = '@picklist/update-failure';

export const UPDATE_IS_PRINTED_REQUEST = '@picklist/update-is-printed-request';
export const UPDATE_IS_PRINTED_SUCCESS = '@picklist/update-is-printed-success';
export const UPDATE_IS_PRINTED_FAILURE = '@picklist/update-is-printed-failure';

export const UPDATE_ACTIVE_REQUEST = '@picklist/update-active-request';
export const UPDATE_ACTIVE_SUCCESS = '@picklist/update-active-success';
export const UPDATE_ACTIVE_FAILURE = '@picklist/update-active-failure';

export const DELETE_REQUEST = '@picklist/delete-request';
export const DELETE_SUCCESS = '@picklist/delete-success';
export const DELETE_FAILURE = '@picklist/delete-failure';

export const UPDATE_BY_TRADE_ORDER_REQUEST = '@picklist/update-by-trade-order-request';
export const UPDATE_BY_TRADE_ORDER_SUCCESS = '@picklist/update-by-trade-order-success';
export const UPDATE_BY_TRADE_ORDER_FAILURE = '@picklist/update-by-trade-order-failure';

export const CLEAR_CONSOLIDATION = '@picklist/clear-consolidation';

export const GET_CHECK_CONSOLIDATION_REQUEST =
  '@picklist/get-check-consolidation-request';
export const GET_CHECK_CONSOLIDATION_SUCCESS =
  '@picklist/get-check-consolidation-success';
export const GET_CHECK_CONSOLIDATION_FAILURE =
  '@picklist/get-check-consolidation-failure';

export function getList(states, stationFrom) {
  let statesAtString = "";
  states.forEach(state => {
    statesAtString += state + ",";
  });
  statesAtString = statesAtString.substring(0, statesAtString.length - 1);
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REQUEST });
      const response = await picklistService.getList(statesAtString, stationFrom);
      console.log('getList action', response);
      dispatch({
        type: GET_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_FAILURE });
      throw error;
    }
  };
}

export function checkConsolidation(picklistNumber, states) {
  let statesAtString = "";
  states.forEach(state => {
    statesAtString += state + ",";
  });
  statesAtString = statesAtString.substring(0, statesAtString.length - 1);
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CHECK_CONSOLIDATION_REQUEST });
      const response = await picklistService.checkConsolidation(picklistNumber, statesAtString);

      dispatch({
        type: GET_CHECK_CONSOLIDATION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CHECK_CONSOLIDATION_FAILURE });
      throw error;
    }
  };
}

export function getOne(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_REQUEST });
      const response = await picklistService.getOne(data);
      dispatch({
        type: GET_ONE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_FAILURE });
      throw error;
    }
  };
}

export function create(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: CREATE_REQUEST });
      const response = await picklistService.createUpload(data);
      dispatch({
        type: CREATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_FAILURE });
      throw error;
    }
  };
}

export function update(picklistNumber, data) {
  console.log(picklistNumber);
  console.log(data);
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_REQUEST });
      const response = await picklistService.update(picklistNumber, data);
      dispatch({
        type: UPDATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_FAILURE });
      throw error;
    }
  };
}

export function deletePicklist(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_REQUEST });
      const response = await picklistService.deletePicklist(data);
      dispatch({
        type: DELETE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_FAILURE });
      throw error;
    }
  };
}

export function updatePicklistIsPrinted(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_IS_PRINTED_REQUEST });
      const response = await picklistService.updatePicklistIsPrinted(id);
      dispatch({
        type: UPDATE_IS_PRINTED_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_IS_PRINTED_FAILURE });
      throw error;
    }
  };
}

export function updateByTradeOrderNumber(tradeOrderNumber, data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_BY_TRADE_ORDER_REQUEST });
      const response = await picklistService.updateByTradeOrderNumber(tradeOrderNumber, data);
      dispatch({
        type: UPDATE_BY_TRADE_ORDER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_BY_TRADE_ORDER_FAILURE });
      throw error;
    }
  };
}

export function clearConsolidation() {
  return async (dispatch) => {
    dispatch({ type: CLEAR_CONSOLIDATION });
  }
}