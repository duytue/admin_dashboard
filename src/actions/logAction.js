import logService from 'src/services/logService';

export const GET_LOG_REQUEST = '@log/get-log-request';
export const GET_LOG_SUCCESS = '@log/get-log-success';
export const GET_LOG_FAILURE = '@log/get-log-failure';

export function getListLog() {
  return async dispatch => {
    try {
      dispatch({ type: GET_LOG_REQUEST });
      const response = await logService.getListLog();
      // console.log('getListLog -> response', response);
      dispatch({
        type: GET_LOG_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_LOG_FAILURE });
      throw error;
    }
  };
}
