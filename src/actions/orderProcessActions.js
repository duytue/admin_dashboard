import orderProcessService from 'src/services/orderProcessService';
import platformService from '../services/platformService';
import {
  CREATE_PLATFORM_FAILURE,
  CREATE_PLATFORM_REQUEST,
  CREATE_PLATFORM_SUCCESS
} from './platformActions';

export const GET_REQUEST = '@orderProcess/get-request';
export const GET_SUCCESS = '@orderProcess/get-success';
export const GET_FAILURE = '@orderProcess/get-failure';

export const GET_ONE_REQUEST = '@orderProcess/get_one-request';
export const GET_ONE_SUCCESS = '@orderProcess/get_one-success';
export const GET_ONE_FAILURE = '@orderProcess/get_one-failure';

export const CREATE_REQUEST = '@orderProcess/create-request';
export const CREATE_SUCCESS = '@orderProcess/create-success';
export const CREATE_FAILURE = '@orderProcess/create-failure';

export const UPDATE_REQUEST = '@orderProcess/update-request';
export const UPDATE_SUCCESS = '@orderProcess/update-success';
export const UPDATE_FAILURE = '@orderProcess/update-failure';

export const UPDATE_IS_PRINTED_REQUEST =
  '@orderProcess/update-is-printed-request';
export const UPDATE_IS_PRINTED_SUCCESS =
  '@orderProcess/update-is-printed-success';
export const UPDATE_IS_PRINTED_FAILURE =
  '@orderProcess/update-is-printed-failure';

export const UPDATE_ACTIVE_REQUEST = '@orderProcess/update-active-request';
export const UPDATE_ACTIVE_SUCCESS = '@orderProcess/update-active-success';
export const UPDATE_ACTIVE_FAILURE = '@orderProcess/update-active-failure';

export const DELETE_REQUEST = '@orderProcess/delete-request';
export const DELETE_SUCCESS = '@orderProcess/delete-success';
export const DELETE_FAILURE = '@orderProcess/delete-failure';

export function getList() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_REQUEST });
      const response = await orderProcessService.getList();
      console.log('getList action', response);
      dispatch({
        type: GET_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_FAILURE });
      throw error;
    }
  };
}

export function getOne(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_REQUEST });
      const response = await orderProcessService.getOne(data);
      dispatch({
        type: GET_ONE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_FAILURE });
      throw error;
    }
  };
}

export function create(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: CREATE_REQUEST });
      const response = await orderProcessService.createUpload(data);
      dispatch({
        type: CREATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_FAILURE });
      throw error;
    }
  };
}

export function update(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_REQUEST });
      const response = await orderProcessService.update(data);
      dispatch({
        type: UPDATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_FAILURE });
      throw error;
    }
  };
}

export function deleteOrderProcess(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_REQUEST });
      const response = await orderProcessService.deleteOrderProcess(data);
      dispatch({
        type: DELETE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_FAILURE });
      throw error;
    }
  };
}

export function updateOrderProcessIsPrinted(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_IS_PRINTED_REQUEST });
      const response = await orderProcessService.updateOrderProcessIsPrinted(
        id
      );
      dispatch({
        type: UPDATE_IS_PRINTED_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_IS_PRINTED_FAILURE });
      throw error;
    }
  };
}
