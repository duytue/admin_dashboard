/* eslint-disable no-param-reassign */
import capacityService from 'src/services/capacityTimeSlotService';
import stationService from 'src/services/stationService';

export const GET_CAPACITIES_REQUEST = '@capacity/get-capacities-request';
export const GET_CAPACITIES_SUCCESS = '@capacity/get-capacities-success';
export const GET_CAPACITIES_FAILURE = '@capacity/get-capacities-failure';

export const GET_CAPACITY_REQUEST = '@capacity/get-capacity-request';
export const GET_CAPACITY_SUCCESS = '@capacity/get-capacity-success';
export const GET_CAPACITY_FAILURE = '@capacity/get-capacity-failure';

export const ADD_CAPACITY_REQUEST = '@capacity/add-capacity-request';
export const ADD_CAPACITY_SUCCESS = '@capacity/add-capacity-success';
export const ADD_CAPACITY_FAILURE = '@capacity/add-capacity-failure';

export const UPDATE_CAPACITY_REQUEST = '@capacity/udpate-capacity-request';
export const UPDATE_CAPACITY_SUCCESS = '@capacity/udpate-capacity-success';
export const UPDATE_CAPACITY_FAILURE = '@capacity/udpate-capacity-failure';

export const DELETE_CAPACITY_REQUEST = '@capacity/delete-capacity-request';
export const DELETE_CAPACITY_SUCCESS = '@capacity/delete-capacity-success';
export const DELETE_CAPACITY_FAILURE = '@capacity/delete-capacity-failure';

export const CLEAR_CAPACITY = '@capacity/clear-capacity';

const getStationName = async (stationId) => {
  const stations = await stationService.getListStations();
  const station = stations.find((st) => st.id === stationId);
  return station ? station.name : '';
};

export function getListCapacities() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CAPACITIES_REQUEST });
      const response = await capacityService.getListCapacities();

      await Promise.all(response.map(async (item) => {
        const name = await getStationName(item.station_id);
        item.station_name = name;
        return item;
      }));

      dispatch({
        type: GET_CAPACITIES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CAPACITIES_FAILURE });
      throw error;
    }
  };
}

export function getCapacity(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CAPACITY_REQUEST });
      const response = await capacityService.getCapacity(id);

      const stName = await getStationName(response.station_id);
      response.station_name = stName;

      dispatch({
        type: GET_CAPACITY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CAPACITY_FAILURE });
      throw error;
    }
  };
}

export function addCapacity(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_CAPACITY_REQUEST });
      const response = await capacityService.createCapacity(data);

      const stName = await getStationName(response.station_id);
      response.station_name = stName;

      dispatch({
        type: ADD_CAPACITY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_CAPACITY_FAILURE });
      throw error;
    }
  };
}

export function updateCapacity(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_CAPACITY_REQUEST });
      await capacityService.updateCapacity(data);

      dispatch({
        type: UPDATE_CAPACITY_SUCCESS,
        payload: {
          response: data
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_CAPACITY_FAILURE });
      throw error;
    }
  };
}

export function deleteCapacity(capacity) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_CAPACITY_REQUEST });
      if (typeof capacity === 'object') {
        await capacityService.deleteCapacity(capacity.id);
      } else {
        await capacityService.deleteCapacity(capacity);
      }
      dispatch({
        type: DELETE_CAPACITY_SUCCESS,
        payload: {
          capacity
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_CAPACITY_FAILURE });
      throw error;
    }
  };
}

export function clearCapacity() {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_CAPACITY
    });
  };
}
