import userService from '../services/userService';

export const GET_UI_CONFIG_REQUEST = '@user/get-ui-config-request';
export const GET_UI_CONFIG_SUCCESS = '@user/get-ui-config-success';
export const GET_UI_CONFIG_FAILURE = '@user/get-ui-config-failure';

// export function getUsers() {
//   return async dispatch => {
//     try {
//       dispatch({ type: GET_UI_CONFIG_REQUEST });
//       const response = await userService.getUsers();
//       dispatch({
//         type: GET_UI_CONFIG_SUCCESS,
//         payload: {
//           response
//         }
//       });
//     } catch (error) {
//       dispatch({ type: GET_UI_CONFIG_FAILURE });
//       throw error;
//     }
//   };
// }
