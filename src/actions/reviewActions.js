import reviewService from 'src/services/reviewService';

export const GET_REVIEWS_REQUEST = '@review/get-reviews-request';
export const GET_REVIEWS_SUCCESS = '@review/get-reviews-success';
export const GET_REVIEWS_FAILURE = '@review/get-reviews-failure';

export const GET_ONE_REVIEW_REQUEST = '@review/get_one-review-request';
export const GET_ONE_REVIEW_SUCCESS = '@review/get_one-review-success';
export const GET_ONE_REVIEW_FAILURE = '@review/get_one-review-failure';

export const UPDATE_REVIEW_REQUEST = '@review/update-review-request';
export const UPDATE_REVIEW_SUCCESS = '@review/update-review-success';
export const UPDATE_REVIEW_FAILURE = '@review/update-review-failure';

export const UPDATE_ACTIVE_REVIEW_REQUEST = '@review/update-active-review-request';
export const UPDATE_ACTIVE_REVIEW_SUCCESS = '@review/update-active-review-success';
export const UPDATE_ACTIVE_REVIEW_FAILURE = '@review/update-active-review-failure';

export const DELETE_REVIEW_REQUEST = '@review/delete-review-request';
export const DELETE_REVIEW_SUCCESS = '@review/delete-review-success';
export const DELETE_REVIEW_FAILURE = '@review/delete-review-failure';

export function getList() {
  return async dispatch => {
    try {
      dispatch({ type: GET_REVIEWS_REQUEST });
      const response = await reviewService.getList();
      console.log('getList action', response);
      dispatch({
        type: GET_REVIEWS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_REVIEWS_FAILURE });
      throw error;
    }
  };
}

export function getOne(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_REVIEW_REQUEST });
      const response = await reviewService.getOne(data);
      dispatch({
        type: GET_ONE_REVIEW_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_REVIEW_FAILURE });
      throw error;
    }
  };
}

export function update(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_REVIEW_REQUEST });
      const response = await reviewService.update(data);
      dispatch({
        type: UPDATE_REVIEW_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_REVIEW_FAILURE });
      throw error;
    }
  };
}

export function deleteComment(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_REVIEW_REQUEST });
      const response = await reviewService.deleteComment(data);
      dispatch({
        type: DELETE_REVIEW_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_REVIEW_FAILURE });
      throw error;
    }
  };
}

