import mediaService from 'src/services/mediaService';
import platformService from '../services/platformService';
import { CREATE_PLATFORM_FAILURE, CREATE_PLATFORM_REQUEST, CREATE_PLATFORM_SUCCESS } from './platformActions';

export const GET_REQUEST = '@media/get-request';
export const GET_SUCCESS = '@media/get-success';
export const GET_FAILURE = '@media/get-failure';

export const GET_ONE_REQUEST = '@media/get_one-request';
export const GET_ONE_SUCCESS = '@media/get_one-success';
export const GET_ONE_FAILURE = '@media/get_one-failure';

export const CREATE_REQUEST = '@media/create-request';
export const CREATE_SUCCESS = '@media/create-success';
export const CREATE_FAILURE = '@media/create-failure';

export const UPDATE_REQUEST = '@media/update-request';
export const UPDATE_SUCCESS = '@media/update-success';
export const UPDATE_FAILURE = '@media/update-failure';

export const UPDATE_ACTIVE_REQUEST = '@media/update-active-request';
export const UPDATE_ACTIVE_SUCCESS = '@media/update-active-success';
export const UPDATE_ACTIVE_FAILURE = '@media/update-active-failure';

export const DELETE_REQUEST = '@media/delete-request';
export const DELETE_SUCCESS = '@media/delete-success';
export const DELETE_FAILURE = '@media/delete-failure';

export function getList() {
  return async dispatch => {
    try {
      dispatch({ type: GET_REQUEST });
      const response = await mediaService.getList();
      console.log('getList action', response);
      dispatch({
        type: GET_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_FAILURE });
      throw error;
    }
  };
}

export function getOne(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_REQUEST });
      const response = await mediaService.getOne(data);
      dispatch({
        type: GET_ONE_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_FAILURE });
      throw error;
    }
  };
}

export function create(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_REQUEST });
      const response = await mediaService.createUpload(data);
      dispatch({
        type: CREATE_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: CREATE_FAILURE });
      throw error;
    }
  };
}

export function update(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_REQUEST });
      const response = await mediaService.update(data);
      dispatch({
        type: UPDATE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_FAILURE });
      throw error;
    }
  };
}

export function deleteMedia(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_REQUEST });
      const response = await mediaService.deleteMedia(data);
      dispatch({
        type: DELETE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_FAILURE });
      throw error;
    }
  };
}

