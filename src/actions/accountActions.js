import axios from 'src/utils/axios';
import authService from 'src/services/authService';
import centralUiConfigService from 'src/services/centralUiConfigService';
import { getListPermissions } from './permissionActions';
import { GET_UI_CONFIG_SUCCESS, GET_UI_CONFIG_FAILURE } from './centralUIConfigActions'
export const LOGIN_REQUEST = '@account/login-request';
export const LOGIN_SUCCESS = '@account/login-success';
export const LOGIN_FAILURE = '@account/login-failure';
export const SILENT_LOGIN = '@account/silent-login';
export const LOGOUT = '@account/logout';
export const REGISTER = '@account/register';
export const UPDATE_PROFILE = '@account/update-profile';

export function login(email, password) {
  return async dispatch => {
    try {
      dispatch({ type: LOGIN_REQUEST });
      const loginResult = await authService.loginWithEmailAndPassword(
        email,
        password
      );
      if (loginResult) {
        const list_ui_config = await centralUiConfigService.getListUserConfig(
          loginResult.permission_ids
        );

        if(list_ui_config){
          dispatch({
            type: GET_UI_CONFIG_SUCCESS,
            payload: {
              list_ui_config,
            }
          });
        }

        const user = await authService.loginInWithToken();

        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            user
          }
        });
      }
    } catch (error) {
      dispatch({ type: LOGIN_FAILURE });
      throw error;
    }
  };
}

export function setUserData(user) {
  return dispatch =>
    dispatch({
      type: SILENT_LOGIN,
      payload: {
        user
      }
    });
}

export function logout() {
  return async dispatch => {
    authService.logout();

    dispatch({
      type: LOGOUT
    });
  };
}

export function register() {
  return true;
}

export function updateProfile(update) {
  const request = axios.post('/api/account/profile', { update });

  return dispatch => {
    request.then(response =>
      dispatch({
        type: UPDATE_PROFILE,
        payload: response.data
      })
    );
  };
}
