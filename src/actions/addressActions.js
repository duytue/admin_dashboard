import addressService from 'src/services/addressService';
import _ from 'lodash';

export const GET_ADDRESSES_REQUEST = '@address/get-addresses-request';
export const GET_ADDRESSES_SUCCESS = '@address/get-addresses-success';
export const GET_ADDRESSES_FAILURE = '@address/get-addresses-failure';

export const GET_ADDRESSES_BY_LVL_1_REQUEST = '@address/get-addresses-by-lvl-1-request';
export const GET_ADDRESSES_BY_LVL_1_SUCCESS = '@address/get-addresses-by-lvl-1-success';
export const GET_ADDRESSES_BY_LVL_1_FAILURE = '@address/get-addresses-by-lvl-1-failure';

export const GET_ADDRESSES_BY_LVL_2_REQUEST = '@address/get-addresses-by-lvl-2-request';
export const GET_ADDRESSES_BY_LVL_2_SUCCESS = '@address/get-addresses-by-lvl-2-success';
export const GET_ADDRESSES_BY_LVL_2_FAILURE = '@address/get-addresses-by-lvl-2-failure';

export const CREATE_ADDRESS_REQUEST = '@address/create-address-request';
export const CREATE_ADDRESS_SUCCESS = '@address/create-address-success';
export const CREATE_ADDRESS_FAILURE = '@address/create-address-failure';

export const GET_ONE_ADDRESS_REQUEST = '@address/get_one-address-request';
export const GET_ONE_ADDRESS_SUCCESS = '@address/get_one-address-success';
export const GET_ONE_ADDRESS_FAILURE = '@address/get_one-address-failure';

export const UPDATE_ADDRESS_REQUEST = '@address/update-address-request';
export const UPDATE_ADDRESS_SUCCESS = '@address/update-address-success';
export const UPDATE_ADDRESS_FAILURE = '@address/update-address-failure';

export const GET_CITIES_REQUEST = '@address/get-cities-request';
export const GET_CITIES_SUCCESS = '@address/get-cities-success';
export const GET_CITIES_FAILURE = '@address/get-cities-failure';

export const GET_WARDS_BY_DISTRICT_REQUEST = '@address/get-cities-request';
export const GET_WARDS_BY_DISTRICT_SUCCESS = '@address/get-cities-succcess';
export const GET_WARDS_BY_DISTRICT_FAILURE = '@address/get-cities-failure';

export const GET_DISTRICT_BY_CITY_REQUEST = '@address/get-cities-request';
export const GET_DISTRICT_BY_CITY_SUCCESS = '@address/get-cities-success';
export const GET_DISTRICT_BY_CITY_FAILURE = '@address/get-cities-failure';

export function getListAddress() {
  return async dispatch => {
    try {
      dispatch({ type: GET_ADDRESSES_REQUEST });
      const response = await addressService.getListAddress();
      dispatch({
        type: GET_ADDRESSES_SUCCESS,
        payload: {
          response,
        },
      });
    } catch (error) {
      dispatch({ type: GET_ADDRESSES_FAILURE });
      throw error;
    }
  };
}

export function getLocationByLvl_1(params) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ADDRESSES_BY_LVL_1_REQUEST });
      const response = await addressService.getLocationByLvl(params);
      dispatch({
        type: GET_ADDRESSES_BY_LVL_1_SUCCESS,
        payload: {
          response,
          params,
        },
      });
    } catch (error) {
      dispatch({ type: GET_ADDRESSES_BY_LVL_1_FAILURE });
      throw error;
    }
  };
}

export function getLocationByLvl_2(params) {
  const omitParamsService = _.omit(params, 'short_id_lvl1');
  return async dispatch => {
    try {
      dispatch({ type: GET_ADDRESSES_BY_LVL_2_REQUEST });
      const response = await addressService.getLocationByLvl(omitParamsService);
      dispatch({
        type: GET_ADDRESSES_BY_LVL_2_SUCCESS,
        payload: {
          response,
          params,
        },
      });
    } catch (error) {
      dispatch({ type: GET_ADDRESSES_BY_LVL_2_FAILURE });
      throw error;
    }
  };
}

export function createAddress(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_ADDRESS_REQUEST });
      const response = await addressService.createOneAddress(data);
      dispatch({
        type: CREATE_ADDRESS_SUCCESS,
        payload: {
          response,
        },
      });
    } catch (error) {
      dispatch({ type: CREATE_ADDRESS_FAILURE });
      throw error;
    }
  };
}

export function getOneAddress(params) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_ADDRESS_REQUEST });
      dispatch({
        type: GET_ONE_ADDRESS_SUCCESS,
        payload: {
          params,
        },
      });
    } catch (error) {
      dispatch({ type: GET_ONE_ADDRESS_FAILURE });
      throw error;
    }
  };
}

export function updateAddress(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_ADDRESS_REQUEST });
      const response = await addressService.updateAddress(data);
      dispatch({
        type: UPDATE_ADDRESS_SUCCESS,
        payload: {
          response,
        },
      });
    } catch (error) {
      dispatch({ type: UPDATE_ADDRESS_FAILURE });
      throw error;
    }
  };
}

export function getListCities() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_CITIES_REQUEST });
      const response = await addressService.getListCities();
      console.log('getListCity action', response);
      dispatch({
        type: GET_CITIES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_CITIES_FAILURE });
      throw error;
    }
  };
}

export function getWardsByDistrictId(districtId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_WARDS_BY_DISTRICT_REQUEST });
      const response = await addressService.getWardsByDistrictId(districtId);
      console.log('GET_WARDS_BY_DISTRICT_REQUEST action', response);
      dispatch({
        type: GET_WARDS_BY_DISTRICT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_WARDS_BY_DISTRICT_FAILURE });
      throw error;
    }
  };
}

export function getDistrictByCityId(cityId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_DISTRICT_BY_CITY_REQUEST });
      const response = await addressService.getDistrictByCityId(cityId);
      console.log('GET_WARDS_BY_DISTRICT_REQUEST action', response);
      dispatch({
        type: GET_DISTRICT_BY_CITY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_DISTRICT_BY_CITY_FAILURE });
      throw error;
    }
  };
}
