import itemAttributeService from 'src/services/itemAttributeService';

export const GET_ITEM_ATTRIBUTES_REQUEST = '@item/get-item-attributes-request';
export const GET_ITEM_ATTRIBUTES_SUCCESS = '@item/get-item-attributes-success';
export const GET_ITEM_ATTRIBUTES_FAILURE = '@item/get-item-attributes-failure';

export const ADD_ITEM_ATTRIBUTE_REQUEST = '@item/add-item-attribute-request';
export const ADD_ITEM_ATTRIBUTE_SUCCESS = '@item/add-item-attribute-success';
export const ADD_ITEM_ATTRIBUTE_FAILURE = '@item/add-item-attribute-failure';

export const GET_ITEM_ATTRIBUTE_REQUEST = '@item/get-item-attribute-request';
export const GET_ITEM_ATTRIBUTE_SUCCESS = '@item/get-item-attribute-success';
export const GET_ITEM_ATTRIBUTE_FAILURE = '@item/get-item-attribute-failure';
export const CREATE_ITEM_ATTRIBUTE_STEP1 = '@item/create-item-attribute-step1';
export const CREATE_ITEM_ATTRIBUTE_STEP2 = '@item/create-item-attribute-step2';

export function geItemAttributes(params) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ITEM_ATTRIBUTES_REQUEST, payload: params });
      const response = await itemAttributeService.getAttributes(params);

      dispatch({
        type: GET_ITEM_ATTRIBUTES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ITEM_ATTRIBUTES_FAILURE });
      throw error;
    }
  };
}

export function addItemAttribute(data) {
  return async dispatch => {
    try {
      dispatch({ type: ADD_ITEM_ATTRIBUTE_REQUEST });
      const response = await itemAttributeService.createItemAttribute(data);
      dispatch({
        type: ADD_ITEM_ATTRIBUTE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_ITEM_ATTRIBUTE_FAILURE });
      throw error;
    }
  };
}

export function createAtributeStep1(data) {
  return dispatch => {
    dispatch({ type: CREATE_ITEM_ATTRIBUTE_STEP1, payload: data });
  };
}

export function createAtributeStep2(data) {
  return dispatch => {
    dispatch({ type: CREATE_ITEM_ATTRIBUTE_STEP2, payload: data });
  };
}

export function getItemAttribute(id) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ITEM_ATTRIBUTE_REQUEST });
      const response = await itemAttributeService.getItemAttribute(id);
      dispatch({
        type: GET_ITEM_ATTRIBUTE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ITEM_ATTRIBUTE_FAILURE });
      throw error;
    }
  };
}
