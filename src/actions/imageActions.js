import imageService from 'src/services/imageService';

export const UPLOAD_IMAGE_REQUEST = '@images/upload-image-request';
export const UPLOAD_IMAGE_SUCCESS = '@images/upload-image-success';
export const UPLOAD_IMAGE_FAILURE = '@images/upload-image-failure';

export function uploadImage(data) {
    return async dispatch => {
      try {
        dispatch({ type: UPLOAD_IMAGE_REQUEST });
        const response = await imageService.uploadImage(data);
        dispatch({
          type: UPLOAD_IMAGE_SUCCESS,
          payload: {
            response
          }
        });
      } catch (error) {
        dispatch({ type: UPLOAD_IMAGE_FAILURE });
        throw error;
      }
    };
  }