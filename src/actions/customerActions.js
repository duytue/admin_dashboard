import customerService from 'src/services/customerService';


export const GET_LISTS_REQUEST = '@customer/get_list-request';
export const GET_LISTS_SUCCESS = '@customer/get_list-success';
export const GET_LISTS_FAILURE = '@customer/get_list-failure';

export const GET_ONE_REQUEST = '@customer/get_one-request';
export const GET_ONE_SUCCESS = '@customer/get_one-success';
export const GET_ONE_FAILURE = '@customer/get_one-failure';

export function getListCustomer() {
  return async dispatch => {
    try {
      dispatch({ type: GET_LISTS_REQUEST });
      const response = await customerService.getListCustomer();
      dispatch({
        type: GET_LISTS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_LISTS_FAILURE });
      throw error;
    }
  };
}

export function getOneCustomer(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_REQUEST });
      const response = await customerService.getOneCustomer(data);
      dispatch({
        type: GET_ONE_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_FAILURE });
      throw error;
    }
  };
}

