import languageService from '../services/languageService';

export const GET_LANGUAGES_REQUEST = '@language/get-languages-request';
export const GET_LANGUAGES_SUCCESS = '@language/get-languages-success';
export const GET_LANGUAGES_FAILURE = '@language/get-languages-failure';

export const CREATE_LANGUAGE_REQUEST = '@language/create-language-request';
export const CREATE_LANGUAGE_SUCCESS = '@language/create-language-success';
export const CREATE_LANGUAGE_FAILURE = '@language/create-language-failure';

export const GET_ONE_LANGUAGE_REQUEST = '@language/get-one-language-request';
export const GET_ONE_LANGUAGE_SUCCESS = '@language/get-one-language-success';
export const GET_ONE_LANGUAGE_FAILURE = '@language/get-one-language-failure';

export const UPDATE_LANGUAGE_REQUEST = '@language/update-language-request';
export const UPDATE_LANGUAGE_SUCCESS = '@language/update-language-success';
export const UPDATE_LANGUAGE_FAILURE = '@language/update-language-failure';


export const DELETE_LANGUAGE_REQUEST = '@language/delete-language-request';
export const DELETE_LANGUAGE_SUCCESS = '@language/delete-language-success';
export const DELETE_LANGUAGE_FAILURE = '@language/delete-language-failure';

export function getLanguages() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_LANGUAGES_REQUEST });
      const response = await languageService.getLanguages();
      dispatch({
        type: GET_LANGUAGES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_LANGUAGES_FAILURE });
      throw error;
    }
  };
}
  
export function createLanguage(data) {
  return async (dispatch) => {
    try {
      const response = await languageService.createLanguage(data);
      dispatch({
        type: CREATE_LANGUAGE_REQUEST,
        payload: {
          response
        }
      })
    } catch (error) {
      dispatch({ type: GET_ONE_LANGUAGE_FAILURE });
      throw error;
    }
  };
}
  
export function getLanguage(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_LANGUAGE_REQUEST });
      const response = await languageService.getOneLanguage(id);
      dispatch({
        type: GET_ONE_LANGUAGE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_LANGUAGE_FAILURE });
      throw error;
    }
  };
}
  
export function updateLanguage(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_LANGUAGE_REQUEST });
      const response = await languageService.updateLanguage(data);
      dispatch({
        type: UPDATE_LANGUAGE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_LANGUAGE_FAILURE });
      throw error;
    }
  };
}

export function deleteLanguage(id) {
  return async (dispatch) => {
    try {
      // eslint-disable-next-line no-undef
      dispatch({ type: DELETE_LANGUAGE_REQUEST });
      const response = await languageService.deleteLanguage(id);

      dispatch({ type: GET_LANGUAGES_REQUEST });
      dispatch({
        type: DELETE_LANGUAGE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      console.log(error, 'error')
      dispatch({ type: DELETE_LANGUAGE_FAILURE });
      throw error;
    }
  };
}
