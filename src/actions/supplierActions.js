import supplierService from 'src/services/supplierService';

export const GET_SUPPLIERS_REQUEST = '@supplier/get-suppliers-request';
export const GET_SUPPLIERS_SUCCESS = '@supplier/get-suppliers-success';
export const GET_SUPPLIERS_FAILURE = '@supplier/get-suppliers-failure';

export const CREATE_SUPPLIER_REQUEST = '@supplier/create-supplier-request';
export const CREATE_SUPPLIER_SUCCESS = '@supplier/create-supplier-success';
export const CREATE_SUPPLIER_FAILURE = '@supplier/create-supplier-failure';

export const GET_ONE_SUPPLIER_REQUEST = '@supplier/get_one-supplier-request';
export const GET_ONE_SUPPLIER_SUCCESS = '@supplier/get_one-supplier-success';
export const GET_ONE_SUPPLIER_FAILURE = '@supplier/get_one-supplier-failure';

export const UPDATE_SUPPLIER_REQUEST = '@supplier/update-supplier-request';
export const UPDATE_SUPPLIER_SUCCESS = '@supplier/update-supplier-success';
export const UPDATE_SUPPLIER_FAILURE = '@supplier/update-supplier-failure';

export const DELETE_SUPPLIER_REQUEST = '@supplier/delete-supplier-request';
export const DELETE_SUPPLIER_SUCCESS = '@supplier/delete-supplier-success';
export const DELETE_SUPPLIER_FAILURE = '@supplier/delete-supplier-failure';

export const DELETE_BULK_SUPPLIER_REQUEST = '@supplier/delete_bulk-supplier-request';
export const DELETE_BULK_SUPPLIER_SUCCESS = '@supplier/delete_bulk-supplier-success';
export const DELETE_BULK_SUPPLIER_FAILURE = '@supplier/delete_bulk-supplier-failure';

export function getListSupplier() {
  return async dispatch => {
    try {
      dispatch({ type: GET_SUPPLIERS_REQUEST });
      const response = await supplierService.getListSupplier();
      console.log('action', response)
      dispatch({
        type: GET_SUPPLIERS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_SUPPLIERS_FAILURE });
      throw error;
    }
  };
}

export function createSupplier(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_SUPPLIER_REQUEST });
      const response = await supplierService.createOneSupplier(data);
      dispatch({
        type: CREATE_SUPPLIER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_SUPPLIER_FAILURE });
      throw error;
    }
  };
}

export function getOneSupplier(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_SUPPLIER_REQUEST });
      const response = await supplierService.getOneSupplier(data);
      dispatch({
        type: GET_ONE_SUPPLIER_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_SUPPLIER_FAILURE });
      throw error;
    }
  };
}

export function updateSupplier(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_SUPPLIER_REQUEST });
      const response = await supplierService.updateSupplier(data);
      dispatch({
        type: UPDATE_SUPPLIER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_SUPPLIER_FAILURE });
      throw error;
    }
  };
}


export function deleteOneSupplier(id) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_SUPPLIER_REQUEST });
      const response = await supplierService.deleteOneSupplier(id);
      dispatch({
        type: DELETE_SUPPLIER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_SUPPLIER_FAILURE });
      throw error;
    }
  };
}

export function deleteBulkSupplier(listSupplier) {
  // supplierService.deleteBulkSupplier(listSupplier);
  return async dispatch => {
    try {
      dispatch({ type: DELETE_BULK_SUPPLIER_REQUEST });
      debugger;
      const response = await supplierService.deleteBulkSupplier(listSupplier);
      dispatch({
        type: DELETE_BULK_SUPPLIER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_BULK_SUPPLIER_FAILURE });
      throw error;
    }
  };
}
