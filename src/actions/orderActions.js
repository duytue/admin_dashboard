/* eslint-disable no-param-reassign */
import orderService from 'src/services/orderService';
import skuService from 'src/services/skuService';

export const GET_ORDERS_REQUEST = '@order/get-orders-request';
export const GET_ORDERS_SUCCESS = '@order/get-orders-success';
export const GET_ORDERS_FAILURE = '@order/get-orders-failure';

export const GET_ORDER_REQUEST = '@order/get-order-request';
export const GET_ORDER_SUCCESS = '@order/get-order-success';
export const GET_ORDER_FAILURE = '@order/get-order-failure';

export const ADD_ORDER_REQUEST = '@order/add-order-request';
export const ADD_ORDER_SUCCESS = '@order/add-order-success';
export const ADD_ORDER_FAILURE = '@order/add-order-failure';

export const UPDATE_ORDER_REQUEST = '@order/udpate-order-request';
export const UPDATE_ORDER_SUCCESS = '@order/udpate-order-success';
export const UPDATE_ORDER_FAILURE = '@order/udpate-order-failure';

export const DELETE_ORDER_REQUEST = '@order/delete-order-request';
export const DELETE_ORDER_SUCCESS = '@order/delete-order-success';
export const DELETE_ORDER_FAILURE = '@order/delete-order-failure';

export const GET_ORDER_ITEMS_REQUEST = '@order/get-order-items-request';
export const GET_ORDER_ITEMS_SUCCESS = '@order/get-order-items-success';
export const GET_ORDER_ITEMS_FAILURE = '@order/get-order-items-failure';

export function getListOrders() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ORDERS_REQUEST });
      const response = await orderService.getListOrders();
      dispatch({
        type: GET_ORDERS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ORDERS_FAILURE });
      throw error;
    }
  };
}

export function getOrder(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ORDER_REQUEST });
      const response = await orderService.getOrder(id);
      dispatch({
        type: GET_ORDER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ORDER_FAILURE });
      throw error;
    }
  };
}

export function addOrder(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_ORDER_REQUEST });
      const response = await orderService.createStation(data);
      dispatch({
        type: ADD_ORDER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_ORDER_FAILURE });
      throw error;
    }
  };
}

export function updateOrder(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ORDER_REQUEST });
      const response = await orderService.updateOrder(data);
      dispatch({
        type: UPDATE_ORDER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ORDER_FAILURE });
      throw error;
    }
  };
}

export function deleteOrder(order) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_ORDER_REQUEST });
      if (typeof order === 'object') {
        await orderService.deleteOrder(order.id);
      } else {
        await orderService.deleteOrder(order);
      }
      dispatch({
        type: DELETE_ORDER_SUCCESS,
        payload: {
          order
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_ORDER_FAILURE });
      throw error;
    }
  };
}

export function getOrderItems(tradeOrderId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ORDER_ITEMS_REQUEST });
      const response = await orderService.getOrderItems(tradeOrderId);

      const retData = [];
      await Promise.all(response.map(async (item) => {
        const { sku } = item;

        const res = await skuService.getSkuById(sku);
        item.image = res.length > 0 && res[0].images[0];
        item.prod_name = res.length > 0 && res[0].item.names.en;
        item.item_sku = res.length > 0 && res[0].sku;
        retData.push(item);
      }));

      dispatch({
        type: GET_ORDER_ITEMS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ORDER_ITEMS_FAILURE });
      throw error;
    }
  };
}
