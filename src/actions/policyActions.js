import policyService from 'src/services/policyService';

export const GET_POLICIES_REQUEST = '@policy/get-policies-request';
export const GET_POLICIES_SUCCESS = '@policy/get-policies-success';
export const GET_POLICIES_FAILURE = '@policy/get-policies-failure';

export const CREATE_POLICY_REQUEST = '@policy/create-policy-request';
export const CREATE_POLICY_SUCCESS = '@policy/create-policy-success';
export const CREATE_POLICY_FAILURE = '@policy/create-policy-failure';

export const GET_ONE_POLICY_REQUEST = '@policy/get_one-policy-request';
export const GET_ONE_POLICY_SUCCESS = '@policy/get_one-policy-success';
export const GET_ONE_POLICY_FAILURE = '@policy/get_one-policy-failure';

export const UPDATE_POLICY_REQUEST = '@policy/update-policy-request';
export const UPDATE_POLICY_SUCCESS = '@policy/update-policy-success';
export const UPDATE_POLICY_FAILURE = '@policy/update-policy-failure';

export const DELETE_POLICY_REQUEST = '@policy/delete-policy-request';
export const DELETE_POLICY_SUCCESS = '@policy/delete-policy-success';
export const DELETE_POLICY_FAILURE = '@policy/delete-policy-failure';

export const GET_POLICY_TYPES_REQUEST = '@policy/get-policy-types-request';
export const GET_POLICY_TYPES_SUCCESS = '@policy/get-policy-types-success';
export const GET_POLICY_TYPES_FAILURE = '@policy/get-policy-types-failure';

export const CREATE_POLICY_TYPE_REQUEST = '@policy/create-policy-type-request';
export const CREATE_POLICY_TYPE_SUCCESS = '@policy/create-policy-type-success';
export const CREATE_POLICY_TYPE_FAILURE = '@policy/create-policy-type-failure';

export const GET_ONE_POLICY_TYPE_REQUEST = '@policy/get_one-policy-type-request';
export const GET_ONE_POLICY_TYPE_SUCCESS = '@policy/get_one-policy-type-success';
export const GET_ONE_POLICY_TYPE_FAILURE = '@policy/get_one-policy-type-failure';

export const UPDATE_POLICY_TYPE_REQUEST = '@policy/update-policy-type-request';
export const UPDATE_POLICY_TYPE_SUCCESS = '@policy/update-policy-type-success';
export const UPDATE_POLICY_TYPE_FAILURE = '@policy/update-policy-type-failure';

// policy
export function getListPolicy(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_POLICIES_REQUEST });
      const response = await policyService.getListPolicy(data);
      dispatch({
        type: GET_POLICIES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_POLICIES_FAILURE });
      throw error;
    }
  };
}

export function createPolicy(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_POLICY_REQUEST });
      const response = await policyService.createOnePolicy(data);
      dispatch({
        type: CREATE_POLICY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_POLICY_FAILURE });
      throw error;
    }
  };
}

export function getOnePolicy(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_POLICY_REQUEST });
      const response = await policyService.getOnePolicy(data);
      dispatch({
        type: GET_ONE_POLICY_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_POLICY_FAILURE });
      throw error;
    }
  };
}

export function updatePolicy(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_POLICY_REQUEST });
      const response = await policyService.updatePolicy(data);
      dispatch({
        type: UPDATE_POLICY_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_POLICY_FAILURE });
      throw error;
    }
  };
}

export function deletePolicy(data) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_POLICY_REQUEST });
      const response = await policyService.deletePolicy(data);
      if(response.code == 'success') {
        const list = await policyService.getListPolicy({});
        dispatch({
          type: DELETE_POLICY_SUCCESS,
          payload: {
            response: list
          }
        });
      }
      
    } catch (error) {
      dispatch({ type: DELETE_POLICY_FAILURE });
      throw error;
    }
  };
}

// policy-type
export function getListPolicyType(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_POLICY_TYPES_REQUEST });
      const response = await policyService.getListPolicyType(data);
      dispatch({
        type: GET_POLICY_TYPES_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_POLICY_TYPES_FAILURE });
      throw error;
    }
  };
}

export function createPolicyType(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_POLICY_TYPE_REQUEST });
      const response = await policyService.createOnePolicyType(data);
      dispatch({
        type: CREATE_POLICY_TYPE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_POLICY_TYPE_FAILURE });
      throw error;
    }
  };
}

export function getOnePolicyType(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_POLICY_TYPE_REQUEST });
      const response = await policyService.getOnePolicyType(data);
      dispatch({
        type: GET_ONE_POLICY_TYPE_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_POLICY_TYPE_FAILURE });
      throw error;
    }
  };
}

export function updatePolicyType(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_POLICY_TYPE_REQUEST });
      const response = await policyService.updatePolicyType(data);
      dispatch({
        type: UPDATE_POLICY_TYPE_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_POLICY_TYPE_FAILURE });
      throw error;
    }
  };
}