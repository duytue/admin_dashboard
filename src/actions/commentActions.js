import commentService from 'src/services/commentService';

export const GET_COMMENTS_REQUEST = '@comment/get-comments-request';
export const GET_COMMENTS_SUCCESS = '@comment/get-comments-success';
export const GET_COMMENTS_FAILURE = '@comment/get-comments-failure';

export const GET_ONE_COMMENT_REQUEST = '@comment/get_one-comment-request';
export const GET_ONE_COMMENT_SUCCESS = '@comment/get_one-comment-success';
export const GET_ONE_COMMENT_FAILURE = '@comment/get_one-comment-failure';

export const UPDATE_COMMENT_REQUEST = '@comment/update-comment-request';
export const UPDATE_COMMENT_SUCCESS = '@comment/update-comment-success';
export const UPDATE_COMMENT_FAILURE = '@comment/update-comment-failure';

export const UPDATE_ACTIVE_COMMENT_REQUEST = '@comment/update-active-comment-request';
export const UPDATE_ACTIVE_COMMENT_SUCCESS = '@comment/update-active-comment-success';
export const UPDATE_ACTIVE_COMMENT_FAILURE = '@comment/update-active-comment-failure';

export const DELETE_COMMENT_REQUEST = '@comment/delete-comment-request';
export const DELETE_COMMENT_SUCCESS = '@comment/delete-comment-success';
export const DELETE_COMMENT_FAILURE = '@comment/delete-comment-failure';

export function getListComment() {
  return async dispatch => {
    try {
      dispatch({ type: GET_COMMENTS_REQUEST });
      const response = await commentService.getListComment();
      dispatch({
        type: GET_COMMENTS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_COMMENTS_FAILURE });
      throw error;
    }
  };
}

export function getOneComment(data) {
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_COMMENT_REQUEST });
      const response = await commentService.getOneComment(data);
      dispatch({
        type: GET_ONE_COMMENT_SUCCESS,
        payload: {
          response,
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_COMMENT_FAILURE });
      throw error;
    }
  };
}

export function updateComment(data) {
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_COMMENT_REQUEST });
      const response = await commentService.updateComment(data);
      dispatch({
        type: UPDATE_COMMENT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_COMMENT_FAILURE });
      throw error;
    }
  };
}

export function deleteComment(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_COMMENT_REQUEST });
      const response = await commentService.deleteComment(data);
      dispatch({
        type: DELETE_COMMENT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_COMMENT_FAILURE });
      throw error;
    }
  };
}

