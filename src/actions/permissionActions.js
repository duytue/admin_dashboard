import permissionService from '../services/permissionService';

export const GET_PERMISSIONS_REQUEST = '@permission/get-permissions-request';
export const GET_PERMISSIONS_SUCCESS = '@permission/get-permissions-success';
export const GET_PERMISSIONS_FAILURE = '@permission/get-permissions-failure';

export const GET_PERMISSIONS_BY_USER_REQUEST = '@user/get-permissions-by-user-request';
export const GET_PERMISSIONS_BY_USER_SUCCESS = '@user/get-permissions-by-user-success';
export const GET_PERMISSIONS_BY_USER_FAILURE = '@user/get-permissions-by-user-failure';

export const GET_PERMISSIONS_OBJECT_REQUEST = '@permission/get-permissions-object-request';
export const GET_PERMISSIONS_OBJECT_SUCCESS = '@permission/get-permissions-object-success';
export const GET_PERMISSIONS_OBJECT_FAILURE = '@permission/get-permissions-object-failure';

export const GET_PERMISSIONS_ACTION_REQUEST = '@permission/get-permissions-action-request';
export const GET_PERMISSIONS_ACTION_SUCCESS = '@permission/get-permissions-action-success';
export const GET_PERMISSIONS_ACTION_FAILURE = '@permission/get-permissions-action-failure';

export const CREATE_PERMISSION_REQUEST = '@permission/create-permission-request';
export const CREATE_PERMISSION_SUCCESS = '@permission/create-permission-success';
export const CREATE_PERMISSION_FAILURE = '@permission/create-permission-failure';

export const GET_ONE_PERMISSION_REQUEST = '@permission/get-one-permission-request';
export const GET_ONE_PERMISSION_SUCCESS = '@permission/get-one-permission-success';
export const GET_ONE_PERMISSION_FAILURE = '@permission/get-one-permission-failure';

export const UPDATE_PERMISSION_REQUEST = '@permission/update-permission-request';
export const UPDATE_PERMISSION_SUCCESS = '@permission/update-permission-success';
export const UPDATE_PERMISSION_FAILURE = '@permission/update-permission-failure';

export const UPDATE_ROLE_OF_PERMISSION_REQUEST = '@permission/update-permission-permission-request';
export const UPDATE_ROLE_OF_PERMISSION_SUCCESS = '@permission/update-permission-permission-success';
export const UPDATE_ROLE_OF_PERMISSION_FAILURE = '@permission/update-permission-permission-failure';

export const UPDATE_ACTIVE_PERMISSION_REQUEST = '@permission/update-active-permission-request';
export const UPDATE_ACTIVE_PERMISSION_SUCCESS = '@permission/update-active-permission-success';
export const UPDATE_ACTIVE_PERMISSION_FAILURE = '@permission/update-active-permission-failure';

export const DELETE_PERMISSION_REQUEST = '@permission/delete-permission-request';
export const DELETE_PERMISSION_SUCCESS = '@permission/delete-permission-success';
export const DELETE_PERMISSION_FAILURE = '@permission/delete-permission-failure';

export function getListPermissions() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PERMISSIONS_REQUEST });
      const response = await permissionService.getListPermissions();
      dispatch({
        type: GET_PERMISSIONS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PERMISSIONS_FAILURE });
      throw error;
    }
  };
}

export function getListPermissionsByUser(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PERMISSIONS_BY_USER_REQUEST });
      const response = await permissionService.getListPermissionsByUser(id);
      dispatch({
        type: GET_PERMISSIONS_BY_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PERMISSIONS_BY_USER_FAILURE });
      throw error;
    }
  };
}

export function getListObjectPermissions() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PERMISSIONS_OBJECT_REQUEST });
      const response = await permissionService.getListObjectPermissions();
      dispatch({
        type: GET_PERMISSIONS_OBJECT_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PERMISSIONS_OBJECT_FAILURE });
      throw error;
    }
  };
}

export function getListActionPermissions() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PERMISSIONS_ACTION_REQUEST });
      const response = await permissionService.getListActionPermissions();
      dispatch({
        type: GET_PERMISSIONS_ACTION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PERMISSIONS_ACTION_FAILURE });
      throw error;
    }
  };
}

export function createPermission(data) {
  return async (dispatch) => {
    try {
      const response = await permissionService.createPermission(data);
      dispatch({
        type: CREATE_PERMISSION_REQUEST,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_PERMISSION_FAILURE });
      throw error;
    }
  };
}

export function getOnePermission(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_PERMISSION_REQUEST });
      const response = await permissionService.getOnePermission(id);
      dispatch({
        type: GET_ONE_PERMISSION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_PERMISSION_FAILURE });
      throw error;
    }
  };
}

export function updatePermission(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_PERMISSION_REQUEST });
      const response = await permissionService.updatePermission(data);
      dispatch({
        type: UPDATE_PERMISSION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PERMISSION_FAILURE });
      throw error;
    }
  };
}

export function updateRoleOfPermission(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ROLE_OF_PERMISSION_REQUEST });
      const response = await permissionService.updateRoleOfPermission(data);
      dispatch({
        type: UPDATE_ROLE_OF_PERMISSION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ROLE_OF_PERMISSION_FAILURE });
      throw error;
    }
  };
}

export function updateActivePermission(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ACTIVE_PERMISSION_REQUEST });
      const response = await permissionService.updateActivePermission(data);
      dispatch({
        type: UPDATE_ACTIVE_PERMISSION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ACTIVE_PERMISSION_FAILURE });
      throw error;
    }
  };
}

export function deletePermission(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_PERMISSION_REQUEST });
      const response = await permissionService.deletePermission(id);
      dispatch({
        type: DELETE_PERMISSION_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({
        type: DELETE_PERMISSION_FAILURE,
        payload: error.message,
      });
      throw error;
    }
  };
}
