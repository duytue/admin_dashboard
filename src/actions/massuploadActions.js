import massuploadService from 'src/services/massuploadService';

export const MASSUPLOAD_REQUEST = '@massupload/massupload-request';
export const MASSUPLOAD_SUCCESS = '@massupload/massupload-success';
export const MASSUPLOAD_FAILURE = '@massupload/massupload-failure';
export const MASSUPLOAD_CLEAR = '@massupload/massupload-clear';

export function upload(data) {
  return async dispatch => {
    try {
      dispatch({ type: MASSUPLOAD_REQUEST });
      const response = await massuploadService.upload(data);
      dispatch({
        type: MASSUPLOAD_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: MASSUPLOAD_FAILURE });
      throw error;
    }
  };
}

export function clearResponse() {
  return async dispatch => {
    try {
      dispatch({ type: MASSUPLOAD_CLEAR });
    } catch (error) {
      dispatch({ type: MASSUPLOAD_FAILURE });
      throw error;
    }
  }
}