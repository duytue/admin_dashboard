import languageSettingService from 'src/services/languageSettingService';

export const GET_LANGUAGE_SETTINGS_REQUEST = '@languageSetting/get-language-settings-request';
export const GET_LANGUAGE_SETTINGS_SUCCESS = '@languageSetting/get-language-settings-success';
export const GET_LANGUAGE_SETTINGS_FAILURE = '@languageSetting/get-language-settings-failure';

export const CREATE_LANGUAGE_SETTING_REQUEST = '@languageSetting/create-language-setting-request';
export const CREATE_LANGUAGE_SETTING_SUCCESS = '@languageSetting/create-language-setting-success';
export const CREATE_LANGUAGE_SETTING_FAILURE = '@languageSetting/create-language-setting-failure';

export const UPDATE_LANGUAGE_SETTING_REQUEST = '@languageSetting/update-language-setting-request';
export const UPDATE_LANGUAGE_SETTING_SUCCESS = '@languageSetting/update-language-setting-success';
export const UPDATE_LANGUAGE_SETTING_FAILURE = '@languageSetting/update-language-setting-failure';

export const GET_LANGUAGE_SETTING_REQUEST = '@languageSetting/get-language-setting-request';
export const GET_LANGUAGE_SETTING_SUCCESS = '@languageSetting/get-language-setting-success';
export const GET_LANGUAGE_SETTING_FAILURE = '@languageSetting/get-language-setting-failure';

export const DELETE_LANGUAGE_SETTING_REQUEST = '@languageSetting/delete-language-setting-request';
export const DELETE_LANGUAGE_SETTING_SUCCESS = '@languageSetting/delete-language-setting-success';
export const DELETE_LANGUAGE_SETTING_FAILURE = '@languageSetting/delete-language-setting-failure';

export function getLanguageSettings() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_LANGUAGE_SETTINGS_REQUEST });
      const response = await languageSettingService.getLanguageSettings();
      dispatch({
        type: GET_LANGUAGE_SETTINGS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_LANGUAGE_SETTINGS_FAILURE });
      throw error;
    }
  };
}

export function createLanguageSetting(data) {
  return async (dispatch) => {
    try {
      const response = await languageSettingService.createLanguageSetting(data);
      dispatch({
        type: CREATE_LANGUAGE_SETTING_REQUEST,
        payload: {
          response
        }
      })
    } catch (error) {
      dispatch({ type: CREATE_LANGUAGE_SETTING_FAILURE });
      throw error;
    }
  };
}

export function updateLanguageSetting(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_LANGUAGE_SETTING_REQUEST });
      const response = await languageSettingService.updateLanguageSetting(data);
      dispatch({
        type: UPDATE_LANGUAGE_SETTING_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_LANGUAGE_SETTING_FAILURE });
      throw error;
    }
  };
}

export function getLanguageSetting(key) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_LANGUAGE_SETTING_REQUEST });
      const response = await languageSettingService.getLanguageSetting(key);
      dispatch({
        type: GET_LANGUAGE_SETTING_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_LANGUAGE_SETTING_FAILURE });
      throw error;
    }
  };
}

export function deleteLanguageSetting(key, platform_key) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETE_LANGUAGE_SETTING_REQUEST });
      const response = await languageSettingService.deleteLanguageSetting(key, platform_key);
      dispatch({
        type: DELETE_LANGUAGE_SETTING_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_LANGUAGE_SETTING_FAILURE });
    }
  };
}