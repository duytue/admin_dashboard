import skuService from 'src/services/skuService';

export const GET_SKUS_REQUEST = '@station/get-skus-request';
export const GET_SKUS_SUCCESS = '@station/get-skus-success';
export const GET_SKUS_FAILURE = '@station/get-skus-failure';

export const ADD_SKU_REQUEST = '@sku/add-sku-request';
export const ADD_SKU_SUCCESS = '@sku/add-sku-success';
export const ADD_SKU_FAILURE = '@sku/add-sku-failure';

export const UPDATE_SKU_REQUEST = '@sku/update-sku-request';
export const UPDATE_SKU_SUCCESS = '@sku/update-sku-success';
export const UPDATE_SKU_FAILURE = '@sku/update-sku-failure';

export function getListSkus(params = {}) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_SKUS_REQUEST });
      const response = await skuService.getListSkus(params);
      dispatch({
        type: GET_SKUS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_SKUS_FAILURE });
      throw error;
    }
  };
}

export function addSku(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_SKU_REQUEST });
      const response = await skuService.createSku(data);
      dispatch({
        type: ADD_SKU_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: ADD_SKU_FAILURE });
      throw error;
    }
  };
}

export function udpateSku(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_SKU_REQUEST });
      const response = await skuService.updateSku(data);
      dispatch({
        type: UPDATE_SKU_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: UPDATE_SKU_FAILURE });
      throw error;
    }
  };
}
