import userService from '../services/userService';

export const GET_USERS_REQUEST = '@user/get-users-request';
export const GET_USERS_SUCCESS = '@user/get-users-success';
export const GET_USERS_FAILURE = '@user/get-users-failure';

export const CREATE_USER_REQUEST = '@user/create-user-request';
export const CREATE_USER_SUCCESS = '@user/create-user-success';
export const CREATE_USER_FAILURE = '@user/create-user-failure';

export const GET_ONE_USER_REQUEST = '@user/get-one-user-request';
export const GET_ONE_USER_SUCCESS = '@user/get-one-user-success';
export const GET_ONE_USER_FAILURE = '@user/get-one-user-failure';

export const UPDATE_USER_REQUEST = '@user/update-user-request';
export const UPDATE_USER_SUCCESS = '@user/update-user-success';
export const UPDATE_USER_FAILURE = '@user/update-user-failure';

export const UPDATE_ROLE_USER_REQUEST = '@user/update-role-user-request';
export const UPDATE_ROLE_USER_SUCCESS = '@user/update-role-user-success';
export const UPDATE_ROLE_USER_FAILURE = '@user/update-role-user-failure';

export const UPDATE_PERMISSION_USER_REQUEST = '@user/update-permission-user-request';
export const UPDATE_PERMISSION_USER_SUCCESS = '@user/update-permission-user-success';
export const UPDATE_PERMISSION_USER_FAILURE = '@user/update-permission-user-failure';

export const UPDATE_ACTIVE_USER_REQUEST = '@user/update-active-user-request';
export const UPDATE_ACTIVE_USER_SUCCESS = '@user/update-active-user-success';
export const UPDATE_ACTIVE_USER_FAILURE = '@user/update-active-user-failure';

export const DELETE_USER_REQUEST = '@user/delete-user-request';
export const DELETE_USER_SUCCESS = '@user/delete-user-success';
export const DELETE_USER_FAILURE = '@user/delete-user-failure';

export function getUsers() {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_USERS_REQUEST });
      const response = await userService.getUsers();
      dispatch({
        type: GET_USERS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_USERS_FAILURE });
      throw error;
    }
  };
}

export function createUser(data) {
  return async (dispatch) => {
    try {
      const response = await userService.createUser(data);
      dispatch({
        type: CREATE_USER_REQUEST,
        payload: {
          response
        }
      })
    } catch (error) {
      dispatch({ type: GET_ONE_USER_FAILURE });
      throw error;
    }
  };
}

export function getOneUser(id) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_ONE_USER_REQUEST });
      const response = await userService.getOneUser(id);
      dispatch({
        type: GET_ONE_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_USER_FAILURE });
      throw error;
    }
  };
}

export function updateUser(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_USER_REQUEST });
      const response = await userService.updateUser(data);
      dispatch({
        type: UPDATE_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_USER_FAILURE });
      throw error;
    }
  };
}

export function updateRoleOfUser(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ROLE_USER_REQUEST });
      const response = await userService.updateRoleOfUser(data);
      dispatch({
        type: UPDATE_ROLE_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ROLE_USER_FAILURE });
      throw error;
    }
  };
}

export function updatePermissionOfUser(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_PERMISSION_USER_REQUEST });
      const response = await userService.updatePermissionOfUser(data);
      dispatch({
        type: UPDATE_PERMISSION_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PERMISSION_USER_FAILURE });
      throw error;
    }
  };
}

export function updateActiveUser(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_ACTIVE_USER_REQUEST });
      const response = await userService.updateActiveUser(data);
      dispatch({
        type: UPDATE_ACTIVE_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_ACTIVE_USER_FAILURE });
      throw error;
    }
  };
}

export function deleteUser(data) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_USER_REQUEST });
      const response = await userService.deleteUser(data);
      dispatch({
        type: DELETE_USER_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_USER_FAILURE });
      throw error;
    }
  };
}
