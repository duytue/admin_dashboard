/* eslint-disable max-len */
import _ from 'lodash';
import itemManagementService from 'src/services/itemManagementService';
import skuService from 'src/services/skuService';

export const SEARCH_PRODUCTS_REQUEST = '@product/search-products-request';
export const SEARCH_PRODUCTS_SUCCESS = '@product/search-products-success';
export const SEARCH_PRODUCTS_FAILURE = '@product/search-products-failure';

export const GET_PRODUCTS_REQUEST = '@product/get-products-request';
export const GET_PRODUCTS_SUCCESS = '@product/get-products-success';
export const GET_PRODUCTS_FAILURE = '@product/get-products-failure';

export const ADD_PRODUCT_REQUEST = '@product/add-product-request';
export const ADD_PRODUCT_SUCCESS = '@product/add-product-success';
export const ADD_PRODUCT_FAILURE = '@product/add-product-failure';

export const UPDATE_PRODUCT_REQUEST = '@product/update-product-request';
export const UPDATE_PRODUCT_SUCCESS = '@product/update-product-success';
export const UPDATE_PRODUCT_FAILURE = '@product/update-product-failure';

export const GET_PRODUCT_REQUEST = '@product/get-product-request';
export const GET_PRODUCT_SUCCESS = '@product/get-product-success';
export const GET_PRODUCT_FAILURE = '@product/get-product-failure';

export const DELETE_PRODUCT_REQUEST = '@product/delete-product-request';
export const DELETE_PRODUCT_SUCCESS = '@product/delete-product-success';
export const DELETE_PRODUCT_FAILURE = '@product/delete-product-failure';

export const CLEAR_PRODUCT = '@product/clear-product';

export function searchProducts(params = {}) {
  return async (dispatch) => {
    try {
      dispatch({ type: SEARCH_PRODUCTS_REQUEST });
      const response = await itemManagementService.searchItem(params);

      dispatch({
        type: SEARCH_PRODUCTS_SUCCESS,
        payload: {
          response: response.data,
          total: response.total_items,
        }
      });
    } catch (error) {
      dispatch({ type: SEARCH_PRODUCTS_FAILURE });
      throw error;
    }
  };
}

export function getListItems(params = {}) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PRODUCTS_REQUEST });
      const productsResponse = await itemManagementService.getItems(params);
      const skuResponse = await skuService.getListSkus();

      const response = _.map(productsResponse.data, (product) => _.extend(product, { skuItems: [_.find(skuResponse, { item_id: product.id })] }));

      dispatch({
        type: GET_PRODUCTS_SUCCESS,
        payload: {
          response,
          total: productsResponse.total_items,
        }
      });
    } catch (error) {
      dispatch({ type: GET_PRODUCTS_FAILURE });
      throw error;
    }
  };
}

export function addItem(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_PRODUCT_REQUEST });
      const response = await itemManagementService.createItem(data);
      dispatch({
        type: ADD_PRODUCT_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: ADD_PRODUCT_FAILURE });
      throw error;
    }
  };
}

export function updateItem(data) {
  return async (dispatch) => {
    try {
      dispatch({ type: UPDATE_PRODUCT_REQUEST });
      const response = await itemManagementService.updateItem(data);
      dispatch({
        type: UPDATE_PRODUCT_SUCCESS,
        payload: {
          response
        }
      });
      return response;
    } catch (error) {
      dispatch({ type: UPDATE_PRODUCT_FAILURE });
      throw error;
    }
  };
}

export function getItem(productId) {
  return async (dispatch) => {
    try {
      dispatch({ type: GET_PRODUCT_REQUEST });
      const product = await itemManagementService.getItem(productId);
      const skuResponse = await skuService.getListSkus();

      const response = _.extend(product, { skuItems: [_.find(skuResponse, { item_id: product.id })] });

      dispatch({
        type: GET_PRODUCT_SUCCESS,
        payload: response
      });
    } catch (error) {
      dispatch({ type: GET_PRODUCT_FAILURE });
      throw error;
    }
  };
}

export function clearProduct() {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_PRODUCT
    });
  };
}

// export function deleteItem(product) {
//   return async (dispatch) => {
//     try {
//       dispatch({ type: DELETE_PRODUCT_REQUEST });
//       console.log('deleteItem object', product);
//       if (typeof station === 'object') {
//         await itemManagementService.deleteStation(product.id);
//       } else {
//         await itemManagementService.deleteStation(product);
//       }
//       dispatch({
//         type: DELETE_PRODUCT_SUCCESS,
//         payload: {
//           product
//         }
//       });
//     } catch (error) {
//       dispatch({ type: DELETE_PRODUCT_FAILURE });
//       throw error;
//     }
//   };
// }
