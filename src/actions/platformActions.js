import platformService from 'src/services/platformService';

export const GET_PLATFORMS_REQUEST = '@platform/get-platforms-request';
export const GET_PLATFORMS_SUCCESS = '@platform/get-platforms-success';
export const GET_PLATFORMS_FAILURE = '@platform/get-platforms-failure';

export const CREATE_PLATFORM_REQUEST = '@platform/create-platform-request';
export const CREATE_PLATFORM_SUCCESS = '@platform/create-platform-success';
export const CREATE_PLATFORM_FAILURE = '@platform/create-platform-failure';

export const GET_ONE_PLATFORM_REQUEST = '@platform/get_one-platform-request';
export const GET_ONE_PLATFORM_SUCCESS = '@platform/get_one-platform-success';
export const GET_ONE_PLATFORM_FAILURE = '@platform/get_one-platform-failure';

export const UPDATE_PLATFORM_REQUEST = '@platform/update-platform-request';
export const UPDATE_PLATFORM_SUCCESS = '@platform/update-platform-success';
export const UPDATE_PLATFORM_FAILURE = '@platform/update-platform-failure';

export const DELETE_PLATFORM_REQUEST = '@platform/delete-platform-request';
export const DELETE_PLATFORM_SUCCESS = '@platform/delete-platform-success';
export const DELETE_PLATFORM_FAILURE = '@platform/delete-platform-failure';

export function getListPlatform() {
  return async dispatch => {
    try {
      dispatch({ type: GET_PLATFORMS_REQUEST });
      const response = await platformService.getListPlatForm();
      // console.log('getListPlatform -> response', response);
      dispatch({
        type: GET_PLATFORMS_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_PLATFORMS_FAILURE });
      throw error;
    }
  };
}

export function createPlatform(data) {
  return async dispatch => {
    try {
      dispatch({ type: CREATE_PLATFORM_REQUEST });
      const response = await platformService.createOnePlatForm(data);
      dispatch({
        type: CREATE_PLATFORM_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: CREATE_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function getOnePlatform(data) {
  console.log('getOnePlatform');
  return async dispatch => {
    try {
      dispatch({ type: GET_ONE_PLATFORM_REQUEST });
      const response = await platformService.getOnePlatForm(data);
      dispatch({
        type: GET_ONE_PLATFORM_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: GET_ONE_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function updatePlatform(data) {
  console.log(1);
  return async dispatch => {
    try {
      dispatch({ type: UPDATE_PLATFORM_REQUEST });
      const response = await platformService.updatePlatform(data);
      dispatch({
        type: UPDATE_PLATFORM_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: UPDATE_PLATFORM_FAILURE });
      throw error;
    }
  };
}

export function deletePlatform(data) {
  return async dispatch => {
    try {
      dispatch({ type: DELETE_PLATFORM_REQUEST });
      const response = await platformService.deletePlatform(data);
      dispatch({
        type: DELETE_PLATFORM_SUCCESS,
        payload: {
          response
        }
      });
    } catch (error) {
      dispatch({ type: DELETE_PLATFORM_FAILURE });
      throw error;
    }
  };
}
