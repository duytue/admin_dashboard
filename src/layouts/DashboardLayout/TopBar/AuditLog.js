import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import {
  Menu,
  Box,
  IconButton,
  Button,
  Link,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  MenuItem,
  Popover,
  SvgIcon,
  Tooltip,
  Typography,
  makeStyles
} from '@material-ui/core';
import { ChevronDown as ChevronDownIcon } from 'react-feather';
import OnlineIndicator from 'src/components/OnlineIndicator';
import { getContacts } from 'src/actions/chatActions';

const useStyles = makeStyles((theme) => ({
  popover: {
    width: 320,
    padding: theme.spacing(2)
  },
  list: {
    padding: theme.spacing(1, 3)
  },
  listItemText: {
    marginRight: theme.spacing(1)
  },
  lastActivity: {
    whiteSpace: 'nowrap'
  },
  colorWhite: {
    color: 'white'
  }
}));

const INTINIAL_PLATFORM = [
  { id: 1, name: 'Platform Management', url: 'platformmanagement' },
  { id: 2, name: 'User Management', url: 'usermanagement' },
  { id: 3, name: 'Audit Log', url: 'auditlog' }
];

function AudiLog() {
  const classes = useStyles();
  const ref = useRef(null);
  const dispatch = useDispatch();
  const { contacts } = useSelector((state) => state.chat);
  const [isOpen, setOpen] = useState(false);
  const [selectedVal, setSelectedVal] = useState('');
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(getContacts());
  }, [dispatch]);

  return (
    <>
      <Tooltip title="Contacts">
        <IconButton color="inherit" onClick={handleOpen} ref={ref}>
          <Typography
            color="textPrimary"
            variant="h5"
            className={classes.colorWhite}
          >
            Control Panel
          </Typography>
          <SvgIcon fontSize="small">
            <ChevronDownIcon />
          </SvgIcon>
        </IconButton>
      </Tooltip>
      <Popover
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        classes={{ paper: classes.popover }}
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
      >
        <Box mt={2}>
          <Menu
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            keepMounted
            PaperProps={{ className: classes.popover }}
            getContentAnchorEl={null}
            anchorEl={ref.current}
            open={isOpen}
          >
            {INTINIAL_PLATFORM.map((platform, index) => {
              return (
                <MenuItem
                  key={index}
                  component={RouterLink}
                  to="/app/controlpanel/auditlog"
                  // sau nay dung de multi-control panel url
                  //to={`/app/controlpanel/${platform.url}`}
                >
                  {platform.name}
                </MenuItem>
              );
            })}
          </Menu>
        </Box>
      </Popover>
    </>
  );
}

export default AudiLog;
