import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {
  Menu,
  Box,
  IconButton,
  MenuItem,
  Popover,
  SvgIcon,
  Typography,
  makeStyles
} from '@material-ui/core';
import { ChevronDown as ChevronDownIcon } from 'react-feather';
import { getContacts } from 'src/actions/chatActions';

const useStyles = makeStyles((theme) => ({
  popover: {
    width: 320,
    padding: theme.spacing(2)
  },
  list: {
    padding: theme.spacing(1, 3)
  },
  listItemText: {
    marginRight: theme.spacing(1)
  },
  lastActivity: {
    whiteSpace: 'nowrap'
  },
  colorWhite: {
    color: 'white'
  }
}));

const INTINIAL_PLATFORM = [
  { id: 1, name: 'Address (Location tree)', linkTo: '/app/masterdata/address' },
  { id: 2, name: 'Reasons' },
  { id: 3, name: 'Localization' },
  { id: 1, name: 'Policies', linkTo: '/app/masterdata/policies' }
];

function MasterData() {
  const classes = useStyles();
  const ref = useRef(null);
  const dispatch = useDispatch();
  const [isOpen, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(getContacts());
  }, [dispatch]);

  return (
    <>
      <IconButton color="inherit" onClick={handleOpen} ref={ref}>
        <Typography
          color="textPrimary"
          variant="h5"
          className={classes.colorWhite}
        >
          Master Data
        </Typography>
        <SvgIcon fontSize="small">
          <ChevronDownIcon />
        </SvgIcon>
      </IconButton>
      <Popover
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        classes={{ paper: classes.popover }}
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
      >
        <Box mt={2}>
          <Menu
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            keepMounted
            PaperProps={{ className: classes.popover }}
            getContentAnchorEl={null}
            anchorEl={ref.current}
            open={isOpen}
          >
            {INTINIAL_PLATFORM.map((platform) => (
              <MenuItem
                key={platform.id}
                component={RouterLink}
                to={platform.linkTo}
              >
                {platform.name}
              </MenuItem>
            ))}
          </Menu>
        </Box>
      </Popover>
    </>
  );
}

export default MasterData;
