import React, { useRef, useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {
  Menu,
  Box,
  IconButton,
  MenuItem,
  Popover,
  SvgIcon,
  Typography,
  makeStyles
} from '@material-ui/core';
import { ChevronDown as ChevronDownIcon } from 'react-feather';
import { getContacts } from 'src/actions/chatActions';

const useStyles = makeStyles((theme) => ({
  popover: {
    width: 320,
    padding: theme.spacing(2)
  },
  list: {
    padding: theme.spacing(1, 3)
  },
  listItemText: {
    marginRight: theme.spacing(1)
  },
  lastActivity: {
    whiteSpace: 'nowrap'
  },
  colorWhite: {
    color: 'white'
  }
}));

const INTINIAL_CONTENT = [
  {
    id: 1,
    name: 'Category Management',
    linkTo: '/content/category-management'
  },
  { id: 2, name: 'Product Management', linkTo: '' },
  { id: 3, name: 'Product Attribute Management', linkTo: '' },
  { id: 4, name: 'Grouping Product', linkTo: '' }
];

function Content() {
  const classes = useStyles();
  const ref = useRef(null);
  const dispatch = useDispatch();
  const [isOpen, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(getContacts());
  }, [dispatch]);

  return (
    <>
      <IconButton color="inherit" onClick={handleOpen} ref={ref}>
        <Typography
          color="textPrimary"
          variant="h5"
          className={classes.colorWhite}
        >
          Content
        </Typography>
        <SvgIcon fontSize="small">
          <ChevronDownIcon />
        </SvgIcon>
      </IconButton>
      <Popover
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        classes={{ paper: classes.popover }}
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
      >
        <Box mt={2}>
          <Menu
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center'
            }}
            keepMounted
            PaperProps={{ className: classes.popover }}
            getContentAnchorEl={null}
            anchorEl={ref.current}
            open={isOpen}
          >
            {INTINIAL_CONTENT.map((platform) => (
              <MenuItem
                key={platform.id}
                component={RouterLink}
                to={platform.linkTo}
              >
                {platform.name}
              </MenuItem>
            ))}
          </Menu>
        </Box>
      </Popover>
    </>
  );
}

export default Content;
