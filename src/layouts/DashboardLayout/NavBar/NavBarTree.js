import React from 'react';
import { Chip } from '@material-ui/core';
import {
  BarChart as BarChartIcon,
  UserPlus as UserPlusIcon,
  User as UserIcon,
  DollarSign as DollarSignIcon,
  MessageCircle as MessageCircleIcon,
  PieChart as PieChartIcon,
  Tablet as TabletIcon,
  CheckCircle,
  CheckSquare,
  Settings as SettingsIcon,
  List as CategoryIcon,
  Archive as ItemIcon,
  Cpu as ProductAttributeIcon,
  Database as StockIcon,
  Anchor as AnchorIcon,
  Codesandbox as SupplierIcon,
  Award as PromotionIcon,
  Clock as CapacityAndTimeslotIcon,
  Bell as NotificationIcon,
  Send as TicketIcon,
  MessageSquare as MessageSquareIcon,
  ShoppingBag as ShoppingBagIcon,
  Framer as AddressIcon
} from 'react-feather';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import RateReviewIcon from '@material-ui/icons/RateReview';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import SecurityIcon from '@material-ui/icons/Security';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import ExtensionIcon from '@material-ui/icons/Extension';
import PagesIcon from '@material-ui/icons/Pages';
import EventNoteIcon from '@material-ui/icons/EventNote';
import PermMediaIcon from '@material-ui/icons/PermMedia';

export default [
  {
    subheader: 'Reports',
    idSubheader: 'ui_reports',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Dashboard New',
        // keyDropdown
        // idDropdown: 'dashboard_new_dropdown',
        idDropdown: 'user_dropdown',
        icon: TabletIcon,
        href: '/app/reports/dashboard_new'
      },
      // {
      //   title: 'Dashboard',
      //   idDropdown: 'dashboard_dropdown',
      //   icon: PieChartIcon,
      //   href: '/app/reports/dashboard'
      // },
      // {
      //   title: 'Dashboard Alternative',
      //   idDropdown: 'dashboard_alternative_dropdown',
      //   icon: BarChartIcon,
      //   href: '/app/reports/dashboard-alternative'
      // }
    ]
  },
  {
    subheader: 'Content',
    idSubheader: 'ui_content',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      // {
      //   title: 'Product',
      //   idDropdown: 'product_dropdown',
      //   icon: ItemIcon,
      //   items: [
      //     {
      //       title: 'List Product',
      //       idDropdown: 'item_dropdown',
      //       icon: ItemIcon,
      //       href: '/app/content/product-management'
      //     },
      //     {
      //       title: 'Create Product',
      //       idDropdown: 'item_dropdown',
      //       icon: ItemIcon,
      //       href: '/app/content/product-management/create'
      //     }
      //   ]
      // },
      // {
      //   title: 'Product',
      //   idDropdown: 'item_dropdown',
      //   icon: ItemIcon,
      //   href: '/app/content/item-management'
      // },
      {
        title: 'Product',
        idDropdown: 'item_dropdown',
        icon: ItemIcon,
        href: '/app/content/product-management'
      },
      {
        title: 'Primary Attribute',
        idDropdown: 'primary_attribute_dropdown',
        icon: ProductAttributeIcon,
        href: '/app/content/variants'
      },
      // {
      //   title: 'Extra Attribute',
      //   idDropdown: 'extra_attribute_dropdown',
      //   icon: ProductAttributeIcon,
      //   href: '/app/content/item-attributes'
      // },
      {
        title: 'Category',
        idDropdown: 'category_dropdown',
        icon: CategoryIcon,
        href: '/content/category-management'
      },
      {
        title: 'Media',
        idDropdown: 'user_dropdown',
        icon: PermMediaIcon,
        href: '/app/content/medias'
      }
    ]
  },
  {
    subheader: 'Sourcing',
    idSubheader: 'ui_sourcing',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Supplier',
        idDropdown: 'supplier_dropdown',
        icon: SupplierIcon,
        items: [
          {
            title: 'List Suppliers',
            idDropdown: 'list_suppliers_dropdown',
            icon: SupplierIcon,
            href: '/app/management/suppliers'
          },
          {
            title: 'Create Supplier',
            idDropdown: 'create_supplier_dropdown',
            icon: SupplierIcon,
            href: '/app/management/supplier/create'
          }
        ]
      },
      {
        title: 'Stock',
        idDropdown: 'stock_dropdown',
        icon: StockIcon,
        href: '/app/management/stocks'
      },
      {
        title: 'Station',
        idDropdown: 'station_dropdown',
        icon: AnchorIcon,
        href: '/app/management/station'
      }
    ]
  },
  {
    subheader: 'Commercial',
    idSubheader: 'ui_commercial',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Promotion',
        idDropdown: 'promotion_dropdown',
        icon: PromotionIcon,
        href: '/app/management/promotion'
      },
      {
        title: 'Customer Fee',
        idDropdown: 'customer_fee_dropdown',
        icon: DollarSignIcon,
        href: '/app/management/customer-fee'
      },
      {
        title: 'Capacity & Time-slot',
        idDropdown: 'capacity_and_time_slot_dropdown',
        icon: CapacityAndTimeslotIcon,
        href: '/app/management/time-slot-capacity'
      }
      // {
      //   title: 'Pricing',
      //   idDropdown: 'pricing_dropdown',
      //   icon: DollarSignIcon,
      //   href: '/app/management/pricing'
      // }
    ]
  },
  {
    subheader: 'Customer Service',
    idSubheader: 'ui_customer_service',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Customer',
        idDropdown: 'customer_dropdown',
        icon: UserIcon,
        href: '/app/management/customers',
        items: [
          {
            id_map_config: 'list_customers',
            title: 'List Customers',
            href: '/app/management/customers'
          },
          {
            id_map_config: 'create_customer',
            title: 'Create Customer',
            href: '/app/management/customer/create'
          },
        ]
      },
      {
        title: 'Notification',
        idDropdown: 'notification_dropdown',
        icon: NotificationIcon,
        href: '/app/management/notification'
      },
      {
        title: 'Order Verification',
        idDropdown: 'order_verification_dropdown',
        icon: CheckCircle,
        href: '/app/management/order-verification'
      },
      {
        title: 'Comment',
        idDropdown: 'comment_dropdown',
        icon: MessageSquareIcon,
        href: '/app/content/comments'
      },
      {
        title: 'Review',
        idDropdown: 'review_dropdown',
        icon: RateReviewIcon,
        href: '/app/content/reviews'
      },
      {
        title: 'Ticket',
        idDropdown: 'ticket_dropdown',
        icon: TicketIcon,
        href: '/app/management/ticket'
      },
      {
        title: 'Refund Transaction',
        idDropdown: 'refund_transaction_dropdown',
        icon: DollarSignIcon,
        href: '/app/management/refund-transaction'
      }
    ]
  },
  {
    subheader: 'Order Management',
    idSubheader: 'ui_order_management',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Order',
        idDropdown: 'order_dropdown',
        icon: ShoppingBagIcon,
        href: '/app/management/orders'
      },
      {
        title: 'Order Process',
        idDropdown: 'order_process_dropdown',
        icon: ShoppingBagIcon,
        href: '/app/management/order-process'
      }
      // {
      //   title: 'Picklist',
      //   idDropdown: 'picklist_dropdown',
      //   icon: ShoppingBagIcon,
      //   href: '/app/management/picklist'
      // },
      // {
      //   title: 'Package',
      //   idDropdown: 'package_dropdown',
      //   icon: ShoppingBagIcon,
      //   href: '/app/management/package'
      // }
    ]
  },
  {
    subheader: 'Fulfillment Service',
    idSubheader: 'ui_fulfillment_service',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Bag Packing',
        idDropdown: 'bag_packing_dropdown',
        icon: ShoppingBagIcon,
        href: '/app/management/picklist/search'
      },
      {
        title: 'Packaging',
        idDropdown: 'packaging_dropdown',
        icon: ShoppingBagIcon,
        href: '/app/management/package/search'
      }
    ]
  },
  {
    subheader: 'Master Data',
    idSubheader: 'ui_master_data',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'User',
        idDropdown: 'user_dropdown',
        icon: UserIcon,
        items: [
          {
            title: 'List Users',
            idDropdown: 'list_users_dropdown',
            icon: UserIcon,
            href: '/app/management/users'
          },
          // {
          //   title: 'Create User',
          //   idDropdown: 'create_user_dropdown',
          //   icon: UserPlusIcon,
          //   href: '/app/management/users/create'
          // },
          {
            title: 'List Roles',
            idDropdown: 'list_roles_dropdown',
            icon: CheckCircle,
            href: '/app/management/roles'
          },
          // {
          //   title: 'Create Role',
          //   idDropdown: 'create_role_dropdown',
          //   icon: CheckCircle,
          //   href: '/app/management/roles/create'
          // },
          {
            title: 'List Permissions',
            idDropdown: 'list_permissions_dropdown',
            icon: CheckSquare,
            href: '/app/management/permissions'
          },
          // {
          //   title: 'Create Permission',
          //   idDropdown: 'create_permission_dropdown',
          //   icon: CheckSquare,
          //   href: '/app/management/permissions/create'
          // }
        ]
      },
      {
        title: 'Location',
        idDropdown: 'address_dropdown',
        icon: AddressIcon,
        href: '/app/masterdata/address'
      },
      {
        title: 'Reason',
        idDropdown: 'reason_dropdown',
        icon: MessageCircleIcon,
        href: '/app/management/reasons'
      },
      {
        title: 'Language',
        idDropdown: 'localization_dropdown',
        icon: GTranslateIcon,
        items: [
          {
            title: 'List Languages',
            idDropdown: 'list_languages_dropdown',
            icon: GTranslateIcon,
            href: '/app/management/languages'
          },
          {
            title: 'Create Language',
            idDropdown: 'create_language_dropdown',
            icon: GTranslateIcon,
            href: '/app/management/languages/create'
          },
          {
            title: 'List Language Settings',
            idDropdown: 'list_language_settings_dropdown',
            icon: SettingsIcon,
            href: '/app/management/language-setting'
          },
          {
            title: 'Create Language Setting',
            idDropdown: 'create_language_setting_dropdown',
            icon: SettingsIcon,
            href: '/app/management/language-setting/create'
          }
        ]
      },
      {
        title: 'Mass Upload',
        idDropdown: 'mass_upload_dropdown',
        icon: CloudUploadIcon,
        href: '/app/management/mass-upload'
      },
      {
        title: 'Policy',
        idDropdown: 'policy_dropdown',
        icon: SecurityIcon,
        href: '/app/masterdata/policies'
      }
      // {
      //   title: 'Metadata',
      //   idDropdown: 'metadata_dropdown',
      //   icon: ExtensionIcon,
      //   href: '/app/management/metadata'
      // }
    ]
  },
  {
    subheader: 'Control Panel',
    idSubheader: 'ui_control_panel',
    iconHeader: PieChartIcon,
    hideInMenu: true,
    items: [
      {
        title: 'Platform',
        idDropdown: 'platform_dropdown',
        icon: PagesIcon,
        items: [
          {
            title: 'List Platforms',
            idDropdown: 'list_platforms_dropdown',
            icon: PagesIcon,
            href: '/app/management/platform'
          },
          {
            title: 'Create Platform',
            idDropdown: 'create_platform_dropdown',
            icon: PagesIcon,
            href: '/app/management/platform/create'
          }
        ]
      },
      {
        title: 'Audit log',
        idDropdown: 'audit_log_dropdown',
        icon: EventNoteIcon,
        href: '/app/controlpanel/auditlog'
      }
    ]
  }
  // {
  //   subheader: 'Management',
  //   idSubheader: 'ui_management',
  //   iconHeader: UsersIcon,
  //   items: [
  //     {
  //       title: 'Users',
  //       idDropdown: 'user_dropdown',
  //       icon: UsersIcon,
  //       href: '/app/management/users',
  //       items: [
  //         {
  //           id_map_config: 'list_users',
  //           title: 'List Users',
  //           href: '/app/management/users',
  //         },
  //         {
  //           id_map_config: 'create_users',
  //           title: 'Create User',
  //           href: '/app/management/users/create',
  //         },
  //       ],
  //     },
  //     {
  //       title: 'Roles',
  //       icon: ApertureIcon,
  //       href: '/app/management/roles',
  //       idDropdown: 'roles_dropdown',
  //       items: [
  //         {
  //           title: 'List Roles',
  //           href: '/app/management/roles',
  //         },
  //         {
  //           title: 'Create Roles',
  //           href: '/app/management/roles/create',
  //         },
  //       ],
  //     },
  //     {
  //       title: 'Permissions',
  //       icon: LayersIcon,
  //       href: '/app/management/permissions',
  //       idDropdown: 'permission_dropdown',
  //       items: [
  //         {
  //           title: 'List Permissions',
  //           href: '/app/management/permissions',
  //         },
  //         {
  //           title: 'Create Permission',
  //           href: '/app/management/permissions/create',
  //         },
  //       ],
  //     },
  //     {
  //       title: 'Platform',
  //       icon: ServerIcon,
  //       href: '/app/management/platform',
  //       idDropdown: 'platform_dropdown',
  //       items: [
  //         {
  //           title: 'List Platform',
  //           href: '/app/management/platform'
  //         },
  //         {
  //           title: 'Create Platform',
  //           href: '/app/management/platform/create'
  //         }
  //       ]
  //     },
  //     {
  //       title: 'Logging',
  //       icon: ActivityIcon,
  //       href: '/app/controlpanel/auditlog',
  //       idDropdown: 'logging_dropdown',
  //       items: [
  //         {
  //           title: 'List Logging',
  //           href: '/app/controlpanel/auditlog'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Location',
  //       icon: MapPinIcon,
  //       href: '/app/masterdata/address',
  //       idDropdown: 'location_dropdown',
  //       items: [
  //         {
  //           title: 'List Location',
  //           href: '/app/masterdata/address'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Station Management',
  //       icon: CheckCircle,
  //       href: '/app/management/station',
  //       idDropdown: 'station_dropdown',
  //       items: [
  //         {
  //           title: 'List Stations',
  //           href: '/app/management/station'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Suppliers',
  //       idDropdown: 'user_dropdown',
  //       icon: UsersIcon,
  //       href: '/app/management/suppliers',
  //       items: [
  //         {
  //           id_map_config: 'list_suppliers',
  //           title: 'List Suppliers',
  //           href: '/app/management/suppliers'
  //         },
  //         {
  //           id_map_config: 'create_users',
  //           title: 'Create Supplier',
  //           href: '/app/management/suppliers/create'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Setting',
  //       idDropdown: 'user_dropdown',
  //       icon: SettingsIcon,
  //       href: '/app/management/reasons',
  //       items: [
  //         {
  //           id_map_config: 'list_reasons',
  //           title: 'List Reasons',
  //           href: '/app/management/reasons'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Languages',
  //       idDropdown: 'user_dropdown',
  //       icon: UsersIcon,
  //       href: '/app/management/languages',
  //       items: [
  //         {
  //           id_map_config: 'list_languages',
  //           title: 'List Languages',
  //           href: '/app/management/languages'
  //         },
  //         {
  //           id_map_config: 'create_languages',
  //           title: 'Create Language',
  //           href: '/app/management/languages/create'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Language Settings',
  //       idDropdown: 'user_dropdown',
  //       icon: UsersIcon,
  //       href: '/app/management/language-setting',
  //       items: [
  //         {
  //           id_map_config: 'list_languages',
  //           title: 'List Language Settings',
  //           href: '/app/management/language-setting'
  //         },
  //         {
  //           id_map_config: 'create_language-setting',
  //           title: 'Create Language Setting',
  //           href: '/app/management/language-setting/create'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Customers',
  //       icon: UsersIcon,
  //       href: '/app/management/customers',
  //       items: [
  //         {
  //           title: 'List Customers',
  //           href: '/app/management/customers'
  //         },
  //         {
  //           title: 'View Customer',
  //           href: '/app/management/customers/1'
  //         },
  //         {
  //           title: 'Edit Customer',
  //           href: '/app/management/customers/1/edit'
  //         }
  //       ]
  //     },
  //     {
  //       title: 'Comments',
  //       idDropdown: 'user_dropdown',
  //       icon: MessageCircleIcon,
  //       href: '/app/content/comments',
  //       items: [
  //         {
  //           id_map_config: 'list_comments',
  //           title: 'List Comments',
  //           href: '/app/content/comments'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Reviews',
  //       idDropdown: 'user_dropdown',
  //       icon: RateReviewIcon,
  //       href: '/app/content/reviews',
  //       items: [
  //         {
  //           id_map_config: 'list_reviews',
  //           title: 'List Reviews',
  //           href: '/app/content/reviews'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Item Attributes',
  //       idDropdown: 'itemAttributes_dropdown',
  //       icon: RateReviewIcon,
  //       href: '/app/content/item-attributes',
  //       items: [
  //         {
  //           id_map_config: 'list_itemAttributes',
  //           title: 'List Item Attributes',
  //           href: '/app/content/item-attributes'
  //         },
  //       ]
  //     },
  //     {
  //       title: 'Item Management',
  //       idDropdown: 'itemManagement_dropdown',
  //       icon: RateReviewIcon,
  //       href: '/app/content/item-management',
  //       items: [
  //         {
  //           id_map_config: 'list_itemAManagement',
  //           title: 'Item Management',
  //           href: '/app/content/item-management'
  //         },
  //       ]
  //     },
  //   ],
  // },
];
