function addCurrentTimeStampToFileName(fileFullName) {
  let nameParts = fileFullName.split('.');
  // Get file's extension
  let fileExtension = nameParts.pop();
  // Get file's name
  let fileName = "";
  nameParts.forEach(part => {
    fileName += part + ".";
  });
  fileName = fileName.substring(0, fileName.length - 1);
  // Add current date time to file's name
  fileName += getCurrentDateAsString() + randomNumber();
  return fileName + '.' + fileExtension;
}

function getCurrentDateAsString() {
  let today = new Date();
  return today.getTime();
}

function randomNumber() {
  return Math.floor(Math.random() * 10).toString(); // Return a random number from 0 -> 9 as string
}

export default addCurrentTimeStampToFileName;
