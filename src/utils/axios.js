import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api-stg-cen.jamalex.net/ms-user',
    headers: {
        "Access-Control-Allow-Origin": "http://localhost:3000",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    },
});

export default instance;
