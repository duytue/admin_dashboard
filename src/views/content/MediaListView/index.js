import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getList } from 'src/actions/mediaActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function MediaListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { medias } = useSelector(state => {
    return state.media;
  });

  useEffect(() => {
    dispatch(getList());
  }, [dispatch]);

  if (!medias) {
    return null;
  }

  return (
    <Page className={classes.root} title="Media List">
      <Container maxWidth={false}>
        <Header />
        {medias && (
          <Box mt={3}>
            <Results medias={medias} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default MediaListView;
