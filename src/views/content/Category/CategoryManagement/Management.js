/* eslint-disable linebreak-style */
/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Tree, Input, Menu, Dropdown, Checkbox } from 'antd';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { Grid, Button, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { updateCategory, deleteCategory, getListCategory, createCategory } from 'src/actions/categoryActions';
import { AlertCircle as AlertIcon } from 'react-feather';
import { useSnackbar } from 'notistack';
import { useHistory } from "react-router-dom";
import { MAX_LEVEL } from '../../../../config'

function Management({ className, ...rest }) {
  const { Search } = Input;
  const history = useHistory();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [checkedKeys, setCheckedKeys] = useState([]);
  const [query, setQuery] = useState('');
  const categories = useSelector(state => {
    return applyFilters(state.category.categories, query);
  });
  // const treeDataa = handleTreeData(categories);
  const [treeData, setTreeData] = useState([]);

  useEffect(() => {
    dispatch(getListCategory());
  }, [dispatch]);

  setTimeout(() => {
    setTreeData(handleTreeData);
  }, 1000);

  if (!categories) {
    return null;
  }

  function applyFilters(categories, query) {
    return categories.filter(category => {
      let matches = true;

      if (query) {
        const properties = ['en', 'vn'];
        let containsQuery = false;

        properties.forEach(property => {
          if (category['name'][property].toLowerCase().includes(query.toLowerCase())) {
            containsQuery = true;
          }
        });

        if (!containsQuery) {
          matches = false;
        }
      }

      return matches;
    });
  }

  const handleQueryChange = (value, event) => {
    event.persist();
    setQuery(value);
  };

  function handleTreeData() {
    let dataLevel = [];
    let categoryLevel = [];
    for (let index = MAX_LEVEL - 1; index >= 0; index--) {
      dataLevel[index] = [];
      categoryLevel[index] = categories.filter((item) => item.level === (index + 1));
      if (index === MAX_LEVEL - 1) {
        categoryLevel[index].forEach(element => {
          let alert = (element.name.en === null || element.name.en === ''
            || element.name.vn === null || element.name.vn === ''
            || element.description.en === null || element.description.en === ''
            || element.description.vn === null || element.description.vn === '');
          dataLevel[index].push({
            title: (
              <>
                {alert && <AlertIcon style={{ height: '13', color: 'red' }}></AlertIcon>}
                <Dropdown.Button overlay={(e) => menu(e, element)} trigger={['click']}>
                  <label style={{ color: !element.is_active ? '#b9b9b9' : '#000000a6' }}>{element.name.vn || element.name.en}</label>
                </Dropdown.Button>
              </>
            ),
            key: element.id
          })
        });
      } else {
        categoryLevel[index].forEach(element => {
          let alert = (element.name.en === null || element.name.en === ''
            || element.name.vn === null || element.name.vn === ''
            || element.description.en === null || element.description.en === ''
            || element.description.vn === null || element.description.vn === '');
          let lstChildrenCategory = categoryLevel[index + 1].filter((item) => item.parent_id === element.id);
          let lstChildrenData = [];
          lstChildrenCategory.forEach(element => {
            lstChildrenData.push(
              dataLevel[index + 1].find(item => item.key === element.id)
            );
          });
          if (lstChildrenData.length > 0) {
            dataLevel[index].push({
              title: (
                <>
                  {alert && <AlertIcon style={{ height: '13', color: 'red' }}></AlertIcon>}
                  <Dropdown.Button overlay={(e) => menu(e, element)} trigger={['click']}>
                    <label style={{ color: !element.is_active ? '#b9b9b9' : '#000000a6' }}>{element.name.vn || element.name.en}</label>
                  </Dropdown.Button>
                </>
              ),
              key: element.id,
              children: lstChildrenData
            });
          } else {
            dataLevel[index].push({
              title: (
                <>
                  {alert && <AlertIcon style={{ height: '13', color: 'red' }}></AlertIcon>}
                  <Dropdown.Button overlay={(e) => menu(e, element)} trigger={['click']}>
                    <label style={{ color: !element.is_active ? '#b9b9b9' : '#000000a6' }}>{element.name.en || element.name.vn}</label>
                  </Dropdown.Button>
                </>
              ),
              key: element.id
            });
          }

        });
      }
    }

    let indexParent;
    for (let index = 0; index < MAX_LEVEL; index++) {
      if (dataLevel[index].length > 0) {
        indexParent = index;
        break;
      }
    }

    return dataLevel[indexParent];

  }

  const menu = (e, element) => {
    return (
      <Menu>
        <Menu.Item key="1" onClick={(info) => onEditMenu(info, element)}>
          Edit
      </Menu.Item>
        <Menu.Item key="2" onClick={(info) => onDeleteMenu(info, element)}>
          Delete
      </Menu.Item>
        <Menu.Item key="3" onClick={(info) => onDuplicateMenu(info, element)}>
          Duplicate
      </Menu.Item>
        {element.is_active && <Menu.Item key="4" onClick={(info) => onDeactiveMenu(info, element)}>
          Deactive
      </Menu.Item>}
        {!element.is_active && <Menu.Item key="4" onClick={(info) => onActiveMenu(info, element)}>
          Active
      </Menu.Item>}
      </Menu>
    )
  };

  function onEditMenu(e, element) {
    history.push(("/content/category-management/" + element.id + "/edit"));
  }

  const onDeleteMenu = async (e, element) => {
    try {
      await dispatch(deleteCategory(element));
      enqueueSnackbar('Delete category successfully', {
        variant: 'success',
      });
    } catch (error) {
      enqueueSnackbar('Delete category failure', {
        variant: 'error',
      });
    }
  }

  const onDuplicateMenu = async (e, element) => {
    let param = {
      "parent_id": element.parent_id,
      "platform_key": element.platform_key,
      "key": element.key,
      "name": element.name,
      "description": element.description,
      "image_url": element.image_url,
      "slug": element.slug,
      "keywords": element.keywords,
      "is_active": element.is_active
    }
    try {
      await dispatch(createCategory(param));
      enqueueSnackbar('Duplicate category successfully', {
        variant: 'success',
      });
    } catch (error) {
      enqueueSnackbar('Duplicate category failure', {
        variant: 'error',
      });
    }
  }

  const onDeactiveMenu = async (e, element) => {
    let param = { ...element };
    param['is_active'] = false;
    try {
      await dispatch(updateCategory(param));
      enqueueSnackbar('Deactive category successfully', {
        variant: 'success',
      });
    } catch (error) {
      enqueueSnackbar('Deactive category failure', {
        variant: 'error',
      });
    }
  }

  const onActiveMenu = async (e, element) => {
    let param = { ...element };
    param['is_active'] = true;
    try {
      await dispatch(updateCategory(param));
      enqueueSnackbar('Active category successfully', {
        variant: 'success',
      });
    } catch (error) {
      enqueueSnackbar('Active category failure', {
        variant: 'error',
      });
    }
  }

  function onCheck(e) {
    setCheckedKeys(e);
  }

  function onChangeSelectedAll(e) {
    if (e.target.checked === true) {
      let lstKeys = [];
      categories.forEach(element => {
        lstKeys.push(element.id);
      });
      setCheckedKeys(lstKeys);
    } else {
      setCheckedKeys([]);
    }
  }

  function onCollapse() {
    setExpandedKeys([]);
  }

  function onAddNew() {
    history.push(("/content/category-management/create"));
  }

  function onShowAll() {
    let lstKeys = [];
    categories.forEach(element => {
      lstKeys.push(element.id);
    });
    setExpandedKeys(lstKeys);
  }

  function onExpand(e) {
    setExpandedKeys(e);
  }

  function onDrop(info) {
    const dropKey = info.node.props.eventKey;
    const dragKey = info.dragNode.props.eventKey;
    const dropPos = info.node.props.pos.split('-');
    const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);

    let childrenCategory = { ...categories.find(item => item.id === dragKey) };
    const loop = (data, key, callback) => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].key === key) {
          return callback(data[i], i, data);
        }
        if (data[i].children) {
          loop(data[i].children, key, callback);
        }
      }
    };
    const data = [...treeData];
    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      let parentCategory = { ...categories.find(item => item.id === dropKey) };
      if (parentCategory.level === MAX_LEVEL) {
        enqueueSnackbar(`Category level ${MAX_LEVEL} cannot be set to parent`, {
          variant: 'error',
        });
      } else {
        childrenCategory['parent_id'] = parentCategory.id;
        // Drop on the content
        loop(data, dropKey, item => {
          item.children = item.children || [];
          // where to insert 示例添加到尾部，可以是随意位置
          item.children.push(dragObj);
        });
      }
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, item => {
        item.children = item.children || [];
        // where to insert 示例添加到头部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else {
      let sameCategory = { ...categories.find(item => item.id === dropKey) };
      childrenCategory['parent_id'] = sameCategory['parent_id'];
      let ar;
      let i;
      loop(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;

      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }
    dispatch(updateCategory({ ...childrenCategory })).then(() => { });
    setTreeData(data);
  };

  return (
    <>
      <Grid item md={12} xs={12} style={{ marginTop: '15px' }}>
        <Search placeholder="Search" onSearch={(value, event) => handleQueryChange(value, event)} />
      </Grid>
      <Grid container spacing={3} style={{ padding: '12px' }} >
        <Grid item md={6} xs={"auto"} >
          <Checkbox onChange={onChangeSelectedAll} style={{ margin: '15px 0px 0px 12px' }}>
            <Typography variant="span" color="textPrimary">Select all</Typography>
          </Checkbox>
        </Grid>
        <Grid item md={6} xs={"auto"} style={{ textAlign: 'right', paddingRight: 0 }} >
          <Button variant="contained" onClick={onAddNew} style={{ margin: '0 5px' }}>
            Add new
          </Button>
          <Button variant="contained" onClick={onCollapse} style={{ margin: '0 5px' }}>
            Collapse
          </Button>
          <Button variant="contained" onClick={onShowAll} style={{ margin: '0 0 0 5px' }}>
            Show all
          </Button>
        </Grid>
      </Grid>
      <Grid item md={12} xs={12} >
        <Tree
          checkable
          draggable
          defaultExpandedKeys={[]}
          defaultSelectedKeys={[]}
          defaultCheckedKeys={[]}
          checkedKeys={checkedKeys}
          expandedKeys={expandedKeys}
          // onSelect={onSelect}
          onExpand={onExpand}
          onCheck={onCheck}
          onDrop={onDrop}
          treeData={treeData}
          style={{ padding: '10px 0', width: '100%' }}
        />
      </Grid>

    </>
  );
}

Management.propTypes = {
  className: PropTypes.string,
  // categories: PropTypes.array
};

export default Management;
