import React, {
  useEffect
} from 'react';
import {  Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Management from './Management';
import { getListCategory } from 'src/actions/categoryActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CategoryListView() {
  const classes = useStyles();

  const dispatch = useDispatch();

  const categories = useSelector(state => {
    return state.category.categories
  });

  useEffect(() => {
    dispatch(getListCategory());
  }, [dispatch]);

  if (!categories) {
    return null;
  }
  return (
    <Page className={classes.root} title="Category List">
      <Container maxWidth={false}>
        <Header />
        <Management />
      </Container>
    </Page>
  );
}

export default CategoryListView;
