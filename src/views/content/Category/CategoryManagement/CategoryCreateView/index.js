import React, { useEffect } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import CategoryCreateForm from './CategoryCreateForm';
import { useDispatch } from 'react-redux';
import { getListCategory } from 'src/actions/categoryActions';
import { getListPlatform } from 'src/actions/platformActions';
const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CategoryCreateView() {
  const dispatch = useDispatch();
  const classes = useStyles();

  useEffect(() => {
    dispatch(getListCategory());
    dispatch(getListPlatform());
  }, [dispatch]);

  return (
    <Page
      className={classes.rootPageCreateView}
      title="Category Create"
    >
      <Container maxWidth={false}>
        <Header />
        <CategoryCreateForm />
      </Container>
    </Page>
  );
}

export default CategoryCreateView;
