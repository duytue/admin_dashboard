import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  Select,
  MenuItem,
  InputLabel,
  Paper,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  List,
  ListItem,
  ListItemText,
  Slide,
  Typography
} from '@material-ui/core';
import {
  Upload as UploadIcon,
  XCircle as DeleteIcon,
  AlertCircle,
  ChevronRight as ArrowIcon
} from 'react-feather';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import categoryService from 'src/services/categoryService';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getOneCategory, updateCategory, getListCategory } from 'src/actions/categoryActions';
import { getListPlatform } from 'src/actions/platformActions';
import { MAX_LEVEL } from '../../../../../config';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectedItem: {
    backgroundColor: '#1976d2',
    color: '#000'
  },
  borderList: {
    border: '1px solid #ccc',
    maxHeight: '400px',
    overflowY: 'auto'
  }
}));

function CategoryEditForm({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const history = useHistory();
  const categoryId = _.trim(window.location.pathname.slice(29, -4), '/');
  const [image_url, setImageUrl] = useState(null);
  const [new_image, setNewImage] = useState(null);
  const [language, setLanguage] = useState('en');
  const [isShowCurrentImage, setIsShowCurrentImage] = useState(true);
  const [open, setOpen] = useState(false);
  const [showList, setShowList] = useState([]);
  const [listParentLv, setListParentLv] = useState([]);
  const [selectedParentLv, setSelectedParentLv] = useState([]);
  const [maxWidth, setMaxWidth] = useState("sm");
  const [isCurrent, setIsCurrent] = useState(true);
  const [selectedParent, setSelectedParent] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  const { user } = useSelector((state) => state.account);
  const { category } = useSelector((state) => {
    return state.category
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  })
  const categories = useSelector((state) => {
    return state.category.categories.filter(item => item.level < MAX_LEVEL);
  })

  useEffect(() => {
    dispatch(getListCategory());
    dispatch(getListPlatform());
    dispatch(getOneCategory(categoryId));
  }, [dispatch, categoryId]);


  if (_.size(category) === 0) {
    return null;
  }

  const handleClickOpen = () => {
    if (isHasChidren(category)) {
      enqueueSnackbar('Cannot change the parent because this category has chidren!', {
        variant: 'error'
      });
    } else {
      if (isCurrent) {
        currentState();
        setIsCurrent(false);
      }
      setOpen(true);
    }

  };

  const currentShowState = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL - 1; index++) {
      if (index === 0) {
        array[index] = true;
      } else {
        array[index] = false;
      }
    }
    return array;
  }

  const currentListParentLv = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL - 1; index++) {
      array[index] = [];
    }
    let listParentLv1 = categories.filter(item => item.level === 1);
    array[0] = listParentLv1;
    return array;
  }

  const currentSelectedParentLv = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL - 1; index++) {
      array[index] = null;
    }
    return array;
  }

  const handleClose = (e, value) => {
    setOpen(false);
    if (value === false) {
      setMaxWidth("sm");
      setShowList(currentShowState);
      setListParentLv(currentListParentLv);
      setSelectedParentLv(currentSelectedParentLv);
      setSelectedParent(null);
    }
  };

  const handleSelectedParentLv = (event, item, index) => {
    if (index < 1) {
      setMaxWidth("sm");
    } else {
      setMaxWidth("md");
    }

    switch (index) {
      case (MAX_LEVEL - 2):
        setSelectedParent(item);
        let newSelect = [...selectedParentLv];
        newSelect[index] = item.id;
        setSelectedParentLv(newSelect);
        break;

      default:
        // set selected parent
        setSelectedParent(item);

        // set selected parent by level
        let newSelectedParentLv = [...selectedParentLv];
        for (let i = 0; i < MAX_LEVEL - 1; i++) {
          if (i === index) {
            newSelectedParentLv[i] = item.id;
          } else if (i > index) {
            newSelectedParentLv[i] = null;
          }
        }
        setSelectedParentLv(newSelectedParentLv);

        // set list parent by level
        let newListParentLv = [...listParentLv];
        for (let i = 0; i < MAX_LEVEL - 1; i++) {
          if (i === index + 1) {
            newListParentLv[i] = categories.filter(x => x.parent_id === item.id);
          }
          if (i > index + 1) {
            newListParentLv[i] = [];
          }
        }
        setListParentLv(newListParentLv);

        // set show state
        let newShowState = [...showList];
        for (let i = 0; i < MAX_LEVEL - 1; i++) {
          if (i === index + 1) {
            newShowState[i] = isHasChidren(item);
          }
          if (i > index + 1) {
            newShowState[i] = false;
          }
        }
        setShowList(newShowState);
        break;
    }
  }

  const currentState = () => {
    let listParent = [];
    let listParentIds = category.parent_ids;
    if (listParentIds === null || listParentIds.length === 0) {
      return;
    } else {
      listParentIds.forEach(element => {
        listParent.push(
          categories.find(item => item.id === element)
        );
      });
      let array = listParent.reverse();
      if (array.length > 1) {
        setMaxWidth("md");
      }
      let newShowList = currentShowState();
      let newListParentLv = currentListParentLv();
      let newSelectedParentLv = currentSelectedParentLv();

      for (let index = 0; index < array.length; index++) {
        const element = array[index];
        newShowList[index] = true;
        newSelectedParentLv[index] = element.id;
        if (index !== array.length - 1) {
          newListParentLv[index + 1] = categories.filter(item => item.parent_id === element.id);
        }
        if (index === array.length - 1) {
          setSelectedParent(element);
        }
      }
      setShowList(newShowList);
      setListParentLv(newListParentLv);
      setSelectedParentLv(newSelectedParentLv);
    }
  }

  const isHasChidren = (item) => {
    let array = categories.filter(element => element.parent_id === item.id);
    return array.length > 0;
  }

  const convertToString = (array) => {
    let string = '';
    if (array != null) {
      for (let index = 0; index < array.length; index++) {
        const element = array[index];
        string += element.trim();
        string += index < (array.length - 1) ? ', ' : '';
      }
    }
    return string;
  }

  const handleChangeImage = (event) => {
    if (event.target.files.length !== 0) {
      setIsShowCurrentImage(false);
      let file = event.target.files[0];
      let type = file.type;
      if (type.indexOf('image/') < 0) {
        enqueueSnackbar('Only image files are allowed', {
          variant: 'error'
        });
      } else {
        setNewImage(file);
        fileToBase64(file);
      }

    }
  }

  const fileToBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImageUrl(reader.result);
    };
  };

  const onClearImage = () => {
    setIsShowCurrentImage(false);
    setImageUrl(null);
    setNewImage(null);
  }

  const onChangeLanguage = (event, value) => {
    setLanguage(value[0]);
  }

  const handleKeyword = (keyword) => {
    let array = keyword.split(',');
    array.forEach(element => {
      element = element.trim();
    });
    return array;
  }

  const handleSeeAll = () => {
    history.push(("/content/category-management"))
  }

  const getName = (id) => {
    if (isCurrent) {
      let item = categories.find(element => element.id === id);
      return item ? item.name.en || item.name.vn : 'Select parent'
    } else {
      return 'Select parent';
    }
  }

  return (
    <Formik
      initialValues={{
        level: category.parent_id ? (category.level - 1).toString() : '1',
        parent_id: category.parent_id ? category.parent_id : '',
        platform_key: category.platform_key,
        key: category.key,
        name_en: category.name?.en,
        name_vn: category.name?.vn,
        description_en: category.description?.en,
        description_vn: category.description?.vn,
        slug: category.slug,
        keyword: convertToString(category.keywords)
      }}
      validationSchema={Yup.object().shape({
        parent_id: Yup.string().max(255),
        platform_key: Yup.string().max(255).required('Platform is required'),
        description_en: Yup.string().max(1000),
        description_vn: Yup.string().max(1000),
        name_en: Yup.string().max(255).required('Name is required'),
        name_vn: Yup.string().max(255),
        key: Yup.string().max(255).required('Key is required'),
        slug: Yup.string().max(255),
        keyword: Yup.string().max(1000)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          let param = {
            "id": category.id,
            "parent_id": selectedParent == null ? null : selectedParent.id,
            "platform_key": values.platform_key,
            "key": values.key,
            "name": {
              "en": values.name_en,
              "vn": values.name_vn
            },
            "description": {
              "en": values.description_en,
              "vn": values.description_vn
            },
            "image_url": "",
            "slug": values.slug,
            "keywords": handleKeyword(values.keyword),
            "is_active": category.is_active
          }

          if (new_image !== null) {
            let media_code = new_image.type.replace('image/', '').toUpperCase();

            let preUploadParam = {
              "creator_by": user.id,
              "name": new_image.name,
              "media_code": media_code,
              "platform_key": values.platform_key
            }

            let responsePre = await categoryService.preUpload(preUploadParam);
            if (responsePre) {
              let responseUpload = await categoryService.uploadImage(responsePre, new_image);
              if (responseUpload) {
                let responseCreateUpload = await categoryService.createUpload(preUploadParam);
                if (responseCreateUpload) {
                  param['image_url'] = responseCreateUpload.url;
                  try {
                    await dispatch(updateCategory(param));
                    setStatus({ success: true });
                    enqueueSnackbar('Category updated', {
                      variant: 'success',
                      action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
                    });
                  } catch (error) {
                    setStatus({ success: false });
                    setErrors({ submit: error.message });
                    enqueueSnackbar('Update category failure', {
                      variant: 'error'
                    });
                  } finally {
                    setSubmitting(false);
                  }
                }
              }
            }
          } else {
            param['image_url'] = isShowCurrentImage ? category.image_url : null;
            try {
              await dispatch(updateCategory(param));
              setStatus({ success: true });
              enqueueSnackbar('Category updated', {
                variant: 'success',
                action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
              });
            } catch (error) {
              setStatus({ success: false });
              setErrors({ submit: error.message });
              enqueueSnackbar('Update category failure', {
                variant: 'error'
              });
            } finally {
              setSubmitting(false);
            }
          }
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values
      }) => (
          <form className={clsx(classes.rootPageCreateView, className)} onSubmit={handleSubmit} {...rest}>

            <Card>
              <CardContent>

                <Grid container spacing={3}>
                  <Grid item xs={12} style={{ textAlign: 'right' }} >
                    <ToggleButtonGroup size="small" aria-label="language" onChange={onChangeLanguage}>
                      <ToggleButton value="en" aria-label="English" style={{ backgroundColor: language === 'en' ? '#8a85ff' : '' }}>
                        {(values.name_en === '' || values.description_en === '') && <AlertCircle />}
                            &nbsp; English
                        </ToggleButton>
                      <ToggleButton value="vn" aria-label="Vietnamese" style={{ backgroundColor: language === 'vn' ? '#8a85ff' : '' }}>
                        {(values.name_vn === '' || values.description_vn === '') && <AlertCircle />}
                            &nbsp; Tiếng Việt
                        </ToggleButton>
                    </ToggleButtonGroup>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12} >
                    <InputLabel shrink id="parent_id">
                      {language == 'en' ? 'Parent' : 'Parent'}
                    </InputLabel>
                    <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                      {selectedParent ? (selectedParent.name.en || selectedParent.name.vn) : getName(values.parent_id)}
                    </Button>
                    <Dialog
                      open={open}
                      TransitionComponent={Transition}
                      keepMounted
                      onClose={handleClose}
                      aria-labelledby="alert-dialog-slide-title"
                      fullWidth={true}
                      maxWidth={maxWidth}
                    >
                      <DialogTitle id="alert-dialog-slide-title">
                        <Typography variant="h3" color="textPrimary">
                          Select the category's parent!
                        </Typography>
                      </DialogTitle>
                      <DialogContent style={{ display: "flex" }}>
                        {showList && showList.map((value, index) => (<Grid key={index} item xs={12} className={classes.borderList} style={{ display: value ? "" : "none" }}>
                          <List>
                            {
                              listParentLv && listParentLv[index] && listParentLv[index].map((item) => (<ListItem key={item.id} button className={item.id === selectedParentLv[index] ? classes.selectedItem : null}
                                onClick={(e) => handleSelectedParentLv(e, item, index)}>
                                <ListItemText primary={`${item.name.en || item.name.vn}`} />
                                {isHasChidren(item) && <ArrowIcon />}
                              </ListItem>))
                            }
                          </List>
                        </Grid>))}
                      </DialogContent>
                      <DialogActions>
                        <Button onClick={(e) => handleClose(e, false)} color="primary">
                          Remove
                        </Button>
                        <Button onClick={(e) => handleClose(e, true)} color="primary" disabled={selectedParent === null}>
                          Accept
                        </Button>
                      </DialogActions>
                    </Dialog>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="platform_key">
                      {language === 'en' ? 'Platform' : 'Nền tảng'}
                    </InputLabel>
                    <Select labelId="platform_key" fullWidth id="platform_key" label="platform_key" name="platform_key"
                      value={values.platform_key} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.id} value={item.key}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.key && errors.key)}
                      fullWidth
                      helperText={touched.key && errors.key}
                      label={language === 'en' ? 'Key' : 'Khóa'}
                      name="key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.key}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    {language === 'en' && <TextField
                      error={Boolean(touched.name_en && errors.name_en)}
                      fullWidth
                      helperText={touched.name_en && errors.name_en}
                      label="Name"
                      name="name_en"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name_en}
                      variant="outlined"
                    />}
                    {language === 'vn' && <TextField
                      error={Boolean(touched.name_vn && errors.name_vn)}
                      fullWidth
                      helperText={touched.name_vn && errors.name_vn}
                      label="Tên"
                      name="name_vn"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name_vn}
                      variant="outlined"
                    />}
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.slug && errors.slug)}
                      fullWidth
                      helperText={touched.slug && errors.slug}
                      label="Slug"
                      name="slug"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.slug}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.keyword && errors.keyword)}
                      fullWidth
                      helperText={touched.keyword && errors.keyword}
                      label={language === 'en' ? 'Keyword (Discriminate by ",")' : 'Từ khóa (Phân biệt bởi dấu ",")'}
                      name="keyword"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.keyword}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    {language === 'en' && <TextField
                      error={Boolean(touched.description_en && errors.description_en)}
                      fullWidth
                      helperText={touched.description_en && errors.description_en}
                      label="Description"
                      name="description_en"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={3}
                      value={values.description_en}
                      variant="outlined"
                    />}
                    {language === 'vn' && <TextField
                      error={Boolean(touched.description_vn && errors.description_vn)}
                      fullWidth
                      helperText={touched.description_vn && errors.description_vn}
                      label="Mô tả"
                      name="description_vn"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={3}
                      value={values.description_vn}
                      variant="outlined"
                    />}
                  </Grid>
                  <Grid item lg={6} md={12}>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="contained-button-file"
                      type="file"
                      style={{ display: 'none' }}
                      onChange={handleChangeImage}
                    />
                    {isShowCurrentImage && <>
                      {category && category.image_url && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                        <label htmlFor="contained-button-file">
                          <img className={classes.imageStyle} src={category?.image_url} style={{ maxWidth: '150px', border: '1px solid' }} />
                        </label>
                        <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }} onClick={onClearImage} />
                      </Paper>}

                      {category && !category.image_url && <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                          {language === 'en' ? 'Choose image' : 'Chọn ảnh'}
                        </Button>
                      </label>}
                    </>}
                    {!isShowCurrentImage && <>
                      {image_url && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                        <label htmlFor="contained-button-file">
                          <img className={classes.imageStyle} src={image_url} style={{ maxWidth: '150px', border: '1px solid' }} />
                        </label>
                        <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }} onClick={onClearImage} />
                      </Paper>}

                      {!image_url && <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                          {language === 'en' ? 'Choose image' : 'Chọn ảnh'}
                        </Button>
                      </label>}
                    </>}
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button variant="contained" color="secondary" type="submit" disabled={isSubmitting}>
                    {language === 'en' ? 'Update category' : 'Cập nhật category'}
                  </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CategoryEditForm.propTypes = {
  className: PropTypes.string,
  category: PropTypes.object.isRequired
};

export default CategoryEditForm;
