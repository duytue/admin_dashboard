import React, {
  // useState,
  // useCallback,
  useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import CategoryEditForm from './CategoryEditForm';
import Header from './Header';
import { getListPlatform } from 'src/actions/platformActions';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CategoriesEditView() {

  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);

  return (
    <Page
      className={classes.root}
      title="Categories Edit"
    >
      <Container maxWidth="lg">
        <Header />
        <Box mt={3}>
          <CategoryEditForm />
        </Box>
      </Container>
    </Page>
  );
}

export default CategoriesEditView;
