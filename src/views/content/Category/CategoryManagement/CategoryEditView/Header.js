import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Breadcrumbs, Link, Typography, makeStyles } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

const useStyles = makeStyles(() => ({
  root: {}
}));

function Header({ className, ...rest }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Breadcrumbs
        separator={<NavigateNextIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        <Link variant="body1" color="inherit" to="/" component={RouterLink}>
          Home
        </Link>
        <Link
          variant="body1"
          color="inherit"
          to="/app/content/item-management"
          component={RouterLink}
        >
          Content
        </Link>
        <Link
          variant="body1"
          color="inherit"
          to="/content/category-management"
          component={RouterLink}
        >
          Category Management
        </Link>
      </Breadcrumbs>
      <Typography variant="h3" color="textPrimary">
        Edit Category
      </Typography>
    </div>
  );
}

Header.propTypes = {
  className: PropTypes.string
};

export default Header;
