/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Edit as EditIcon,
  Search as SearchIcon
} from 'react-feather';
import { update, deleteComment } from 'src/actions/reviewActions';
import { getListCustomer } from '../../../actions/customerAction';
import { getListPlatform } from '../../../actions/platformActions';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  }
];

function applyFilters(reviews, query, filters) {
  return reviews.filter(review => {
    let matches = true;

    if (query) {
      const properties = ['review'];
      let containsQuery = false;

      properties.forEach(property => {
        if (review[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach(key => {
      const value = filters[key];

      if (value && review[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(reviews, page, limit) {
  return reviews.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(reviews, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = reviews.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
}

const useStyles = makeStyles(theme => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  content: {
    width: '40%',
  }
}));

function Results({ className, reviews, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const { customers } = useSelector((state) => {
    return state.customer;
  });

  useEffect(() => {
    dispatch(getListCustomer());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelected([]);
    setCurrentTab(value);
  };

  const handleQueryChange = event => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = event => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAll = event => {
    setSelected(
      event.target.checked ? reviews.map(obj => obj.id) : []
    );
  };

  const handleDeleteAll = () => {
    if (window.confirm("Are you sure delete the review?")) {
      selected.map(obj => {
        // obj is id
        dispatch(deleteComment(obj));
      });
    }
  };

  const handleSelectOne = (event, objId) => {
    if (!selected.includes(objId)) {
      setSelected(prevSelected => [...prevSelected, objId]);
    } else {
      setSelected(prevSelected =>
        prevSelected.filter(id => id !== objId)
      );
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = event => {
    setLimit(event.target.value);
  };

  const handleChangeActive = obj => {
    let dataObj = {};
    dataObj.id = obj.id;
    dataObj.is_visible = !obj.is_visible;
    dispatch(update(dataObj));
  };

  const handleDelete = obj => {
    if (window.confirm("Are you sure delete the review?")) {
      dispatch(deleteComment(obj.id));
    }
  };

  const getDetailLink = (objectType, objectId) => {
    // Add detail link here if there are new object type
    if (objectType == "item") {
      return `/app/content/product-management/edit/${objectId}`;
    } else {
      return `/app/content/reviews`;
    }
  }

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(reviews, query, filters);
  const sorted = applySort(filtered, sort);
  const paginated = applyPagination(sorted, page, limit);
  const enableBulkOperations = selected.length > 0;
  const selectedSome =
    selected.length > 0 && selected.length < reviews.length;
  const selectedAll = selected.length === reviews.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search reviews"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAll}
              indeterminate={selectedSome}
              onChange={handleSelectAll}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAll}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAll}
                    indeterminate={selectedSome}
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>Object</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>User</TableCell>
                <TableCell className={classes.content}>Review</TableCell>
                <TableCell>Rating</TableCell>
                <TableCell>Visible</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginated.map(obj => {
                const isSelected = selected.includes(obj.id);
                let customerObj = customers.filter(customer => customer.id == obj.creator_id);
                if (customerObj) {
                  customerObj = customerObj[0];
                }
                return (
                  <TableRow hover key={obj.id} selected={isSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isSelected}
                        onChange={event =>
                          handleSelectOne(event, obj.id)
                        }
                        value={isSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={getDetailLink(obj.type, obj.object_id)}
                        variant="h6"
                      >
                        Link sản phẩm
                      </Link>
                    </TableCell>
                    <TableCell>{obj.type}</TableCell>
                    <TableCell>
                      {customerObj ?
                        <Link
                          color="inherit"
                          component={RouterLink}
                          to={`/app/management/customer//view`}
                          variant="h6"
                        >
                          {customerObj.first_name} {customerObj.last_name}
                        </Link>
                        : '-'
                      }
                    </TableCell>
                    <TableCell>{obj.review}</TableCell>
                    <TableCell>
                      {obj.rating}
                    </TableCell>
                    <TableCell>
                      <Switch
                        disableRipple
                        checked={obj.is_visible}
                        onChange={() => handleChangeActive(obj)}
                        color="secondary"
                        edge="start"
                      />
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/content/review/${obj.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => handleDelete(obj)}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  reviews: PropTypes.array
};

Results.defaultProps = {
  reviews: []
};

export default Results;
