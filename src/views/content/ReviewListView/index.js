import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getList } from 'src/actions/reviewActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ReviewListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { reviews } = useSelector(state => {
    return state.review;
  });

  useEffect(() => {
    dispatch(getList());
  }, [dispatch]);

  if (!reviews) {
    return null;
  }

  return (
    <Page className={classes.root} title="Reviews List">
      <Container maxWidth={false}>
        <Header />
        {reviews && (
          <Box mt={3}>
            <Results reviews={reviews} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ReviewListView;
