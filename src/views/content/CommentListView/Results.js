/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Edit as EditIcon,
  Search as SearchIcon
} from 'react-feather';
import { updateComment, deleteComment } from 'src/actions/commentActions';
import { deleteMedia } from '../../../actions/mediaActions';
import { getListCustomer } from '../../../actions/customerActions';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  }
];

function applyFilters(comments, query, filters) {
  return comments.filter(comment => {
    let matches = true;

    if (query) {
      const properties = ['content'];
      let containsQuery = false;

      properties.forEach(property => {
        if (comment[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach(key => {
      const value = filters[key];

      if (value && comment[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(comments, page, limit) {
  return comments.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(comments, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = comments.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
}

const useStyles = makeStyles(theme => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  content: {
    width: '40%',
  }
}));

function Results({ className, comments, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedComments, setSelectedComments] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const { customers } = useSelector((state) => {
    return state.customer;
  });

  useEffect(() => {
    dispatch(getListCustomer());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedComments([]);
    setCurrentTab(value);
  };

  const handleQueryChange = event => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = event => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllComments = event => {
    setSelectedComments(
      event.target.checked ? comments.map(comment => comment.id) : []
    );
  };

  const handleDeleteAllComment = () => {
    if (window.confirm("Are you sure delete the comment?")) {
      selectedComments.map(comment => {
        // comment is id
        dispatch(deleteComment(comment));
      });
    }
  };

  const handleSelectOneComment = (event, commentId) => {
    if (!selectedComments.includes(commentId)) {
      setSelectedComments(prevSelected => [...prevSelected, commentId]);
    } else {
      setSelectedComments(prevSelected =>
        prevSelected.filter(id => id !== commentId)
      );
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = event => {
    setLimit(event.target.value);
  };

  const handleChangeActiveComment = comment => {
    let commentObj = {};
    commentObj.id = comment.id;
    commentObj.is_visible = !comment.is_visible;
    dispatch(updateComment(commentObj));
  };

  const handleChangeDeleteComment = comment => {
    if (window.confirm("Are you sure delete the comment?")) {
      dispatch(deleteComment(comment.id));
    }
  };

  // Usually query is done on backend with indexing solutions
  const filteredComments = applyFilters(comments, query, filters);
  const sortedComments = applySort(filteredComments, sort);
  const paginatedComments = applyPagination(sortedComments, page, limit);
  const enableBulkOperations = selectedComments.length > 0;
  const selectedSomeComments =
    selectedComments.length > 0 && selectedComments.length < comments.length;
  const selectedAllComments = selectedComments.length === comments.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search comments"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredComments.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllComments}
              indeterminate={selectedSomeComments}
              onChange={handleSelectAllComments}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllComment}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllComments}
                    indeterminate={selectedSomeComments}
                    onChange={handleSelectAllComments}
                  />
                </TableCell>
                <TableCell>Object</TableCell>
                <TableCell>User</TableCell>
                <TableCell>Type</TableCell>
                <TableCell className={classes.content}>Content</TableCell>
                <TableCell>Visible</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedComments.map(comment => {
                const isCommentSelected = selectedComments.includes(comment.id);

                let customerObj = customers.filter(customer => customer.id == comment.creator_id);
                if (customerObj) {
                  customerObj = customerObj[0];
                }
                return (
                  <TableRow hover key={comment.id} selected={isCommentSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isCommentSelected}
                        onChange={event =>
                          handleSelectOneComment(event, comment.id)
                        }
                        value={isCommentSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to=""
                        variant="h6"
                      >
                        {comment.object_id}
                      </Link>
                    </TableCell>
                    <TableCell>
                      {customerObj ?
                        <Link
                          color="inherit"
                          component={RouterLink}
                          to=''
                          variant="h6"
                        >
                          {customerObj.first_name} {customerObj.last_name}
                        </Link>
                        : '-'
                      }
                    </TableCell>
                    <TableCell>{comment.type}</TableCell>
                    <TableCell>{comment.content}</TableCell>
                    <TableCell>
                      <Switch
                        disableRipple
                        checked={comment.is_visible}
                        onChange={() => handleChangeActiveComment(comment)}
                        color="secondary"
                        edge="start"
                      />
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/content/comments/${comment.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => handleChangeDeleteComment(comment)}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredComments.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  comments: PropTypes.array
};

Results.defaultProps = {
  comments: []
};

export default Results;
