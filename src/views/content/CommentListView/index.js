import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getListComment } from 'src/actions/commentActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CommentListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { comments } = useSelector(state => {
    return state.comment;
  });

  useEffect(() => {
    dispatch(getListComment());
  }, [dispatch]);

  if (!comments) {
    return null;
  }

  return (
    <Page className={classes.root} title="Comments List">
      <Container maxWidth={false}>
        <Header />
        {comments && (
          <Box mt={3}>
            <Results comments={comments} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default CommentListView;
