import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  Typography,
  Switch
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import moment from 'moment';
import { deleteComment, getOneComment, updateComment } from 'src/actions/commentActions';
import { getOneCustomer } from '../../../actions/customerActions';

const useStyles = makeStyles(() => ({
  root: {}
}));

function CommentEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const commentId = _.trim(window.location.pathname.slice(22, -4), '/');
  const dispatch = useDispatch();
  const { comment } = useSelector((state) => {
    return state.comment
  });
  const history = useHistory();

  useEffect(() => {
    dispatch(getOneComment(commentId));
  }, [dispatch, commentId]);

  const { customer } = useSelector((state) => {
    return state.customer;
  });

  // ID for test: e3721cf1-02cf-4045-a151-3417bb88054d
  useEffect(() => {
    dispatch(getOneCustomer(comment.creator_id));
  }, [dispatch]);

  if (_.size(comment) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        object_id: comment.object_id || '',
        type: comment.type || '',
        content: comment.content || '',
        created_at: comment.created_at || '',
        creator_id: comment.creator_id || '',
      }}
      validationSchema={Yup.object().shape({
        content: Yup.string().max(255)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        if (window.confirm("Are you sure update the comment?")) {
          try {
            // Make API request
            let valuesPush = {};
            valuesPush.content = values.content;

            dispatch(updateComment({ ...valuesPush, id: commentId })).then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar('Comment updated', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(("/app/content/comments"))
            });

          } catch (error) {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
          }
        }
      }
      }
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.object_id && errors.object_id)}
                      fullWidth
                      helperText={touched.object_id && errors.object_id}
                      label="Object"
                      name="object"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.object_id}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.type && errors.type)}
                      fullWidth
                      helperText={touched.type && errors.type}
                      label="Type"
                      name="type"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.type}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <Typography variant="h5" color="textPrimary">
                      Customer
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {
                        customer ? (customer.first_name ? customer.first_name : '') + ' ' + (customer.last_name ? customer.last_name : '') : '-'
                      }
                    </Typography>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.created_at && errors.created_at)}
                      fullWidth
                      helperText={touched.created_at && errors.created_at}
                      label="Created"
                      name="created_at"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.created_at ? moment(values.created_at).format('DD/MM/YYYY') : ''}
                      variant="outlined"
                    />
                  </Grid>
                  {/*<Grid item lg={6} md={12} xs={12}>*/}
                    {/*<Typography variant="h5" color="textPrimary">*/}
                      {/*Comment visible*/}
                    {/*</Typography>*/}
                    {/*<Typography variant="body2" color="textSecondary">*/}
                      {/*This will give the comment is visible to show*/}
                    {/*</Typography>*/}
                    {/*<Switch*/}
                      {/*checked={values.is_visible}*/}
                      {/*color="secondary"*/}
                      {/*edge="start"*/}
                      {/*name="is_visible"*/}
                      {/*onChange={handleChange}*/}
                      {/*value={values.is_visible}*/}
                    {/*/>*/}
                  {/*</Grid>*/}
                </Grid>
                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.content && errors.content)}
                      fullWidth
                      helperText={touched.content && errors.content}
                      label="Content"
                      name="content"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.content}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Update comment
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CommentEditForm.propTypes = {
  className: PropTypes.string,
  comment: PropTypes.object.isRequired
};

export default CommentEditForm;
