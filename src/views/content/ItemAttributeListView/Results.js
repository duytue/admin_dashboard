/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { Edit as EditIcon, Search as SearchIcon } from 'react-feather';
import { updateComment, deleteComment } from 'src/actions/commentActions';
import { useDispatch } from 'react-redux';
import ItemAttributeHelpModal from './ItemAttributeHelpModal';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  }
];

// function applyFilters(itemAttributes, query, filters) {
//   return itemAttributes.filter(itemAttribute => {
//     let matches = true;

//     if (query) {
//       const properties = [
//         'admin_name',
//         'modified_by',
//         'display_name',
//         'display_label'
//       ];
//       let containsQuery = false;

//       properties.forEach(property => {
//         if (
//           itemAttribute[property].toLowerCase().includes(query.toLowerCase())
//         ) {
//           containsQuery = true;
//         }
//       });

//       if (!containsQuery) {
//         matches = false;
//       }
//     }

//     Object.keys(filters).forEach(key => {
//       const value = filters[key];

//       if (value && itemAttribute[key] !== value) {
//         matches = false;
//       }
//     });

//     return matches;
//   });
// }

function applyPagination(itemAttributes, page, limit) {
  return itemAttributes.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(itemAttributes, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = itemAttributes.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
}

const useStyles = makeStyles(theme => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({
  className,
  itemAttributes,
  handleGetValueSearching,
  ...rest
}) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedItemAttributes, setSelectedItemAttributes] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [isOpenModal, setOpenModal] = useState(false);
  const [itemId, setItemId] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedItemAttributes([]);
    setCurrentTab(value);
  };

  const handleQueryChange = event => {
    event.persist();
    setQuery(event.target.value);
    handleGetValueSearching(event.target.value);
  };

  const handleSortChange = event => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllItemAttributes = event => {
    setSelectedItemAttributes(
      event.target.checked
        ? itemAttributes.map(itemAttribute => itemAttribute.id)
        : []
    );
  };

  const handleDeleteAllItemAttributes = () => {
    selectedItemAttributes.map(itemAttribute => {
      // comment is id
      dispatch(deleteComment(itemAttribute));
    });
  };

  const handleSelectOneItemAttribute = (event, itemAttributeId) => {
    if (!selectedItemAttributes.includes(itemAttributeId)) {
      setSelectedItemAttributes(prevSelected => [
        ...prevSelected,
        itemAttributeId
      ]);
    } else {
      setSelectedItemAttributes(prevSelected =>
        prevSelected.filter(id => id !== itemAttributeId)
      );
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = event => {
    setLimit(event.target.value);
  };

  // const handleChangeActiveComment = comment => {
  //   const commentObj = {};
  //   commentObj.id = comment.id;
  //   commentObj.is_visible = !comment.is_visible;
  //   dispatch(updateComment(commentObj));
  // };

  const handleChangeDeleteComment = comment => {
    dispatch(deleteComment(comment.id));
  };

  const handleClickOpen = itemId => {
    setOpenModal(true);
    setItemId(itemId);
  };

  const handleClickClose = () => {
    setOpenModal(false);
  };

  // const filteredItemAttributes = applyFilters(itemAttributes, query, filters);
  // const sortedItemAttributes = applySort(filteredItemAttributes, sort);
  // const paginatedItemAttributes = applyPagination(
  //   sortedItemAttributes,
  //   page,
  //   limit
  // );
  const enableBulkOperations = selectedItemAttributes.length > 0;
  const selectedSomeComments =
    selectedItemAttributes.length > 0 &&
    selectedItemAttributes.length < itemAttributes.length;
  const selectedAllComments =
    selectedItemAttributes.length === itemAttributes.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search product attribute"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={50}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllComments}
              indeterminate={selectedSomeComments}
              onChange={handleSelectAllItemAttributes}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllItemAttributes}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllComments}
                    indeterminate={selectedSomeComments}
                    onChange={handleSelectAllItemAttributes}
                  />
                </TableCell>
                <TableCell>Admin Name</TableCell>
                <TableCell>Modified By</TableCell>
                <TableCell>Display Name</TableCell>
                <TableCell>Display Lable</TableCell>
                <TableCell>Input Type</TableCell>
                <TableCell>Help Modal</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {itemAttributes.map(itemAttribute => {
                const isItemAttributeSelected = selectedItemAttributes.includes(
                  itemAttribute.id
                );

                return (
                  <TableRow
                    hover
                    key={itemAttribute.id}
                    selected={isItemAttributeSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isItemAttributeSelected}
                        onChange={event =>
                          handleSelectOneItemAttribute(event, itemAttribute.id)
                        }
                        value={isItemAttributeSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/content/comments/${itemAttribute.id}/edit`}
                        variant="h6"
                      >
                        {itemAttribute.admin_name}
                      </Link>
                    </TableCell>
                    <TableCell>{itemAttribute.modified_by}</TableCell>
                    <TableCell>{itemAttribute?.name?.vn}</TableCell>
                    <TableCell>{itemAttribute.display_label}</TableCell>
                    <TableCell>{itemAttribute.data_type}</TableCell>
                    <TableCell>
                      <Link
                        color="primary"
                        component={RouterLink}
                        onClick={() => handleClickOpen(itemAttribute.id)}
                        variant="h6"
                      >
                        {itemAttribute.help_modal ? 'View' : null}
                      </Link>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/content/comments/${itemAttribute.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => handleChangeDeleteComment(itemAttribute)}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={50}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <ItemAttributeHelpModal
        isOpenModal={isOpenModal}
        handleClickClose={handleClickClose}
        itemId={itemId}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  itemAttributes: PropTypes.array,
  handleGetValueSearching: PropTypes.func
};

Results.defaultProps = {
  itemAttributes: []
};

export default Results;
