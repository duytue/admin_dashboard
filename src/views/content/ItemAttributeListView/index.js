import React, { useEffect, useState } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import Page from 'src/components/Page';
import { geItemAttributes } from '../../../actions/itemAttributeAction';
import Header from './Header';
import Results from './Results';
import useDebounce from '../../../hooks/useDebounce';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ItemAttributeListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [searching, setSearching] = useState('');
  const listAttributes = useSelector(state => state.itemAttribute.attributes);

  const handleGetValueSearching = value => {
    setSearching(value);
  };

  const debouncedSearchTerm = useDebounce(searching, 500);

  useEffect(() => {
    if (debouncedSearchTerm) {
      dispatch(geItemAttributes({ admin_name: debouncedSearchTerm }));
    } else {
      dispatch(geItemAttributes({ admin_name: '' }));
    }
  }, [debouncedSearchTerm]);

  return (
    <Page className={classes.root} title="Comments List">
      <Container maxWidth={false}>
        <Header />
        {listAttributes && (
          <Box mt={3}>
            <Results
              itemAttributes={listAttributes}
              handleGetValueSearching={handleGetValueSearching}
            />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ItemAttributeListView;
