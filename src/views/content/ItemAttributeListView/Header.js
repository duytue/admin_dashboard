import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import ItemAttributeCreateModal from './ItemAttributeCreateModal';
import {
  Breadcrumbs,
  Grid,
  Link,
  Typography,
  makeStyles,
  Button,
  SvgIcon,
  Card
} from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { PlusCircle as PlusCircleIcon } from 'react-feather';

const useStyles = makeStyles((theme) => ({
  root: {},
  action: {
    marginBottom: theme.spacing(1),
    '& + &': {
      marginLeft: theme.spacing(1)
    }
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  }
}));

function Header({ className, ...rest }) {
  const [isOpenModalCreate, setOpenModalCreate] = useState(false);
  const classes = useStyles();

  const handleClickOpenModelCreate = () => {
    setOpenModalCreate(true);
  };

  const handleClickCloseModelCreate = () => {
    setOpenModalCreate(false);
  };

  return (
    <Grid
      className={clsx(classes.root, className)}
      container
      justify="space-between"
      spacing={3}
      {...rest}
    >
      <Grid item>
        <Breadcrumbs
          separator={<NavigateNextIcon fontSize="small" />}
          aria-label="breadcrumb"
        >
          <Link variant="body1" color="inherit" to="/" component={RouterLink}>
            Home
          </Link>
          <Link
            variant="body1"
            color="inherit"
            to="/app/content/item-management"
            component={RouterLink}
          >
            Content
          </Link>
          <Typography variant="body1" color="textPrimary">
            Extra Attribute
          </Typography>
        </Breadcrumbs>
        <Typography variant="h3" color="textPrimary">
          All Extra Attributes
        </Typography>
        <Typography variant="h5" color="secondary">
          (These attributes do not affecting to product's variant)
        </Typography>
      </Grid>
      <Grid item>
        <Button
          color="secondary"
          variant="contained"
          className={classes.action}
          onClick={handleClickOpenModelCreate}
        >
          <SvgIcon fontSize="small" className={classes.actionIcon}>
            <PlusCircleIcon />
          </SvgIcon>
          New Product Attribute
        </Button>
        <ItemAttributeCreateModal
          isOpenModal={isOpenModalCreate}
          handleClickClose={handleClickCloseModelCreate}
        />
      </Grid>
    </Grid>
  );
}

Header.propTypes = {
  className: PropTypes.string
};

export default Header;
