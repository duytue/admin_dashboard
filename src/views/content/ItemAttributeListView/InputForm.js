import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  MenuItem,
  Select,
  Typography,
  makeStyles,
  FormHelperText
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { getListPlatform } from 'src/actions/platformActions';
import {
  addItemAttribute,
  createAtributeStep2
} from 'src/actions/itemAttributeAction';

const useStyles = makeStyles(theme => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: 0,
    paddingBottom: 0
  },
  selectBirthDay: {
    width: '100%',
    marginTop: 0
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  buttonArrowDropDown: {
    minWidth: 0
  },
  textRight: {
    textAlign: 'right'
  }
}));

function InputFrom({ className, handleClickNext, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  // const { loadingCreateLanguage } = useSelector(state => state.language);
  // const { platforms } = useSelector(state => state.platform);

  // useEffect(() => {
  //   dispatch(getListPlatform());
  // }, [dispatch]);
  const inputTypes = ['list', 'text', 'date'];

  return (
    <Formik
      initialValues={{
        input_type: ''
      }}
      validationSchema={Yup.object().shape({
        input_type: Yup.string()
          .max(255)
          .required('Input type is required')
      })}
      // onSubmit={async (
      //   values,
      //   { resetForm, setErrors, setStatus, setSubmitting }
      // ) => {
      //   try {
      //     // TODO remove when update done api
      //     const value = {
      //       admin_name: 'Size Of X',
      //       name: {
      //         vn: 'Size',
      //         en: 'Size'
      //       },
      //       key: 'size',
      //       description: 'This is just size of a product',
      //       data_type: values.input_type,
      //       values_lis: '',
      //       display_order: 1,
      //       help_data: {
      //         code: 1
      //       },
      //       active: true,
      //       platform_key: 'system'
      //     };
      //     dispatch(addItemAttribute(value)).then(() => {
      //       resetForm();
      //       setStatus({ success: true });
      //       setSubmitting(false);
      //       enqueueSnackbar('Attribute created', {
      //         variant: 'success',
      //         action: <Button>See all</Button>
      //       });
      //       history.push('/app/content/item-attributes');
      //     });
      //   } catch (error) {
      //     setStatus({ success: false });
      //     setErrors({ submit: error.message });
      //     setSubmitting(false);
      //   }
      // }}
    >
      {({
        errors,
        // handleBlur,
        handleChange,
        handleSubmit,
        // handleReset,
        // isSubmitting,
        // setFieldValue,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Typography variant="body1" color="secondary">
                    Input type*
                  </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Select
                    labelId="action-select-label"
                    id="action_select_id"
                    name="input_type"
                    error={Boolean(touched.action && errors.action)}
                    fullWidth
                    required
                    value={values.input_type}
                    onChange={handleChange}
                  >
                    {inputTypes &&
                      inputTypes.map(item => (
                        <MenuItem key={item} value={item}>
                          {item}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText error>
                    {touched.input_type && errors.input_type}
                  </FormHelperText>
                </Grid>
              </Grid>
              <Box mt={2} className={classes.textRight}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    handleClickNext(2);
                    dispatch(
                      createAtributeStep2({ data_type: values.input_type })
                    );
                  }}
                >
                  Next
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

InputFrom.propTypes = {
  className: PropTypes.string,
  handleClickNext: PropTypes.func
};

export default InputFrom;
