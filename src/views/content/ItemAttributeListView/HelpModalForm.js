import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
// import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  // Checkbox,
  // Chip,
  Grid,
  // InputLabel,
  // ListItemText,
  // MenuItem,
  // Select,
  // Switch,
  TextField,
  Typography,
  makeStyles
  // FormHelperText
} from '@material-ui/core';
// import { KeyboardDatePicker } from '@material-ui/pickers';
import { useHistory } from 'react-router-dom';
// import { createLanguage } from 'src/actions/languageActions';
import { getListPlatform } from 'src/actions/platformActions';
import _ from 'lodash';
import DraftEditor from './DaftEditor';
import { addItemAttribute } from '../../../actions/itemAttributeAction';

const useStyles = makeStyles(theme => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: 0,
    paddingBottom: 0
  }
}));

function HelpModalForm({
  className,
  handleClickClose,
  handleChangeTab,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  // const { enqueueSnackbar } = useSnackbar();
  // const history = useHistory();

  const [bodies, updateBodies] = useState('');

  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);

  const handleUpdateBodies = editorState => {
    updateBodies(editorState.blocks[0].text);
  };
  const dataStep1 = useSelector(state => state.itemAttribute.dataStep1);
  const dataStep2 = useSelector(state => state.itemAttribute.dataStep2);

  return (
    <Formik
      initialValues={{
        labels: ''
      }}
      validationSchema={Yup.object().shape({
        labels: Yup.string()
          .max(255)
          .required('Label is required')
        // bodies: Yup.string().required('Body is required')
      })}
      // onSubmit={async (
      //   values,
      //   { resetForm, setErrors, setStatus, setSubmitting }
      // ) => {
      //   try {
      //     // TODO remove when update done api
      //     const value = {
      //       admin_name: 'Size Of X',
      //       name: {
      //         vn: 'Size',
      //         en: 'Size'
      //       },
      //       key: 'size',
      //       description: 'This is just size of a product',
      //       data_type: 'text',
      //       values_lis: '',
      //       display_order: 1,
      //       help_data: {
      //         code: 1
      //       },
      //       active: true,
      //       platform_key: 'system'
      //     };
      //     dispatch(addItemAttribute(value)).then(() => {
      //       resetForm();
      //       setStatus({ success: true });
      //       setSubmitting(false);
      //       enqueueSnackbar('Attribute created', {
      //         variant: 'success',
      //         action: <Button>See all</Button>
      //       });
      //       history.push('/app/content/item-attributes');
      //     });
      //   } catch (error) {
      //     setStatus({ success: false });
      //     setErrors({ submit: error.message });
      //     setSubmitting(false);
      //   }
      // }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Typography variant="body1" color="secondary">
                    Label
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={Boolean(touched.labels && errors.labels)}
                    fullWidth
                    helperText={touched.labels && errors.labels}
                    label="Size Chart"
                    name="labels"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.labels}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="body1" color="secondary">
                    Modal Body
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <DraftEditor
                    bodies={bodies}
                    updateState={handleUpdateBodies}
                  />
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                  onClick={() => {
                    dispatch(
                      addItemAttribute({
                        ...dataStep1,
                        data_type: dataStep2.data_type,
                        key: values.labels,
                        help_data: {
                          code: bodies
                        },
                        values_list: '',
                        display_order: 1,
                        description: '',
                        active: true
                      })
                    );
                    handleClickClose();
                    handleChangeTab(0);
                  }}
                >
                  Save
                </Button>
                &nbsp;&nbsp;
                <Button
                  color="secondary"
                  variant="contained"
                  className={classes.action}
                  onClick={() => {
                    handleClickClose();
                    handleChangeTab(0);
                  }}
                >
                  CANCEL
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

HelpModalForm.propTypes = {
  className: PropTypes.string,
  handleClickClose: PropTypes.func
};

export default HelpModalForm;
