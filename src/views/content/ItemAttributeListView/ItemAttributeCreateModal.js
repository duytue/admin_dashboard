import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import GeneralFrom from './GeneralFrom';
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  SvgIcon,
  Divider
} from '@material-ui/core';

import { XCircle as CloseIcon } from 'react-feather';
import SimpleTabs from './TabPanel';
import clsx from 'clsx';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import InputFrom from './InputForm';
import HelpModalForm from './HelpModalForm';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
    fontFamily: 'Roboto',
    fontWeight: '400px',
    fontSize: '0.875rem'
  },
  grid: {
    paddingTop: '20px'
  },
  box: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'right',
    marginLeft: 'auto',
    paddingBottom: '5px'
  }
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

function ItemAttributeCreateModal({
  isOpenModal,
  handleClickClose,
  itemId,
  ...rest
}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeTab = tab => {
    setValue(tab);
  };

  return (
    <Dialog onClose={handleClickClose} open={isOpenModal} fullWidth {...rest}>
      <div className={classes.root}>
        <Box className={classes.box}>
          <IconButton
            onClick={() => {
              handleChangeTab(0);
              handleClickClose();
            }}
          >
            <SvgIcon>
              <CloseIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Grid className={classes.grid} container spacing={5}>
          <div className={clsx(classes.root)}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
              textColor="primary"
              variant="fullWidth"
              indicatorColor="primary"
            >
              <Tab label="General" {...a11yProps(0)} />
              <Tab label="Input" {...a11yProps(1)} />
              <Tab label="Help Modal" {...a11yProps(2)} />
            </Tabs>
            <Divider />
            <TabPanel value={value} index={0}>
              <GeneralFrom handleClickNext={handleChangeTab} />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <InputFrom handleClickNext={handleChangeTab} />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <HelpModalForm
                handleClickClose={handleClickClose}
                handleChangeTab={handleChangeTab}
              />
            </TabPanel>
          </div>
        </Grid>
      </div>
    </Dialog>
  );
}

ItemAttributeCreateModal.propTypes = {
  isOpenModal: PropTypes.bool,
  handleClickClose: PropTypes.func
};

ItemAttributeCreateModal.defaultProps = {
  isOpenModal: false
};

export default ItemAttributeCreateModal;
