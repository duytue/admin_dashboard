import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  SvgIcon,
  Divider
} from '@material-ui/core';
import { XCircle as CloseIcon } from 'react-feather';
import SimpleTabs from './TabPanel';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    fontFamily: 'Roboto',
    fontWeight: '400px',
    fontSize: '0.875rem',
  },
  grid: {
    paddingTop: '20px'
  },
  box: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'right',
    marginLeft: 'auto',
    paddingBottom: '5px'
  }
}));

function ItemAttributeHelpModal({
  isOpenModal, handleClickClose, itemId, ...rest
}) {
  const classes = useStyles();
  return (
    <Dialog
      onClose={handleClickClose}
      open={isOpenModal}
      maxWidth="md"
      fullWidth
      {...rest}
    >
      <div className={classes.root}>
        <Box
          className={classes.box}

        >
          <IconButton onClick={handleClickClose}>
            <SvgIcon>
              <CloseIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Grid
          className={classes.grid}
          container
          spacing={5}
        >
          <SimpleTabs />
        </Grid>
      </div>
    </Dialog>
  );
}

ItemAttributeHelpModal.propTypes = {
  isOpenModal: PropTypes.bool,
  handleClickClose: PropTypes.func,
  itemId: PropTypes.string
};

ItemAttributeHelpModal.defaultProps = {
  isOpenModal: false,
  itemId: ''
};

export default ItemAttributeHelpModal;
