import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  MenuItem,
  Select,
  Typography
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import {
  addItemAttribute,
  createAtributeStep1
} from 'src/actions/itemAttributeAction';
import { getListPlatform } from 'src/actions/platformActions';

const useStyles = makeStyles(theme => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: 0,
    paddingBottom: 0
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  buttonArrowDropDown: {
    minWidth: 0
  },
  textRight: {
    textAlign: 'right'
  }
}));

function GeneralFrom({ className, handleClickNext, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [platform, setPlatform] = useState('');

  const { platforms } = useSelector(state => {
    return state.platform;
  });

  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);
  const handleSelectChange = event => {
    setPlatform(event.target.value);
  };
  return (
    <Formik
      initialValues={{
        admin_name: '',
        display_names: ''
      }}
      validationSchema={Yup.object().shape({
        admin_name: Yup.string()
          .max(255)
          .required('Key is required'),
        display_names: Yup.string()
          .max(255)
          .required('Name is required')
      })}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    error={Boolean(touched.admin_name && errors.admin_name)}
                    fullWidth
                    helperText={touched.admin_name && errors.admin_name}
                    label="Administrative Name"
                    name="admin_name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.admin_name}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={Boolean(
                      touched.display_names && errors.display_names
                    )}
                    fullWidth
                    helperText={touched.display_names && errors.display_names}
                    label="Display Name"
                    name="display_names"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.display_names}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="body1" color="secondary">
                    Platform *
                  </Typography>
                  <Select
                    labelId="action-select-label"
                    id="action_select_id"
                    name="Platform *"
                    // error={Boolean(touched.action && errors.action)}
                    fullWidth
                    required
                    value={platform}
                    onChange={handleSelectChange}
                  >
                    {platforms &&
                      platforms.map(item => (
                        <MenuItem key={item.key} value={item.name}>
                          {item.name}
                        </MenuItem>
                      ))}
                  </Select>
                </Grid>
              </Grid>
              <Box mt={2} className={classes.textRight}>
                <Button
                  variant="contained"
                  color="secondary"
                  disabled={isSubmitting}
                  onClick={() => {
                    handleClickNext(1);
                    dispatch(
                      createAtributeStep1({
                        admin_name: values.admin_name,
                        name: { vn: values.display_names },
                        platform_key: platform
                      })
                    );
                  }}
                >
                  Next
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

GeneralFrom.propTypes = {
  className: PropTypes.string,
  handleClickNext: PropTypes.func
};

export default GeneralFrom;
