// import React, { useState, useEffect } from 'react';
// import moment from 'moment';
// import { makeStyles } from '@material-ui/core/styles';
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
// import {
//   Divider, Box, Button, DialogActions
// } from '@material-ui/core';
// import clsx from 'clsx';
// import _ from 'lodash';
// import PropTypes from 'prop-types';
// import { Formik } from 'formik';
// import { addItem, getItem, updateItem } from 'src/actions/itemManagementAction';
// import { useDispatch, useSelector } from 'react-redux';
// import { useSnackbar } from 'notistack';
// // import { addSku } from 'src/actions/skuActions';
// // import skuService from 'src/services/skuService';
// // import mediaService from 'src/services/mediaService';
// // import { create } from 'src/actions/mediaActions';
// // import GeneralTab from './GeneralTab';
// // import CategoryTab from './CategoryTab';
// // import StorageAndTransportTab from './StorageAndTransportTab';
// // import AttributesTab from './AttributesTab';
// // import SkuTab from './SkuTab';
// // import VariantTab from './VariantTab';
// // import PolicyTab from './PolicyTab';

// function TabPanel(props) {
//   const {
//     children, value, index, ...other
//   } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`simple-tabpanel-${index}`}
//       aria-labelledby={`simple-tab-${index}`}
//       {...other}
//     >
//       {value === index && <Box p={3}>{children}</Box>}
//     </div>
//   );
// }

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.any.isRequired,
//   value: PropTypes.any.isRequired
// };

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     maxWidth: '100%',
//     maxHeight: '100%',
//     backgroundColor: theme.palette.background.paper
//   },
//   rootPageCreateView: {
//     backgroundColor: theme.palette.background.dark,
//     minHeight: '100%',
//     paddingTop: theme.spacing(3),
//     paddingBottom: theme.spacing(3)
//   },
//   selectedItem: {
//     backgroundColor: '#1976d2',
//     color: '#000'
//   },
//   borderList: {
//     border: '1px solid #ccc',
//     maxHeight: '400px',
//     overflowY: 'auto'
//   }
// }));

// function a11yProps(index) {
//   return {
//     id: `simple-tab-${index}`,
//     'aria-controls': `simple-tabpanel-${index}`
//   };
// }

// function ItemEditorTabs({ handleClickClose, isEditMode }) {
//   const classes = useStyles();
//   const [value, setValue] = React.useState(0);
//   const dispatch = useDispatch();
//   const { enqueueSnackbar } = useSnackbar();
//   const { user } = useSelector((state) => state.account);
//   const { item } = useSelector((state) => state.item);

//   const handleTabChange = (event, newValue) => {
//     setValue(newValue);
//   };

//   return (
//     <div className={clsx(classes.root)}>
//     </div>
//   );
// }

// ItemEditorTabs.propTypes = {
//   handleClickClose: PropTypes.func,
//   isEditMode: PropTypes.bool
// };

// export default ItemEditorTabs;
