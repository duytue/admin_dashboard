/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Divider,
  InputAdornment,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles,
  IconButton,
  Menu,
  MenuItem,
  Avatar,
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import MoreVertRoundedIcon from '@material-ui/icons/MoreVertRounded';
import { searchProducts } from 'src/actions/productActions';

const tabs = [
  {
    value: 'itemManagement',
    label: 'All'
  },
  // {
  //   value: 'qna',
  //   label: 'Q&A'
  // }
];

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('itemManagement');
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const history = useHistory();
  const [itemId, setItemId] = useState('');
  const dispatch = useDispatch();
  const { searchProductResult, total } = useSelector((state) => state.product);

  useEffect(() => {
    dispatch(searchProducts({ size: limit, page: page + 1, item_name: query }));
  }, [dispatch, limit, page, query]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleMoreClick = (event, id) => {
    setAnchorEl(event.currentTarget);
    setItemId(id);
  };

  const handleMoreClose = () => {
    setAnchorEl(null);
  };

  const handleViewProductClick = (id) => {
    setAnchorEl(null);
    history.push(`/app/content/product-management/edit/${id}`);
  };

  const handleEditProductClick = (id) => {
    setAnchorEl(null);
    history.push(`/app/content/product-management/edit/${id}`);
  };

  // eslint-disable-next-line no-unused-vars
  const handleViewPricingClick = (_id) => {
    setAnchorEl(null);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search items"
          value={query}
          variant="outlined"
        />
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={total}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell padding="checkbox" />
                <TableCell>Item</TableCell>
                <TableCell>Item Identifier</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>SKU</TableCell>
                <TableCell>Seller SKU</TableCell>
                {/* <TableCell>Variant</TableCell> */}
                {/* <TableCell>Quantity</TableCell> */}
                <TableCell>Measure</TableCell>
                <TableCell>Visible</TableCell>
                <TableCell>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {searchProductResult.map((obj) => (
                <>
                  <TableRow hover key={obj.item.id}>
                    <TableCell padding="checkbox" rowSpan={2}>
                      <IconButton onClick={(event) => handleMoreClick(event, obj.item.id)}>
                        <SvgIcon fontSize="small">
                          <MoreVertRoundedIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                    <TableCell rowSpan={2}>
                      <Avatar alt="Remy Sharp" src={obj.item.images ? obj.item.images[0] : null} />
                    </TableCell>
                    <TableCell rowSpan={2}>{obj.item.names.en || obj.item.names.vn}</TableCell>
                    <TableCell rowSpan={2}>{obj.item.identifier}</TableCell>
                    <TableCell rowSpan={2}>{obj.item.type}</TableCell>
                  </TableRow>
                  <TableRow hover>
                    <TableCell rowSpan={1}>{obj.sku || 'N/A'}</TableCell>
                    <TableCell rowSpan={1}>{obj.source_sku || 'N/A'}</TableCell>
                    {/* <TableCell rowSpan={1}>{obj.variant_values.en || 'N/A'}</TableCell> */}
                    {/* <TableCell rowSpan={1}>{obj.quantity || 'N/A'}</TableCell> */}
                    <TableCell rowSpan={1}>{obj.unit_of_measurement || 'N/A'}</TableCell>
                    <TableCell rowSpan={1}>TRUE</TableCell>
                    <TableCell rowSpan={1}>
                      <Switch
                        disableRipple
                        checked={obj.is_active || null}
                        // onChange={() => handleChangeActive(obj)}
                        color="secondary"
                        edge="start"
                      />
                    </TableCell>
                  </TableRow>
                </>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={total}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <Menu
        id="more-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMoreClose}
      >
        <MenuItem onClick={() => handleViewProductClick(itemId)}>View Product</MenuItem>
        <MenuItem onClick={() => handleEditProductClick(itemId)}>Edit Product</MenuItem>
        <MenuItem onClick={() => handleViewPricingClick(itemId)}>View Pricing History</MenuItem>
      </Menu>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string
};

export default Results;
