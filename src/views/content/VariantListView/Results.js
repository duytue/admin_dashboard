/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import isEmpty from 'lodash/isEmpty';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { Edit as EditIcon, Search as SearchIcon } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from 'src/actions/userActions';
import { deleteVariant, getVariant } from 'src/actions/variantActions';
import ItemAttributeHelpModal from './HelpModal/ItemAttributeHelpModal';
import ItemAttributeFormModal from './FormModal/ItemAttributeFormModal';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  }
];

function applyFilters(variants, query, filters) {
  return variants.filter((variant) => {
    let matches = true;

    if (query) {
      const properties = [
        'admin_name',
        'modified_by',
        'display_name',
        'display_label'
      ];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          variant[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && variant[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(variants, page, limit) {
  return variants.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(variants, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = variants.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, variants, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedItemAttributes, setSelectedItemAttributes] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [currentItemId, setCurrentItemId] = useState();

  const [isOpenModal, setOpenModal] = useState(false);
  const [isOpenModalForm, setIsOpenModalForm] = useState(false);

  const [curHelpInfo, setCurHelpInfo] = useState({});
  const [sort, setSort] = useState(sortOptions[0].value);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.user);
  const { variant } = useSelector((state) => state.variant);

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedItemAttributes([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const toggleFormModal = () => {
    setIsOpenModalForm(!isOpenModalForm);
  };

  const handleSelectAllItemAttributes = (event) => {
    setSelectedItemAttributes(
      event.target.checked
        ? variants.map((item) => item.id)
        : []
    );
  };

  const handleDeleteAllItemAttributes = () => {
    selectedItemAttributes.map((item) => dispatch(deleteVariant(item)));
  };

  const handleSelectOneItemAttribute = (event, variantId) => {
    if (!selectedItemAttributes.includes(variantId)) {
      setSelectedItemAttributes((prevSelected) => [
        ...prevSelected,
        variantId
      ]);
    } else {
      setSelectedItemAttributes((prevSelected) => prevSelected.filter((id) => id !== variantId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleChangeDeleteVariant = (comment) => {
    dispatch(deleteVariant(comment.id));
  };

  const handleClickOpen = (data) => {
    setOpenModal(true);
    setCurHelpInfo(data);
  };

  const handleClickClose = () => {
    setOpenModal(false);
  };

  const onOpenEditForm = (id) => {
    dispatch(getVariant(id));
    setCurrentItemId(id);
    setIsOpenModalForm(true);
  };

  const filteredItemAttributes = applyFilters(variants, query, filters);
  const sortedItemAttributes = applySort(filteredItemAttributes, sort);
  const paginatedItemAttributes = applyPagination(
    sortedItemAttributes,
    page,
    limit
  );
  const enableBulkOperations = selectedItemAttributes.length > 0;
  const selectedSomeVariants = selectedItemAttributes.length > 0
    && selectedItemAttributes.length < variants.length;
  const selectedAllVariants = selectedItemAttributes.length === variants.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search product attribute"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredItemAttributes.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllVariants}
              indeterminate={selectedSomeVariants}
              onChange={handleSelectAllItemAttributes}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllItemAttributes}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllVariants}
                    indeterminate={selectedSomeVariants}
                    onChange={handleSelectAllItemAttributes}
                  />
                </TableCell>
                <TableCell>Admin Name</TableCell>
                {/* <TableCell>Modified By</TableCell> */}
                <TableCell>Display Name</TableCell>
                <TableCell>Display Lable</TableCell>
                <TableCell>Input Type</TableCell>
                <TableCell>Help Modal</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedItemAttributes.map((item) => {
                const isItemAttributeSelected = selectedItemAttributes.includes(
                  item.id
                );
                const currentUser = users.find((user) => user.id === item.updater_id);

                return (
                  <TableRow
                    hover
                    key={item.id}
                    selected={isItemAttributeSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isItemAttributeSelected}
                        onChange={(event) => handleSelectOneItemAttribute(event, item.id)}
                        value={isItemAttributeSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        onClick={() => onOpenEditForm(item.id)}
                        variant="h6"
                      >
                        {item.admin_name}
                      </Link>
                    </TableCell>
                    {/* <TableCell>
                      {(currentUser && currentUser.name) || ''}
                    </TableCell> */}
                    <TableCell>{item.display_names.vi || item.display_names.en || ''}</TableCell>
                    <TableCell>{item.display_labels.vi || item.display_labels.en || ''}</TableCell>
                    <TableCell>{item.input_type}</TableCell>
                    <TableCell>
                      <Link
                        color="primary"
                        component={RouterLink}
                        onClick={() => handleClickOpen(item.help_modal_data)}
                        variant="h6"
                      >
                        {item.help_modal_data ? 'View' : null}
                      </Link>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        onClick={() => onOpenEditForm(item.id)}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => handleChangeDeleteVariant(variant)}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredItemAttributes.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <ItemAttributeHelpModal
        isOpenModal={isOpenModal}
        handleClickClose={handleClickClose}
        helpInfo={curHelpInfo}
      />
      {!isEmpty(variant) && (
      <ItemAttributeFormModal
        isOpenModal={isOpenModalForm}
        handleClickClose={toggleFormModal}
        itemId={currentItemId}
        variant={variant}
      />
      )}
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  variants: PropTypes.array
};

Results.defaultProps = {
  variants: []
};

export default Results;
