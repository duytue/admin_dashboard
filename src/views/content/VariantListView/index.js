/* eslint-disable linebreak-style */
import React, {
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getListVariants } from 'src/actions/variantActions';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ItemAttributeListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { variants } = useSelector((state) => state.variant);

  useEffect(() => {
    dispatch(getListVariants());
  }, [dispatch]);

  if (!variants) {
    return null;
  }

  return (
    <Page className={classes.root} title="Variant List">
      <Container maxWidth={false}>
        <Header />
        {variants && (
          <Box mt={3}>
            <Results variants={variants} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ItemAttributeListView;
