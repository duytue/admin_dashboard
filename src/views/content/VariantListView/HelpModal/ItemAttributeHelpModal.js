/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  SvgIcon,
  Typography,
  Card,
  CardHeader,
  CardContent,
} from '@material-ui/core';
import { XCircle as CloseIcon } from 'react-feather';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    fontFamily: 'Roboto',
    fontWeight: '400px',
    fontSize: '0.875rem',
  },
  grid: {
    paddingTop: '20px'
  },
}));

function ItemAttributeHelpModal({
  isOpenModal, handleClickClose, helpInfo, ...rest
}) {
  const classes = useStyles();
  return (
    <Dialog
      onClose={handleClickClose}
      open={isOpenModal}
      maxWidth="md"
      fullWidth
      {...rest}
    >
      <div className={classes.root}>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="h2">Help Modal</Typography>
          <IconButton onClick={handleClickClose}>
            <SvgIcon>
              <CloseIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Card>
          <CardHeader title={helpInfo.label} />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <div dangerouslySetInnerHTML={{ __html: helpInfo.body }} />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    </Dialog>
  );
}

ItemAttributeHelpModal.propTypes = {
  isOpenModal: PropTypes.bool,
  handleClickClose: PropTypes.func,
  helpInfo: PropTypes.object
};

ItemAttributeHelpModal.defaultProps = {
  isOpenModal: false,
  helpInfo: {}
};

export default ItemAttributeHelpModal;
