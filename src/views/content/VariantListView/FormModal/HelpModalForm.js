/* eslint-disable react/prop-types */
import React from 'react';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import {
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
  makeStyles,
} from '@material-ui/core';
import DraftEditor from 'src/components/DraftEditor';

const useStyles = makeStyles(() => ({
  root: {}
}));

function HelpModalForm(props) {
  const classes = useStyles();

  const {
    touched, handleBlur, values, errors, setFieldValue
  } = props;

  const getEditorState = (value) => {
    const blocksFromHtml = htmlToDraft(value);
    const { contentBlocks, entityMap } = blocksFromHtml;
    const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
    const editorState = EditorState.createWithContent(contentState);
    return editorState;
  };

  const initValue = (values.help_body && getEditorState(values.help_body)) || '';

  const onBodyChange = (event) => {
    const value = draftToHtml(convertToRaw(event.getCurrentContent()));

    // setBodies(event);
    setFieldValue('help_body', value);
  };

  const handleChange = (event) => {
    const val = event.target.value;
    setFieldValue('help_label', val);
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <Grid
          container
          spacing={3}
        >
          <Grid item xs={12}>
            <Typography
              variant="body1"
              color="secondary"
            >
              Label
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
          >
            <TextField
              error={Boolean(touched.help_label && errors.help_label)}
              fullWidth
              helperText={touched.help_label && errors.help_label}
              label="Size Chart"
              name="help label"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              defaultValue={values.help_label}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="body1"
              color="secondary"
            >
              Modal Body
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
          >
            <DraftEditor defaultEditorState={initValue} onEditorStateChange={onBodyChange} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default HelpModalForm;
