/* eslint-disable react/prop-types */
import React from 'react';
import {
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {}
}));

function GeneralFrom(props) {
  const classes = useStyles();

  const {
    touched, handleBlur, values, errors, setFieldValue
  } = props;

  const handleChange = (event, target) => {
    const val = event.target.value;
    setFieldValue(target, val);
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              error={Boolean(touched.admin_name && errors.admin_name)}
              fullWidth
              helperText={touched.admin_name && errors.admin_name}
              label="Administrative Name"
              name="admin name"
              onBlur={handleBlur}
              onChange={(event) => handleChange(event, 'admin_name')}
              required
              defaultValue={values.admin_name}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={Boolean(
                touched.display_names && errors.display_names
              )}
              fullWidth
              helperText={touched.display_names && errors.display_names}
              label="Display Name"
              name="display name"
              onBlur={handleBlur}
              onChange={(event) => handleChange(event, 'display_names')}
              required
              defaultValue={values.display_names}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              error={Boolean(touched.display_labels && errors.display_labels)}
              fullWidth
              helperText={touched.display_labels && errors.display_labels}
              label="Display Label"
              name="display labels"
              onBlur={handleBlur}
              onChange={(event) => handleChange(event, 'display_labels')}
              required
              defaultValue={values.display_labels}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default GeneralFrom;
