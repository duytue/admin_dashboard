/* eslint-disable react/prop-types */
import React from 'react';
import {
  Card,
  CardContent,
  Grid,
  MenuItem,
  TextField,
} from '@material-ui/core';

const inputTypes = [
  'list',
  'text',
  'boolean'
];

function InputFrom(props) {
  const {
    touched, handleBlur, values, errors, setFieldValue
  } = props;

  const handleChange = (event) => {
    const val = event.target.value;
    setFieldValue('input_type', val);
  };

  return (
    <Card>
      <CardContent>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            xs={12}
          >
            <TextField
              error={Boolean(touched.input_type && errors.input_type)}
              fullWidth
              select
              helperText={touched.input_type && errors.input_type}
              label="Input type"
              name="input_type"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              defaultValue={values.input_type}
              variant="outlined"
            >
              {inputTypes && inputTypes.map((item) => (
                <MenuItem key={item} value={item}>{item}</MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default InputFrom;
