import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { useSnackbar } from 'notistack';
import Dialog from '@material-ui/core/Dialog';
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  SvgIcon,
  Divider,
  Button
} from '@material-ui/core';
import { XCircle as CloseIcon } from 'react-feather';
import { Formik } from 'formik';
import clsx from 'clsx';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import {
  addVariant,
  updateVariant,
  clearVariant,
  getListVariants,
} from 'src/actions/variantActions';

import GeneralFrom from './GeneralFrom';
import InputFrom from './InputForm';
import HelpModalForm from './HelpModalForm';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    fontFamily: 'Roboto',
    fontWeight: '400px',
    fontSize: '0.875rem'
  },
  grid: {
    paddingTop: '20px'
  },
  box: {
    paddingBottom: '5px'
  }
}));

function TabPanel(props) {
  const {
    children, value, index, ...other
  } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

function ItemAttributeCreateModal({
  isOpenModal,
  variant,
  handleClickClose,
  itemId,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const [value, setValue] = React.useState(0);

  useEffect(() => () => {
    dispatch(clearVariant());
  }, []);

  const handleTabChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Dialog onClose={handleClickClose} open={isOpenModal} fullWidth {...rest}>
      <div className={classes.root}>
        <Box className={classes.box} display="flex" alignItems="center" justifyContent="space-between">
          <Typography variant="h2">{itemId ? 'Edit Variant' : 'Create Variant'}</Typography>
          <IconButton onClick={handleClickClose}>
            <SvgIcon>
              <CloseIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Grid className={classes.grid} container spacing={5}>
          <Formik
            initialValues={{
              admin_name: variant.admin_name || '',
              display_names: (variant.display_names && (variant.display_names.vn || variant.display_names.vi || variant.display_names.en || '')) || '',
              display_labels: (variant.display_labels && (variant.display_labels.vn || variant.display_labels.vi || variant.display_labels.en || '')) || '',
              input_type: variant.input_type || '',
              help_label: (variant.help_modal_data && variant.help_modal_data.label) || '',
              help_body: (variant.help_modal_data && variant.help_modal_data.body) || '',
            }}
            validationSchema={Yup.object().shape({
              admin_name: Yup.string().required(),
              display_names: Yup.string().required(),
              display_labels: Yup.string().required(),
              input_type: Yup.string().required(),
              help_label: Yup.string().required(),
            })}
            onSubmit={async (values, {
              resetForm,
              setErrors,
              setStatus,
              setSubmitting
            }) => {
              try {
                // Format time
                const submitObj = { ...values };
                submitObj.display_names = {
                  vi: submitObj.display_names,
                  en: submitObj.display_names,
                };
                submitObj.display_labels = {
                  vi: submitObj.display_labels,
                  en: submitObj.display_labels,
                };
                submitObj.help_modal_data = {
                  label: submitObj.help_label,
                  body: submitObj.help_body,
                };
                delete submitObj.help_label;
                delete submitObj.help_body;

                // Make API request
                dispatch(itemId
                  ? updateVariant({ ...submitObj, id: itemId }) : addVariant(submitObj))
                  .then(() => {
                    resetForm();
                    setStatus({ success: true });
                    setSubmitting(false);
                    enqueueSnackbar(itemId ? 'Variant updated' : 'Variant created', {
                      variant: 'success',
                      action: <Button>See all</Button>
                    });
                    handleClickClose();
                    dispatch(getListVariants());
                  });
              } catch (error) {
                setStatus({ success: false });
                setErrors({ submit: error.message });
                setSubmitting(false);
              }
            }}
          >
            {(props) => {
              // eslint-disable-next-line react/prop-types
              const { isSubmitting, handleSubmit } = props;
              return (
                <form
                  className={clsx(classes.root)}
                  onSubmit={handleSubmit}
                  {...rest}
                >
                  <div className={clsx(classes.root)}>
                    <Tabs
                      value={value}
                      onChange={handleTabChange}
                      aria-label="simple tabs example"
                      textColor="primary"
                      variant="fullWidth"
                      indicatorColor="primary"
                    >
                      <Tab label="General" {...a11yProps(0)} />
                      <Tab label="Input" {...a11yProps(1)} />
                      <Tab label="Help Modal" {...a11yProps(2)} />
                    </Tabs>
                    <Divider />
                    <TabPanel value={value} index={0}>
                      <GeneralFrom {...props} />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                      <InputFrom {...props} />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                      <HelpModalForm {...props} />
                    </TabPanel>

                    <Box display="flex" alignItems="center" justifyContent="flex-end">
                      <Button
                        variant="contained"
                        color="secondary"
                        type="submit"
                        disabled={isSubmitting}
                      >
                        Save
                      </Button>
                    </Box>
                  </div>
                </form>
              );
            }}
          </Formik>
        </Grid>
      </div>
    </Dialog>
  );
}

ItemAttributeCreateModal.propTypes = {
  isOpenModal: PropTypes.bool,
  handleClickClose: PropTypes.func,
  itemId: PropTypes.string,
  variant: PropTypes.object,
};

ItemAttributeCreateModal.defaultProps = {
  isOpenModal: false,
  variant: {},
};

export default ItemAttributeCreateModal;
