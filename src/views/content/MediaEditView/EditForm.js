import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  Paper,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';
import {
  Upload as UploadIcon,
  XCircle as DeleteIcon,
  AlertCircle
} from 'react-feather';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getListPlatform } from 'src/actions/platformActions';
import { create, getOne, update } from '../../../actions/mediaActions';
import mediaService from 'src/services/mediaService';

const useStyles = makeStyles(() => ({
  root: {}
}));

function EditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const mediaId = _.trim(window.location.pathname.slice(19, -4), '/');
  const dispatch = useDispatch();
  const { media } = useSelector((state) => {
    return state.media
  });
  const history = useHistory();
  const [language, setLanguage] = useState('en');
  const [fileUrl, setFileUrl] = useState(null);
  const [newFile, setNewFile] = useState(null);
  const { user } = useSelector((state) => state.account);

  useEffect(() => {
    dispatch(getOne(mediaId));
  }, [dispatch, mediaId]);

  const { platforms } = useSelector((state) => {
    return state.platform;
  });

  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);

  const handleChangeFile = (event) => {
    if (event.target.files.length != 0) {
      let file = event.target.files[0];
      let type = file.type;
      let typeExt = file.type;

      if (type.indexOf('image/') >= 0) {
        setNewFile(file);
        fileToBase64(file);
      } else if (type.indexOf('audio/') >= 0) {
        setNewFile(file);
        setFileUrl('/static/images/icon/icon_mp3.png');
      } else if (type.indexOf('video/') >= 0) {
        setNewFile(file);
        setFileUrl('/static/images/icon/icon_video.png');
      }
      else {
        enqueueSnackbar('Only image / audio / video files are allowed', {
          variant: 'error'
        });
      }
    }
  };

  const fileToBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setFileUrl(reader.result);
    };
  };

  const onClearFile = () => {
    setFileUrl(null);
    setNewFile(null);
  };


  if (_.size(media) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        description: media.description || '',
        name: media.name || '',
        media_code: media.media_code || '',
        url: media.url || ''
      }}
      validationSchema={Yup.object().shape({
        description: Yup.string().max(255),
        name: Yup.string().max(255).required('Platform is required'),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        if (window.confirm("Are you sure update the media?")) {
          try {
            let param = {
              'name': media.name,
              'description': media.description,
            };

            // Make API request
            dispatch(update({ ...values, id: mediaId })).then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar('Media updated', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(("/app/content/medias"))
            });
          } catch (error) {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
          }
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid item lg={12} md={12}>
                    <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                      <label htmlFor="contained-button-file">
                        {values.media_code && (values.media_code.toUpperCase() == 'MP4') &&
                        <img src="/static/images/icon/icon_video.png"
                             style={{ maxWidth: '150px', maxHeight: '150px', border: '1px solid' }}/>}
                        {values.media_code && (values.media_code.toUpperCase() == 'MP3') &&
                        <img src="/static/images/icon/icon_mp3.png"
                             style={{ maxWidth: '150px', maxHeight: '150px', border: '1px solid' }}/>}
                        {values.media_code && (values.media_code.toUpperCase() != 'MP3') && (values.media_code.toUpperCase() != 'MP4') &&
                        <img src={values.url}
                             style={{ maxWidth: '150px', maxHeight: '150px', border: '1px solid' }}/>}
                      </label>
                    </Paper>
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Update Media
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

EditForm.propTypes = {
  className: PropTypes.string,
};

export default EditForm;
