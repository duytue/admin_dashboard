import React, {
  // useState,
  // useCallback,
  // useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import EditForm from './EditForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function MediaEditView() {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Media Edit"
    >
      <Container maxWidth={false}>
        <Header />
        <Box mt={3}>
          <EditForm />
        </Box>
      </Container>
    </Page>
  );
}

export default MediaEditView;
