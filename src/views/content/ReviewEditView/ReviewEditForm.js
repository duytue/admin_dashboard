import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  Typography,
  Switch
} from '@material-ui/core';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { FormLabel } from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import moment from 'moment';
import { deleteComment, getOne, update } from 'src/actions/reviewActions';
import { getOneCustomer } from '../../../actions/customerActions';

const useStyles = makeStyles(() => ({
  root: {},
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
}));

function ReviewEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const objId = _.trim(window.location.pathname.slice(20, -4), '/');
  const dispatch = useDispatch();
  const { review } = useSelector((state) => {
    return state.review
  });
  const history = useHistory();

  useEffect(() => {
    dispatch(getOne(objId));
  }, [dispatch, objId]);

  const { customer } = useSelector((state) => {
    return state.customer;
  });

  // ID for test: e3721cf1-02cf-4045-a151-3417bb88054d
  useEffect(() => {
    dispatch(getOneCustomer(review.creator_id));
  }, [dispatch]);

  if (_.size(review) === 0) {
    return null;
  }

  const addDefaultSrc = ev => {
    ev.target.src = '/static/images/error-image-generic.png';
  };

  return (
    <Formik
      initialValues={{
        review: review.review || '',
        object_id: review.object_id || '',
        creator_id: review.creator_id || '',
        platform_key: review.platform_key || '',
        rating: review.rating || '',
        type: review.type || '',
        images: review.images || '',
        created_at: review.created_at || '',
      }}
      validationSchema={Yup.object().shape({
        review: Yup.string().max(255)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        if (window.confirm("Are you sure update the review?")) {
          try {
            // Make API request
            let valuesPush = {};
            valuesPush.review = values.review;

            dispatch(update({ ...valuesPush, id: objId })).then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar('Review updated', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(("/app/content/reviews"))
            });

          } catch (error) {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
          }
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>
                <Grid
                  container
                  spacing={3}
                >
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.object_id && errors.object_id)}
                      fullWidth
                      helperText={touched.object_id && errors.object_id}
                      label="Object"
                      name="object"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.object_id}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.type && errors.type)}
                      fullWidth
                      helperText={touched.type && errors.type}
                      label="Type"
                      name="type"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.type}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.rating && errors.rating)}
                      fullWidth
                      helperText={touched.rating && errors.rating}
                      label="Rating"
                      name="rating"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.rating}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.created_at && errors.created_at)}
                      fullWidth
                      helperText={touched.created_at && errors.created_at}
                      label="Created"
                      name="created_at"
                      disabled
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.created_at ? moment(values.created_at).format('DD/MM/YYYY') : ''}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <Typography variant="h5" color="textPrimary">
                      Customer
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {
                        customer ? (customer.first_name ? customer.first_name : '') + ' ' + (customer.last_name ? customer.last_name : '') : '-'
                      }
                    </Typography>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    {/*<Typography variant="h5" color="textPrimary">*/}
                      {/*Review visible*/}
                    {/*</Typography>*/}
                    {/*<Typography variant="body2" color="textSecondary">*/}
                      {/*This will give the review is visible to show*/}
                    {/*</Typography>*/}
                    {/*<Switch*/}
                      {/*checked={values.is_visible}*/}
                      {/*color="secondary"*/}
                      {/*edge="start"*/}
                      {/*name="is_visible"*/}
                      {/*onChange={handleChange}*/}
                      {/*value={values.is_visible}*/}
                    {/*/>*/}
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                      <TextField
                        error={Boolean(touched.review && errors.review)}
                        fullWidth
                        helperText={touched.review && errors.review}
                        label="Review"
                        name="review"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                        value={values.review}
                        variant="outlined"
                      />
                  </Grid>
                </Grid>
                <Grid
                  item
                  md={12}
                  xs={12}
                  mt={2}
                >
                  <br/>
                  <Typography variant="h5" color="textPrimary" mt={2} mb={2}>
                    Images list
                  </Typography>
                  <br/>
                  <GridList className={classes.gridList} cols={5}>
                    {
                      values.images ?
                        values.images.map((image) => (
                          <GridListTile key={image.id}>
                            {image.image_path ?
                              <img src={image.image_path} onError={addDefaultSrc}/>
                              :''
                            }
                          </GridListTile>
                        )) : 'No Image'
                    }
                  </GridList>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Update review
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

ReviewEditForm.propTypes = {
  className: PropTypes.string,
  review: PropTypes.object.isRequired
};

export default ReviewEditForm;
