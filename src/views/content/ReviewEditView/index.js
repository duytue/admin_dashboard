import React, {
  // useState,
  // useCallback,
  // useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import ReviewEditForm from './ReviewEditForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ReviewEditView() {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Review Edit"
    >
      <Container maxWidth={false}>
        <Header />
        <Box mt={3}>
          <ReviewEditForm />
        </Box>
      </Container>
    </Page>
  );
}

export default ReviewEditView;
