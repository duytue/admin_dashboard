import React from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import Page from 'src/components/Page';
import ProductForm from './ProductForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ProductFormView() {
  const classes = useStyles();
  const { productId } = useParams();

  return (
    <Page
      className={classes.root}
      title={productId ? 'Product Edit' : 'Product Create'}
    >
      <Container maxWidth="lg">
        <Header productId={productId} />
        <Box mt={3}>
          <ProductForm />
        </Box>
      </Container>
    </Page>
  );
}

export default ProductFormView;
