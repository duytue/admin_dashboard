/* eslint-disable max-len */
import React, { useEffect, useState } from 'react';
import moment from 'moment';
// import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Button,
  makeStyles,
  Tabs,
  Tab,
  Divider,
  Box,
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import _ from 'lodash';

// Actions
import { addItem, getItem, updateItem } from 'src/actions/productActions';
import { getListCategory } from 'src/actions/categoryActions';
import { addSku, udpateSku } from 'src/actions/skuActions';

import mediaService from 'src/services/mediaService';
// components
import TabPanel from './components/TabPanel';
import GeneralTab from './components/GeneralTab';
import CategoryTab from './components/CategoryTab';
import StorageAndTransportTab from './components/StorageAndTransportTab';
// import AttributesTab from './components/AttributesTab';
import SkuTab from './components/SkuTab';
import VariantTab from './components/VariantTab';
import PolicyTab from './components/PolicyTab';

const storageList = [
  {
    name: 'Dangerous',
    value: '5222a67f-bd20-4437-bd29-83929cd36d89'
  },
  {
    name: 'Fragile',
    value: 'a0b4af02-dd91-4e56-a901-88dbc6e6dc84'
  },
  {
    name: 'Perishable',
    value: '345-3453-3451145'
  },
  {
    name: 'Chilled',
    value: '469-43-00000'
  },
  {
    name: 'Liquid',
    value: '666-3453-345676'
  }
];

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

const useStyles = makeStyles((theme) => ({
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  fHeight: {
    height: '100%'
  },
  root: {
    flexGrow: 1,
    maxWidth: '100%',
    maxHeight: '100%',
    backgroundColor: theme.palette.background.paper
  },
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectedItem: {
    backgroundColor: '#1976d2',
    color: '#000'
  },
  borderList: {
    border: '1px solid #ccc',
    maxHeight: '400px',
    overflowY: 'auto'
  }
}));

function StockForm() {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const { productId } = useParams();
  const dispatch = useDispatch();

  // States
  const [tab, setTab] = useState(0);

  const { item } = useSelector((state) => state.product);
  const { user } = useSelector((state) => state.account);

  useEffect(() => {
    if (productId) {
      dispatch(getItem(productId));
    }
    dispatch(getListCategory());
  }, [dispatch, productId]);

  // useEffect(() => () => dispatch(clearStock()), []);

  const handleTabChange = (event, newValue) => {
    setTab(newValue);
  };

  const uploadImage = async (image, platformKey) => {
    const mediaCode = image.type.replace('image/', '').replace('video/', '').replace('audio/', '').toUpperCase();

    const preUploadParam = {
      creator_by: user.id,
      name: image.name,
      media_code: mediaCode,
      platform_key: platformKey,
    };

    try {
      const responsePre = await mediaService.preUpload(preUploadParam);

      if (responsePre) {
        const responseUpload = await mediaService.uploadFile(responsePre, image);

        if (responseUpload) {
          // Make API request
          preUploadParam.description = 'product image';

          const res = await mediaService.createUpload(preUploadParam);
          return res;
        }
      }
    } catch (err) {
      return null;
    }

    return null;
  };

  if (productId && _.isEmpty(item)) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        name: (item.names && item.names.en) || '',
        is_active: item.isActive || true,
        type: item.type || '',
        brand: item.brand || '',
        identifier: item.identifier || '',
        descriptions: (item.descriptions && (item.descriptions.en || item.descriptions.vn)) || '',
        category_ids: item.category_ids || [],
        payment_methods: item.payment_methods || [],
        priority: item.priority,
        storage_and_transport_attributes: item.storage_and_transport_attributes || [],
        policy_ids: item.policy_ids || [],
        attributes: [],
        platform_key: user.list_platform[0].key,
        color: '',
        size: '',
        origin: item.origin || '',
        unit_of_measurement: (item.skuItems && item.skuItems[0] && item.skuItems[0].unit_of_measurement) || '',
        package_weight: (item.skuItems && item.skuItems[0] && item.skuItems[0].package_weight) || '',
        pack_size: (item.skuItems && item.skuItems[0] && item.skuItems[0].pack_size) || '',
        package_height: (item.skuItems && item.skuItems[0] && item.skuItems[0].package_height) || '',
        package_length: (item.skuItems && item.skuItems[0] && item.skuItems[0].package_length) || '',
        package_width: (item.skuItems && item.skuItems[0] && item.skuItems[0].package_width) || '',
        tax_rate: (item.skuItems && item.skuItems[0] && item.skuItems[0].tax_rate) || '',
        cogs: (item.skuItems && item.skuItems[0] && item.skuItems[0].cogs) || '',
        normal_price: (item.skuItems && item.skuItems[0] && item.skuItems[0].normal_price) || '',
        selling_price: (item.skuItems && item.skuItems[0] && item.skuItems[0].selling_price) || '',
        selling_price_start_time: (item.skuItems && item.skuItems[0] && item.skuItems[0].selling_price_start_time) || moment(),
        selling_price_end_time: (item.skuItems && item.skuItems[0] && item.skuItems[0].selling_price_end_time) || moment(),
        manufacturing_barcode: (item.skuItems && item.skuItems[0] && item.skuItems[0].manufacturing_barcode) || '',
        variant_values: '',
        attribute_values: [],
        level: '1',
        parent_id: '',
        images: (item.skuItems && item.skuItems[0] && item.skuItems[0].images && item.skuItems[0].images[0]) || '',
      }}
      // validationSchema={Yup.object().shape({
      //   name: Yup.string().required(),
      //   type: Yup.string().required(),
      //   brand: Yup.string().required(),
      //   descriptions: Yup.string(),
      //   priority: Yup.number().required(),
      //   identifier: Yup.number().required(),
      //   sku: Yup.string().required(),
      //   unit_of_measurement: Yup.string().required(),
      //   pack_size: Yup.number().required(),
      //   package_weight: Yup.number().required(),
      //   package_height: Yup.number().required(),
      //   package_length: Yup.number().required(),
      //   package_width: Yup.number().required(),
      //   tax_rate: Yup.number().required(),
      //   selling_price: Yup.number().required(),
      //   cogs: Yup.number().required(),
      //   normal_price: Yup.number().required(),
      //   manufacturing_barcode: Yup.string().required(),
      // })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          let img = [values.images] || [];
          const initImg = (item.skuItems && item.skuItems[0] && item.skuItems[0].images && item.skuItems[0].images[0]) || '';
          if (values.images !== initImg) {
            const res = await uploadImage(values.images, values.platform_key);
            img = [res.url];
          }

          if (!productId) {
            const data = {
              names: { en: values.name, vn: values.name },
              is_active: values.is_active,
              identifier: values.identifier,
              type: values.type,
              brand: values.brand,
              descriptions: { en: values.descriptions, vn: values.descriptions },
              category_ids: values.category_ids,
              payment_methods: values.payment_methods,
              storage_and_transport_attributes: values.storage_and_transport_attributes,
              attributes: values.attributes,
              priority: values.priority,
              platform_key: values.platform_key,
              policy_ids: values.policy_ids,
              origin: values.origin,
              images: img
            };
            dispatch(addItem(data)).then((result) => {
              const skuData = {
                source_sku: values.sku,
                unit_of_measurement: values.unit_of_measurement,
                package_weight: parseFloat(values.package_weight),
                pack_size: parseFloat(values.pack_size),
                package_height: parseFloat(values.package_height),
                package_length: parseFloat(values.package_length),
                package_width: parseFloat(values.package_width),
                tax_rate: parseFloat(values.tax_rate),
                cogs: parseFloat(values.cogs),
                normal_price: parseFloat(values.normal_price),
                selling_price: parseFloat(values.selling_price),
                selling_price_start_time: values.selling_price_start_time,
                selling_price_end_time: values.selling_price_end_time,
                manufacturing_barcode: values.manufacturing_barcode,
                variant_values: [{
                  name: 'product_name',
                  value: values.variant_values
                }],
                attribute_values: values.attribute_values,
                item_id: result.id,
                platform_key: values.platform_key,
                images: img,
              };

              dispatch(addSku(skuData)).then(() => {
                resetForm();
                setStatus({ success: true });
                setSubmitting(false);
                enqueueSnackbar('Item created', {
                  variant: 'success',
                  action: <Button>See all</Button>
                });
                history.push('/app/content/product-management');
              });
            });
          } else {
            const data = {
              names: { en: values.name, vn: values.name },
              is_active: values.is_active,
              identifier: values.identifier,
              type: values.type,
              brand: values.brand,
              descriptions: { en: values.descriptions, vn: values.descriptions },
              category_ids: values.category_ids,
              payment_methods: values.payment_methods,
              storage_and_transport_attributes: values.storage_and_transport_attributes,
              attributes: values.attributes,
              priority: values.priority,
              policy_ids: values.policy_ids,
              origin: values.origin,
              platform_key: values.platform_key,
              images: img,
            };
            dispatch(updateItem({ ...data, id: productId })).then((result) => {
              const skuData = {
                source_sku: values.sku,
                unit_of_measurement: values.unit_of_measurement,
                package_weight: parseFloat(values.package_weight),
                pack_size: parseFloat(values.pack_size),
                package_height: parseFloat(values.package_height),
                package_length: parseFloat(values.package_length),
                package_width: parseFloat(values.package_width),
                tax_rate: parseFloat(values.tax_rate),
                cogs: parseFloat(values.cogs),
                normal_price: parseFloat(values.normal_price),
                selling_price: parseFloat(values.selling_price),
                selling_price_start_time: values.selling_price_start_time,
                selling_price_end_time: values.selling_price_end_time,
                manufacturing_barcode: values.manufacturing_barcode,
                attribute_values: values.attribute_values,
                variant_values: [{
                  name: 'product_name',
                  value: values.variant_values
                }],
                item_id: result.id,
                platform_key: values.platform_key,
                images: img,
              };

              if (item.skuItems && item.skuItems[0] && item.skuItems[0].id) {
                skuData.id = item.skuItems[0].id;
                dispatch(udpateSku(skuData)).then(() => {
                  resetForm();
                  setStatus({ success: true });
                  setSubmitting(false);
                  enqueueSnackbar('Item updated', {
                    variant: 'success',
                    action: <Button>See all</Button>
                  });
                  history.push('/app/content/product-management');
                });
              } else {
                dispatch(addSku(skuData)).then(() => {
                  resetForm();
                  setStatus({ success: true });
                  setSubmitting(false);
                  enqueueSnackbar('Item updated', {
                    variant: 'success',
                    action: <Button>See all</Button>
                  });
                  history.push('/app/content/product-management');
                });
              }
            });
          }
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
          enqueueSnackbar('Submit Error', {
            variant: 'error',
            action: <Button>See all</Button>
          });
        }
      }}
    >
      {({
        handleSubmit,
        values,
        errors,
        handleBlur,
        handleChange,
        setFieldValue,
        touched,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>
          <Tabs
            value={tab}
            onChange={handleTabChange}
            aria-label="simple tabs example"
            textColor="primary"
            variant="fullWidth"
            indicatorColor="primary"
          >
            <Tab label="General" {...a11yProps(0)} />
            <Tab label="Category" {...a11yProps(1)} />
            <Tab label="Storage & Transport" {...a11yProps(2)} />
            {/* <Tab label="Attributes" {...a11yProps(3)} /> */}
            <Tab label="Variant" {...a11yProps(4)} />
            <Tab label="SKU" {...a11yProps(5)} />
            <Tab label="Policy" {...a11yProps(6)} />
          </Tabs>
          <Divider />
          <TabPanel value={tab} index={0}>
            <GeneralTab
              touched={touched}
              errors={errors}
              handleBlur={handleBlur}
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              values={values}
            />
          </TabPanel>
          <TabPanel value={tab} index={1}>
            <CategoryTab
              values={values}
              setFieldValue={setFieldValue}
            />
          </TabPanel>
          <TabPanel value={tab} index={2}>
            <StorageAndTransportTab
              setFieldValue={setFieldValue}
              values={values}
              storageList={storageList}
            />
          </TabPanel>
          {/* <TabPanel value={value} index={3}>
          <AttributesTab values={values} />
        </TabPanel> */}
          <TabPanel value={tab} index={3}>
            <VariantTab
              value={tab}
              handleTabChange={handleTabChange}
            />
          </TabPanel>
          <TabPanel value={tab} index={4}>
            <SkuTab
              touched={touched}
              errors={errors}
              handleBlur={handleBlur}
              handleChange={handleChange}
              values={values}
              setFieldValue={setFieldValue}
              classes={classes}
            />
          </TabPanel>
          <TabPanel value={tab} index={5}>
            <PolicyTab values={values} setFieldValue={setFieldValue} />
          </TabPanel>
          <Box display="flex" justifyContent="flex-end">
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              disabled={isSubmitting}
            >
              Submit
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
}

export default StockForm;
