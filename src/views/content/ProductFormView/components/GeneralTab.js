import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Button,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import _ from 'lodash';
import DraftEditor from 'src/components/DraftEditor';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectBirthDay: {
    width: '100%',
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  buttonArrowDropDown: {
    minWidth: 0,
  },
  next: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  draft: {
    backgroundColor: 'inherit'
  }
}));

const allowedPaymentMethods = [
  {
    id: 1,
    type: 'All'
  },
  {
    id: 2,
    type: 'VNPay'
  },
  {
    id: 3,
    type: 'Momo'
  },
  {
    id: 4,
    type: 'COD'
  }
];

function GeneralTab({
  touched,
  errors,
  handleBlur,
  handleChange,
  values,
  setFieldValue
}) {
  const classes = useStyles();
  const [openAllowedPaymentMethodSelect, setOpenAllowedPaymentMethodSelect] = useState(false);

  const getEditorState = (value) => {
    const blocksFromHtml = htmlToDraft(value);
    const { contentBlocks, entityMap } = blocksFromHtml;
    const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
    const editorState = EditorState.createWithContent(contentState);
    return editorState;
  };

  const initDesc = (values.descriptions && getEditorState(values.descriptions)) || '';

  const onDescChange = (event) => {
    const value = draftToHtml(convertToRaw(event.getCurrentContent()));

    // setBodies(event);
    setFieldValue('descriptions', value);
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item md={6} xs={12}>
          <TextField
            error={Boolean(touched.name && errors.name)}
            fullWidth
            helperText={touched.name && errors.name}
            label="Name"
            name="name"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            value={values.name}
            variant="outlined"
          />
        </Grid>
        <Grid item md={6} xs={12} className={classes.is_active}>
          <Typography variant="h5" color="textPrimary">Active</Typography>
          <Switch
            checked={values.is_active}
            color="secondary"
            edge="start"
            name="is_active"
            onChange={handleChange}
            value={values.is_active}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            error={Boolean(touched.type && errors.type)}
            fullWidth
            helperText={touched.type && errors.type}
            label="Type"
            name="type"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            value={values.type}
            variant="outlined"
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            error={Boolean(touched.brand && errors.brand)}
            fullWidth
            helperText={touched.brand && errors.brand}
            label="Brand"
            name="brand"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            value={values.brand}
            variant="outlined"
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <InputLabel shrink id="payment_methods">
            Allowed Payment Methods
          </InputLabel>
          <Select
            fullWidth
            multiple
            labelId="payment_methods_lbl"
            id="allowed_payment_method"
            label="Allowed Payment Methods"
            name="payment_methods"
            value={values.payment_methods}
            open={openAllowedPaymentMethodSelect}
            onClose={() => setOpenAllowedPaymentMethodSelect(false)}
            IconComponent={() => (
              <Button
                className={classes.buttonArrowDropDown}
                onClick={() => setOpenAllowedPaymentMethodSelect(true)}
              >
                <ArrowDropDownIcon />
              </Button>
            )}
            onChange={handleChange}
            renderValue={(selected) => (
              <div className={classes.chips}>
                {selected.map((item) => (
                  <Chip
                    key={item}
                    label={item}
                    onDelete={() => setFieldValue('payment_methods', _.reject(values.payment_methods, (i) => i === item))}
                    className={classes.chip}
                  />
                ))}
              </div>
            )}
            className={clsx(classes.selectEmpty)}
          >
            {
              allowedPaymentMethods && allowedPaymentMethods.map((item) => (
                <MenuItem key={item.id} value={item.type}>
                  <Checkbox checked={values.payment_methods.indexOf(item.type) > -1} />
                  <ListItemText primary={item.type} />
                </MenuItem>
              ))
            }
          </Select>
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            error={Boolean(touched.priority && errors.priority)}
            fullWidth
            helperText={touched.priority && errors.priority}
            label="Priority"
            name="priority"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.priority}
            variant="outlined"
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <TextField
            error={Boolean(touched.identifier && errors.identifier)}
            fullWidth
            helperText={touched.identifier && errors.identifier}
            label="Product Identifier"
            name="identifier"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            value={values.identifier}
            variant="outlined"
          />
        </Grid>
        <Grid item md={12} xs={12} className={classes.draft}>
          <DraftEditor defaultEditorState={initDesc} onEditorStateChange={onDescChange} />
        </Grid>
      </Grid>
    </>
  );
}

GeneralTab.propTypes = {
  touched: PropTypes.object,
  errors: PropTypes.object,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
  values: PropTypes.object,
  setFieldValue: PropTypes.func
};

export default GeneralTab;
