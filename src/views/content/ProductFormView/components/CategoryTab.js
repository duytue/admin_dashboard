/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import CheckboxTree from 'react-checkbox-tree';
import {
  Grid,
  makeStyles
} from '@material-ui/core';
import {
  ChevronRight,
  ChevronDown,
} from 'react-feather';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';

const useStyles = makeStyles(() => ({
  root: {
    '.rct-title': {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    }
  }
}));

function CategoryTab({ values, setFieldValue }) {
  const classes = useStyles();
  const [checked, setChecked] = useState(values.category_ids || []);
  const [expanded, setExpanded] = useState([]);

  const categories = useSelector((state) => state.category.categories.filter((item) => item.level < 3));
  const groupedCate = _.groupBy(categories, 'parent_id');

  const formattedCate = [];
  groupedCate.null.forEach((cate) => {
    const children = [];

    const ret = {
      ...cate,
      value: cate.id,
      label: <span style={{ fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif' }}>{cate.name.vi || cate.name.vn || cate.name.en}</span>,
    };

    if (groupedCate[cate.id]) {
      groupedCate[cate.id].forEach((childCate) => {
        const retChild = {
          ...childCate,
          value: childCate.id,
          label: <span style={{ fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif' }}>{childCate.name.vi || childCate.name.vn || childCate.name.en}</span>,
        };

        children.push(retChild);
      });
      ret.children = children;
    }

    formattedCate.push(ret);
  });

  const onCheck = (val) => {
    setFieldValue('category_ids', val);
    setChecked(val);
  };

  return (
    <Grid container spacing={3}>
      <Grid item md={12} xs={12}>
        <CheckboxTree
          className={classes.root}
          nodes={formattedCate}
          checked={checked}
          expanded={expanded}
          onCheck={(val) => onCheck(val)}
          onExpand={(val) => setExpanded(val)}
          nativeCheckboxes
          icons={{
            expandClose: <ChevronRight />,
            expandOpen: <ChevronDown />,
            expandAll: null,
            collapseAll: null,
            parentClose: null,
            parentOpen: null,
            leaf: null,
          }}
        />
      </Grid>
    </Grid>
  );
}

CategoryTab.propTypes = {
  values: PropTypes.object,
  setFieldValue: PropTypes.func,
};

export default CategoryTab;
