/* eslint-disable max-len */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import {
  Grid, TextField, MenuItem, Divider, Checkbox, IconButton, Paper, InputLabel, Select, SvgIcon
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useSnackbar } from 'notistack';
import {
  XCircle as DeleteIcon,
} from 'react-feather';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import addCurrentTimeStampToFileName from 'src/utils/dateString';

export default function SkuTab({
  touched,
  errors,
  handleBlur,
  handleChange,
  values,
  setFieldValue,
  classes
}) {
  const { enqueueSnackbar } = useSnackbar();
  const { variant } = useSelector((state) => state.variant);
  const [fileUrl, setFileUrl] = useState(values.images || null);

  const fileToBase64 = (file) => {
    const reader = new FileReader();
    const newFileName = addCurrentTimeStampToFileName(file.name);
    Object.defineProperty(file, 'name', {
      writable: true,
      value: newFileName
    });
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setFileUrl(reader.result);
    };
  };

  const onClearFile = () => {
    setFileUrl(null);
    setFieldValue('images', null);
  };

  const handleChangeFile = (event) => {
    if (event.target.files.length !== 0) {
      const file = event.target.files[0];
      const { type } = file;
      if (type.indexOf('image/') >= 0) {
        fileToBase64(file);
        setFieldValue('images', file);
      } else if (type.indexOf('audio/') >= 0) {
        setFieldValue('images', file);
        setFileUrl('/static/images/icon/icon_mp3.png');
      } else if (type.indexOf('video/') >= 0) {
        setFieldValue('images', file);
        setFileUrl('/static/images/icon/icon_video.png');
      } else {
        enqueueSnackbar('Only image / audio / video files are allowed', {
          variant: 'error'
        });
      }
    }
  };

  function renderVariant() {
    switch (variant.input_type) {
      case 'list':
        return (
          <TextField
            error={Boolean(touched.variant_values && errors.variant_values)}
            fullWidth
            helperText={touched.variant_values && errors.variant_values}
            label="Variant"
            name="variant_values"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.variant_values}
            variant="standard"
          />
        );
      case 'text':
        return (
          <TextField
            error={Boolean(touched.variant_values && errors.variant_values)}
            fullWidth
            helperText={touched.variant_values && errors.variant_values}
            label="Variant"
            name="variant_values"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.variant_values}
            variant="standard"
          />
        );
      case 'boolean':
        return (
          <Checkbox
            checked={values.variant_values}
            name="variant_values"
            onChange={handleChange}
          />
        );
      case 'date':
        return (
          <KeyboardDatePicker
            disableToolbar
            inputVariant="standard"
            format="DD/MM/YYYY"
            label="variant_date"
            value={values.variant_values}
            onChange={(value) => setFieldValue('variant_date', value)}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        );
      default:
        return null;
    }
  }

  const unitOfMesurement = [
    {
      id: 1,
      name: 'Piece'
    },
    {
      id: 2,
      name: 'Box'
    },
    {
      id: 3,
      name: 'g'
    },
    {
      id: 4,
      name: 'Kg'
    },
  ];

  const taxRate = [
    {
      id: 1,
      name: 'Free Tax',
      value: 0,
    },
    {
      id: 2,
      name: '1%',
      value: 1,
    },
    {
      id: 3,
      name: '5%',
      value: 5,
    },
    {
      id: 4,
      name: '10%',
      value: 10,
    },
    {
      id: 5,
      name: '15%',
      value: 15,
    },
  ];

  return (
    <>
      <Grid item md={12} xs={12}>
        <Grid item md={12} xs={12} style={{ paddingBottom: '15px' }}>
          <Typography>VARIANT</Typography>
        </Grid>
        <Grid item md={12} xs={12} style={{ display: 'flex' }}>
          {renderVariant()}
        </Grid>
      </Grid>
      <Divider style={{ marginTop: '15px', marginBottom: '15px' }} />
      <Grid item md={12} xs={12}>
        <Grid item md={12} xs={12}>
          <Typography variant="body1">Origin</Typography>
        </Grid>
        <Grid item md={12} xs={12} style={{ display: 'flex' }}>
          <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
            <TextField
              error={Boolean(touched.origin && errors.origin)}
              fullWidth
              helperText={touched.origin && errors.origin}
              label="Origin"
              name="origin"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.origin}
              variant="standard"
            />
          </Grid>
        </Grid>
        <Divider style={{ marginTop: '15px', marginBottom: '15px' }} />
        <Grid item md={12} xs={12}>
          <Grid item md={12} xs={12}>
            <Typography variant="body1">DIMENSION</Typography>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
              <InputLabel shrink id="unit_of_measurement">Unit Of Mesurement</InputLabel>
              <Select
                labelId="unit_of_measurement"
                fullWidth
                id="unit_of_measurement"
                label="unit_of_measurement"
                name="unit_of_measurement"
                value={values.unit_of_measurement}
                onChange={handleChange}
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {
                  unitOfMesurement && unitOfMesurement.map((item) => (
                    <MenuItem key={item.id} value={item.name}>{item.name}</MenuItem>
                  ))
                }
              </Select>
            </Grid>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <TextField
                error={Boolean(touched.pack_size && errors.pack_size)}
                fullWidth
                helperText={touched.pack_size && errors.pack_size}
                label="Pack Size"
                name="pack_size"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.pack_size}
                variant="standard"
              />
            </Grid>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
              <TextField
                error={Boolean(touched.package_weight && errors.package_weight)}
                fullWidth
                helperText={touched.package_weight && errors.package_weight}
                label="Package Weight"
                name="package_weight"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.package_weight}
                variant="standard"
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <TextField
                error={Boolean(touched.package_height && errors.package_height)}
                fullWidth
                helperText={touched.package_height && errors.package_height}
                label="Package Height"
                name="package_height"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.package_height}
                variant="standard"
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <TextField
                error={Boolean(touched.package_width && errors.package_width)}
                fullWidth
                helperText={touched.package_width && errors.package_width}
                label="Package Width"
                name="package_width"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.package_width}
                variant="standard"
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <TextField
                error={Boolean(touched.package_length && errors.package_length)}
                fullWidth
                helperText={touched.package_length && errors.package_length}
                label="Package Length"
                name="package_length"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.package_length}
                variant="standard"
              />
            </Grid>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: '15px', marginBottom: '15px' }} />
        <Grid item md={12} xs={12}>
          <Grid item md={12} xs={12}>
            <Typography>PRICE</Typography>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
              <InputLabel shrink id="tax_rate">Tax Rate</InputLabel>
              <Select
                labelId="tax_rate"
                fullWidth
                id="tax_rate"
                label="tax_rate"
                name="tax_rate"
                value={values.tax_rate}
                onChange={handleChange}
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {
                  taxRate && taxRate.map((item) => (
                    <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                  ))
                }
              </Select>
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <TextField
                error={Boolean(touched.selling_price && errors.selling_price)}
                fullWidth
                helperText={touched.selling_price && errors.selling_price}
                label="Selling Price"
                name="selling_price"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.selling_price}
                variant="standard"
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <KeyboardDatePicker
                disableToolbar
                inputVariant="standard"
                format="DD/MM/YYYY"
                label="from"
                value={values.selling_price_start_time}
                onChange={(value) => setFieldValue('selling_price_start_time', value)}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <KeyboardDatePicker
                disableToolbar
                inputVariant="standard"
                format="DD/MM/YYYY"
                label="to"
                value={values.selling_price_end_time}
                onChange={(value) => setFieldValue('selling_price_end_time', value)}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </Grid>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
              <TextField
                error={Boolean(touched.cogs && errors.cogs)}
                fullWidth
                helperText={touched.cogs && errors.cogs}
                label="COGS"
                name="cogs"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.cogs}
                variant="standard"
              />
            </Grid>
            {/* <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px', marginLeft: '15px' }}>
              <Button variant="contained" color="secondary">add selling price</Button>
            </Grid> */}
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            <Grid item md={6} xs={12} style={{ marginTop: '15px', marginBottom: '15px' }}>
              <TextField
                error={Boolean(touched.normal_price && errors.normal_price)}
                fullWidth
                helperText={touched.normal_price && errors.normal_price}
                label="Normal Price"
                name="normal_price"
                onBlur={handleBlur}
                onChange={handleChange}
                value={values.normal_price}
                variant="standard"
              />
            </Grid>
          </Grid>
        </Grid>
        <Divider style={{ marginTop: '15px', marginBottom: '15px' }} />
        <Grid md={12} xs={12}>
          <Grid item md={12} xs={12} style={{ marginBottom: '20px' }}>
            <Typography>IMAGE</Typography>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex' }}>
            {/* input file 1 */}
            {fileUrl && (
              <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                <img
                  src={fileUrl}
                  style={{ maxWidth: '150px', border: '1px solid' }}
                  alt="test"
                />
                <DeleteIcon
                  style={{
                    color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20
                  }}
                  onClick={onClearFile}
                />
              </Paper>
            )}
            <input
              name="file"
              className={classes.input}
              id="contained-button-file"
              type="file"
              style={{ display: 'none' }}
              onChange={handleChangeFile}
            />
            <label htmlFor="contained-button-file" style={{ paddingRight: '15px' }}>
              {!fileUrl && (
                <IconButton
                  variant="contained"
                  color="primary"
                  component="span"
                  className={clsx(classes.button, classes.margin)}
                >
                  <SvgIcon fontSize="large">
                    <AccountCircleOutlinedIcon />
                  </SvgIcon>
                </IconButton>
              )}
            </label>
            {/* input file 1 */}
          </Grid>
        </Grid>
        <Divider style={{ marginTop: '20px', marginBottom: '15px' }} />
        <Grid md={12} xs={12}>
          <Grid item md={12} xs={12} style={{ marginBottom: '20px' }}>
            <Typography>BAR CODE</Typography>
          </Grid>
          <Grid item md={12} xs={12} style={{ display: 'flex', marginBottom: '20px' }}>
            <TextField
              error={Boolean(touched.manufacturing_barcode && errors.manufacturing_barcode)}
              fullWidth
              helperText={touched.manufacturing_barcode && errors.manufacturing_barcode}
              label="Barcode"
              name="manufacturing_barcode"
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.manufacturing_barcode}
              variant="standard"
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

SkuTab.propTypes = {
  touched: PropTypes.object,
  errors: PropTypes.object,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
  values: PropTypes.object,
  setFieldValue: PropTypes.func,
  classes: PropTypes.object
};
