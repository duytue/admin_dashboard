import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Typography,
  Checkbox,
  FormControlLabel
} from '@material-ui/core';

function StorageAndTransportTab({ setFieldValue, values, storageList }) {
  // eslint-disable-next-line max-len
  const [selectedStorages, setselectedStorages] = useState(values.storage_and_transport_attributes || []);

  useEffect(() => {
    setFieldValue('storage_and_transport_attributes', selectedStorages);
  }, [selectedStorages]);

  const handleSelectOneStorage = (event, storageId) => {
    if (!selectedStorages.includes(storageId)) {
      setselectedStorages((prevSelected) => [...prevSelected, storageId]);
    } else {
      setselectedStorages((prevSelected) => prevSelected.filter((id) => id !== storageId));
    }
  };

  return (
    <Grid container spacing={3}>
      <Typography variant="h6" color="textPrimary">
        Please select Storage & Transport attributes for the Product (multi selection)
      </Typography>
      {storageList && storageList.map((item) => {
        const isStorageSelected = selectedStorages.includes(item.name);

        return (
          <Grid item md={12} xs={12} key={item.value}>
            <FormControlLabel
              control={(
                <Checkbox
                  checked={isStorageSelected}
                  onChange={(event) => handleSelectOneStorage(event, item.name)}
                  value={isStorageSelected}
                />
              )}
              label={item.name}
            />
          </Grid>
        );
      })}
    </Grid>
  );
}

StorageAndTransportTab.propTypes = {
  setFieldValue: PropTypes.func,
  values: PropTypes.object,
  storageList: PropTypes.array
};

export default StorageAndTransportTab;
