/* eslint-disable max-len */
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Button,
  Grid,
  ListItemText,
  Dialog,
  DialogTitle,
  Typography,
  DialogContent,
  ListItem,
  DialogActions,
  List,
  TextField
} from '@material-ui/core';
import {
  ChevronRight as ArrowIcon
} from 'react-feather';

function CategoryTab({
  classes, values, handleChange, setFieldValue
}) {
  const [open, setOpen] = useState(false);
  const [selectedParentLv1, setSelectedParentLv1] = useState([]);
  const [selectedParentLv2, setSelectedParentLv2] = useState([]);
  const [listParentLv2, setListParentLv2] = useState([]);
  const [selectedParent, setSelectedParent] = useState(null);
  const categories = useSelector((state) => state.category.categories.filter((item) => item.level < 3));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (e, value) => {
    setOpen(false);
    setFieldValue('category_ids', selectedParent.id);
    if (value === false) {
      setSelectedParentLv1(null);
      setSelectedParentLv2(null);
      setSelectedParent(null);
      setListParentLv2([]);
    }
  };

  const handleSelectedParentLv1 = (event, item) => {
    const listItemLvl2 = categories.filter((element) => element.parent_id === item.id);
    if (listItemLvl2.length > 0) {
      setSelectedParent(item);
      setListParentLv2(listItemLvl2);
    } else {
      setSelectedParentLv1([...selectedParentLv1, item.id]);
    }
  };

  const handleSelectedParentLv2 = (event, item) => {
    setSelectedParent(item);
    setSelectedParentLv2(item.id);
  };

  const isHasChidren = (item) => {
    const array = categories.filter((element) => element.parent_id === item.id);
    return array.length > 0;
  };

  return (
    <Grid container spacing={3}>
      <Grid item md={12} xs={12}>
        <TextField
          fullWidth
          label={selectedParent ? (selectedParent.name.en || selectedParent.name.vn) : 'Select category'}
          name="category_ids"
          renderValue={() => (
            <div className={classes.chips}>
              {/* {selected.map((item) => (
                <Chip
                  key={item}
                  label={item}
                  onDelete={() => setFieldValue('category_ids', _.reject(values.category_ids, (i) => i === item))}
                  className={classes.chip}
                />
              ))} */}
            </div>
          )}
          variant="outlined"
          onClick={handleClickOpen}
        />
        <Dialog
          open={open}
          keepMounted
          onClose={handleClose}
          aria-labelledby="alert-dialog-slide-title"
          fullWidth
        >
          <DialogTitle id="alert-dialog-slide-title">
            <Typography variant="h3" color="textPrimary">
              Select the categories
            </Typography>
          </DialogTitle>
          <DialogContent style={{ display: 'flex' }}>
            <Grid item lg={6} md={6} xs={12} className={classes.borderList}>
              <List>
                {
                  categories && categories.map((item) => (
                    item.level === 1 && (
                      <ListItem
                        button
                        className={selectedParentLv1.includes(item.id) ? classes.selectedItem : null}
                        onClick={(e) => handleSelectedParentLv1(e, item)}
                      >
                        <ListItemText primary={`${item.name.en || item.name.vn}`} />
                        {isHasChidren(item) && <ArrowIcon />}
                      </ListItem>
                    )))
                }
              </List>
            </Grid>
            <Grid item lg={6} md={6} xs={12} className={classes.borderList}>
              <List>
                {
                  listParentLv2 && listParentLv2.map((item) => (
                    <ListItem
                      button
                      className={item.id === selectedParentLv2 ? classes.selectedItem : null}
                      onClick={(e) => handleSelectedParentLv2(e, item)}
                    >
                      <ListItemText primary={`${item.name.en || item.name.vn}`} />
                    </ListItem>
                  ))
                }
              </List>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={(e) => handleClose(e, false)} color="primary">
              Remove
            </Button>
            <Button onClick={(e) => handleClose(e, true)} color="primary" disabled={selectedParent === null}>
              Accept
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    </Grid>
  );
}

CategoryTab.propTypes = {
  classes: PropTypes.object,
  values: PropTypes.object,
  handleChange: PropTypes.func
};

export default CategoryTab;
