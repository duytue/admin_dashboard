import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Editor } from 'react-draft-wysiwyg';
import { makeStyles } from '@material-ui/core';
import { EditorState, convertToRaw, convertFromHTML } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const useStyles = makeStyles((theme) => ({
  root: {
    fontFamily: theme.typography.fontFamily,
    '& .rdw-option-wrapper': {
      background: 'transparent',
      border: 'none',
      minWidth: 26,
      padding: 6,
      '&:hover': {
        boxShadow: 'none',
        backgroundColor: theme.palette.action.hover
      }
    },
    '& .rdw-option-active': {
      boxShadow: 'none',
      backgroundColor: theme.palette.action.selected
    },
    '& .rdw-dropdown-wrapper': {
      boxShadow: 'none',
      background: 'transparent'
    },
    '& .rdw-dropdown-optionwrapper': {
      overflowY: 'auto',
      boxShadow: theme.shadows[10],
      padding: theme.spacing(1)
    }
  },
  toolbar: {
    marginBottom: 0,
    borderLeft: 'none',
    borderTop: 'none',
    borderRight: 'none',
    borderBottom: `1px solid ${theme.palette.divider}`,
    background: 'transparent'
  },
  editor: {
    padding: theme.spacing(2),
    color: theme.palette.text.primary
  }
}));

function DraftEditor({ className, values, ...rest }) {
  const classes = useStyles();
  const [editorStateValue, setEditorState] = useState(EditorState.createEmpty());

  console.log('values.descriptions', values.descriptions);

  // if (values.descriptions) {
  //   setEditorState(values.descriptions);
  // }

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
  };

  console.log('onEditorStateChange -> editorState', draftToHtml(convertToRaw(editorStateValue.getCurrentContent())));
  values.descriptions = draftToHtml(convertToRaw(editorStateValue.getCurrentContent()));

  return (
    <Editor
      editorState={editorStateValue}
      onEditorStateChange={onEditorStateChange}
      wrapperClassName={clsx(classes.root, className)}
      toolbarClassName={classes.toolbar}
      editorClassName={classes.editor}
      {...rest}
    />
  );
}

DraftEditor.propTypes = {
  className: PropTypes.string,
  values: PropTypes.object
};

export default DraftEditor;
