import React, { useState } from 'react';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {
  Divider, Box, Button, DialogActions
} from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { addItem } from 'src/actions/itemManagementAction';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { addSku } from 'src/actions/skuActions';
import skuService from 'src/services/skuService';
import mediaService from 'src/services/mediaService';
import GeneralTab from './GeneralTab';
import CategoryTab from './CategoryTab';
import StorageAndTransportTab from './StorageAndTransportTab';
import AttributesTab from './AttributesTab';
import SkuTab from './SkuTab';
import VariantTab from './VariantTab';
import { create } from 'src/actions/mediaActions';

function TabPanel(props) {
  const {
    children, value, index, ...other
  } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: '100%',
    maxHeight: '100%',
    backgroundColor: theme.palette.background.paper
  }
}));

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

export default function ItemEditorTabs({ handleClickClose }) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { items } = useSelector((state) => state.item);
  const [newFile, setNewFile] = useState(null);
  const [newFile2, setNewFile2] = useState(null);
  const [newFile3, setNewFile3] = useState(null);
  const { user } = useSelector((state) => state.account);

  const handleTabChange = (event, newValue) => {
    setValue(newValue);
  };

  const storageList = [
    {
      name: 'Dangerous',
      value: '5222a67f-bd20-4437-bd29-83929cd36d89'
    },
    {
      name: 'Fragile',
      value: 'a0b4af02-dd91-4e56-a901-88dbc6e6dc84'
    },
    {
      name: 'Perishable',
      value: '345-3453-3451145'
    },
    {
      name: 'Chilled',
      value: '469-43-00000'
    },
    {
      name: 'Liquid',
      value: '666-3453-345676'
    }
  ];

  return (
    <div className={clsx(classes.root)}>
      <Formik
        initialValues={{
          name: '',
          is_active: true,
          identifier: '',
          type: '',
          brand: '',
          descriptions: '',
          category_ids: [],
          payment_methods: [],
          storage_and_transport_attributes: [],
          attributes: [],
          priority: '',
          platform_key: '',
          color: '',
          size: '',
          origin: '',
          unit_of_measurement: '',
          package_weight: '',
          pack_size: '',
          package_height: '',
          package_length: '',
          package_width: '',
          tax_rate: '',
          cogs: '',
          normal_price: '',
          selling_price: '',
          selling_price_start_time: moment(),
          selling_price_end_time: moment(),
          manufacturing_barcode: '',
          variant_values: '',
          attribute_values: [],
          selectedCategories: []
        }}
        onSubmit={async (values, {
          resetForm,
          setErrors,
          setStatus,
          setSubmitting
        }) => {
          try {
            console.log('item add values', values);
            const data = {
              names: { en: values.name, vn: values.name },
              is_active: values.is_active,
              identifier: values.identifier,
              type: values.type,
              brand: values.brand,
              descriptions: { en: values.descriptions, vn: values.descriptions },
              category_ids: values.category_ids,
              payment_methods: values.payment_methods,
              storage_and_transport_attributes: values.storage_and_transport_attributes,
              attributes: values.attributes,
              priority: values.priority,
              platform_key: 'centralweb'
            };

            if (value >= 5) {
              console.log('item add data', data);
              const mediaCode = newFile.type.replace('image/', '').replace('video/', '').replace('audio/', '').toUpperCase();
              const mediaCode2 = newFile2.type.replace('image/', '').replace('video/', '').replace('audio/', '').toUpperCase();
              const mediaCode3 = newFile3.type.replace('image/', '').replace('video/', '').replace('audio/', '').toUpperCase();

              const preUploadParam = {
                creator_by: user.id,
                name: newFile.name,
                media_code: mediaCode,
                platform_key: 'centralweb'
              };

              const preUploadParam2 = {
                creator_by: user.id,
                name: newFile2.name,
                media_code: mediaCode2,
                platform_key: 'centralweb'
              };

              const preUploadParam3 = {
                creator_by: user.id,
                name: newFile3.name,
                media_code: mediaCode3,
                platform_key: 'centralweb'
              };

              const responsePre = await mediaService.preUpload(preUploadParam);
              const responsePre2 = await mediaService.preUpload(preUploadParam2);
              const responsePre3 = await mediaService.preUpload(preUploadParam3);

              if (responsePre || responsePre2 || responsePre3) {
                const responseUpload = await mediaService.uploadFile(responsePre, newFile);
                const responseUpload2 = await mediaService.uploadFile(responsePre2, newFile2);
                const responseUpload3 = await mediaService.uploadFile(responsePre3, newFile3);

                if (responseUpload || responseUpload2 || responseUpload3) {
                  preUploadParam.description = values.description;

                  dispatch(create(preUploadParam)).then((response) => {
                    dispatch(create(preUploadParam2)).then((response2) => {
                      dispatch(create(preUploadParam3)).then((response3) => {
                        console.log("ItemEditorTabs -> response", response);
                        dispatch(addItem(data)).then((result) => {
                          console.log("addItem -> response", response);
                          console.log('item add response', result);
                          const skuData = {
                            origin: 'origin test',
                            unit_of_measurement: values.unit_of_measurement,
                            package_weight: values.package_weight,
                            pack_size: values.pack_size,
                            package_height: values.package_height,
                            package_length: values.package_length,
                            package_width: values.package_width,
                            tax_rate: values.tax_rate,
                            cogs: values.cogs,
                            normal_price: values.normal_price,
                            selling_price: values.selling_price,
                            selling_price_start_time: values.selling_price_start_time,
                            selling_price_end_time: values.selling_price_end_time,
                            manufacturing_barcode: values.manufacturing_barcode,
                            variant_values: values.variant_values,
                            attribute_values: values.attribute_values,
                            item_id: result.id,
                            platform_key: 'centralweb',
                            images: [response.url, response2.url, response3.url]
                          };
                          skuService.createSku(skuData).then(() => {
                            resetForm();
                            setStatus({ success: true });
                            setSubmitting(false);
                            enqueueSnackbar('Item created', {
                              variant: 'success',
                              action: <Button>See all</Button>
                            });
                          });
                        });
                      });
                      handleClickClose();
                    });
                  });
                  enqueueSnackbar('Uploading images...', {
                    variant: 'info',
                    action: <Button>Please wait...</Button>
                  });
                }
              }
            } else {
              setValue(value + 1);
            }
          } catch (error) {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
          }
        }}
      >
        {({
          handleSubmit,
          values,
          errors,
          handleBlur,
          handleChange,
          setFieldValue,
          touched,
        }) => (
            <form onSubmit={handleSubmit}>
              <Tabs
                value={value}
                onChange={handleTabChange}
                aria-label="simple tabs example"
                textColor="primary"
                variant="fullWidth"
                indicatorColor="primary"
              >
                <Tab label="General" {...a11yProps(0)} />
                <Tab label="Category" {...a11yProps(1)} />
                <Tab label="Storage & Transport" {...a11yProps(2)} />
                <Tab label="Attributes" {...a11yProps(3)} />
                <Tab label="Variant" {...a11yProps(4)} />
                <Tab label="SKU" {...a11yProps(5)} />
              </Tabs>
              <Divider />
              <TabPanel value={value} index={0}>
                <GeneralTab
                  touched={touched}
                  errors={errors}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  setFieldValue={setFieldValue}
                  values={values}
                />
              </TabPanel>
              <TabPanel value={value} index={1}>
                <CategoryTab
                  touched={touched}
                  errors={errors}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  values={values}
                  classes={classes}
                  setFieldValue={setFieldValue}
                />
              </TabPanel>
              <TabPanel value={value} index={2}>
                <StorageAndTransportTab
                  handleChange={handleChange}
                  values={values}
                  storageList={storageList}
                />
              </TabPanel>
              <TabPanel value={value} index={3}>
                <AttributesTab values={values} />
              </TabPanel>
              <TabPanel value={value} index={4}>
                <VariantTab
                  value={value}
                  handleTabChange={handleTabChange}
                />
              </TabPanel>
              <TabPanel value={value} index={5}>
                <SkuTab
                  touched={touched}
                  errors={errors}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  values={values}
                  setFieldValue={setFieldValue}
                  classes={classes}
                  newFile={newFile}
                  setNewFile={setNewFile}
                  newFile2={newFile2}
                  setNewFile2={setNewFile2}
                  newFile3={newFile3}
                  setNewFile3={setNewFile3}
                />
              </TabPanel>
              <DialogActions>
                {value === 4 ? null : (
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                  >
                    {value >= 5 ? 'Submit' : 'Next'}
                  </Button>
                )}
              </DialogActions>
            </form>
          )}
      </Formik>
    </div>
  );
}
