import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getListItems } from 'src/actions/itemManagementAction';
import { getListCategory } from 'src/actions/categoryActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ItemManagementListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { items } = useSelector((state) => state.item);

  useEffect(() => {
    dispatch(getListItems());
    dispatch(getListCategory());
  }, [dispatch]);

  if (!items) {
    return null;
  }

  return (
    <Page className={classes.root} title="Product Management">
      <Container maxWidth={false}>
        <Header />
        {items && (
          <Box mt={3}>
            <Results items={items} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ItemManagementListView;
