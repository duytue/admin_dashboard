import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes, { element } from 'prop-types';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Grid,
  MenuItem,
  TextField,
  makeStyles,
  Menu,
  Input,
  Select,
  Checkbox,
  ListItemText,
  Chip,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  List,
  ListItem,
  Slide,
  Typography
} from '@material-ui/core';
import _ from 'lodash';
import { getListCategoriesByLevel, getListCategory } from 'src/actions/categoryActions';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import clsx from 'clsx';
import { MAX_LEVEL } from '../../../config';
import { ChevronRight as ArrowIcon } from 'react-feather';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  selectedItem: {
    backgroundColor: '#1976d2',
    color: '#000'
  },
  borderList: {
    border: '1px solid #ccc',
    maxHeight: '400px',
    overflowY: 'auto'
  }
}));

function CategoryTab({
  className,
  handleBlur,
  handleChange,
  values,
  setFieldValue
}) {
  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.category);
  const [open, setOpen] = useState(false);
  const [selectedParent, setSelectedParent] = useState(null);
  const [selectedCategories, setSelectedCategories] = useState(values.selectedCategories);
  const [showList, setShowList] = useState([]);
  const [listParentLv, setListParentLv] = useState([]);
  const [selectedParentLv, setSelectedParentLv] = useState([]);
  const [maxWidth, setMaxWidth] = useState("sm");
  const [isCurrent, setIsCurrent] = useState(true);

  const classes = useStyles();

  useEffect(() => {
    dispatch(getListCategoriesByLevel(1, null));
    dispatch(getListCategory());
  }, [dispatch]);

  const handleClickOpen = () => {
    if(isCurrent) {
      setMaxWidth("sm");
      setShowList(currentShowState);
      setListParentLv(currentListParentLv);
      setSelectedParentLv(currentSelectedParentLv);
      setIsCurrent(false);
    }
    setOpen(true);
  };

  const currentShowState = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL; index++) {
      if (index === 0) {
        array[index] = true;
      } else {
        array[index] = false;
      }
    }
    return array;
  }

  const currentListParentLv = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL; index++) {
      array[index] = [];
    }
    let listParentLv1 = categories.filter(item => item.level === 1);
    array[0] = listParentLv1;
    return array;
  }

  const currentSelectedParentLv = () => {
    let array = [];
    for (let index = 0; index < MAX_LEVEL; index++) {
      array[index] = null;
    }
    return array;
  }

  const handleClose = (e, value) => {
    setOpen(false);
    setMaxWidth("sm");
    setShowList(currentShowState);
    setListParentLv(currentListParentLv);
    setSelectedParentLv(currentSelectedParentLv);
    // setSelectedParent(null);
    setIsCurrent(true);
    if(value === true) {
      addSelectedCategory();
    } else {
      setSelectedParent(null);
    }
  };

  const handleSelectedParentLv = (event, item, index) => {
    if(index < 1) {
      setMaxWidth("sm");
    } else {
      setMaxWidth("md");
    }

    switch (index) {
      case (MAX_LEVEL - 1):
        setSelectedParent(item);
        let newSelect = [...selectedParentLv];
        newSelect[index] = item.id;
        setSelectedParentLv(newSelect);
        break;
    
      default:
        // set selected parent
        setSelectedParent(item);

        // set selected parent by level
        let newSelectedParentLv = [...selectedParentLv];
        for (let i = 0; i < MAX_LEVEL; i++) {
          if(i === index) {
            newSelectedParentLv[i] = item.id;
          } else if (i > index) {
            newSelectedParentLv[i] = null;
          }
        }
        setSelectedParentLv(newSelectedParentLv);

        // set list parent by level
        let newListParentLv = [...listParentLv];
        for (let i = 0; i < MAX_LEVEL; i++) {
          if(i === index + 1) {
            newListParentLv[i] = categories.filter(x => x.parent_id === item.id);
          }
          if(i > index + 1) {
            newListParentLv[i] = [];
          }
        }
        setListParentLv(newListParentLv);

        // set show state
        let newShowState = [...showList];
        for (let i = 0; i < MAX_LEVEL; i++) {
          if(i === index + 1) {
            newShowState[i] = isHasChidren(item);
          }
          if(i > index + 1) {
            newShowState[i] = false;
          }
        }
        setShowList(newShowState);
        break;
    }
  }

  const isHasChidren = (item) => {
    let array = categories.filter(element => element.parent_id === item.id);
    return array.length > 0;
  }

  const removeSelectedCategory = (item) => {
    let array = selectedCategories.filter(element => element.id !== item.id);
    setSelectedCategories(array);
    values.selectedCategories = array;
    values.category_ids = values.category_ids.filter(element => element !== item.id)
  }

  const addSelectedCategory = () => {
    let array = [...selectedCategories];
    array.push(selectedParent);
    setSelectedCategories(array);
    values.selectedCategories = array;
    values.category_ids.push(selectedParent.id);
    setSelectedParent(null);
  }

  const getDefaultSelectedCategories = () => {
    let array = [];
    values.category_ids.forEach(element => {
      let item = categories.find(e => e.id === element);
      array.push(item);
    });
    return array;
  }

  return (
    <Grid container spacing={3}>
      <Grid item md={12} xs={12}>
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>Select categories</Button>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          aria-labelledby="alert-dialog-slide-title"
          fullWidth={true}
          maxWidth={maxWidth}
        >
          <DialogTitle id="alert-dialog-slide-title">
            <Typography variant="h3" color="textPrimary">
              Select the category's parent!
            </Typography>
          </DialogTitle>
          <DialogContent style={{display: "flex"}}>
            { showList && showList.map((value, index) => (<Grid key={index} item xs={12} className={classes.borderList} style={{ display: value ? "" : "none" }}>
              <List>
                {
                  listParentLv && listParentLv[index] && listParentLv[index].map((item) => (<ListItem key={item.id} button className={ item.id === selectedParentLv[index] ? classes.selectedItem : null} 
                  onClick={(e) => handleSelectedParentLv(e, item, index)}>
                    <ListItemText primary={`${item.name.en || item.name.vn}`} />
                    { isHasChidren(item) && <ArrowIcon />}
                  </ListItem>))
                }
              </List>
            </Grid>)) }
          </DialogContent>
          <DialogActions>
            <Button onClick={(e) => handleClose(e, false)} color="primary">
              Cancel
            </Button>
            <Button onClick={(e) => handleClose(e, true)} color="primary" disabled={selectedParent === null}>
              Accept
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
      <Grid item md={12} xs={12}>
        { selectedCategories && selectedCategories.map((item) => (<Chip
            key={item.id}
            label={item.name.en || item.name.vn}
            onDelete={() => removeSelectedCategory(item)}
            className={classes.chip}
          />))}
      </Grid>
    </Grid>
  );
}

CategoryTab.propTypes = {
  classes: PropTypes.object,
  values: PropTypes.object,
  setFieldValue: PropTypes.func
};

export default CategoryTab;
