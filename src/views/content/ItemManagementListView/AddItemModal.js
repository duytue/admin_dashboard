import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import {
  makeStyles,
  Grid,
  Box,
  IconButton,
  SvgIcon,
  Typography
} from '@material-ui/core';
import { XCircle as CloseIcon } from 'react-feather';
import ItemEditorTabs from './ItemEditorTabsContainer';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    fontFamily: 'Roboto',
    fontWeight: '400px',
    fontSize: '0.875rem'
  },
  grid: {
    paddingTop: '20px'
  },
  box: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: 'auto'
  },
  text: {
    marginRight: 'auto'
  }
}));

function AddItemModal({ isEditMode, isOpenModal, handleClickClose, ...rest }) {
  const classes = useStyles();
  return (
    <Dialog
      onClose={handleClickClose}
      open={isOpenModal}
      maxWidth="md"
      fullWidth
      {...rest}
    >
      <div className={classes.root}>
        <Box className={classes.box}>
          <Typography className={classes.text} variant="h3">
            Product Editor
          </Typography>
          <IconButton onClick={handleClickClose}>
            <SvgIcon fontSize="small">
              <CloseIcon />
            </SvgIcon>
          </IconButton>
        </Box>
        <Grid className={classes.grid} container spacing={5}>
          <ItemEditorTabs handleClickClose={handleClickClose} isEditMode={isEditMode} />
        </Grid>
      </div>
    </Dialog>
  );
}

AddItemModal.propTypes = {
  isOpenModal: PropTypes.bool,
  isEditMode: PropTypes.bool,
  handleClickClose: PropTypes.func
};

AddItemModal.defaultProps = {
  isOpenModal: false
};

export default AddItemModal;
