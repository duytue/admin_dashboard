/* eslint-disable max-len */
import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Divider,
  InputAdornment,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles,
  IconButton,
  Menu,
  MenuItem,
  Avatar
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import MoreVertRoundedIcon from '@material-ui/icons/MoreVertRounded';
import { useDispatch, useSelector } from 'react-redux';
import AddItemModal from './AddItemModal';
import { getItem } from 'src/actions/itemManagementAction';

const tabs = [
  {
    value: 'itemManagement',
    label: 'All'
  },
  // {
  //   value: 'qna',
  //   label: 'Q&A'
  // }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  }
];

function applyFilters(items, query, filters) {
  return items.filter((item) => {
    let matches = true;

    if (query) {
      const properties = ['identifier'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (item[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && item[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(reviews, page, limit) {
  return reviews.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(reviews, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = reviews.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, items, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('itemManagement');
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const [itemId, setItemId] = useState('');
  const [isOpenModal, setOpenModal] = useState(false);
  const [isEditMode, setEditMode] = useState(false);
  const dispatch = useDispatch();

  const handleClickOpen = (id) => {
    dispatch(getItem(id))
      .then(() => {
        setOpenModal(true);
      });
  };

  const handleClickClose = () => {
    setOpenModal(false);
  };

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleMoreClick = (event, id) => {
    setAnchorEl(event.currentTarget);
    setItemId(id);
  };

  const handleMoreClose = () => {
    setAnchorEl(null);
  };

  const handleViewProductClick = (id) => {
    console.log('handleViewProductClick -> id', id);
    setAnchorEl(null);
    handleClickOpen(id);
  };

  const handleEditProductClick = (id) => {
    console.log('handleEditProductClick -> id', id);
    setAnchorEl(null);
    handleClickOpen(id);
    setEditMode(true);
  };

  const handleViewPricingClick = (id) => {
    console.log('handleViewPricingClick -> id', id);
    setAnchorEl(null);
  };

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(items, query, filters);
  const sorted = applySort(filtered, sort);
  const paginated = applyPagination(sorted, page, limit);

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search items"
          value={query}
          variant="outlined"
        />
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell padding="checkbox" />
                <TableCell>Item</TableCell>
                <TableCell>Item Identifier</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>SKU</TableCell>
                <TableCell>Seller SKU</TableCell>
                {/* <TableCell>Variant</TableCell> */}
                {/* <TableCell>Quantity</TableCell> */}
                <TableCell>Measure</TableCell>
                <TableCell>Visible</TableCell>
                <TableCell>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginated.map((obj) => (
                <>
                  <TableRow hover key={obj.id}>
                    <TableCell padding="checkbox" rowSpan={obj.skuItems ? 2 : 2}>
                      <IconButton onClick={(event) => handleMoreClick(event, obj.id)}>
                        <SvgIcon fontSize="small">
                          <MoreVertRoundedIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                    <TableCell rowSpan={obj.skuItems ? 2 : 2}>
                      <Avatar alt="Remy Sharp" src={obj.images ? obj.images[0] : null} />
                    </TableCell>
                    <TableCell rowSpan={obj.skuItems ? 2 : 2}>{obj.names.en || obj.names.vn}</TableCell>
                    <TableCell rowSpan={obj.skuItems ? 2 : 2}>{obj.identifier}</TableCell>
                    <TableCell rowSpan={obj.skuItems ? 2 : 2}>{obj.type}</TableCell>
                  </TableRow>
                  {obj.skuItems ? obj.skuItems.map((skuItem) => (
                    <TableRow hover>
                      <TableCell rowSpan={1}>{skuItem ? skuItem.sku : 'N/A'}</TableCell>
                      <TableCell rowSpan={1}>{skuItem ? skuItem.source_sku : 'N/A'}</TableCell>
                      {/* <TableCell rowSpan={1}>{skuItem ? skuItem.variant_values.en : 'N/A'}</TableCell> */}
                      {/* <TableCell rowSpan={1}>{skuItem ? skuItem.quantity : 'N/A'}</TableCell> */}
                      <TableCell rowSpan={1}>{skuItem ? skuItem.unit_of_measurement : 'N/A'}</TableCell>
                      <TableCell rowSpan={1}>{skuItem ? 'TRUE' : 'FALSE'}</TableCell>
                      <TableCell rowSpan={1}>
                        <Switch
                          disableRipple
                          checked={skuItem ? skuItem.is_active : null}
                          // onChange={() => handleChangeActive(obj)}
                          color="secondary"
                          edge="start"
                        />
                      </TableCell>
                    </TableRow>
                  ))
                    : (
                      <TableRow hover>
                        <TableCell rowSpan={1}>N/A</TableCell>
                        <TableCell rowSpan={1}>N/A</TableCell>
                        {/* <TableCell rowSpan={1}>N/A</TableCell> */}
                        {/* <TableCell rowSpan={1}>N/A</TableCell> */}
                        <TableCell rowSpan={1}>N/A</TableCell>
                        <TableCell rowSpan={1}>N/A</TableCell>
                        <TableCell rowSpan={1}>N/A</TableCell>
                      </TableRow>
                    )}
                </>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <Menu
        id="more-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMoreClose}
      >
        <MenuItem onClick={() => handleViewProductClick(itemId)}>View Product</MenuItem>
        <MenuItem onClick={() => handleEditProductClick(itemId)}>Edit Product</MenuItem>
        <MenuItem onClick={() => handleViewPricingClick(itemId)}>View Pricing History</MenuItem>
      </Menu>
      <AddItemModal
        isOpenModal={isOpenModal}
        handleClickClose={handleClickClose}
        isEditMode={isEditMode}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array
};

Results.defaultProps = {
  items: []
};

export default Results;
