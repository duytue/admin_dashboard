import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  Paper,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import {
  Upload as UploadIcon,
  XCircle as DeleteIcon,
  AlertCircle
} from 'react-feather';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import { getListPlatform } from 'src/actions/platformActions';
import { create } from '../../../actions/mediaActions';
import mediaService from 'src/services/mediaService';
import addCurrentTimeStampToFileName from "src/utils/dateString";

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [language, setLanguage] = useState('en');
  const { user } = useSelector((state) => state.account);
  const [fileUrl, setFileUrl] = useState(null);
  const [newFile, setNewFile] = useState(null);

  const { platforms } = useSelector((state) => {
    return state.platform;
  });

  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);

  const handleChangeFile = (event) => {
    if (event.target.files.length != 0) {
      let file = event.target.files[0];
      let type = file.type;
      let typeExt = file.type;

      if (type.indexOf('image/') >= 0) {
        setNewFile(file);
        fileToBase64(file);
      } else if (type.indexOf('audio/') >= 0) {
        setNewFile(file);
        setFileUrl('/static/images/icon/icon_mp3.png');
      } else if (type.indexOf('video/') >= 0) {
        setNewFile(file);
        setFileUrl('/static/images/icon/icon_video.png');
      }
      else {
        enqueueSnackbar('Only image / audio / video files are allowed', {
          variant: 'error'
        });
      }
    }
  };

  const fileToBase64 = (file) => {
    let reader = new FileReader();
    let newFileName = addCurrentTimeStampToFileName(file.name);
    Object.defineProperty(file, "name", {
      writable: true,
      value: newFileName
    });
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setFileUrl(reader.result);
    };
  };

  const onClearFile = () => {
    setFileUrl(null);
    setNewFile(null);
  };

  const onChangeLanguage = (event, value) => {
    setLanguage(value[0]);
  };

  const handleKeyword = (keyword) => {
    let array = keyword.split(',');
    array.forEach(element => {
      element = element.trim();
    });
    return array;
  };

  return (
    <Formik
      initialValues={{
        file: '',
        platform_key: '',
        description: ''
      }}
      validationSchema={Yup.object().shape({
        platform_key: Yup.string().max(255).required('Platform is required'),
        description: Yup.string().max(255)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          let param = {
            'platform_key': values.platform_key,
            'name': '',
            'description': values.description,
            'file_url': ''
          };

          if (newFile != null) {
            let media_code = newFile.type.replace('image/', '').replace('video/', '').replace('audio/', '').toUpperCase();

            let preUploadParam = {
              'creator_by': user.id,
              'name': newFile.name,
              'media_code': media_code,
              'platform_key': values.platform_key
            };

            let responsePre = await mediaService.preUpload(preUploadParam);

            if (responsePre) {
              let responseUpload = await mediaService.uploadFile(responsePre, newFile);

              if (responseUpload) {
                // Make API request
                preUploadParam['description'] = values.description;

                dispatch(create(preUploadParam)).then(() => {
                  resetForm();
                  setStatus({ success: true });
                  setSubmitting(false);
                  enqueueSnackbar('Media created', {
                    variant: 'success',
                    action: <Button>See all</Button>
                  });
                  history.push(('/app/content/medias'));
                });
              }
            }
          }
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid item lg={12} md={12}>
                    {fileUrl &&
                      <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                        <label htmlFor="contained-button-file">
                          <img src={fileUrl}
                            style={{ maxWidth: '150px', border: '1px solid' }} />
                        </label>
                        <DeleteIcon
                          style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }}
                          onClick={onClearFile} />
                      </Paper>}

                    <input
                      name="file"
                      className={classes.input}
                      id="contained-button-file"
                      type="file"
                      required
                      style={{ display: 'none' }}
                      onChange={handleChangeFile}
                    />
                    <label htmlFor="contained-button-file">
                      {!fileUrl && <Button variant="contained" color="primary" component="span"
                        className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                        {language == 'en' ? 'Choose File' : 'Chọn file'}
                      </Button>}
                    </label>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="platform_key">
                      {language == 'en' ? 'Platform' : 'Nền tảng'}
                    </InputLabel>
                    <Select labelId="platform_key" fullWidth id="platform_key" label="platform_key" name="platform_key" required
                      value={values.platform_key} onChange={handleChange} displayEmpty
                      className={classes.selectEmpty}>
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.id} value={item.key}>{item.key}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Media
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CreateForm.propTypes = {
  className: PropTypes.string
};

export default CreateForm;
