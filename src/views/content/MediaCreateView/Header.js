import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Breadcrumbs, Link, Typography, makeStyles } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import {
  Box,
  Button,
  Grid,
  SvgIcon
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles(() => ({
  root: {}
}));

function Header({ className, ...rest }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Grid
        className={clsx(classes.root, className)}
        container
        justify="space-between"
        spacing={3}
        {...rest}
      >
        <Grid item>
          <Breadcrumbs
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            <Link variant="body1" color="inherit" to="/" component={RouterLink}>
              Home
            </Link>
            <Typography variant="body1" color="textPrimary">
              Content
            </Typography>
            <Link
              variant="body1"
              color="inherit"
              to="/app/content/medias"
              component={RouterLink}
            >
              Media
            </Link>
          </Breadcrumbs>
        </Grid>
        <Grid item>
          <Link to="/app/content/medias" component={RouterLink}>
            <Button
              color="secondary"
              variant="contained"
              className={classes.action}
            >
              <SvgIcon fontSize="small" className={classes.actionIcon}>
                <ArrowBackIcon />
              </SvgIcon>
              Back
            </Button>
          </Link>
        </Grid>
      </Grid>

      <Typography variant="h3" color="textPrimary">
        Create new Media
      </Typography>
    </div>
  );
}

Header.propTypes = {
  className: PropTypes.string
};

export default Header;
