/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Card,
  Divider,
  IconButton,
  Tab,
  Tabs,
  makeStyles,
  Grid,
  Paper,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  Menu,
  MenuItem,
  Typography,
  Button
} from '@material-ui/core';
import {
  MoreHorizontal,
  PlusCircle as PlusCircleIcon,
  AlertCircle
} from 'react-feather';
import { getListPolicy, updatePolicy, deletePolicy, createPolicy } from 'src/actions/policyActions';
import { useDispatch, useSelector } from 'react-redux';
import PolicyDetail from './PolicyDetail';
import PolicyCreateForm from './PolicyCreateForm';
import { useSnackbar } from 'notistack';

function applyFilters(policies, type) {
  return policies.filter((policy) => {
    return policy.type === type;
  });
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px',
  },
  margin: {
    margin: '10px 0'
  },
  padding: {
    padding: '10px'
  }
}));

function Results({ className, platforms, policyTypes, curentPolicyTypeId, ...rest }) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [isEdit, setIsEdit] = useState(false);
  const [currentTab, setCurrentTab] = useState(curentPolicyTypeId);
  const [curentPolicyTypeIdState, setCurentPolicyTypeIdState] = useState(curentPolicyTypeId);
  const [currentPolicyDraff, setCurrentPolicyDtaff] = useState(null);
  const [currentPolicy, setCurrentPolicy] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);
  const { enqueueSnackbar } = useSnackbar();

  const { policies } = useSelector((state) => {
    return state.policy
  });

  const { user } = useSelector((state) => state.account);

  useEffect(() => {
    dispatch(getListPolicy({}));
  }, [dispatch]);


  if (!policies) {
    return null;
  }

  const handleTabsChange = (event, value) => {
    setCurrentPolicy(null);
    setCurentPolicyTypeIdState(value);
    setCurrentTab(value);
    setIsEdit(false);
  };

  const handleClick = (event, value) => {
    setCurrentPolicyDtaff(value);
    setAnchorEl(event.currentTarget);
  };

  const handleClickEdit = (event, value) => {
    setCurrentPolicyDtaff(value);
    setCurrentPolicy(value);
    setIsEdit(true);
    setAnchorEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClearPolicy = () => {
    setAnchorEl(null);
    setCurrentPolicyDtaff(null);
    setCurrentPolicy(null);
  };

  const handleDuplicate = async () => {
    // setCurrentPolicy(currentPolicyDraff);
    // setIsEdit(false);
    setAnchorEl(null);
    let param = {
      "names": currentPolicyDraff.names,
      "descriptions": currentPolicyDraff.descriptions,
      "image_url": currentPolicyDraff.image_url,
      "platform_key": currentPolicyDraff.platform_key,
      "type": currentPolicyDraff.type
    }

    try {
      await dispatch(createPolicy(param));
      enqueueSnackbar('Duplicate policy success!', {
        variant: 'success'
      });
      setCurrentPolicyDtaff(null);
    } catch (error) {
      enqueueSnackbar('Duplicate policy failure!', {
        variant: 'error'
      });
      setCurrentPolicyDtaff(null);
    }

  };

  const handleDelete = async () => {
    setAnchorEl(null);
    let param = {
      "ids": [
        currentPolicyDraff.id
      ]
    }
    try {
      await dispatch(deletePolicy(param));
      enqueueSnackbar('Delete policy success!', {
        variant: 'success'
      });
      setCurrentPolicyDtaff(null);
    } catch (error) {
      enqueueSnackbar('Delete policy failure!', {
        variant: 'error'
      });
    }
  };

  const handleActive = async () => {
    setAnchorEl(null);
    let param = {
      "id": currentPolicyDraff.id,
      "creator_id": currentPolicyDraff.creator_id,
      "updater_id": user.id,
      "is_active": true,
      "platform_key": currentPolicyDraff.platform_key,
      "image_url": currentPolicyDraff.image_url,
      "names": currentPolicyDraff.names,
      "descriptions": currentPolicyDraff.descriptions,
      "type": currentPolicyDraff.type
    };
    try {
      await dispatch(updatePolicy(param));
      enqueueSnackbar('Activate policy success!', {
        variant: 'success'
      });
    } catch (error) {
      enqueueSnackbar('Activate policy failure!', {
        variant: 'error'
      });
    }
  };

  const handleDeactive = async () => {
    setAnchorEl(null);
    let param = {
      "id": currentPolicyDraff.id,
      "creator_id": currentPolicyDraff.creator_id,
      "updater_id": user.id,
      "is_active": false,
      "platform_key": currentPolicyDraff.platform_key,
      "image_url": currentPolicyDraff.image_url,
      "names": currentPolicyDraff.names,
      "descriptions": currentPolicyDraff.descriptions,
      "type": currentPolicyDraff.type
    };
    try {
      await dispatch(updatePolicy(param));
      enqueueSnackbar('Deactivate policy success!', {
        variant: 'success'
      });
    } catch (error) {
      enqueueSnackbar('Deactivate policy failure!', {
        variant: 'error'
      });
    }
  };

  function checkAlert(policy) {
    return  policy.names.en === null || policy.names.en === ''
            || policy.names.vn === null || policy.names.vn === ''
            || policy.descriptions.en === null || policy.descriptions.en === ''
            || policy.descriptions.vn === null || policy.descriptions.vn === ''
  }

  // Usually query is done on backend with indexing solutions
  const filteredPolicies = applyFilters(policies, curentPolicyTypeIdState);
  // const enableBulkOperations = selectedUsers.length > 0;
  // const selectedSomeUsers = selectedUsers.length > 0 && selectedUsers.length < users.length;
  // const selectedAllUsers = selectedUsers.length === users.length;

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {policyTypes.map((tab) => (
          <Tab
            key={tab.id}
            value={tab.id}
            label={tab.name}
          />
        ))}
      </Tabs>
      <Divider />
        <Box minWidth={700}>
          <Grid container spacing={1}>
            <Grid item md={3} xs={12}>
              <Paper className={classes.paper} elevation={0}>
                <List>
                { filteredPolicies.map((policy) => {
                  return (
                    <ListItem key={policy.id} onClick={(e) => handleClickEdit(e, policy)} style={{ cursor: 'pointer', backgroundColor: policy.id === currentPolicyDraff?.id ? '#ececec80': ''}}>
                      <ListItemIcon>
                        { checkAlert(policy) && <AlertCircle style={{color: !policy.is_active ? '#bec8cc' : ''}} />}
                      </ListItemIcon>
                      <ListItemText style={{color: !policy.is_active ? '#bec8cc' : ''}} primary={policy?.names?.vn || policy?.names?.en} />
                      <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="more" onClick={(e) => handleClick(e, policy)}>
                          <MoreHorizontal />
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  )
                })}
                </List>
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <MenuItem onClick={handleDuplicate}>Duplicate</MenuItem>
                  <MenuItem onClick={handleDelete}>Delete</MenuItem>
                  { currentPolicyDraff && currentPolicyDraff.is_active !== true && <MenuItem onClick={handleActive}>Active</MenuItem>}
                  { currentPolicyDraff && currentPolicyDraff.is_active === true && <MenuItem onClick={handleDeactive}>Deactive</MenuItem>}
                </Menu>
              </Paper>
            </Grid>
            <Grid item md={9} xs={12}>
              <Grid container style={{padding: 10}}>
                <Grid item xs={6}>
                  <Typography variant="h4" color="textPrimary">{ currentPolicy ? 'Policy Detail' : 'Create policy'}</Typography>
                </Grid>
                <Grid item xs={6} style={{ textAlign: 'right'}}>
                  {currentPolicy && <Button onClick={handleClearPolicy} className={classes.margin} variant="contained" color="primary" startIcon={<PlusCircleIcon />}>
                    Add New
                  </Button>}
                </Grid>
              </Grid>
              
              {currentPolicy && <PolicyDetail user={user} type={curentPolicyTypeIdState} platforms={platforms} policy_id={currentPolicy?.id} ></PolicyDetail>}
              {!currentPolicy && <PolicyCreateForm user={user} type={curentPolicyTypeIdState} platforms={platforms} ></PolicyCreateForm>}
            </Grid>
          </Grid>
        </Box>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  platforms: PropTypes.array,
  policyTypes: PropTypes.array,
  curentPolicyTypeId: PropTypes.string
};

Results.defaultProps = {
  platforms: [],
  policyTypes: []
};

export default Results;
