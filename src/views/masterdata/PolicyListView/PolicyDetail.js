import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Button,
  Grid,
  TextField,
  makeStyles,
  Paper,
  InputLabel,
  Select,
  Chip,
  MenuItem,
  Checkbox,
  ListItemText
} from '@material-ui/core';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import { updatePolicy, getOnePolicy } from 'src/actions/policyActions';
import {
    Edit as EditIcon,
    AlertCircle,
    Upload as UploadIcon,
    XCircle as DeleteIcon,
} from 'react-feather';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import _ from 'lodash';
import policyService from 'src/services/policyService';

const useStyles = makeStyles(() => ({
  root: {},
  margin: {
    margin: '10px 0'
  },
  padding: {
    padding: '10px'
  },
  imageStyle: {
      width: '100%'
  }
}));

function PolicyDetail({ policy_id, user, type, platforms, className, ...rest }) {
    const classes = useStyles();
    const { policy } = useSelector((state) => {
        return state.policy
    });
    const [image_url, setImageUrl] = useState(null);
    const [new_image, setNewImage] = useState(null);
    const [language, setLanguage] = useState('en');
    const [openRoleSelect, setOpenRoleSelect] = useState(false);
    const [isShowCurrentImage, setIsShowCurrentImage] = useState(true);
    const dispatch = useDispatch();
    const { enqueueSnackbar } = useSnackbar();
    const onChangeLanguage = (event, value) => {
        setLanguage(value[0]);
    }

    useEffect(() => {
        setIsShowCurrentImage(true);
        setImageUrl(null);
        setNewImage(null);
        dispatch(getOnePolicy(policy_id));
    }, [dispatch, policy_id]);

    if (_.size(policy) === 0) {
        return null;
    }

    const handleChangeImage = (event) => {
        if(event.target.files.length !== 0) {
            setIsShowCurrentImage(false);
            let file = event.target.files[0];
            let type = file.type;
            if(type.indexOf('image/') < 0) {
                enqueueSnackbar('Only image files are allowed', {
                    variant: 'error'
                });
            } else {
                setNewImage(file);
                fileToBase64(file);
            }
            
        }
    }

    const fileToBase64 = (file) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            setImageUrl(reader.result);
        };
    };

    const onClearImage = () => {
      setIsShowCurrentImage(false);
      setImageUrl(null);
      setNewImage(null);
    }

    return (
        <Formik
        enableReinitialize
        initialValues={{
            platform_key: policy != null ? policy.platform_key : '',
            name_en: policy != null ? policy.names?.en : '',
            name_vn: policy != null ? policy.names?.vn : '',
            description_en: policy != null ? policy.descriptions?.en : '',
            description_vn: policy != null ? policy.descriptions?.vn : ''
        }}
        validationSchema={Yup.object().shape({
            name_en: Yup.string().max(255),
            name_vn: Yup.string().max(255),
            description_en: Yup.string().max(1000),
            description_vn: Yup.string().max(1000),
            platform_key: Yup.string().max(255).required('Platform required!')
        })}
        onSubmit={async (values, {
            resetForm,
            setErrors,
            setStatus,
            setSubmitting
        }) => {
            let param = {
                "names": {
                    "en": values.name_en,
                    "vn": values.name_vn
                },
                "descriptions": {
                    "en": values.description_en,
                    "vn": values.description_vn
                },
                "platform_key": values.platform_key,
                "type": type,
                "id": policy.id,
                "creator_id": policy.creator_id,
                "updater_id": user.id,
            }
            if(new_image != null) {
                let media_code = new_image.type.replace('image/', '').toUpperCase();
                
                let preUploadParam = {
                    "creator_by": user.id,
                    "name": new_image.name,
                    "media_code": media_code,
                    "platform_key": values.platform_key
                }

                let responsePre = await policyService.preUpload(preUploadParam);
                if(responsePre) {
                    let responseUpload = await policyService.uploadImage(responsePre, new_image);

                    if(responseUpload) {
                        let responseCreateUpload = await policyService.createUpload(preUploadParam);
                        if(responseCreateUpload) {
                            param['image_url'] = responseCreateUpload.url;
                            try {
                                await dispatch(updatePolicy(param));
                                setStatus({ success: true });
                                enqueueSnackbar('Policy updated', {
                                    variant: 'success'
                                });
                            } catch (error) {
                                setStatus({ success: false });
                                setErrors({ submit: error.message });
                            } finally {
                                setSubmitting(false);
                            }
                        }
                    }
                }
            } else {
                param['image_url'] = isShowCurrentImage ? policy.image_url : null;
                try {
                    await dispatch(updatePolicy(param));
                    setStatus({ success: true });
                    enqueueSnackbar('Policy updated', {
                        variant: 'success'
                    });
                } catch (error) {
                    setStatus({ success: false });
                    setErrors({ submit: error.message });
                } finally {
                    setSubmitting(false);
                }
            }
            
            
        }}
        >
        {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            setFieldValue,
            isSubmitting,
            touched,
            values
        }) => (
            <form onSubmit={handleSubmit}>
            <Paper className={classes.paper, classes.padding} elevation={0}>
                <Grid container spacing={1}>
                    <Grid item md={8} xs={6}>
                    <Paper className={classes.paper} elevation={0}>
                        <Grid
                        container
                        spacing={4}
                        >
                            <Grid item md={12} xs={12} >
                                <InputLabel shrink id="Platform">
                                    Platform
                                </InputLabel>
                                <Select
                                    error={Boolean(touched.platform_key && errors.platform_key)}
                                    fullWidth
                                    labelId="Platform"
                                    id="Platform"
                                    label="Platform"
                                    name="platform_key"
                                    value={values.platform_key}
                                    open={openRoleSelect}
                                    onClose={() => setOpenRoleSelect(false)}
                                    IconComponent={() => (
                                        <Button className={classes.buttonArrowDropDown} onClick={() => setOpenRoleSelect(true)}>
                                            <ArrowDropDownIcon />
                                        </Button>
                                    )}
                                    onChange={handleChange}
                                    renderValue={(selected) => (
                                        <div className={classes.chips}>
                                        {<Chip
                                            key={selected}
                                            label={selected}
                                            onDelete={() => setFieldValue("platform_key", '')}
                                            className={classes.chip}
                                        />}
                                        </div>
                                    )}
                                    className={clsx(classes.selectEmpty)}
                                    >
                                    {
                                        platforms && platforms.map((item) => (
                                        <MenuItem key={item.id} value={item.key}>
                                            <Checkbox checked={values.platform_key.indexOf(item.key) > -1} />
                                            <ListItemText primary={item.key} />
                                        </MenuItem>
                                        ))
                                    }
                                </Select>
                            </Grid>
                            <Grid item md={12} xs={12} >
                                {language== 'en' && <TextField
                                    error={Boolean(touched.name_en && errors.name_en)}
                                    fullWidth
                                    helperText={touched.name_en && errors.name_en}
                                    label="Name"
                                    name="name_en"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    required
                                    type="name_en"
                                    value={values.name_en}
                                    variant="outlined"
                                />}
                                {language== 'vn' && <TextField
                                    error={Boolean(touched.name_vn && errors.name_vn)}
                                    fullWidth
                                    helperText={touched.name_vn && errors.name_vn}
                                    label="Tên"
                                    name="name_vn"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    required
                                    type="name_vn"
                                    value={values.name_vn}
                                    variant="outlined"
                                />}
                            </Grid>
                            <Grid item md={12} xs={12} >
                                {language== 'en' && <TextField
                                    error={Boolean(touched.description_en && errors.description_en)}
                                    fullWidth
                                    helperText={touched.description_en && errors.description_en}
                                    label="Description"
                                    name="description_en"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    required
                                    multiline={true}
                                    rows={3}
                                    type="description_en"
                                    value={values.description_en}
                                    variant="outlined"
                                />}
                                {language== 'vn' && <TextField
                                    error={Boolean(touched.description_vn && errors.description_vn)}
                                    fullWidth
                                    helperText={touched.description_vn && errors.description_vn}
                                    label="Mô tả"
                                    name="description_vn"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    required
                                    multiline={true}
                                    rows={3}
                                    type="description_vn"
                                    value={values.description_vn}
                                    variant="outlined"
                                />}
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs={12} style={{textAlign: 'right'}}>
                                <Button type="submit" className={classes.margin} variant="contained" color="primary" startIcon={<EditIcon />}>
                                    Update
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                    </Grid>
                    <Grid item md={4} xs={6}>
                        <Paper className={classes.paper} elevation={0}>
                            <ToggleButtonGroup size="small" aria-label="language" onChange={onChangeLanguage}>
                                <ToggleButton value="en" aria-label="English">
                                    { (values.name_en == '' || values.description_en == '') && <AlertCircle />}
                                    &nbsp; English
                                </ToggleButton>
                                <ToggleButton value="vn" aria-label="Vietnamese">
                                    { (values.name_vn == '' || values.description_vn == '') && <AlertCircle />}
                                    &nbsp; Tiếng Việt
                                </ToggleButton>
                            </ToggleButtonGroup>
                            <input
                            accept="image/*"
                            className={classes.input}
                            id="contained-button-file"
                            type="file"
                            style={{display: 'none'}}
                            onChange={handleChangeImage}
                            />
                            {isShowCurrentImage && <>
                                {policy && policy.image_url && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                                    <label htmlFor="contained-button-file">
                                    <img className={classes.imageStyle} src={policy?.image_url}/>
                                    </label>
                                    <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20}} onClick={onClearImage} />
                                </Paper>}
                                
                                {policy && !policy.image_url && <label htmlFor="contained-button-file">
                                    <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                                    { language === 'en' ? 'Choose image' : 'Chọn ảnh' }
                                    </Button>
                                </label>}
                                </>}
                            {!isShowCurrentImage && <>
                                {image_url && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                                    <label htmlFor="contained-button-file">
                                    <img className={classes.imageStyle} src={image_url}/>
                                    </label>
                                    <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20}} onClick={onClearImage} />
                                </Paper>}
                                
                                {!image_url && <label htmlFor="contained-button-file">
                                    <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                                    { language === 'en' ? 'Choose image' : 'Chọn ảnh' }
                                    </Button>
                                </label>}
                            </>}
                        </Paper>
                    </Grid>
                </Grid>
                
                
                
            </Paper>
        </form>)}
        </Formik>
    );
}

PolicyDetail.propTypes = {
  className: PropTypes.string,
  policy_id: PropTypes.string,
  platforms: PropTypes.array,
  user: PropTypes.object,
  type: PropTypes.string,
};

PolicyDetail.defaultProps = {
    policy_id: null,
    platforms: [],
    user: null,
    type: null,
};

export default PolicyDetail;
