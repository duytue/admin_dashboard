import React, {
  // useState,
  useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getListPlatform } from 'src/actions/platformActions';
import { getListPolicyType } from 'src/actions/policyActions';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PoliciesListView() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { permissions } = useSelector((state) => {
    return state.permission
  });

  const { policyTypes } = useSelector((state) => {
    return state.policy
  });

  const { platforms } = useSelector((state) => {
    return state.platform
  });

  let curentPolicyTypeId = null;

  useEffect(() => {
    dispatch(getListPlatform());
    dispatch(getListPolicyType({}));
  }, [dispatch]);

  if (!platforms) {
    return null;
  }

  if (!policyTypes || policyTypes.length == 0) {
    return null;
  } else {
    curentPolicyTypeId = policyTypes[0].id;
  }

  return (
    <Page
      className={classes.root}
      title="Policies List"
    >
      <Container maxWidth={false}>
        <Header />
        {policyTypes && curentPolicyTypeId && platforms && (
          <Box mt={3}>
            <Results platforms={platforms} policyTypes={policyTypes} curentPolicyTypeId={curentPolicyTypeId} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default PoliciesListView;
