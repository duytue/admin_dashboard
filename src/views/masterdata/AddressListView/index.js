import React, { useState, useEffect, useCallback } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Page from 'src/components/Page';
import axios from 'src/utils/axios';
import { getListAddress } from 'src/actions/addressActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
}));

function AddressListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { addresses } = useSelector(state => state.address);

  useEffect(() => {
    dispatch(getListAddress());
  }, [dispatch]);

  if (!addresses) {
    return null;
  }

  return (
    <Page className={classes.root} title="Address List">
      <Container maxWidth={false}>
        <Header />
        {addresses && (
          <Box mt={3}>
            <Results addresses={addresses} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default AddressListView;
