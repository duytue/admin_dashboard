/* eslint-disable max-len */
import React, { useState, useCallback, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import axios from 'src/utils/axios';
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Chip,
  Divider,
  Grid,
  TextField,
  Typography,
  makeStyles,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { TreeView, TreeItem } from '@material-ui/lab';
import { ChevronRight, ExpandMore } from '@material-ui/icons';
import { getLocationByLvl_1, getLocationByLvl_2, getOneAddress } from 'src/actions/addressActions';
import FormAddress from './components/FormAddress';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    padding: theme.spacing(2),
  },
  bulkOperations: {
    position: 'relative',
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default,
  },
  bulkAction: {
    marginLeft: theme.spacing(2),
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1),
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px',
  },
  chipsExpand: {
    display: 'flex',
    flexFlow: 'row-reverse',
  },
  chipExpand: {
    margin: 2,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
    marginBottom: '16px',
    marginTop: '17px',
  },
}));

function Results({ className, addresses, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [expandData, setExpandData] = useState([]);

  const onLabelLevel_1_Click = item => {
    const newParams = {
      parent_short_id: item.short_id,
      loc_lvl: 2,
      size: 100,
      page: 1,
    };
    if (item.data_lvl2 === undefined) {
      dispatch(getLocationByLvl_1(newParams));
    }
    if (_.includes(expandData, item.short_id)) {
      const newExpandData = _.reject(expandData, i => i === item.short_id);
      setExpandData(newExpandData);
    } else setExpandData([...expandData, item.short_id]);
  };

  const onLabelLevel_2_Click = (idx, item) => {
    const newParams = {
      parent_short_id: idx.short_id,
      loc_lvl: 3,
      size: 100,
      page: 1,
      short_id_lvl1: item.short_id,
    };

    if (idx.data_lvl3 === undefined) {
      dispatch(getLocationByLvl_2(newParams));
    } else {
      const findIndex = _.findIndex(addresses, i => i.short_id === item.short_id);
      const findIndexLvl2 = _.findIndex(
        addresses[findIndex].data_lvl2,
        i => i.short_id === idx.short_id,
      );
      dispatch(
        getOneAddress(
          _.omit(addresses[findIndex].data_lvl2[findIndexLvl2], 'data_lvl2', 'data_lvl3'),
        ),
      );
    }

    if (_.includes(expandData, idx.short_id)) {
      const newExpandData = _.reject(expandData, i => i === idx.short_id);
      setExpandData(newExpandData);
    } else setExpandData([...expandData, idx.short_id]);
  };

  const onLabelLevel_3_Click = item => {
    dispatch(getOneAddress(item));
  };

  return (
    <Grid container spacing={3}>
      <Grid item md={4} xs={6}>
        <Card className={clsx(classes.root, className)} {...rest}>
          <div className={classes.chipsExpand}>
            <Chip key={2} label="Collapse All" className={classes.chipExpand} />
            <Chip key={3} label="Expand All" className={classes.chipExpand} />
          </div>
          <TreeView
            className={classes.root}
            defaultCollapseIcon={<ExpandMore />}
            defaultExpandIcon={<ChevronRight />}
            expanded={expandData}
          >
            {addresses &&
              addresses.map(item => (
                <TreeItem
                  nodeId={item.short_id}
                  label={item.name}
                  onLabelClick={() => onLabelLevel_1_Click(item)}
                >
                  {item.data_lvl2 ? (
                    item.data_lvl2.map(idx => (
                      <TreeItem
                        nodeId={idx.short_id}
                        label={idx.name}
                        onLabelClick={() => onLabelLevel_2_Click(idx, item)}
                      >
                        {idx.data_lvl3 ? (
                          idx.data_lvl3.map(i => (
                            <TreeItem
                              nodeId={i.short_id}
                              label={i.name}
                              onLabelClick={() => onLabelLevel_3_Click(i)}
                            />
                          ))
                        ) : (
                          <TreeItem nodeId="" label="" />
                        )}
                      </TreeItem>
                    ))
                  ) : (
                    <TreeItem nodeId="" label="" />
                  )}
                </TreeItem>
              ))}
          </TreeView>
        </Card>
      </Grid>
      <Grid item md={8} xs={18}>
        <div className={classes.chips}>
          <Chip key={1} label="Add News" className={classes.chip} />
        </div>
        <Card className={clsx(classes.root, className)} {...rest}>
          <Typography variant="h4">Location Information</Typography>
          <FormAddress />
        </Card>
      </Grid>
    </Grid>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  addresses: PropTypes.array,
};

Results.defaultProps = {
  addresses: [],
};

export default Results;
