import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
  Select,
  Switch,
  makeStyles,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { getOneAddress } from 'src/actions/addressActions';

const useStyles = makeStyles(() => ({
  root: {},
  selectBirthDay: {
    width: '100%',
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  cardPermission: {
    marginTop: '16px',
  },
  buttonArrowDropDown: {
    minWidth: 0,
  },
  inputLabel: {
    marginTop: '8px',
    marginBottom: '8px',
  },
}));

function FormAddress({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const history = useHistory();
  const { address } = useSelector(state => state.address);

  return (
    <Formik
      enableReinitialize
      initialValues={{
        full_name: address.full_name || '',
        name: address.name || '',
        short_id: address.short_id || '',
        parent_short_id: address.parent_short_id || '',
        latitude: address.latitude || 0,
        longitude: address.longitude || 0,
      }}
      validationSchema={Yup.object().shape({
        full_name: Yup.string(),
        name: Yup.string(),
        short_id: Yup.string(),
        parent_short_id: Yup.string(),
        latitude: Yup.number(),
        longitude: Yup.number(),
      })}
      onSubmit={async (values, { resetForm, setErrors, setStatus, setSubmitting }) => {
        try {
          // Make API request
          console.log(values);
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values,
      }) => (
        <form className={clsx(classes.root, className)} onSubmit={handleSubmit} {...rest}>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <Grid item md={12} xs={12}>
                <InputLabel className={clsx(classes.inputLabel)} shrink id="full_name_label">
                  Full Name
                </InputLabel>
                <TextField
                  error={Boolean(touched.full_name && errors.full_name)}
                  fullWidth
                  helperText={touched.full_name && errors.full_name}
                  label="Full Name"
                  name="full_name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  required
                  value={values.full_name}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={12} xs={12}>
                <InputLabel className={clsx(classes.inputLabel)} shrink id="short_name_label">
                  Short Name
                </InputLabel>
                <TextField
                  error={Boolean(touched.name && errors.name)}
                  fullWidth
                  helperText={touched.name && errors.name}
                  label="Short name"
                  name="name"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.name}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={12} xs={12}>
                <InputLabel className={clsx(classes.inputLabel)} shrink id="short_id_label">
                  Short ID
                </InputLabel>
                <TextField
                  error={Boolean(touched.short_id && errors.short_id)}
                  fullWidth
                  helperText={touched.short_id && errors.short_id}
                  label="Short ID"
                  name="short_id"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.short_id}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={12} xs={12}>
                <InputLabel className={clsx(classes.inputLabel)} shrink id="parent_short_id_label">
                  Parent Short ID
                </InputLabel>
                <TextField
                  error={Boolean(touched.parent_short_id && errors.parent_short_id)}
                  fullWidth
                  helperText={touched.parent_short_id && errors.parent_short_id}
                  label="Parent Short ID"
                  name="parent_short_id"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.parent_short_id}
                  variant="outlined"
                />
              </Grid>
              <Grid container spacing={2}>
                <Grid item md={12} xs={12}>
                  <InputLabel className={clsx(classes.inputLabel)} shrink id="geographic_label">
                    Geographic Coordinates
                  </InputLabel>
                </Grid>
                <Grid item md={6} xs={12}>
                  <InputLabel className={clsx(classes.inputLabel)} shrink id="latitude_label">
                    Latitude
                  </InputLabel>
                  <TextField
                    fullWidth
                    label="Latitude"
                    name="latitude"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.latitude}
                    variant="outlined"
                  />
                </Grid>
                <Grid item md={6} xs={12}>
                  <InputLabel className={clsx(classes.inputLabel)} shrink id="longitude_label">
                    Longitude
                  </InputLabel>
                  <TextField
                    fullWidth
                    label="Longitude"
                    name="longitude"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.longitude}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item md={6} xs={12}>
              {address.latitude ? (
                <iFrame
                  src={`https://www.google.com/maps/embed/v1/place?q=${address.latitude}%2C${address.longitude}&key=AIzaSyDtKkOZ4LFFoX8wkVFhgk726xc20_mm8Y8`}
                  width="100%"
                  height="500px"
                  name="the-iFrame"
                  frameBorder="0"
                  allowFullScreen
                />
              ) : (
                <img
                  src="https://storage.googleapis.com/support-forums-api/attachment/thread-9669071-5362475160764970172.jpg"
                  alt="oops"
                  style={{ height: 700 }}
                />
              )}
            </Grid>
          </Grid>
        </form>
      )}
    </Formik>
  );
}

export default FormAddress;
