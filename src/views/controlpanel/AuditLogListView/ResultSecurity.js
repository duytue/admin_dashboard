/* eslint-disable max-len */
import React, { useState, useCallback } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  makeStyles,
  IconButton,
  SvgIcon
} from '@material-ui/core';
import { ArrowRight as ArrowRightIcon } from 'react-feather';
import _ from 'lodash';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {},
  queryField: {
    width: 500
  },

  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

const rows = [
  {
    id: 1,
    _id: 'AL-1',
    date_time: '10-02-1996',
    user: 'Dao Quang Trung',
    platform: 'APP-mobile',
    ip: '185.57.96.146',
    resource: 'Resource - 1',
    action: 'update'
  },
  {
    id: 2,
    _id: 'AL-1',
    date_time: '10-02-1996',
    user: 'Dao Quang Trung',
    platform: 'APP-mobile',
    ip: '185.57.96.146',
    resource: 'Resource - 1',
    action: 'update'
  }
];

function ResultSecurity({ className, dataAuditLog, ...rest }) {
  const classes = useStyles();
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = event => {
    setLimit(event.target.value);
  };
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        onChangePage={handlePageChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
        onChangeRowsPerPage={handleLimitChange}
        count={10}
      />
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table stickyHeader className={classes.tableWidth}>
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Date time</TableCell>
                <TableCell>User</TableCell>
                <TableCell>Platform</TableCell>
                <TableCell>IP Address</TableCell>
                <TableCell>Resource</TableCell>
                <TableCell>Action</TableCell>
                <TableCell>Result</TableCell>
              </TableRow>
            </TableHead>
            {dataAuditLog.length !== 0 ? (
              <TableBody>
                {dataAuditLog.map(row => (
                  <TableRow key={row.id} hover>
                    <TableCell>{row.log_type_id}</TableCell>
                    <TableCell>
                      {' '}
                      {moment(row.updated_at).format('DD/MM/YYYY | HH:MM')}
                    </TableCell>
                    <TableCell></TableCell>
                    <TableCell>{row.platform_key}</TableCell>
                    <TableCell>{row.ip_address}</TableCell>
                    <TableCell>{row.resource_affected}</TableCell>
                    <TableCell>{row.action}</TableCell>
                    <TableCell>
                      <IconButton
                        component={RouterLink}
                        to={`/app/controlpanel/auditlog/${row.id}`}
                      >
                        <SvgIcon fontSize="small">
                          <ArrowRightIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            ) : null}
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        onChangePage={handlePageChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
        onChangeRowsPerPage={handleLimitChange}
        count={10}
      />
    </div>
  );
}

ResultSecurity.propTypes = {
  className: PropTypes.string
};

export default ResultSecurity;
