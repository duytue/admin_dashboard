/* eslint-disable max-len */
import React, { useState, useCallback } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import {
  Box,
  Card,
  Divider,
  InputAdornment,
  SvgIcon,
  Tab,
  Tabs,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import _ from 'lodash';
import ResultSystem from './ResultSystem';
import ResultSecurity from './ResultSecurity';

const tabs = [
  {
    value: 'security',
    label: 'Security'
  },
  {
    value: 'system',
    label: 'System'
  }
];

const useStyles = makeStyles(theme => ({
  root: {},
  queryDate: {
    margin: theme.spacing(2)
  },
  queryField: {
    width: 500
    // margin: theme.spacing(2)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));
const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  },
  {
    value: 'platform|desc',
    label: 'Platform'
  }
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(users, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = users.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map(el => el[0]);
}

function TabsAuditLog({ className, dataAuditLog, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('security');
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const [fromDate, handleFromDateChange] = useState(new Date());
  const [toDate, handleToDateChange] = useState(new Date());
  const [filters, setFilters] = useState({
    security: null,
    system: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      security: null,
      system: null
    };

    if (value !== 'security') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setCurrentTab(value);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div role="tabpanel" hidden={value !== index} {...other}>
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  const handleQueryChange = event => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = event => {
    event.persist();
    setSort(event.target.value);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <KeyboardDatePicker
          autoOk
          variant="inline"
          inputVariant="outlined"
          label="From"
          format="DD/MM/yyyy"
          value={fromDate}
          onChange={date => handleFromDateChange(date)}
        />
        <KeyboardDatePicker
          className={classes.queryDate}
          autoOk
          variant="inline"
          inputVariant="outlined"
          label="To"
          format="DD/MM/yyyy"
          value={toDate}
          onChange={date => handleToDateChange(date)}
        />
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search "
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />

        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TabPanel value={currentTab} index={'security'}>
        <PerfectScrollbar>
          <Box minWidth={700}>
            <ResultSecurity dataAuditLog={dataAuditLog} />
          </Box>
        </PerfectScrollbar>
      </TabPanel>
      <TabPanel value={currentTab} index={'system'}>
        <PerfectScrollbar>
          <Box minWidth={700}>
            <ResultSystem />
          </Box>
        </PerfectScrollbar>
      </TabPanel>
    </Card>
  );
}

TabsAuditLog.propTypes = {
  className: PropTypes.string
};

export default TabsAuditLog;
