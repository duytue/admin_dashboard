import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import TabsAuditLog from './TabsAuditLog';
import { useDispatch, useSelector } from 'react-redux';
import { getListLog } from 'src/actions/logAction';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function AuditLogListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { logs } = useSelector(state => {
    return state.auditLog;
  });
  useEffect(() => {
    dispatch(getListLog());
  }, [dispatch]);
  // if (logs.length !== 0) {
  //   return null;
  // }
  return (
    <Page className={classes.root} title="Audit Log">
      <Container maxWidth={false}>
        <Header />
        <Box mt={3}>
          <TabsAuditLog dataAuditLog={logs} />
        </Box>
      </Container>
    </Page>
  );
}

export default AuditLogListView;
