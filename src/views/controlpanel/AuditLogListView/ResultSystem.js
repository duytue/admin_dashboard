/* eslint-disable max-len */
import React, { useState, useCallback } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  IconButton,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  makeStyles
} from '@material-ui/core';
import { ArrowRight as ArrowRightIcon } from 'react-feather';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
  // tableWidth: {
  //   width: '1000px',
  // },
}));

const rows = [
  {
    id: 1,
    _id: 'AL-1',
    date_time: '10-02-1996',
    user: 'Dao Quang Trung',
    platform: 'APP-mobile',
    ip: '185.57.96.146',
    resource: 'Resource - 1',
    action: 'update'
  },
  {
    id: 2,
    _id: 'AL-1',
    date_time: '10-02-1996',
    user: 'Dao Quang Trung',
    platform: 'APP-mobile',
    ip: '185.57.96.146',
    resource: 'Resource - 1',
    action: 'update'
  }
];
function ResultSystem({ className, ...rest }) {
  const classes = useStyles();
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };
  const handleLimitChange = event => {
    setLimit(event.target.value);
  };
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        onChangePage={handlePageChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
        onChangeRowsPerPage={handleLimitChange}
        count={10}
      />
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table stickyHeader className={classes.tableWidth}>
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Date time</TableCell>
                <TableCell>Platform</TableCell>
                <TableCell>Resource</TableCell>
                <TableCell>Action</TableCell>
                <TableCell>Result</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.id} hover>
                  <TableCell>{row._id}</TableCell>
                  <TableCell>{row.date_time}</TableCell>
                  <TableCell>{row.platform}</TableCell>
                  <TableCell>{row.resource}</TableCell>
                  <TableCell>{row.action}</TableCell>
                  <TableCell>
                    <IconButton
                      component={RouterLink}
                      to={`/app/controlpanel/auditlog/${row.id}`}
                    >
                      <SvgIcon fontSize="small">
                        <ArrowRightIcon />
                      </SvgIcon>
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 10, 25]}
        onChangePage={handlePageChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
        onChangeRowsPerPage={handleLimitChange}
        count={10}
      />
    </div>
  );
}

ResultSystem.propTypes = {
  className: PropTypes.string
};

export default ResultSystem;
