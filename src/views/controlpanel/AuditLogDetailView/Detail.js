/* eslint-disable max-len */
import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';

import {
  Box,
  Card,
  CardContent,
  Divider,
  Tab,
  Grid,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Tabs,
  Typography,
  makeStyles,
  TextField
} from '@material-ui/core';
import _ from 'lodash';

const tabs = [
  {
    value: 'normal_view',
    label: 'Normal View'
  },
  {
    value: 'json_view',
    label: 'JSON View'
  }
];

const useStyles = makeStyles(theme => ({
  root: {},
  queryDate: {
    margin: theme.spacing(2)
  },
  queryField: {
    width: 500
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold'
  },
  fontWeightMedium: {
    fontSize: 18
  }
}));

function DetailAuditLog({ className, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('normal_view');
  const [filters, setFilters] = useState({
    security: null,
    system: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      normal_view: null,
      json_view: null
    };

    if (value !== 'normal_view') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setCurrentTab(value);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div role="tabpanel" hidden={value !== index} {...other}>
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  return (
    <div>
      <CardContent>
        <Typography
          //   className={classes.title}
          color="textSecondary"
          variant="h1"
          align="Left"
          gutterBottom
        >
          DETAIL
        </Typography>
      </CardContent>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <TabPanel value={currentTab} index={'normal_view'}>
        <Grid
          className={clsx(classes.root, className)}
          container
          spacing={3}
          {...rest}
        >
          <Grid item lg={6} md={6} xl={4} xs={12}>
            <Card className={clsx(classes.root, className)} {...rest}>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      User
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        Dao Quang Trung
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Even ID
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        AL-1
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Type
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        Security
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Source
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        Resource-1
                      </Typography>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Card>
          </Grid>
          <Grid item lg={6} md={6} xl={4} xs={12}>
            <Card className={clsx(classes.root, className)} {...rest}>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Logged at
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        10:00 - 31/05/2020
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      IP
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        185.57.94.196
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Platform
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        APP-mobile
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell className={classes.fontWeightMedium}>
                      Actions
                    </TableCell>
                    <TableCell>
                      <Typography variant="body1" color="textSecondary">
                        Update
                      </Typography>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Card>
          </Grid>
          <Grid item lg={12} md={12} xl={4} xs={12}>
            <TextField
              id="outlined-multiline-static"
              fullWidth={true}
              multiline
              rows={4}
              defaultValue="Cum sociis natoque penatibus et magnis dis parturient. 
              Gallia est omnis divisa in partes tres, quarum.
               Prima luce, cum quibus mons aliud consensu ab eo."
              variant="outlined"
              disabled={true}
            ></TextField>
          </Grid>
        </Grid>
      </TabPanel>
      <TabPanel value={currentTab} index={'json_view'}>
        <PerfectScrollbar>
          <Box minWidth={700}>{/* <ResultSystem /> */}</Box>
        </PerfectScrollbar>
      </TabPanel>
    </div>
  );
}

DetailAuditLog.propTypes = {
  className: PropTypes.string
};

export default DetailAuditLog;
