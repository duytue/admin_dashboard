import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import DetailAuditLog from './Detail';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function AuditLogDetailView() {
  const classes = useStyles();

  return (
    <Page className={classes.root} title="Audit Log Details">
      <Container maxWidth={false}>
        <Header />
        <Box mt={3}>
          <DetailAuditLog />
        </Box>
      </Container>
    </Page>
  );
}

export default AuditLogDetailView;
