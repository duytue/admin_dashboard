import React, { useState } from 'react';
import {
  Container, Button, ButtonGroup,
  Grid,
  IconButton,
  makeStyles,
  Box,
  Typography,
  Card,
  CardContent,
} from '@material-ui/core';
import {
  ChevronRight as ChevronRightIcon,
  ChevronLeft as ChevronLeftIcon,
  Edit as EditIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import { Alert } from '@material-ui/lab';
import Page from 'src/components/Page';
import Header from './Header';
import mockData from './data';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  container: {
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 64,
      paddingRight: 64
    }
  },
  pagination: {
    marginTop: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  content: {
    marginTop: theme.spacing(2)
  },
  imageContainer: {
    height: 64,
    width: 64,
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  blockPreNext: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  elementItemTitle: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
}));

function DashboardView() {
  const classes = useStyles();
  const [isAlertVisible, setAlertVisible] = React.useState(true);
  const [products] = useState(mockData);

  return (
    <Page
      className={classes.root}
      title="Dashboard"
    >
      <Container
        maxWidth={false}
        className={classes.container}
      >
        <Header />

        {isAlertVisible && (
          <Box mb={3}>
            <Alert
              onClose={() => setAlertVisible(false)}
              severity="info"
            >
              <Typography
                variant="h4"
                color="textSecondary"
              >
                CURRENT WORKING PLATFORM
                </Typography>
              <div>
                You are woking on Platform "SELECT PLATFORM NAME".
                If this is not correct one, you can change it by clicking here or select from drop-dơwn on Top nav bar.
                </div>
            </Alert>
          </Box>
        )}

        <Typography
          variant="h3"
          color="textSecondary"
        >
          Global Announcements
        </Typography>

        <div className={classes.blockPreNext}>
          <div>
            <Typography variant="caption">1-6 of 20</Typography>
            <IconButton>
              <ChevronLeftIcon />
            </IconButton>
            <IconButton>
              <ChevronRightIcon />
            </IconButton>
          </div>

          <Button
            variant="contained"
            color='secondary'
          >
            + Add Announcements
          </Button>
        </div>

        <div className={classes.content}>
          <Grid
            container
            spacing={3}
          >
            {products.map(product => (
              <Grid
                item
                key={product.id}
                lg={4}
                md={6}
                xs={12}
              >
                <Typography
                  variant="h4"
                  color="textSecondary"
                >
                  <Card>
                    <CardContent>
                      <div className={classes.elementItemTitle}>
                        <div>

                        </div>
                        <Typography
                          gutterBottom
                          variant="h4"
                        >
                          Announcements Titles
                        </Typography>
                        <Typography
                          gutterBottom
                          variant="overline"
                        >
                          11 hours ago
                        </Typography>
                      </div>
                      <Typography
                        gutterBottom
                        variant="body2"
                      >
                        Lorem ipsum dolor sit amet consectetur adipiscing elit. aliquam tincidunt elementum sem non luctus
                        lorem ipsum dolor sit amet consectetur adipiscing elit. aliquam tincidunt elementum sem non luctus
                      </Typography>
                      <ButtonGroup variant="text" color="primary" aria-label="text primary button group" className={classes.pagination}>
                        <Button >
                          <EditIcon />
                        </Button>
                        <Button>
                          <DeleteIcon />
                        </Button>
                      </ButtonGroup>
                    </CardContent>
                  </Card>
                </Typography>
              </Grid>
            ))}
          </Grid>
        </div>

        <div className={classes.pagination}>
          <Typography variant="caption">1-6 of 20</Typography>
          <IconButton>
            <ChevronLeftIcon />
          </IconButton>
          <IconButton>
            <ChevronRightIcon />
          </IconButton>
        </div>

      </Container>
    </Page>
  );
}

export default DashboardView;
