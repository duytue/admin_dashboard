/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  makeStyles,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Input
} from '@material-ui/core';
import { upload, clearResponse } from 'src/actions/massuploadActions';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import printer from './printer';
import FilesDropzonev2 from 'src/components/FilesDropzonev2';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'created_at|desc',
    label: 'Last create (newest first)'
  },
  {
    value: 'created_at|asc',
    label: 'Last create (oldest first)'
  }
];

function applyFilters(orderProcesses, query, filters) {
  return orderProcesses.filter((orderProcesses) => {
    let matches = true;

    if (query) {
      const properties = ['state'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          orderProcesses[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && orderProcesses[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(orderProcesses, page, limit) {
  return orderProcesses.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(orderProcesses, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = orderProcesses.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '20px 40px'
  },
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  flexText: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '15px'
  },
  smallFlex: {
    display: 'flex',
    flexDirection: 'column'
  },
  smallFlexInvi: {
    display: 'flex',
    flexDirection: 'column',
    color: 'transparent'
  },
  outsideDialog: {
    // width:
  },
  dialogContainer: {
    padding: '20px 40px'
  },
  smallMarginBottom: {
    marginBottom: '15px'
  },
  bigTitle: {
    marginBottom: '20px'
  },
  smallTitle: {
    fontWeight: 'bold',
    fontSize: '1.3em'
  },
  pictureFrame: {
    width: '250px',
    height: '150px',
    border: '1px solid grey'
  },
  smallPictureFrame: {
    width: '180px',
    height: '120px'
  },
  alignCenter: {
    alignItems: 'center'
  },
  justifyCenter: {
    justifyContent: 'center'
  },
  imageStyle: {
    width: '100%',
    height: '100%'
  },
  bigIcon: {
    justifyContent: 'center',
    fontSize: '8em'
  },
  dFlex: {
    display: 'flex'
  },
  marginLeft: {
    marginLeft: '75px'
  },
  qrScanner: {
    width: '500px'
  },
  fWidth: {
    width: '100%'
  },
  flexColumn: {
    flexDirection: 'column'
  }
}));

function Results({ className, orderProcesses, ...rest }) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { massuploadResponse } = useSelector((state) => {
    return state.massuploadResponse;
  });
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleSubmit = (event) => {
    event.persist();
    event.preventDefault();
    if (uploadFile.length == 0) {
      enqueueSnackbar("Please choose file to upload.", {
        variant: 'error',
      });
    }
    let form = new FormData(event.target);
    form.append("type", uploadObject);
    form.append("file", uploadFile[0]);
    form.append("platform_key", "centralweb");
    dispatch(upload(form));
  };

  useEffect(() => {
    if (massuploadResponse) {
      if (massuploadResponse.code == "success") {
        enqueueSnackbar("Upload successfully!", {
          variant: 'success',
        });
        dispatch(clearResponse());
        setUploadFile([]);
        dropzoneRef.current.removeAllFiles();
      } else {
        enqueueSnackbar("Upload failed!", {
          variant: 'error',
        });
      }
    }
  }, [massuploadResponse]);

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(orderProcesses, query, filters);
  const sorted = applySort(filtered, sort);
  const [paginated, setPaginated] = useState([]);
  const [uploadObject, setUploadObject] = useState('');
  const [uploadFile, setUploadFile] = useState([]);

  useEffect(() => {
    setPaginated(applyPagination(sorted, page, limit));
  }, []);

  const onSelectObject = (event) => {
    setUploadObject(event.target.value);
  };

  const { useRef } = React;
  const dropzoneRef = useRef();

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <FormControl variant="outlined" className={`${classes.formControl}`}>
        <form onSubmit={handleSubmit}>
          <div className={`${classes.smallMarginBottom}`}>
            <InputLabel id="object-label">Choose object to upload</InputLabel>
            <Select
              labelId="object-label"
              id="object-id"
              value={uploadObject}
              onChange={onSelectObject}
              label="Choose object to upload"
              className={`${classes.fWidth} ${classes.smallMarginBottom}`}
            >
              <MenuItem key={'object-1'} value={'pricing'}>
                Pricing
              </MenuItem>
              <MenuItem key={'object-2'} value={'stock'}>
                Stock
              </MenuItem>
              <MenuItem key={'object-3'} value={'items'}>
                Product
              </MenuItem>
            </Select>
          </div>
          <FilesDropzonev2 needUploadButton={false} setFormUploadFile={setUploadFile} ref={dropzoneRef} />
          <Button
            color="secondary"
            size="small"
            variant="contained"
            type="submit"
          >
            Upload file
          </Button>
        </form>
      </FormControl>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  orderProcesses: PropTypes.array
};

Results.defaultProps = {
  orderProcesses: []
};

export default Results;
