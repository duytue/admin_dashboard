import React from 'react';
import {
  useDispatch,
  // useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles
} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { createRole } from 'src/actions/roleActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  return (
    <Formik
      initialValues={{
        name: '',
        description: '',
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        description: Yup.string(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          const newsValue = _.omit(values, ['name'])
          dispatch(createRole({
            ...newsValue,
            name: _.startCase(values.name),
            key: `${_.kebabCase(_.join(_.slice(_.words(values.name), 0, 2)))}${_.lowerCase(_.join(_.slice(_.words(values.name), 2)))}`,
            platform_key: "centralweb",
          })).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Role created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/roles"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>
                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={12}
                    xs={24}
                  >
                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={24}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Role
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CustomerEditForm.propTypes = {
  className: PropTypes.string,
};

export default CustomerEditForm;
