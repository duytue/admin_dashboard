import CustomerInfo from './CustomerInfo';
import clsx from 'clsx';
import { Grid, makeStyles } from '@material-ui/core';
import Order from './Order';
import OtherActions from './OtherActions';
import React, {
  useCallback,
  useState,
  useEffect
} from 'react';
import Page from 'src/components/Page';
import axios from 'src/utils/axios';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import { useDispatch, useSelector } from 'react-redux';
import { getOneCustomer } from 'src/actions/customerAction';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function Details() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const dispatch = useDispatch();
  const customerId = _.trim(window.location.pathname.slice(25), '/');
  const { customer } = useSelector((state) => {
    return state.customer
  });

  useEffect(() => {
    dispatch(getOneCustomer(customerId));
  }, [isMountedRef]);

  if (!customer) {
    return null;
  }


  return (
    <Grid
      className={clsx(classes.root)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <CustomerInfo customer={customer} />
      </Grid>
      {/* <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <Invoices customer={customer} />
      </Grid> */}
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <Order customer={customer} />
      </Grid>
      <Grid
        item
        lg={4}
        md={6}
        xl={3}
        xs={12}
      >
        <OtherActions customer={customer} />
      </Grid>
    </Grid>
  );
}
export default Details;
