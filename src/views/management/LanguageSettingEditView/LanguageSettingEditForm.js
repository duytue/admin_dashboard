import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
  Select,
  Switch,
  makeStyles,
  FormHelperText
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { getLanguageSetting, updateLanguageSetting } from '../../../actions/languageSettingActions';
import { getListPlatform } from 'src/actions/platformActions';
const useStyles = makeStyles(() => ({
  root: {
    '.MuiSelect-icon': {
      display: 'none'
    }
  },
  selectBirthDay: {
    width: '100%',
    marginTop: 0
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  cardPermission: {
    marginTop: '16px'
  },
  buttonArrowDropDown: {
    minWidth: 0
  }
}));

function LanguageSettingEditForm({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  // TODO
  const language_setting_key = _.trim(window.location.pathname.slice(33, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const [openPlatformSelect, setOpenPlatformSelect] = useState(false);
  const { language_setting } = useSelector(state => {
    return state.language_setting;
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  });
  useEffect(() => {
    dispatch(getListPlatform())
  }, [dispatch])

  useEffect(() => {
    dispatch(getLanguageSetting(language_setting_key));
  }, [dispatch, language_setting_key]);

  if (_.size(language_setting) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        key: language_setting[0].key || '',
        value: language_setting[0].value || '',
        description: language_setting[0].description || '',
        language_key: language_setting[0].language_key || '',
        platform_key: language_setting[0].platform_key || ''
      }}
      validationSchema={Yup.object().shape({
        key: Yup.string().max(255).required('Key is required'),
        value: Yup.string().max(255).required('Value is required'),
        description: Yup.string().max(255),
        language_key: Yup.string().max(255).required('Language Key is required'),
        platform_key: Yup.string().max(255).required('Platform Key is required')
      })}
      onSubmit={async (
        values,
        { resetForm, setErrors, setStatus, setSubmitting }
      ) => {
        try {
          dispatch(updateLanguageSetting(values)).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Language Setting updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push('/app/management/language-setting');
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Card>
              <CardContent>
                {/* Key */}
                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.key && errors.key)}
                      fullWidth
                      helperText={touched.key && errors.key}
                      label="Key"
                      name="key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.key}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Key */}
                  {/* Value */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.value && errors.value)}
                      fullWidth
                      helperText={touched.value && errors.value}
                      label="Value"
                      name="value"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.value}
                      variant="outlined"
                    />
                  </Grid>
                  {/* Language Key */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.language_key && errors.language_key)}
                      fullWidth
                      helperText={touched.language_key && errors.language_key}
                      label="Language Key"
                      name="language_key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      required
                      value={values.language_key}
                      variant="outlined"
                    />
                  </Grid>
                  {/* Platform Key */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    {/* <TextField
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      helperText={touched.platform_key && errors.platform_key}
                      label="Platform Key"
                      name="platform_key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.platform_key}
                      variant="outlined"
                    /> */}
                    <InputLabel error={Boolean(touched.platform_key && errors.platform_key)} id="platform-select-label">Platform</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="platform_key"
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      required
                      value={values.platform_key}
                      onChange={handleChange}
                    >
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.key} value={item.key}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.platform_key && errors.platform_key}</FormHelperText>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Edit Language Setting
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

LanguageSettingEditForm.propTypes = {
  className: PropTypes.string
};

export default LanguageSettingEditForm;
