import React from 'react';
import {
    Box,
    Container,
    makeStyles
  } from '@material-ui/core';
  import Page from 'src/components/Page';
  import LanguageSettingEditForm from './LanguageSettingEditForm';
  import Header from './Header';
  
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3)
    }
  }));
  
  function LanguageSettingEditView() {
    const classes = useStyles();
  
    return (
      <Page
        className={classes.root}
        title="Language Setting Edit"
      >
        <Container maxWidth="lg">
          <Header />
          <Box mt={3}>
            <LanguageSettingEditForm />
          </Box>
        </Container>
      </Page>
    );
  }
  
  export default LanguageSettingEditView;
