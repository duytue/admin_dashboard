/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import moment from 'moment';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Search as SearchIcon
} from 'react-feather';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCustomerFee } from 'src/actions/customerFeeActions';
import { getUsers } from 'src/actions/userActions';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

// const sortOptions = [
//   {
//     value: 'updated_at|desc',
//     label: 'Last update (newest first)'
//   },
//   {
//     value: 'updated_at|asc',
//     label: 'Last update (oldest first)'
//   },
//   {
//     value: 'orders|desc',
//     label: 'Total orders (high to low)'
//   },
//   {
//     value: 'orders|asc',
//     label: 'Total orders (low to high)'
//   }
// ];

function applyFilters(customerFees, query, filters, dates) {
  return customerFees.filter((fee) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (fee[property] && fee[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    if (dates.filterStartDate && !dates.filterEndDate) {
      if (moment(fee.start_time).format('DD/MM/YYY') >= moment(dates.filterStartDate).format('DD/MM/YYY')) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (!dates.filterStartDate && dates.filterEndDate) {
      if (moment(fee.end_time).format('DD/MM/YYY') <= moment(dates.filterEndDate).format('DD/MM/YYY')) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (dates.filterStartDate && dates.filterEndDate) {
      if (moment(fee.start_time).format('DD/MM/YYY') >= moment(dates.filterStartDate).format('DD/MM/YYY')
        && moment(fee.end_time).format('DD/MM/YYY') <= moment(dates.filterEndDate).format('DD/MM/YYY')) {
        matches = true;
      } else {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && fee[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(customerFees, page, limit) {
  return customerFees.slice(page * limit, page * limit + limit);
}

// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }

//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }

//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// function applySort(customerFees, sort) {
//   const [orderBy, order] = sort.split('|');
//   const comparator = getComparator(order, orderBy);
//   const stabilizedThis = customerFees.map((el, index) => [el, index]);

//   stabilizedThis.sort((a, b) => {
//     // eslint-disable-next-line no-shadow
//     const order = comparator(a[0], b[0]);

//     if (order !== 0) return order;

//     return a[1] - b[1];
//   });

//   return stabilizedThis.map((el) => el[0]);
// }

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  datePicker: {
    marginLeft: theme.spacing(2)
  }
}));

function Results({
  className, customerFees, ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.user);

  const [filterStartDate, setFilterStartDate] = useState(null);
  const [filterEndDate, setFilterEndDate] = useState(null);
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedFees, setselectedFees] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setselectedFees([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSelectAllFees = (event) => {
    setselectedFees(
      event.target.checked ? customerFees.map((fee) => fee.id) : []
    );
  };

  const handleDeleteAllFee = () => {
    selectedFees.map((fee) => dispatch(deleteCustomerFee(fee)));
  };

  const handleSelectOneFee = (event, feeId) => {
    if (!selectedFees.includes(feeId)) {
      setselectedFees((prevSelected) => [...prevSelected, feeId]);
    } else {
      setselectedFees((prevSelected) => prevSelected.filter((id) => id !== feeId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const filteredFees = applyFilters(customerFees, query, filters, { filterStartDate, filterEndDate });
  // const sortedFees = applySort(filteredFees, sort);
  const paginatedFees = applyPagination(filteredFees, page, limit);
  const enableBulkOperations = selectedFees.length > 0;
  const selectedSomeFees = selectedFees.length > 0 && selectedFees.length < customerFees.length;
  const selectedAllUsers = selectedFees.length === customerFees.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search Customer Fee"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <KeyboardDatePicker
          className={classes.datePicker}
          disableToolbar
          inputVariant="outlined"
          format="DD/MM/YYYY"
          label="Start"
          value={filterStartDate}
          onChange={(value) => setFilterStartDate(value)}
          KeyboardButtonProps={{
            'aria-label': 'start date filter',
          }}
        />
        <KeyboardDatePicker
          className={classes.datePicker}
          disableToolbar
          inputVariant="outlined"
          format="DD/MM/YYYY"
          label="End"
          value={filterEndDate}
          onChange={(value) => setFilterEndDate(value)}
          KeyboardButtonProps={{
            'aria-label': 'end date filter',
          }}
        />
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredFees.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeFees}
              onChange={handleSelectAllFees}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllFee}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeFees}
                    onChange={handleSelectAllFees}
                  />
                </TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Starts</TableCell>
                <TableCell>Ends</TableCell>
                <TableCell>Created At</TableCell>
                {/* <TableCell>Created By</TableCell> */}
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedFees.map((fee) => {
                const isFeeSelected = selectedFees.includes(fee.id);
                const currentUser = users.find((user) => user.id === fee.creator_id);

                return (
                  <TableRow hover key={fee.id} selected={isFeeSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isFeeSelected}
                        onChange={(event) => handleSelectOneFee(event, fee.id)}
                        value={isFeeSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/management/customer-fee/edit/${fee.id}`}
                        variant="h6"
                      >
                        {fee.name}
                      </Link>
                    </TableCell>
                    <TableCell>{moment(fee.start_time).format('hh:mm DD-MMM-YYYY')}</TableCell>
                    <TableCell>{moment(fee.end_time).format('hh:mm DD-MMM-YYYY')}</TableCell>
                    <TableCell>{moment(fee.created_at).format('hh:mm DD-MMM-YYYY')}</TableCell>
                    {/* <TableCell>{currentUser && currentUser.email}</TableCell> */}
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/customer-fee/edit/${fee.id}`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>

                      <IconButton component={RouterLink} to="/app/management/customer-fee" onClick={() => { dispatch(deleteCustomerFee(fee)); }}>
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>

                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredFees.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  customerFees: PropTypes.array
};

Results.defaultProps = {
  customerFees: []
};

export default Results;
