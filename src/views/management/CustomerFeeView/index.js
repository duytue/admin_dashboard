import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getCustomerFees } from 'src/actions/customerFeeActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerFeeListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { customerFees } = useSelector((state) => state.customer_fee);

  useEffect(() => {
    dispatch(getCustomerFees());
  }, [dispatch]);

  if (!customerFees) {
    return null;
  }

  return (
    <Page className={classes.root} title="Customer Fee Management">
      <Container maxWidth={false}>
        <Header />
        {customerFees && (
          <Box mt={3}>
            <Results customerFees={customerFees} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default CustomerFeeListView;
