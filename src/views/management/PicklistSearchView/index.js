import React, { useState, useEffect, useCallback } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PicklistSearchView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const dispatch = useDispatch();
  // const { picklists } = useSelector((state) => {
  //   return state.picklist;
  // });

  // useEffect(() => {
  //   dispatch(getList());
  // }, [dispatch]);
  // const [picklists, setPicklists] = useState([
  //   {
  //     id: 'id_1',
  //     trade_order_id: 'trade order id 1',
  //     state: 'Picked',
  //     is_printed: false,
  //     order_number: 'order number 1',
  //     picklist_id: 'picklist id 1',
  //     consolidate_at: 'station name 1'
  //   },
  //   {
  //     id: 'id_2',
  //     trade_order_id: 'trade order id 2',
  //     state: 'Picked',
  //     is_printed: false,
  //     order_number: 'order number 2',
  //     picklist_id: 'picklist id 2',
  //     consolidate_at: 'station name 2'
  //   },
  //   {
  //     id: 'id_3',
  //     trade_order_id: 'trade order id 3',
  //     state: 'Picked',
  //     is_printed: false,
  //     order_number: 'order number 3',
  //     picklist_id: 'picklist id 3',
  //     consolidate_at: 'station name 3'
  //   },
  //   {
  //     id: 'id_4',
  //     trade_order_id: 'trade order id 4',
  //     state: 'Picked',
  //     is_printed: false,
  //     order_number: 'order number 4',
  //     picklist_id: 'picklist id 4',
  //     consolidate_at: 'station name 4'
  //   },
  //   {
  //     id: 'id_5',
  //     trade_order_id: 'trade order id 5',
  //     state: 'Picked',
  //     is_printed: false,
  //     order_number: 'order number 5',
  //     picklist_id: 'picklist id 5',
  //     consolidate_at: 'station name 5'
  //   }
  // ]);

  // if (!picklists) {
  //   return null;
  // }

  return (
    <Page className={classes.root} title="Picklist List">
      <Container maxWidth={false}>
        <Header />
        <Box mt={3}>
          <Results />
        </Box>
      </Container>
    </Page>
  );
}

export default PicklistSearchView;
