import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as jsBarcode from "jsbarcode";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

export default function printer(picklist, station_to) {
  let canvas = document.createElement("canvas");
  jsBarcode(canvas, picklist.picklist_number, {
    format: "CODE128",
    displayValue: true
  });
  const docDefinition = {
    pageSize: { width: 480, height: 300 },
    content: [
      {
        margin: [180, 0, 0, 30],
        width: '100%',
        text: 'Bag A',
        bold: true,
        fontSize: 20
      },
      {
        columns: [
          {
            margin: [0, 0, 10, 20],
            width: 'auto',
            qr: picklist.picklist_number,
            alignment: 'center'
          },
          {
            width: 300,
            height: 115,
            alignment: 'center',
            image: canvas.toDataURL("image/png")
          }
        ]
      },
      {
        margin: [0, 0, 0, 5],
        width: '100%',
        text: 'Mã đơn hàng: ' + picklist.trade_order_number
      },
      {
        width: '100%',
        text: 'Gộp kiện tại: ' + station_to
      }
    ]
  };
  pdfMake.createPdf(docDefinition).print();
}
