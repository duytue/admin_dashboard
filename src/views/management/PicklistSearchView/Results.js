/* eslint-disable max-len */
import React, { useState, useEffect, useCallback } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  TextField,
  makeStyles,
  Button,
  Dialog,
  Divider,
  IconButton,
  Typography,
  TablePagination,
  Checkbox,
  Box,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  InputLabel,
  Select,
  MenuItem,
  FormControl
} from '@material-ui/core';
import {
  deletePicklist,
  update,
  checkConsolidation
} from 'src/actions/picklistActions';
import { getListStations } from 'src/actions/stationActions';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import printer from './printer';
import ScreenShareIcon from '@material-ui/icons/ScreenShare';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import QrReader from 'react-qr-reader';
import { withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { STATES, BAG_PACKING_MODAL_MESSAGE } from 'src/constants';
import { getList } from 'src/actions/picklistActions';
import _ from 'lodash';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'created_at|desc',
    label: 'Last create (newest first)'
  },
  {
    value: 'created_at|asc',
    label: 'Last create (oldest first)'
  }
];

function applyFilters(picklists, query, filters) {
  return picklists.filter((picklist) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (picklist[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && picklist[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(picklists, page, limit) {
  return picklists.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(picklists, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = picklists.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '30px'
  },
  marginLeft: {
    marginLeft: '75px'
  },
  qrScanner: {
    width: '500px'
  },
  bigFontSize: {
    fontSize: '3em'
  },
  dFlex: {
    display: 'flex'
  },
  searchContainer: {
    height: '56px'
  },
  alignCenter: {
    alignItems: 'center'
  },
  fullHeight: {
    height: '100%'
  },
  searchButtons: {
    border: '1px solid lightgrey'
  },
  hide: {
    display: 'none'
  },
  show: {
    display: 'block'
  },
  dialogContainer: {
    padding: '0 40px 20px'
  },
  flexText: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '15px'
  },
  smallFlex: {
    display: 'flex',
    flexDirection: 'column'
  },
  smallFlexInvi: {
    display: 'flex',
    flexDirection: 'column',
    color: 'transparent'
  },
  smallMargin: {
    marginBottom: '15px'
  },
  bigTitle: {
    marginBottom: '20px'
  },
  smallTitle: {
    fontWeight: 'bold',
    fontSize: '1.3em'
  },
  pictureFrame: {
    width: '250px',
    height: '150px',
    border: '1px solid grey'
  },
  smallPictureFrame: {
    width: '180px',
    height: '120px'
  },
  justifyCenter: {
    justifyContent: 'center'
  },
  imageStyle: {
    width: '100%',
    height: '100%'
  },
  bigIcon: {
    justifyContent: 'center',
    fontSize: '8em'
  },
  textFieldWidth: {
    width: '30vw'
  },
  justifyBetween: {
    justifyContent: 'space-between'
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  selectContainer: {
    marginLeft: '50px',
    width: '300px',
    height: '100%',
    '& > *': {
      height: '100%'
    }
  }
}));

function Results({ className, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const [showBagModal, setShowBagModal] = useState(false);
  const [currentStationId, setCurrentStationId] = useState('');
  const [scannerIsVisible, setScannerIsVisible] = useState(false);
  const [modalMessage1, setModalMessage1] = useState('');
  const [modalMessage2, setModalMessage2] = useState('');
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const dispatch = useDispatch();

  const { stations } = useSelector((state) => {
    return state.station;
  });
  const { picklists } = useSelector((state) => {
    return state.picklist;
  });
  const { consolidation } = useSelector((state) => {
    return state.picklist;
  });

  useEffect(() => {
    dispatch(getListStations());
    dispatch(getList([STATES.CHECKED_OUT], ''));
  }, [dispatch]);

  const { enqueueSnackbar } = useSnackbar();

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelected([]);
    setCurrentTab(value);
  };

  const handleSelectAll = (event) => {
    setSelected(event.target.checked ? picklists.map((obj) => obj.id) : []);
  };

  const handleDeleteAll = () => {
    if (window.confirm('Are you sure delete the item?')) {
      selected.map((obj) => {
        // obj is id
        dispatch(deletePicklist(obj));
      });
    }
  };

  const handleSelectOne = (event, objId) => {
    if (!selected.includes(objId)) {
      setSelected((prevSelected) => [...prevSelected, objId]);
    } else {
      setSelected((prevSelected) => prevSelected.filter((id) => id !== objId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const callPrinter = (currentPicklist) => {
    let stationName = '';
    stations.forEach((station) => {
      if (station.id == currentPicklist.station_to) {
        stationName = station.name;
      }
    });
    printer(
      currentPicklist,
      stationName
    );
  };

  // On close modal
  const handleClose = () => {
    setShowBagModal(false);
  };

  // When changing current station from <Select>
  const handleStationChange = (event) => {
    setCurrentStationId(event.target.value);
  };
  useEffect(() => {
    // Handle states for getList()
    let states = [STATES.CHECKED_OUT];
    dispatch(getList(states, currentStationId));
  }, [currentStationId]);

  // On modal confirm
  const onConfirm = () => {
    dispatch(
      update(currentPicklist.picklist_number, {
        state: consolidation.state
      })
    );
    setShowBagModal(false);
    enqueueSnackbar(
      `Picklist "${currentPicklist.picklist_number}" updated to "${consolidation.state}".`,
      {
        variant: 'success'
      }
    );
    setPicklistNumberSearchText('');
    dispatch(getList([STATES.CHECKED_OUT], currentStationId));
  };

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(picklists, query, filters);
  const sorted = applySort(filtered, sort);
  const paginated = applyPagination(sorted, page, limit);
  const enableBulkOperations = selected.length > 0;
  const selectedSome =
    selected.length > 0 && selected.length < picklists.length;
  const selectedAll = selected.length === picklists.length;
  const [currentPicklist, setCurrentPicklist] = useState(null);
  const [qrContent, setQrContent] = useState(null);
  const [picklistNumberSearchText, setPicklistNumberSearchText] = useState('');

  const qrScannerErrorHandler = (err) => {
    console.log(err);
  };

  const qrScannerScanHandler = (data) => {
    if (data != null) {
      paginated.forEach((picklist) => {
        if (picklist.cart_number == data) {
          setCurrentPicklist(picklist);
          setShowBagModal(true);
        }
      });
    }
  };

  const toggleScanner = () => {
    setScannerIsVisible(!scannerIsVisible);
  };

  // On click Search button
  const handleSearch = () => {
    let isExist = false;
    paginated.forEach((picklist) => {
      if (picklist.picklist_number == picklistNumberSearchText) {
        setCurrentPicklist(picklist);
        setShowBagModal(true);
        isExist = true;
      }
    });
    if (!isExist) {
      enqueueSnackbar('Picklist number you entered not found.', {
        variant: 'error'
      });
    }
  };
  useEffect(() => {
    if (currentPicklist?.picklist_number != undefined) {
      dispatch(checkConsolidation(currentPicklist.picklist_number, [STATES.CHECKED_OUT]));
    }
  }, [currentPicklist]);
  useEffect(() => {
    if (!_.isEmpty(consolidation)) {
      let isTransitToFC = false;
      consolidation.pick_lists.forEach((picklist) => {
        if (currentPicklist.station_from != picklist.station_from) {
          setModalMessage1(
            `Rổ ${BAG_PACKING_MODAL_MESSAGE.NEED_TO_TRANSIT_FC.BASKET_NUMBER}`
          );
          setModalMessage2(
            `${BAG_PACKING_MODAL_MESSAGE.NEED_TO_TRANSIT_FC.MESSAGE}`
          );
          isTransitToFC = true;
        }
      });
      if (!isTransitToFC) {
        setModalMessage1(
          `Rổ ${BAG_PACKING_MODAL_MESSAGE.READY_TO_CONSOLIDATE.BASKET_NUMBER}`
        );
        setModalMessage2(
          `${BAG_PACKING_MODAL_MESSAGE.READY_TO_CONSOLIDATE.MESSAGE}`
        );
      }
    }
  }, [consolidation]);

  const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500]
    }
  });

  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <div
        className={`${classes.dFlex} ${classes.alignCenter} ${classes.searchContainer}`}
      >
        <TextField
          className={`${classes.fullHeight} ${classes.textFieldWidth}`}
          label="Scan cart's number or enter picklist number"
          variant="outlined"
          value={picklistNumberSearchText}
          onChange={(event) => setPicklistNumberSearchText(event.target.value)}
        />
        <Button
          className={`${classes.fullHeight} ${classes.searchButtons}`}
          onClick={toggleScanner}
        >
          <ScreenShareIcon className={classes.bigFontSize} />
        </Button>
        <Button
          className={`${classes.fullHeight} ${classes.searchButtons}`}
          onClick={handleSearch}
        >
          Search
        </Button>
        {stations && (
          <FormControl className={`${classes.selectContainer}`}>
            <InputLabel
              // error={Boolean(touched.promotion_type && errors.promotion_type)}
              id="station-select-label"
            >
              Station
            </InputLabel>
            <Select
              labelId="station-select-label"
              id="station-select"
              name="station"
              // error={Boolean(touched.promotion_type && errors.promotion_type)}
              fullWidth
              value={currentStationId}
              onChange={handleStationChange}
            >
              {stations &&
                stations.map((station) => (
                  <MenuItem key={station.id} value={station.id}>
                    {station.name}
                  </MenuItem>
                ))}
            </Select>
            {/* <FormHelperText error>
            {touched.promotion_type && errors.promotion_type}
          </FormHelperText> */}
          </FormControl>
        )}
      </div>

      {/* ////////////////////////////////////// */}
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAll}
              indeterminate={selectedSome}
              onChange={handleSelectAll}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAll}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAll}
                    indeterminate={selectedSome}
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>Trade Order Number</TableCell>
                <TableCell>Picklist Number</TableCell>
                <TableCell>Cart Number</TableCell>
                <TableCell>State</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginated.map((obj) => {
                const isSelected = selected.includes(obj.id);
                return (
                  <TableRow hover key={obj.id} selected={isSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isSelected}
                        onChange={(event) => handleSelectOne(event, obj.id)}
                        value={isSelected}
                      />
                    </TableCell>
                    <TableCell>{obj.trade_order_number}</TableCell>
                    <TableCell>{obj.picklist_number}</TableCell>
                    <TableCell>{obj.cart_number}</TableCell>
                    <TableCell>{obj.state}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {/* ////////////////////////////////////// */}

      {scannerIsVisible && (
        <QrReader
          delay={300}
          onError={qrScannerErrorHandler}
          onScan={qrScannerScanHandler}
          className={classes.qrScanner}
        />
      )}
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={showBagModal}
        fullWidth={true}
        maxWidth={'md'}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        ></DialogTitle>
        <div className={classes.dialogContainer}>
          <h1 id="form-dialog-title" className={classes.bigTitle}>
            Gói hàng cho Picklist {currentPicklist?.picklist_number}
          </h1>
          <p className={`${classes.smallMargin} ${classes.smallTitle}`}>
            1. Vui lòng cho các sản phẩm vào các túi riêng theo nhóm
          </p>
          <div className={classes.flexText}>
            <div>Non Food</div>
            <div>Frozen</div>
            <div>Chilled</div>
            <div>Fragile</div>
            <div>Liquid</div>
            <div>Dry Food</div>
            <div>Fruit and Vegetable</div>
            <div>Other</div>
          </div>
          <Divider className={classes.smallMargin} />
          <div className={classes.flexText}>
            <div className={classes.smallFlex}>
              <span>Fragile</span>
              <span>Dangerous</span>
              <span>Liquid</span>
            </div>
            <div className={classes.smallFlex}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlex}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlexInvi}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlexInvi}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlexInvi}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlexInvi}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
            <div className={classes.smallFlexInvi}>
              <span>Ready To Eat</span>
              <span>Non-RTE</span>
            </div>
          </div>
          <p className={`${classes.smallMargin} ${classes.smallTitle}`}>
            2. In nhãn và phân loại vào rổ
          </p>
          <div
            className={`${classes.smallMargin} ${classes.dFlex} ${classes.alignCenter}`}
          >
            <div className={`${classes.pictureFrame}`}>
              <img
                className={classes.imageStyle}
                alt="barcode"
                src="https://www.clipartkey.com/mpngs/m/68-684611_barcode-laser-code-black-png-image-useless-barcode.png"
              ></img>
            </div>
            <div className={`${classes.smallFlex} ${classes.justifyCenter}`}>
              <ArrowRightAltIcon className={classes.bigIcon} />
            </div>
            <div
              className={`${classes.smallFlex} ${classes.smallPictureFrame} ${classes.justifyCenter}`}
            >
              <img
                className={classes.imageStyle}
                alt="box-icon"
                src="https://image.flaticon.com/icons/svg/31/31821.svg"
              ></img>
            </div>
            <div className={classes.smallFlex}>
              <h3>{modalMessage1}</h3>
              <h2>{modalMessage2}</h2>
            </div>
          </div>
          <div className={`${classes.dFlex} ${classes.justifyBetween}`}>
            <Button
              variant="contained"
              color="primary"
              className={classes.marginLeft}
              onClick={() => callPrinter(currentPicklist)}
            >
              In nhãn
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => onConfirm()}
            >
              Xác nhận
            </Button>
          </div>
        </div>
      </Dialog>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  picklists: PropTypes.array
};

Results.defaultProps = {
  picklists: []
};

export default Results;
