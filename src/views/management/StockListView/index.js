import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getListStocks } from 'src/actions/stockActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { stocks } = useSelector((state) => state.stock);

  useEffect(() => {
    dispatch(getListStocks());
  }, [dispatch]);

  if (!stocks) {
    return null;
  }

  return (
    <Page className={classes.root} title="Stock List">
      <Container maxWidth={false}>
        <Header />
        {stocks && (
          <Box mt={3}>
            <Results stocks={stocks} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default StationListView;
