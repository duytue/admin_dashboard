/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Edit as EditIcon,
  // ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
// import Axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { deleteStock } from 'src/actions/stockActions';
import { getListStations } from 'src/actions/stationActions';
// import { API_BASE_URL_MSSTATION } from 'src/config';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  }
];

function applyFilters(stocks, query, filters) {
  return stocks.filter((stock) => {
    let matches = true;

    if (query) {
      const properties = ['item_names', 'variant', 'batch_code', 'source_sku', 'bin_number'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          property === 'item_names'
          && stock.item_names.vn.toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        } else if (
          stock[property] && String(stock[property]).toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && stock[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(stocks, page, limit) {
  return stocks.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(stocks, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = stocks.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  mL16: {
    marginLeft: '16px'
  }
}));

function Results({ className, stocks, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedStocks, setselectedStocks] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  // const [isDeletedItem, setIsDeletedItem] = useState(false);
  const [sort, setSort] = useState(sortOptions[0].value);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setselectedStocks([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllStocks = (event) => {
    setselectedStocks(
      event.target.checked ? stocks.map((stock) => stock.id) : []
    );
  };

  const handleDeleteAllStock = () => {
    // handle stock as string not object
    selectedStocks.map((stock) => dispatch(deleteStock(stock)));
  };

  const handleSelectOneStock = (event, stockId) => {
    if (!selectedStocks.includes(stockId)) {
      setselectedStocks((prevSelected) => [...prevSelected, stockId]);
    } else {
      setselectedStocks((prevSelected) => prevSelected.filter((id) => id !== stockId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const filteredStocks = applyFilters(stocks, query, filters);
  const sortedStocks = applySort(filteredStocks, sort);
  const paginatedStocks = applyPagination(sortedStocks, page, limit);
  const enableBulkOperations = selectedStocks.length > 0;
  const selectedSomeStocks = selectedStocks.length > 0 && selectedStocks.length < stocks.length;
  const selectedAllUsers = selectedStocks.length === stocks.length;

  const { stations } = useSelector((state) => state.station);
  useEffect(() => {
    dispatch(getListStations());
  }, [dispatch]);

  const findStationName = (id) => {
    const station = stations.filter((st) => String(st.id) === String(id))[0];
    if (station) {
      return station.name;
    }

    return '';
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search stocks"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Status"
          name="status"
          select
          SelectProps={{ native: true }}
          variant="outlined"
        >
          <option value="all">ALL</option>
        </TextField>
        <TextField
          className={classes.mL16}
          label="Station"
          name="station"
          select
          SelectProps={{ native: true }}
          variant="outlined"
        >
          <option value="all">ALL</option>
        </TextField>
        <TextField
          className={classes.mL16}
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredStocks.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeStocks}
              onChange={handleSelectAllStocks}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllStock}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeStocks}
                    onChange={handleSelectAllStocks}
                  />
                </TableCell>
                <TableCell>Product</TableCell>
                <TableCell>SKU</TableCell>
                <TableCell>Variant</TableCell>
                <TableCell>Station</TableCell>
                <TableCell>Bin Number</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Selling Qty</TableCell>
                <TableCell>Reserved Qty</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedStocks.map((stock) => {
                const isStockSelected = selectedStocks.includes(stock.id);

                return (
                  <TableRow hover key={stock.id} selected={isStockSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isStockSelected}
                        onChange={(event) => handleSelectOneStock(event, stock.id)}
                        value={isStockSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/management/stocks/edit/${stock.id}`}
                        variant="h6"
                      >
                        {stock.item_names.vn}
                      </Link>
                    </TableCell>
                    <TableCell>{stock.source_sku}</TableCell>
                    <TableCell>{stock.variant}</TableCell>
                    <TableCell>{findStationName(stock.station_id)}</TableCell>
                    <TableCell>{stock.bin_number}</TableCell>
                    <TableCell>{stock.status}</TableCell>
                    <TableCell>{stock.selling_quantity}</TableCell>
                    <TableCell>{stock.reserved_quantity}</TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/stocks/edit/${stock.id}`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>

                      <IconButton
                        component={RouterLink}
                        to="/app/management/stocks"
                        onClick={() => {
                          dispatch(deleteStock(stock));
                        }}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredStocks.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  stocks: PropTypes.array
};

Results.defaultProps = {
  stocks: []
};

export default Results;
