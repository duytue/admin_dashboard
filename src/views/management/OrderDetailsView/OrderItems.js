import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  skuImage: {
    maxWidth: '200px',
  }
}));

function OrderItems({ orderItems, className, ...rest }) {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Order items" />
      <Divider />
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell rowSpan={2}>
                  No.
                </TableCell>
                <TableCell rowSpan={2}>
                  Image
                </TableCell>
                <TableCell rowSpan={2}>
                  Name
                </TableCell>
                <TableCell rowSpan={2}>
                  SKU
                </TableCell>
                <TableCell rowSpan={2}>
                  State
                </TableCell>
                <TableCell rowSpan={2}>
                  Unit Price
                </TableCell>
                <TableCell align="center" colSpan={4}>
                  Weight
                </TableCell>
                <TableCell align="center" colSpan={4}>
                  Quantity
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Ordered
                </TableCell>
                <TableCell>
                  Picked
                </TableCell>
                <TableCell>
                  Checked Out
                </TableCell>
                <TableCell>
                  Return
                </TableCell>

                <TableCell>
                  Ordered
                </TableCell>
                <TableCell>
                  Picked
                </TableCell>
                <TableCell>
                  Checked Out
                </TableCell>
                <TableCell>
                  Return
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orderItems && orderItems.map((item, index) => (
                <TableRow key={item.id}>
                  <TableCell>
                    {index}
                  </TableCell>
                  <TableCell>
                    <img src={item.image} alt="sku" className={classes.skuImage} />
                  </TableCell>
                  <TableCell>{item.prod_name}</TableCell>
                  <TableCell>{item.item_sku}</TableCell>
                  <TableCell>{item.state}</TableCell>
                  <TableCell>{item.ordered_unit_price}</TableCell>

                  <TableCell>{item.ordered_weight}</TableCell>
                  <TableCell>{item.picked_weight}</TableCell>
                  <TableCell>{item.checked_out_weight}</TableCell>
                  <TableCell>{item.returned_weight}</TableCell>

                  <TableCell>{item.ordered_quantity}</TableCell>
                  <TableCell>{item.picked_quantity}</TableCell>
                  <TableCell>{item.checked_out_quantity}</TableCell>
                  <TableCell>{item.returned_quantity}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={(orderItems && orderItems.length) || 0}
        onChangePage={() => {}}
        onChangeRowsPerPage={() => {}}
        page={0}
        rowsPerPage={5}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

OrderItems.propTypes = {
  className: PropTypes.string,
  orderItems: PropTypes.array.isRequired
};

export default OrderItems;
