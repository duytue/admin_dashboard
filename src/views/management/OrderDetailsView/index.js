import React, { useEffect } from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getOrder, getOrderItems } from 'src/actions/orderActions';
import Page from 'src/components/Page';
import Header from './Header';
import OrderInfo from './OrderInfo';
import OrderItems from './OrderItems';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function OrderDetailsView() {
  const classes = useStyles();
  const { orderId } = useParams();
  const dispatch = useDispatch();
  const { order } = useSelector((state) => state.order);
  const { orderItems } = useSelector((state) => state.order);

  useEffect(() => {
    dispatch(getOrder(orderId));
    dispatch(getOrderItems(orderId));
  }, [dispatch]);

  if (!order) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Order Details"
    >
      <Container maxWidth={false}>
        <Header order={order} />
        <Box mt={2}>
          <OrderInfo order={order} />
        </Box>
        <Box mt={2}>
          <OrderItems orderItems={orderItems} />
        </Box>
      </Container>
    </Page>
  );
}

export default OrderDetailsView;
