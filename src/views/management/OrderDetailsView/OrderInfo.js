import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Button,
  Card,
  Grid,
  Typography,
  Chip,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '16px',
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  tags: {
    marginRight: '6px',
  },
  grid_left: {
    borderRight: '1px solid rgba(81, 81, 81, 1)'
  },
  grid_right: {
    paddingLeft: '16px',
  },
  section: {
    marginTop: '10px',
  },
  action_btn_area: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
}));

function OrderInfo({ order, className, ...rest }) {
  const classes = useStyles();

  const onCancelOrder = () => {
    // TODO: Cancel Ordder
  };

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Grid container>
        <Grid item xl={3} xs={12} className={classes.grid_left}>
          <Typography>Order Number</Typography>
          <Typography variant="h3">{order.order_number}</Typography>
          <div className={classes.section}>
            {order.tags && order.tags.length > 0
              && order.tags.map((item) => <Chip key={item} color="primary" variant="outlined" className={classes.tags} label={item} />)}
          </div>
          <div className={classes.section}>
            <Typography>State</Typography>
            <Typography>{order.state}</Typography>
          </div>
          <div className={classes.section}>
            <Typography>
              {order.sku || 0}
              {' '}
              SKUs - Quantity
              {' '}
              {order.checked_out_quantity || 0}
            </Typography>
          </div>
        </Grid>
        <Grid item xl={3} xs={12} className={classes.grid_right}>
          <Typography>Ordered Time</Typography>
          <Typography>{moment(order.created_at).format('ddd, MMM DD, YYYY hh:mm A')}</Typography>
          <div className={classes.section}>
            <Typography>Pickup Point</Typography>
            <Typography>{order.station_name || 'undefined'}</Typography>
            <Typography>
              {moment(order.min_promise).format('MMM DD, YYYY hh:mm A')}
              {' '}
              -
              {' '}
              {moment(order.max_promise).format('MMM DD, YYYY hh:mm A')}
            </Typography>
          </div>
          <div className={classes.section}>
            <Typography>Remark</Typography>
            <Typography>{order.remark}</Typography>
          </div>
        </Grid>
        <Grid item xl={3} xs={12} className={classes.grid_right}>
          <Typography>Customer</Typography>
          <Typography>{order.customer_phone_number || 'xxx-xxxx-xxxx'}</Typography>
          <div className={classes.section}>
            <Typography>Voucher Discount</Typography>
            <Typography>
              {'VND '}
              {order.voucher_discount}
            </Typography>
          </div>
          <div className={classes.section}>
            <Typography>Grand Total</Typography>
            <Typography>
              {'VND '}
              {order.ordered_grand_total}
              {' / '}
              {'VND '}
              {order.grand_total}
            </Typography>
          </div>
        </Grid>
        <Grid item xl={3} xs={12} className={classes.grid_right}>
          <div className={classes.action_btn_area}>
            <Button variant="outlined" color="primary" onClick={onCancelOrder}>
              Cancel
            </Button>
          </div>
        </Grid>
      </Grid>
    </Card>
  );
}

OrderInfo.propTypes = {
  className: PropTypes.string,
  order: PropTypes.object.isRequired
};

export default OrderInfo;
