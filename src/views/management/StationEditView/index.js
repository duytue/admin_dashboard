import React, { useEffect } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

import { getListStationFunctions, getStation } from 'src/actions/stationActions';
import Header from './Header';
import StationEditForm from './StationEditForm';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationEditView() {
  const dispatch = useDispatch();
  const classes = useStyles();

  const stationId = _.trim(window.location.pathname.slice(24, -4), '/');
  const { stationFunctions } = useSelector((state) => state.station);
  const { station } = useSelector((state) => state.station);

  useEffect(() => {
    dispatch(getStation(stationId));
    dispatch(getListStationFunctions());
  }, [dispatch, stationId]);

  return (
    <Page
      className={classes.rootPageCreateView}
      title="Platform Create"
    >
      <Container maxWidth={false}>
        <Header />
        <StationEditForm
          stationFunctions={stationFunctions}
          station={station}
          stationId={stationId}
        />
      </Container>
    </Page>
  );
}

export default StationEditView;
