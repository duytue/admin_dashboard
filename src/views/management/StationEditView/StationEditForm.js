/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import {
  useDispatch, useSelector,
  // useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import _ from 'lodash';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  InputLabel,
  Select,
  FormHelperText,
  Switch,
  FormControlLabel,
  Chip,
  MenuItem,
  Checkbox,
  ListItemText
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useHistory } from 'react-router-dom';
import { getListCities } from 'src/actions/addressActions';
import addressService from 'src/services/addressService';
import { addStation } from 'src/actions/stationActions';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationEditForm({
  station, stationFunctions, className, ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const { cities } = useSelector((state) => state.address);
  const [openStationFunctionLists, setStationFunctions] = useState(false);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const getDistricts = (cityId) => new Promise((resolve, reject) => {
    addressService.getDistrictByCityId(cityId)
      .then((result) => {
        resolve(result);
        setDistricts(result);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const getWards = (districtId) => new Promise((resolve, reject) => {
    addressService.getWardsByDistrictId(districtId)
      .then((result) => {
        resolve(result);
        setWards(result);
      })
      .catch((error) => {
        reject(error);
      });
  });

  useEffect(() => {
    dispatch(getListCities());
    getDistricts(station.city_id);
    getWards(station.district_id);
  }, [dispatch]);

  // Get function_keys by format values to select box.
  const getFunctionKeys = (functionKeys) => {
    return _.map(functionKeys, function(functionKey, index) {
      return functionKey.name;
    });
  };

  return (
    <Formik
      enableReinitialize
      initialValues={{
        key: station.id || '',
        name: station.name || '',
        address: station.address || '',
        contact: station.contact || '',
        contact_email: station.contact_email || '',
        province: station.city_id || '',
        district: station.district_id || '',
        districts,
        ward: station.ward_id || '',
        wards,
        isActive: station.is_active || true,
        function_keys: getFunctionKeys(station.station_functions) || []
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        address: Yup.string().max(255).required('Address is required'),
        contact: Yup.string().max(255).required('Contact is required'),
        contact_email: Yup.string().max(255).required('Email is required'),
        province: Yup.string().max(255).required('Province/City is required'),
        district: Yup.string().max(255).required('District is required'),
        ward: Yup.string().max(255).required('Ward is required'),
        function_keys: Yup.array().required('Function is required')
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          const data = {
            name: values.name,
            address: values.address,
            is_active: values.isActive,
            contact: values.contact,
            contact_email: values.contact_email,
            location_short_id: values.ward,
            latitude: 10.775659,
            longitude: 106.700424,
            city_id: values.province,
            district_id: values.district,
            ward_id: values.ward,
            functions_id: values.function_keys
          };

          dispatch(addStation(data)).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Station updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(('/app/management/station'));
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid
                container
                spacing={3}
              >
                {/* Name */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.name}
                    variant="outlined"
                  />
                </Grid>
                {/* End Name */}
                {/* Is active */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Box
                    mt={2}
                    px={1}
                  >
                    <FormControlLabel
                      label="Is Active"
                      control={(
                        <Switch
                          checked={values.isActive}
                          edge="end"
                          name="Is Active"
                          onChange={(event) => handleChange('showMenuIcon', event.target.checked)}
                        />
                        )}
                    />
                  </Box>
                </Grid>
                {/* End isactive */}
                {/* Address Street No. & Name */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.address && errors.address)}
                    fullWidth
                    helperText={touched.address && errors.address}
                    label="Address Street No. & Name"
                    name="address"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.address}
                    variant="outlined"
                  />
                </Grid>
                {/* End Address Street No. & Name */}
                {/* Contact */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.contact && errors.contact)}
                    fullWidth
                    helperText={touched.contact && errors.contact}
                    label="Contact"
                    name="contact"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.contact}
                    variant="outlined"
                  />
                </Grid>
                {/* End Contact */}
                {/* Province/City */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.province && errors.province)} id="province-select-label">Province/City</InputLabel>
                  <Select
                    labelId="province-select-label"
                    id="province_select_id"
                    name="province"
                    error={Boolean(touched.province && errors.province)}
                    helperText={touched.province && errors.province}
                    fullWidth
                    required
                    value={values.province}
                    onChange={async (e) => {
                      const { value } = e.target;
                      console.log('value={values.province}', value);
                      const _district = await getDistricts(value);
                      console.log('district', _district);
                      setFieldValue('province', value);
                      setFieldValue('district', '');
                      setFieldValue('districts', _district);
                    }}
                  >
                    {
                        cities && cities.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                      }
                  </Select>
                  <FormHelperText error>{touched.province && errors.province}</FormHelperText>
                </Grid>
                {/* End Province/City */}
                {/* Email */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.contact_email && errors.contact_email)}
                    fullWidth
                    helperText={touched.contact_email && errors.contact_email}
                    label="Email"
                    name="contact_email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.contact_email}
                    variant="outlined"
                  />
                </Grid>
                {/* End Email */}
                {/* District */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.district && errors.district)} id="action-select-label">District</InputLabel>
                  <Select
                    labelId="district-select-label"
                    id="districtselect_id"
                    name="district"
                    error={Boolean(touched.district && errors.district)}
                    helperText={touched.district && errors.district}
                    fullWidth
                    required
                    value={values.district}
                    onChange={async (e) => {
                      const { value } = e.target;
                      console.log('value={values.district}', value);
                      const _wards = await getWards(value);
                      console.log('wards', _wards);
                      setFieldValue('district', value);
                      setFieldValue('ward', '');
                      setFieldValue('wards', _wards);
                    }}
                  >
                    {
                        values.districts && values.districts.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                    }
                  </Select>
                  <FormHelperText error>{touched.district && errors.district}</FormHelperText>
                </Grid>
                {/* End District */}
                {/* Function */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel shrink id="list_function">
                    Function
                  </InputLabel>
                  <Select
                    fullWidth
                    multiple
                    labelId="Function"
                    id={classes.function_keys}
                    label="Function"
                    name="function_keys"
                    value={values.function_keys}
                    open={openStationFunctionLists}
                    onClose={() => setStationFunctions(false)}
                    IconComponent={() => (
                      <Button
                        className={classes.buttonArrowDropDown}
                        onClick={() => setStationFunctions(true)}
                      >
                        <ArrowDropDownIcon />
                      </Button>
                    )}
                    onChange={handleChange}
                    renderValue={(selected) => (
                      <div className={classes.chips}>
                        {selected.map((item) => (
                          <Chip
                            key={item}
                            label={item}
                            onDelete={() => setFieldValue('function_keys', _.reject(values.function_keys, (i) => i === item))}
                            className={classes.chip}
                          />
                        ))}
                      </div>
                    )}
                    className={clsx(classes.selectEmpty)}
                  >
                    {
                        stationFunctions && stationFunctions.map((item) => (
                          <MenuItem key={item.id} value={item.name}>
                            <Checkbox checked={values.function_keys.indexOf(item.name) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))
                      }
                  </Select>
                </Grid>
                {/* end Function */}
                {/* ward */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.ward && errors.ward)} id="action-select-label">Ward</InputLabel>
                  <Select
                    labelId="ward-select-label"
                    id="ward_select_id"
                    name="ward"
                    error={Boolean(touched.ward && errors.ward)}
                    helperText={touched.ward && errors.ward}
                    fullWidth
                    required
                    value={values.ward}
                    onChange={handleChange}
                  >
                    {
                        values.wards && values.wards.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                      }
                  </Select>
                  <FormHelperText error>{touched.ward && errors.ward}</FormHelperText>
                </Grid>
                {/* End Ward */}
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Update Station
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

StationEditForm.propTypes = {
  className: PropTypes.string,
  stationFunctions: PropTypes.array,
  station: PropTypes.array
};

StationEditForm.defaultProps = {
  stationFunctions: [],
  station: []
};

export default StationEditForm;
