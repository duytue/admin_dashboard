import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Grid,
  TextField,
  makeStyles,
  MenuItem,
  Switch,
  Divider,
  SvgIcon,
  Typography,
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import {
  KeyboardTimePicker,
} from '@material-ui/pickers';
import { useHistory, useParams } from 'react-router-dom';
import {
  Save as SaveIcon
  // Download as DownloadIcon,
  // Upload as UploadIcon
} from 'react-feather';
import _ from 'lodash';
import {
  getCapacity,
  updateCapacity,
  addCapacity,
  clearCapacity
} from 'src/actions/capacityTimeSlotActions';
import { getListStations } from 'src/actions/stationActions';

const paymentList = [
  { value: 'ALL', label: 'ALL' },
  { value: 'COD', label: 'COD' },
  { value: 'VNPAY', label: 'VN PAY' },
];

const useStyles = makeStyles((theme) => ({
  root: {},
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  btnSave: {
    margin: theme.spacing(1)
  },
  activeLabel: {
    marginRight: theme.spacing(2)
  },
}));

function CapacityForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { slotId } = useParams();
  const dispatch = useDispatch();
  const { capacity } = useSelector((state) => state.capacity_timeslot);
  const { stations } = useSelector((state) => state.station);
  const history = useHistory();

  const getTime = (time) => {
    if (time) {
      const day = moment();
      const splitTime = time.split(/:/);
      day.hours(splitTime[0]).minutes(splitTime[1]).seconds(0).milliseconds(0);
      return day;
    }

    return moment();
  };

  useEffect(() => {
    if (slotId) {
      dispatch(getCapacity(slotId));
    }
    dispatch(getListStations());
  }, [dispatch]);

  useEffect(() => () => dispatch(clearCapacity()), []);

  if (slotId && _.size(capacity) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        position: capacity.position || '',
        station_id: capacity.station_id || '',
        from: (slotId && getTime(capacity.from)) || moment(),
        to: (slotId && getTime(capacity.to)) || moment(),
        payment_method: capacity.payment_method || 'ALL',
        capacity: capacity.capacity || '',
        active: capacity.active || true,
      }}
      validationSchema={Yup.object().shape({
        position: Yup.number().required(),
        station_id: Yup.string().required(),
        from: Yup.string().required(),
        to: Yup.string().required(),
        payment_method: Yup.string().required(),
        capacity: Yup.number().required(),
        active: Yup.bool().required(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Format time
          const submitObj = { ...values };
          submitObj.from = moment(submitObj.from).format('hh:mm');
          submitObj.to = moment(submitObj.to).format('hh:mm');

          // Make API request
          dispatch(slotId ? updateCapacity({ ...submitObj, id: slotId }) : addCapacity(submitObj))
            .then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar(slotId ? 'Capacity updated' : 'Capacity created', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(('/app/management/time-slot-capacity'));
            });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardHeader title="Capacity Info" />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <TextField
                    error={Boolean(touched.position && errors.position)}
                    fullWidth
                    helperText={touched.position && errors.position}
                    label="Position"
                    name="position"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.position}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <TextField
                    error={Boolean(touched.station_id && errors.station_id)}
                    fullWidth
                    select
                    helperText={touched.station_id && errors.station_id}
                    label="Station"
                    name="station_id"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.station_id}
                    variant="outlined"
                  >
                    {stations.map((option) => (
                      <MenuItem key={option.id} value={option.id}>
                        {option.name}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <KeyboardTimePicker
                    disableToolbar
                    inputVariant="outlined"
                    ampm={false}
                    fullWidth
                    format="hh:mm"
                    label="From"
                    value={values.from}
                    onChange={(value) => setFieldValue('from', value)}
                    KeyboardButtonProps={{
                      'aria-label': 'change from',
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <KeyboardTimePicker
                    disableToolbar
                    inputVariant="outlined"
                    ampm={false}
                    format="hh:mm"
                    fullWidth
                    label="To"
                    value={values.to}
                    onChange={(value) => setFieldValue('to', value)}
                    KeyboardButtonProps={{
                      'aria-label': 'change to',
                    }}
                  />
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <TextField
                    error={Boolean(touched.payment_method && errors.payment_method)}
                    fullWidth
                    select
                    helperText={touched.payment_method && errors.payment_method}
                    label="Payment method"
                    name="payment_method"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.payment_method}
                    variant="outlined"
                  >
                    {paymentList.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <TextField
                    error={Boolean(touched.capacity && errors.capacity)}
                    fullWidth
                    helperText={touched.capacity && errors.capacity}
                    label="Capacity"
                    name="capacity"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.capacity}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  lg={4}
                  md={6}
                  sm={12}
                >
                  <Typography className={classes.activeLabel}>Active</Typography>
                  <Switch
                    checked={values.active}
                    onChange={() => setFieldValue('active', !values.active)}
                    name="capacity_active"
                    inputProps={{ 'aria-label': 'checkbox' }}
                  />
                </Grid>
              </Grid>
            </CardContent>
            <Divider />
            <CardActions>
              <Button
                variant="contained"
                color="secondary"
                type="submit"
                disabled={isSubmitting}
                className={classes.btnSave}
              >
                <SvgIcon fontSize="small" className={classes.actionIcon}>
                  <SaveIcon />
                </SvgIcon>
                Save
              </Button>
            </CardActions>
          </Card>
        </form>
      )}
    </Formik>
  );
}

CapacityForm.propTypes = {
  className: PropTypes.string,
};

export default CapacityForm;
