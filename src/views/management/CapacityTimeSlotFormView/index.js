import React from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import Page from 'src/components/Page';
import CapacityForm from './CapacityForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CapacityTimeSlotView() {
  const classes = useStyles();
  const { slotId } = useParams();

  return (
    <Page
      className={classes.root}
      title={slotId ? 'Edit Capacity' : 'Create Capacity'}
    >
      <Container maxWidth="lg">
        <Header isEdit={slotId} />
        <Box mt={3}>
          <CapacityForm />
        </Box>
      </Container>
    </Page>
  );
}

export default CapacityTimeSlotView;
