import React, {
  useEffect,
} from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import CustomerCreateForm from './CustomerCreateForm';
import { useDispatch } from 'react-redux';
import { getListCustomer, getListMetaData } from 'src/actions/customerAction';
import { getListPlatform } from 'src/actions/platformActions';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function UserCreateView() {
  const dispatch = useDispatch();
  const classes = useStyles();

  useEffect(() => {
    dispatch(getListCustomer());
    dispatch(getListPlatform());
    dispatch(getListMetaData());
  }, [dispatch]);

  return (
    <Page
      className={classes.rootPageCreateView}
      title="User Create"
    >
      <Container maxWidth={false}>
        <Header />
        <CustomerCreateForm />
      </Container>
    </Page>
  );
}

export default UserCreateView;
