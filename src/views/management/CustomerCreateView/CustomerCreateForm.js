import React, { useState } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  FormHelperText,
  TextField,
  makeStyles,
  Typography,
  Switch,
  Select,
  MenuItem,
  InputLabel,
  Paper
} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { KeyboardDatePicker } from '@material-ui/pickers';
import { createCustomer, getListMetaData } from 'src/actions/customerAction';
import _ from 'lodash';
import {
  Upload as UploadIcon,
  XCircle as DeleteIcon,
  AlertCircle
} from 'react-feather';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import customerService from 'src/services/customerService';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));
const genders = [
  {"value": "male", "name":"male"},
  {"value": "female", "name":"female"}
];
const types = [
  {"value": "enterprise", "name":"enterprise"},
  {"value": "individual", "name":"individual"}
]

function CustomerCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [avatar, setImageUrl] = useState(null);
  const [new_image, setNewImage] = useState(null);
  const [language, setLanguage] = useState('en');
  const { platforms } = useSelector((state) => {
    console.log(state.platform);
    return state.platform
  })
  const { metadatas } = useSelector((state) => {
    return state.customer
  })
  const categories = useSelector((state) => {
    return state.customer.customers.filter(item => item.level < 3);
  })
  const { user } = useSelector((state) => state.account);

  const handleChangeImage = (event) => {
    if (event.target.files.length != 0) {
      let file = event.target.files[0];
      let type = file.type;
      if (type.indexOf('image/') < 0) {
        enqueueSnackbar('Only image files are allowed', {
          variant: 'error'
        });
      } else {
        setNewImage(file);
        fileToBase64(file);
      }

    }
  }

  const fileToBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImageUrl(reader.result);
    };
  };

  const onClearImage = () => {
    setImageUrl(null);
    setNewImage(null);
  }

  const handleSeeAll = () => {
    history.push(("/app/management/customers"))
  }

  return (
    <Formik
      initialValues={{
        email: '',
        customer_type: '',
        first_name: '',
        last_name: '',
        phone_number: '',
        platform_key: '',
        is_active: true,
        birthday: new Date(),
        gender: '',
        customer_group: '',
        tax_code: '',
        password: '',
        // keyword: convertToString(customer.keywords)
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email('Must be a valid email')
          .max(255)
          .required('Email is required'),
        first_name: Yup.string()
          .max(255)
          .required('First name is required'),
        last_name: Yup.string()
          .max(255)
          .required('last name is required'),
        password: Yup.string()
        .required('Password is required') 
        .min(8, 'Password is too short - should be 8 chars minimum.')
        .max(64, 'Password is too long - should be 64 chars minimum.')
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, 'password lowercase letter missing, password uppercase letter missing'),

        phone_number: Yup.string().max(15),
        customer_type: Yup.string().max(255),
        platform_keys: Yup.array(),
        gender: Yup.string().max(15),
        birthday: Yup.date(),
        is_active: Yup.bool(),
        customer_group: Yup.string(),
        tax_code: Yup.string(),

      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          let param = {
            "platform_key": values.platform_key,
            "gender": values.gender,
            "email": values.email,
            "customer_type": values.customer_type,
            "first_name": values.first_name,
            "last_name": values.last_name,
            "phone_number": values.phone_number,
            "birthday": values.birthday,
            "is_active": values.is_active,
            "customer_group": values.customer_group,
            "tax_code": values.tax_code,
            "password": values.password,
          }

          if (new_image != null) {
            let media_code = new_image.type.replace('image/', '').toUpperCase();

            let preUploadParam = {
              "creator_by": user.id,
              "name": new_image.name,
              "media_code": media_code,
              "platform_key": values.platform_key
            }

            let responsePre = await customerService.preUpload(preUploadParam);
            if (responsePre) {
              let responseUpload = await customerService.uploadImage(responsePre, new_image);

              if (responseUpload) {
                let responseCreateUpload = await customerService.createUpload(preUploadParam);
                if (responseCreateUpload) {
                  param['avatar'] = responseCreateUpload.url;
                  try {
                    await dispatch(createCustomer(param));
                    setStatus({ success: true });
                    enqueueSnackbar('customer created', {
                      variant: 'success',
                      action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
                    });
                  } catch (error) {
                    setStatus({ success: false });
                    setErrors({ submit: error.message });
                    enqueueSnackbar('Create customer failure', {
                      variant: 'error'
                    });
                  } finally {
                    setSubmitting(false);
                  }
                }
              }
            }
          } else {
            try {
              await dispatch(createCustomer(param));
              setStatus({ success: true });
              enqueueSnackbar('customer created', {
                variant: 'success',
                action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
              });
            } catch (error) {
              setStatus({ success: false });
              setErrors({ submit: error.message });
              enqueueSnackbar('Create customer failure', {
                variant: 'error'
              });
            } finally {
              setSubmitting(false);
            }
          }
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values
      }) => (
          <form className={clsx(classes.rootPageCreateView, className)} onSubmit={handleSubmit} {...rest}>

            <Card>
              <CardContent>

                <Grid container spacing={3}>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.platform_key && errors.platform_key)} id="platform-select-label">Platform</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="platform_key"
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      required
                      value={values.platform_key}
                      onChange={handleChange}
                    >
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.id} value={item.key}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.platform_key && errors.platform_key}</FormHelperText>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="customer_group">
                      customer group
                    </InputLabel>
                    <Select labelId="customer_group" fullWidth id="customer_group" label="customer_group" name="customer_group"
                      value={values.customer_group} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {
                        metadatas && metadatas.map((item) => (
                          <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email}
                      label="Email"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.first_name && errors.first_name)}
                      fullWidth
                      helperText={touched.first_name && errors.first_name}
                      label="First Name"
                      name="first_name"
                      type="first_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.first_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.last_name && errors.last_name)}
                      fullWidth
                      helperText={touched.last_name && errors.last_name}
                      label="Last Name"
                      name="last_name"
                      type="last_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.last_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.password && errors.password)}
                      fullWidth
                      helperText={touched.password && errors.password}
                      label="Password"
                      name="password"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      type="password"
                      value={values.password}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.phone_number && errors.phone_number)}
                      fullWidth
                      helperText={touched.phone_number && errors.phone_number}
                      label="Phone"
                      name="phone_number"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.phone_number}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item  lg={6} md={12} xs={12}>
                    <KeyboardDatePicker
                      className={clsx(classes.selectBirthDay)}
                      disableToolbar
                      fullWidth
                      variant="inline"
                      format="DD/MM/YYYY"
                      name="birthday"
                      margin="normal"
                      id="Date Of Birth"
                      label="Date Of Birth"
                      value={values.birthday}
                      onChange={val => {
                        setFieldValue('birthday', val);
                      }}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </Grid>
                  {/* <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.gender && errors.gender)}
                      fullWidth
                      helperText={touched.gender && errors.gender}
                      label="Gender"
                      name="gender"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.gender}
                      variant="outlined"
                    />
                  </Grid> */}
                   <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.gender && errors.gender)} id="platform-select-label">gender</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="gender"
                      error={Boolean(touched.gender && errors.gender)}
                      fullWidth
                      required
                      value={values.gender}
                      onChange={handleChange}
                    >
                      {
                        genders && genders.map((item) => (
                          <MenuItem value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    </Grid>
                    <Grid item  lg={6} md={12} xs={12}>
                    <InputLabel error={Boolean(touched.customer_type && errors.customer_type)} id="platform-select-label">Customer Type</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="customer_type"
                      error={Boolean(touched.customer_type && errors.customer_type)}
                      fullWidth
                      required
                      value={values.customer_type}
                      onChange={handleChange}
                    >
                      {
                        types && types.map((item) => (
                          <MenuItem value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.tax_code && errors.tax_code)}
                      fullWidth
                      helperText={touched.tax_code && errors.tax_code}
                      label="Tax code"
                      name="tax_code"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.tax_code}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12}>
                    {avatar && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                      <label htmlFor="contained-button-file">
                        <img className={classes.imageStyle} src={avatar} style={{ maxWidth: '150px', border: '1px solid' }} />
                      </label>
                      <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }} onClick={onClearImage} />

                    </Paper>}
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="contained-button-file"
                      type="file"
                      style={{ display: 'none' }}
                      onChange={handleChangeImage}
                    />
                    <label htmlFor="contained-button-file">
                      {!avatar && <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                        {language == 'en' ? 'Choose image' : 'Chọn ảnh'}
                      </Button>}
                    </label>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <Typography
                      variant="h5"
                      color="textPrimary"
                    >
                      Customer Active
                  </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                    >
                      This will give the customer is active to work
                  </Typography>
                    <Switch
                      checked={values.is_active}
                      color="secondary"
                      edge="start"
                      name="is_active"
                      onChange={handleChange}
                      value={values.is_active}
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button variant="contained" color="secondary" type="submit" disabled={isSubmitting}>
                    Create Customer
                  </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CustomerCreateForm.propTypes = {
  className: PropTypes.string,
};

export default CustomerCreateForm;
