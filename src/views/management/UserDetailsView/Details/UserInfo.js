import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import PersonIcon from '@material-ui/icons/PersonOutline';
import Label from 'src/components/Label';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { updateUser } from 'src/actions/userActions';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {},
  fontWeightMedium: {
    fontWeight: theme.typography.fontWeightMedium
  },
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function UserInfo({ user, className, ...rest }) {
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  const handleClickOpen = (val) => {
    setOpen(true)
  }


  const handleClose = (val) => {
    setOpen(false)
  }

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="User info" />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Email
            </TableCell>
            <TableCell>
              <Typography
                variant="body2"
                color="textSecondary"
              >
                {user.email}
              </Typography>
              <Label color={user.active ? 'success' : 'error'}>
                {user.active
                  ? 'Email active'
                  : 'Email not active'}
              </Label>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              First Name
            </TableCell>
            <TableCell>
              <Typography
                variant="body2"
                color="textSecondary"
              >
                {user.first_name}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Last Name
            </TableCell>
            <TableCell>
              <Typography
                variant="body2"
                color="textSecondary"
              >
                {user.last_name}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Date Of Birth
            </TableCell>
            <TableCell>
              <Typography
                variant="body2"
                color="textSecondary"
              >
                {user.birthday}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell className={classes.fontWeightMedium}>
              Phone
            </TableCell>
            <TableCell>
              <Typography
                variant="body2"
                color="textSecondary"
              >
                {user.phone}
              </Typography>
            </TableCell>
          </TableRow>

        </TableBody>
      </Table>
      <Dialog
        maxWidth="sm"
        fullWidth
        open={open}
        aria-labelledby="confirmation-dialog-title"
        aria-describedby="confirmation-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Do you want to change password?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="confirmation-dialog-title">
            <Formik
              initialValues={{
                password: '',
              }}
              validationSchema={Yup.object().shape({
                password: Yup.string().min(7, 'Must be at least 8 characters').max(255).required('Required'),
              })}
              onSubmit={async (values, {
                resetForm,
                setErrors,
                setStatus,
                setSubmitting
              }) => {
                try {
                  // Make API request
                  await dispatch(updateUser({ ...values, id: user.id })).then(() => {
                    setOpen(false)
                    resetForm();
                    setStatus({ success: true });
                    setSubmitting(false);
                  })
                } catch (error) {
                  setStatus({ success: false });
                  setErrors({ submit: error.message });
                  setSubmitting(false);
                }
              }}
            >
              {({
                errors,
                handleBlur,
                handleChange,
                handleSubmit,
                isSubmitting,
                touched,
                values
              }) => (
                  <form onSubmit={handleSubmit}>
                    <Box mt={2}>
                      <TextField
                        error={Boolean(touched.password && errors.password)}
                        fullWidth
                        helperText={touched.password && errors.password}
                        label="New password"
                        name="password"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        type="password"
                        value={values.password}
                        variant="outlined"
                      />
                    </Box>
                    <DialogActions>
                      <Button onClick={handleClose} color="primary">
                        Disagree
                      </Button>
                      <Button type="submit" color="primary" autoFocus>
                        Agree
                      </Button>
                    </DialogActions>
                  </form>
                )}
            </Formik>
          </DialogContentText>
        </DialogContent>

      </Dialog>
      <Box
        p={1}
        display="flex"
        flexDirection="column"
        alignItems="flex-start"
      >
        <Button onClick={() => handleClickOpen(user)}>
          <LockOpenIcon className={classes.actionIcon} />
          Change Password
        </Button>
        <Button>
          <PersonIcon className={classes.actionIcon} />
          Login as Customer
        </Button>
      </Box>
    </Card>
  );
}

UserInfo.propTypes = {
  className: PropTypes.string,
  user: PropTypes.object.isRequired
};

export default UserInfo;
