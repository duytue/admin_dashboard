import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getListCustomer } from 'src/actions/customerAction';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { customers } = useSelector(state => {
    return state.customer;
  });

  useEffect(() => {
    dispatch(getListCustomer());
  }, [dispatch]);

  if (!customers) {
    return null;
  }

  return (
    <Page className={classes.root} title="Supplier List">
      <Container maxWidth={false}>
        <Header />
        {customers && (
          <Box mt={3}>
            <Results customers={customers} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default CustomerListView;
