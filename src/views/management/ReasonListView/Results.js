/* eslint-disable max-len */
import React, { useState, useCallback, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useHistory } from "react-router-dom";
import { createReason, getListReasons, getReason, getReasonType, updateReason } from 'src/actions/reasonActions';
import {
  Box,
  Button,
  ButtonGroup,
  Card,
  Divider,
  InputAdornment,
  SvgIcon,
  Switch,
  Tab,
  Tabs,
  TextField,
  Typography,
  Paper,
  Grid,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  withStyles, CardContent
} from '@material-ui/core';
import {
  PlusCircle as PlusCircleIcon,
  Search as SearchIcon
} from 'react-feather';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import _ from 'lodash';
import Edit from './Edit';


const tabs = [];

const useStyles = makeStyles((theme) => ({
  root: {},
  reasonList: {
    width: '100%'
  },
  addReasonForm: {
    padding: 20,
  },
  lnActive: {
    backgroundColor: '#8a85ff',
    color: 'black',
    '&:hover': {
      backgroundColor: '#8a85ff',
      opacity: '0.6'
    }
  },
  mBottom: {
    marginBottom: "5px"
  },
  dNone: {
    display: 'none'
  }
}));


const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: 'secondary',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: 'secondary',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => (
  <Switch
    focusVisibleClassName={classes.focusVisible}
    disableRipple
    classes={{
      root: classes.root,
      switchBase: classes.switchBase,
      thumb: classes.thumb,
      track: classes.track,
      checked: classes.checked,
    }}
    {...props}
  />
));

function Results({
  className, reasonTypeList, currentReasonTypeId, ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [selectedUsers, setSelectedUsers] = useState([]);
  const { user } = useSelector(state => state.account);
  const { reason } = useSelector(state => state.reason);
  const [query, setQuery] = useState('');
  const [isEdit, setIsEdit] = useState(false);
  const { reasonList } = useSelector((state) => state.reason);
  const [language, setLanguage] = useState('en');
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const [updatedReasonType, setUpdatedReasonType] = useState(currentReasonTypeId);

  useEffect(() => {
    dispatch(getListReasons(currentReasonTypeId));
  }, [dispatch]);

  const detailReason = (id) => {
    setIsEdit(true)
    dispatch(getReason(id));
  }

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'reasonType1') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedUsers([]);
    dispatch(getListReasons(value));
    setUpdatedReasonType(value);
    history.push((`/app/management/reasons?tab=${value}`))
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const onChangeLanguage = (event, value) => {
    setLanguage(value[0]);
  }

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={updatedReasonType}
        variant="scrollable"
      >
        {reasonTypeList.map((tab) => (
          <Tab
            key={tab.id}
            value={tab.id}
            label={tab.name}
          />
        ))}
      </Tabs>
      <Divider />
      <Box
        p={2}
        minHeight={56}
        display="flex"
        alignItems="center"
      >
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <TextField
              className={classes.reasonList}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      fontSize="small"
                      color="action"
                    >
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                )
              }}
              onChange={handleQueryChange}
              placeholder="Search Reason"
              value={query}
              variant="outlined"
            />
            <Paper>
              <List>
                {reasonList.map((reason, i) => (
                  <ListItem key={i} button onClick={() => { detailReason(reason.id) }}>
                    <ListItemText primary={reason.names.en} key={i} />
                  </ListItem>
                ))}
                {/*<ListItem button>*/}
                {/*  <ListItemText primary="(New Reason)"  />*/}
                {/*</ListItem>*/}
              </List>
            </Paper>
          </Grid>
          <Grid item xs={8}>
            <div>
              <Button
                color="secondary"
                variant="contained"
                className={classes.action}
                onClick={() => { setIsEdit(false) }}
              >
                <SvgIcon
                  fontSize="small"
                  className={classes.actionIcon}
                >
                  <PlusCircleIcon />
                </SvgIcon>
            &nbsp;&nbsp;  Add Reason
              </Button>
            </div>
            <br />
            {isEdit
              ? <Edit reason={reason} type={currentReasonTypeId} setIsEdit={setIsEdit} />
              : <Formik
                initialValues={{
                  name_en: '',
                  name_vi: '',
                  description_en: '',
                  description_vi: ''
                }}
                validationSchema={Yup.object().shape({
                  name_en: Yup.string().max(255).required('Name is required'),
                  name_vi: Yup.string().max(255).required('Name is required'),
                })}
                onSubmit={async (values, {
                  resetForm,
                  setErrors,
                  setStatus,
                  setSubmitting
                }) => {
                  try {
                    const dataReason = {
                      type: currentReasonTypeId,
                      platform_key: user.list_platform[0].id,
                      creator: user.id,
                      active: true,
                      names: {
                        en: values.name_en,
                        vi: values.name_vi
                      },
                      descriptions: {
                        en: values.description_en,
                        vi: values.description_vi
                      }
                    }
                    // Make API request
                    dispatch(createReason({ ...dataReason })).then(() => {
                      resetForm();
                      setStatus({ success: true }); setSubmitting(false);
                      enqueueSnackbar('Reason created', {
                        variant: 'success',
                        action: <Button>See all</Button>
                      });
                      dispatch(getListReasons(currentReasonTypeId))
                      history.push((`/app/management/reasons/?tab=${currentReasonTypeId}`))
                    });
                  } catch (error) {
                    setStatus({ success: false });
                    setErrors({ submit: error.message });
                    setSubmitting(false);
                  }
                }}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  setFieldValue,
                  touched,
                  values
                }) => (
                    <form
                      className={clsx(classes.rootPageCreateView, className)}
                      onSubmit={handleSubmit}
                      {...rest}
                    >
                      <Paper className={classes.addReasonForm}>
                        <Grid container spacing={0}>
                          <Grid item xs={5}>
                            <Typography
                              variant="h4"
                              color="textPrimary"
                            >
                              Reason Creator
                          </Typography>
                          </Grid>
                          <Grid item xs={7}>
                            <Grid container spacing={0}>
                              <Grid item xs={3}>
                                <Typography
                                  variant="body1"
                                  color="textPrimary"
                                >
                                  Language
                              </Typography>
                              </Grid>
                              <Grid item xs={9}>
                                <ToggleButtonGroup color="secondary" aria-label="outlined primary button group" onChange={onChangeLanguage}>
                                  <ToggleButton value="en" className={`${language == "en" ? classes.lnActive : ''}`} variant="contained" color="secondary" aria-label="contained primary button group">EN</ToggleButton>
                                  <ToggleButton value="vi" className={`${language == "vi" ? classes.lnActive : ''}`}>VN</ToggleButton>
                                  <ToggleButton value="fr" className={`${language == "fr" ? classes.lnActive : ''}`}>FR</ToggleButton>
                                </ToggleButtonGroup>
                              </Grid>
                            </Grid>
                          </Grid>
                          <br />
                          <br />
                          <Grid item xs={12}>
                            <Grid component="label" container alignItems="center" spacing={1}>
                              <Grid item>
                                <Typography
                                  variant="body1"
                                  color="secondary"
                                >
                                  ACTIVE
                              </Typography>
                              </Grid>
                              <Grid item>
                                <IOSSwitch name="activeSwitch" />
                              </Grid>
                            </Grid>
                          </Grid>
                          <br />
                          <br />
                          <Grid item xs={9} className={`${classes.mBottom}`}>
                            <Typography
                              variant="body1"
                              color="secondary"
                            >
                              REASON NAME
                          </Typography>
                          </Grid>
                          <Grid item xs={9} className={`${classes.mBottom}`}>
                            <TextField
                              id="outlined-basic"
                              label="(New Reason)"
                              error={Boolean(touched.name_en && errors.name_en)}
                              helperText={touched.name_en && errors.name_en}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name_en}
                              name="name_en"
                              variant="outlined"
                              className={`${classes.reasonList} ${language == 'en' ? '' : classes.dNone}`} />
                            <TextField
                              id="outlined-basic2"
                              label="(New Reason)"
                              error={Boolean(touched.name_vi && errors.name_vi)}
                              helperText={touched.name_vi && errors.name_vi}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name_vi}
                              name="name_vi"
                              variant="outlined"
                              className={`${classes.reasonList} ${language == 'vi' ? '' : classes.dNone}`} />
                          </Grid>
                          <br />
                          <br />
                          <br />
                          <Grid item xs={9} className={`${classes.mBottom}`}>
                            <Typography
                              variant="body1"
                              color="secondary"
                            >
                              DESCRIPTION
                          </Typography>
                          </Grid>
                          <Grid item xs={9}>
                            <TextField
                              multiline
                              rows={4}
                              variant="outlined"
                              value={values.description_en}
                              name="description_en"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              className={`${classes.reasonList} ${language == 'en' ? '' : classes.dNone}`} />
                            <TextField
                              multiline
                              rows={4}
                              variant="outlined"
                              value={values.description_vi}
                              name="description_vi"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              className={`${classes.reasonList} ${language == 'vi' ? '' : classes.dNone}`} />
                          </Grid>
                          <br />
                          <br />
                          <br />
                          <Grid item xs={9}>
                            <Typography
                              variant="body1"
                              color="secondary"
                            >
                              &nbsp;&nbsp;
                          </Typography>
                            <Grid container spacing={0}>
                              <Grid item xs={12}>
                                <Button
                                  type="submit"
                                  color="secondary"
                                  variant="contained"
                                  className={classes.action}
                                  disabled={isSubmitting}
                                >
                                  ADD
                                </Button>
                              &nbsp;&nbsp;
                              <Button
                                  color="secondary"
                                  variant="contained"
                                  className={classes.action}
                                >
                                  CANCEL
                              </Button>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Paper>
                    </form>
                  )}
              </Formik>
            }
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  reasonTypeList: PropTypes.array
};

Results.defaultProps = {
  reasonTypeList: []
};

export default Results;
