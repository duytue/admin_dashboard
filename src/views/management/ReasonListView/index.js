import React, { useEffect, } from 'react';

import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import Page from 'src/components/Page';
import { getReasonType } from 'src/actions/reasonActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function ReasonListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { reasonTypeList } = useSelector((state) => state.reason);

  useEffect(() => {
    dispatch(getReasonType());
  }, [dispatch]);

  let currentReasonTypeId = null;

  if (!reasonTypeList || reasonTypeList.length === 0) {
    return null;
  // eslint-disable-next-line no-else-return
  } else {
    currentReasonTypeId = reasonTypeList[0].id;
  }

  return (
    <Page
      className={classes.root}
      title="Reason List"
    >
      <Container maxWidth={false}>
        <Header />
        {reasonTypeList && currentReasonTypeId && (
          <Box mt={3}>
            <Results
              reasonTypeList={reasonTypeList}
              currentReasonTypeId={currentReasonTypeId}
            />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default ReasonListView;
