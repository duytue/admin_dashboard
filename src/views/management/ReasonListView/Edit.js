/* eslint-disable max-len */
import React, { useState, useCallback } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import {
  Button,
  ButtonGroup,
  Switch,
  TextField,
  Typography,
  Paper,
  Grid,
  makeStyles,
  withStyles
} from '@material-ui/core';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import _ from 'lodash';
import { getListReasons, updateReason, deleteReason } from '../../../actions/reasonActions';

const useStyles = makeStyles((theme) => ({
  root: {},
  reasonList: {
    width: '100%'
  },
  addReasonForm: {
    padding: 20,
  },
  delete: {
    float: 'right'
  },
  lnActive: {
    backgroundColor: '#8a85ff',
    color: 'black',
    '&:hover': {
      backgroundColor: '#8a85ff',
      opacity: '0.6'
    }
  },
  mBottom: {
    marginBottom: "5px"
  },
  dNone: {
    display: 'none'
  }
}));


const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: 'secondary',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: 'secondary',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => (
  <Switch
    focusVisibleClassName={classes.focusVisible}
    disableRipple
    classes={{
      root: classes.root,
      switchBase: classes.switchBase,
      thumb: classes.thumb,
      track: classes.track,
      checked: classes.checked,
    }}
    {...props}
  />
));

function Edit({ className, reason, type, setIsEdit, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const { user } = useSelector(state => state.account);
  const [language, setLanguage] = useState('en');

  const deleteReasonById = (id) => {
    dispatch(deleteReason(id))
    enqueueSnackbar('Reason deleted', {
      variant: 'success',
      action: <Button>See all</Button>
    });
    dispatch(getListReasons(type))
    history.push((`/app/management/reasons/?tab=${type}`))
    setIsEdit(false)
  }

  const onChangeLanguage = (event, value) => {
    setLanguage(value[0]);
  }

  if (_.isEmpty(reason)) {
    return null;
  }


  return (
    <Formik
      initialValues={{
        id: reason.id || '',
        name_en: reason.names.en || '',
        name_vi: reason.names.vi || '',
        description_en: reason.descriptions.en || '',
        description_vi: reason.descriptions.vi || '',
        is_active: reason.is_active

      }}
      validationSchema={Yup.object().shape({
        name_en: Yup.string().max(255).required('Name is required'),
        name_vi: Yup.string().max(255).required('Name is required'),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          console.log(values);
          const dataReason = {
            id: values.id,
            is_active: values.is_active,
            names: {
              en: values.name_en,
              vi: values.name_vi
            },
            descriptions: {
              en: values.description_en,
              vi: values.description_vi
            }
          }
          // Make API request
          dispatch(updateReason(dataReason)).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Reason updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/reasons"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        handleChangeSwitch,
        setFieldValue,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Paper className={classes.addReasonForm}>
              <Grid container spacing={0}>
                <Grid item xs={5}>
                  <Typography
                    variant="h4"
                    color="textPrimary"
                  >
                    Reason Editor
                </Typography>
                </Grid>
                <Grid item xs={7}>
                  <Grid container spacing={0}>
                    <Grid item xs={3}>
                      <Typography
                        variant="body1"
                        color="textPrimary"
                      >
                        Language
                    </Typography>
                    </Grid>
                    <Grid item xs={9}>
                      <ToggleButtonGroup color="secondary" aria-label="outlined primary button group" onChange={onChangeLanguage}>
                        <ToggleButton value="en" className={`${language == "en" ? classes.lnActive : ''}`} variant="contained" color="secondary" aria-label="contained primary button group">EN</ToggleButton>
                        <ToggleButton value="vi" className={`${language == "vi" ? classes.lnActive : ''}`}>VN</ToggleButton>
                        <ToggleButton value="fr" className={`${language == "fr" ? classes.lnActive : ''}`}>FR</ToggleButton>
                      </ToggleButtonGroup>
                    </Grid>
                  </Grid>
                </Grid>
                <br />
                <br />
                <Grid item xs={12}>
                  <Grid component="label" container alignItems="center" spacing={1}>
                    <Grid item>
                      <Typography
                        variant="body1"
                        color="secondary"
                      >
                        ACTIVE
                    </Typography>
                    </Grid>
                    <Grid item>
                      <IOSSwitch
                        name="is_active"
                        checked={values.is_active}
                        onChange={() => { setFieldValue('is_active', !values.is_active) }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <br />
                <br />
                <Grid item xs={9} className={`${classes.mBottom}`}>
                  <Typography
                    variant="body1"
                    color="secondary"
                  >
                    REASON NAME
                </Typography>
                </Grid>
                <Grid item xs={9} className={`${classes.mBottom}`}>
                  <TextField
                    id="outlined-basic"
                    label="Reason Name"
                    error={Boolean(touched.name_en && errors.name_en)}
                    helperText={touched.name_en && errors.name_en}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name_en}
                    name="name_en"
                    variant="outlined"
                    className={`${classes.reasonList} ${language == 'en' ? '' : classes.dNone}`} />
                  <TextField
                    id="outlined-basic2"
                    label="Reason Name"
                    error={Boolean(touched.name_vi && errors.name_vi)}
                    helperText={touched.name_vi && errors.name_vi}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name_vi}
                    name="name_vi"
                    variant="outlined"
                    className={`${classes.reasonList} ${language == 'vi' ? '' : classes.dNone}`} />
                </Grid>
                <br />
                <br />
                <br />
                <Grid item xs={9} className={`${classes.mBottom}`}>
                  <Typography
                    variant="body1"
                    color="secondary"
                  >
                    DESCRIPTION
                </Typography>
                </Grid>
                <Grid item xs={9}>
                  <TextField
                    multiline
                    rows={4}
                    variant="outlined"
                    value={values.description_en}
                    name="description_en"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={`${classes.reasonList} ${language == 'en' ? '' : classes.dNone}`} />
                  <TextField
                    multiline
                    rows={4}
                    variant="outlined"
                    value={values.description_vi}
                    name="description_vi"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={`${classes.reasonList} ${language == 'vi' ? '' : classes.dNone}`} />
                </Grid>
                <br />
                <br />
                <br />
                <Grid item xs={9}>
                  <Typography
                    variant="body1"
                    color="secondary"
                  >
                    &nbsp;&nbsp;
                </Typography>
                  <Grid container spacing={0}>
                    <Grid item xs={12}>
                      <Button
                        type="submit"
                        color="secondary"
                        variant="contained"
                        className={classes.action}
                        disabled={isSubmitting}
                      >
                        EDIT
                    </Button>
                    &nbsp;&nbsp;
                    <Button
                        color="secondary"
                        variant="contained"
                        className={classes.action}
                      >
                        CANCEL
                    </Button>
                      <Button
                        color="default"
                        variant="contained"
                        className={classes.delete}
                        onClick={() => { deleteReasonById(values.id) }}
                      >
                        Delete
                    </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </form>
        )}
    </Formik>
  );
}

Edit.propTypes = {
  reason: PropTypes.object,
  type: PropTypes.string,
  setIsEdit: PropTypes.func
};

Edit.defaultProps = {
  reason: {},
  type: null,
  setIsEdit: () => { }
};

export default Edit;
