import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import SupplierCreateForm from './SupplierCreateForm';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function SupplierCreateView() {
  const classes = useStyles();
  return (
    <Page
      className={classes.rootPageCreateView}
      title="Supplier Create"
    >
      <Container maxWidth={false}>
        <Header />
        <SupplierCreateForm />
      </Container>
    </Page>
  );
}

export default SupplierCreateView;
