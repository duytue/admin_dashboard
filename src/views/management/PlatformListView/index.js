import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getListPlatform } from 'src/actions/platformActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PlatformListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { platforms } = useSelector(state => {
    return state.platform;
  });

  useEffect(() => {
    dispatch(getListPlatform());
  }, [dispatch]);

  if (!platforms) {
    return null;
  }

  return (
    <Page className={classes.root} title="Platform List">
      <Container maxWidth={false}>
        <Header />
        {platforms && (
          <Box mt={3}>
            <Results platforms={platforms} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default PlatformListView;
