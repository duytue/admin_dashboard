import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  FormHelperText,
  Grid,
  InputLabel,
  Select,
  MenuItem,
  Checkbox,
  ListItemText,
  TextField,
  makeStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import _ from 'lodash';
import { getOnePermission, getListObjectPermissions, getListActionPermissions, updatePermission, updateRoleOfPermission } from 'src/actions/permissionActions';
import { getRoles } from 'src/actions/roleActions';

const useStyles = makeStyles(() => ({
  root: {},
  buttonArrowDropDown: {
    minWidth: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
}));

function PermissionEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [saveCallback, setSaveCallback] = useState();
  const { enqueueSnackbar } = useSnackbar();
  const permissionId = _.trim(window.location.pathname.slice(28, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const [openRoleSelect, setOpenRoleSelect] = useState(false);
  const { permission, objects, actions } = useSelector((state) => {
    return state.permission
  });
  const { roles, loadingUpdateRoleOfPermission } = useSelector((state) => {
    return state.role
  })

  useEffect(() => {
    dispatch(getOnePermission(permissionId));
  }, [dispatch, permissionId]);

  useEffect(() => {
    dispatch(getRoles());
    dispatch(getListObjectPermissions());
    dispatch(getListActionPermissions());
  }, [dispatch])

  const handleAddRole = (valueRoles) => {
    try {
      dispatch(updateRoleOfPermission({ permission_id: permissionId, role_ids: valueRoles })).then(() => {
        enqueueSnackbar('User updated Role', {
          variant: 'success',
          action: <Button>See all</Button>
        });
      })
    } catch (error) {
      enqueueSnackbar('User updated Failed', {
        variant: 'error',
        action: <Button>See all</Button>
      });
    }
  };

  const handleSave = (callback) => {
    setOpen(true);
    setSaveCallback(callback);
  };

  const handleClose = (action) => {
    setOpen(false);
    if (action) {
      saveCallback();
    }
  };

  if (_.size(permission) === 0) {
    return null;
  }

  return (
    <>
      <Formik
        initialValues={{
          object: _.words(permission.name)[2] ? `${_.capitalize(_.words(permission.name)[1])}-${_.capitalize(_.words(permission.name)[2])}` : _.capitalize(_.words(permission.name)[1]) || '',
          action: _.capitalize(_.words(permission.name)[0]) || '',
          description: permission.description || '',
          rolesUser: permission.roles && permission.roles.data ? _.map(permission.roles.data, 'id') : [],
        }}
        validationSchema={Yup.object().shape({
          object: Yup.string().max(255).required('Object is required'),
          action: Yup.string().max(255).required('Action is required'),
          description: Yup.string(),
          rolesUser: Yup.array(),
        })}
        onSubmit={async (values, {
          resetForm,
          setErrors,
          setStatus,
          setSubmitting
        }) => {
          // Make API request
          const newValues = _.omit(values, ['object', 'action']);
          const detailObjectId = _.find(objects, (i) => i.name === values.object);
          const detailActiontId = _.find(actions, (i) => i.name === values.action);

          const callback = () => () => dispatch(updatePermission({
            ...newValues,
            name: `${detailActiontId.name} ${detailObjectId.name}`,
            key: `${detailActiontId.key}_${detailObjectId.key}`,
            id: permissionId
          })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Permission updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(('/app/management/permissions'));
          }).catch((error) => {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
            enqueueSnackbar(error.response.data.message, {
              variant: 'error'
            });
          });

          handleSave(callback);
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
          isSubmitting,
          touched,
          values
        }) => (
            <form
              className={clsx(classes.root, className)}
              onSubmit={handleSubmit}
              {...rest}
            >

              <Grid
                container
                spacing={3}
              >
                <Grid item xs={8}>
                  <Card>
                    <CardContent>
                      <Grid
                        container
                        spacing={3}
                      >
                        <Grid
                          item
                          md={6}
                          xs={12}
                        >
                          <InputLabel error={Boolean(touched.action && errors.action)} id="action-select-label">Action</InputLabel>
                          <Select
                            labelId="action-select-label"
                            id="action_select_id"
                            name="action"
                            error={Boolean(touched.action && errors.action)}
                            helperText={touched.action && errors.action}
                            fullWidth
                            required
                            value={values.action}
                            onChange={handleChange}
                          >
                            {
                              actions && actions.map((item) => (
                                <MenuItem key={item.id} value={item.name}>{item.name}</MenuItem>
                              ))
                            }
                          </Select>
                          <FormHelperText error>{touched.action && errors.action}</FormHelperText>
                        </Grid>
                        <Grid
                          item
                          md={6}
                          xs={12}
                        >
                          <InputLabel error={Boolean(touched.object && errors.object)} id="object-select-label">Object</InputLabel>
                          <Select
                            labelId="object-select-label"
                            id="object_select_id"
                            name="object"
                            error={Boolean(touched.object && errors.object)}
                            fullWidth
                            required
                            value={values.object}
                            onChange={handleChange}
                          >
                            {
                              objects && objects.map((item) => (
                                <MenuItem key={item.id} value={item.name}>{item.name}</MenuItem>
                              ))
                            }
                          </Select>
                          <FormHelperText error>{touched.object && errors.object}</FormHelperText>
                        </Grid>
                        <Grid
                          item
                          md={12}
                          xs={24}
                        >
                          <TextField
                            fullWidth
                            label="Description"
                            name="description"
                            multiline
                            rows={4}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.description}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                      <Box mt={2}>
                        <Button
                          variant="contained"
                          color="secondary"
                          type="submit"
                          disabled={isSubmitting}
                        >
                          Update permission
                          </Button>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={4}>
                  <Card>
                    <CardContent>
                      <InputLabel shrink id="Roles_User">
                        Roles User
                      </InputLabel>
                      <Select
                        multiple
                        fullWidth
                        labelId="Roles_User"
                        id="Roles_User"
                        label="Roles User"
                        name="rolesUser"
                        value={values.rolesUser}
                        onChange={handleChange}
                        open={openRoleSelect}
                        onClose={() => setOpenRoleSelect(false)}
                        IconComponent={() => (
                          <Button className={classes.buttonArrowDropDown} onClick={() => setOpenRoleSelect(true)}>
                            <ArrowDropDownIcon />
                          </Button>
                        )}
                        renderValue={(selected) => (
                          <div className={classes.chips}>
                            {selected.map((item) => (
                              <Chip
                                key={item}
                                label={_.get(_.find(roles, i => i.id === item), "name")}
                                onDelete={() => setFieldValue("rolesUser", _.reject(values.rolesUser, i => i === item))}
                                className={classes.chip}
                              />
                            ))}
                          </div>
                        )}
                      >
                        {
                          roles && roles.map((item) => (
                            <MenuItem key={item.id} value={item.id}>
                              <Checkbox checked={values.rolesUser.indexOf(item.id) > -1} />
                              <ListItemText primary={item.name} />
                            </MenuItem>
                          ))
                        }
                      </Select>
                      <Box mt={2}>
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => handleAddRole(values.rolesUser)}
                          disabled={loadingUpdateRoleOfPermission}
                        >
                          Update Permission
                        </Button>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>

              </Grid>
            </form>
          )}
      </Formik>
      <Dialog
        open={open}
        onClose={() => handleClose(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Save Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Do you want to save the changes?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose(false)} color="primary">
            No
          </Button>
          <Button onClick={() => handleClose(true)} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

PermissionEditForm.propTypes = {
  className: PropTypes.string,
  permission: PropTypes.object.isRequired
};

export default PermissionEditForm;
