/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import {
  useDispatch, useSelector,
  // useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import _ from 'lodash';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  InputLabel,
  Select,
  FormHelperText,
  Switch,
  FormControlLabel,
  Chip,
  MenuItem,
  Checkbox,
  ListItemText
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { useHistory } from 'react-router-dom';
import { getListCities } from 'src/actions/addressActions';
import addressService from 'src/services/addressService';
import { addStation } from 'src/actions/stationActions';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationCreateForm({
  stationFunctions, platforms, className, ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const { cities } = useSelector((state) => state.address);
  const [openStationFunctionLists, setStationFunctions] = useState(false);

  useEffect(() => {
    dispatch(getListCities());
  }, [dispatch]);

  const getDistricts = (cityId) => new Promise((resolve, reject) => {
    addressService.getDistrictByCityId(cityId)
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });

  const getWards = (districtId) => new Promise((resolve, reject) => {
    addressService.getWardsByDistrictId(districtId)
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });

  return (
    <Formik
      initialValues={{
        name: '',
        address: '',
        contact: '',
        contact_email: '',
        province: '',
        district: '',
        districts: [],
        ward: '',
        wards: [],
        isActive: true,
        functions_key: [],
        platform_key: ''
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        address: Yup.string().max(255).required('Address is required'),
        contact: Yup.string().max(255).required('Contact is required'),
        contact_email: Yup.string().max(255).required('Email is required'),
        province: Yup.string().max(255).required('Province/City is required'),
        district: Yup.string().max(255).required('District is required'),
        ward: Yup.string().max(255).required('Ward is required'),
        functions_key: Yup.array().required('Function is required')
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          const data = {
            name: values.name,
            address: values.address,
            is_active: values.isActive,
            contact: values.contact,
            contact_email: values.contact_email,
            location_short_id: values.ward,
            latitude: 10.775659,
            longitude: 106.700424,
            city_id: values.province,
            district_id: values.district,
            ward_id: values.ward,
            function_key: values.function_keys,
            platform_key: values.platform_key
          };

          dispatch(addStation(data)).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Station created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(('/app/management/station'));
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid
                container
                spacing={3}
              >
                {/* Name */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.name}
                    variant="outlined"
                  />
                </Grid>
                {/* End Name */}
                {/* Is active */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Box
                    mt={2}
                    px={1}
                  >
                    <FormControlLabel
                      label="Is Active"
                      control={(
                        <Switch
                          checked={values.isActive}
                          edge="end"
                          name="Is Active"
                          onChange={(event) => handleChange('showMenuIcon', event.target.checked)}
                        />
                        )}
                    />
                  </Box>
                </Grid>
                {/* End isactive */}
                {/* Address Street No. & Name */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.address && errors.address)}
                    fullWidth
                    helperText={touched.address && errors.address}
                    label="Address Street No. & Name"
                    name="address"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.address}
                    variant="outlined"
                  />
                </Grid>
                {/* End Address Street No. & Name */}
                {/* Contact */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.contact && errors.contact)}
                    fullWidth
                    helperText={touched.contact && errors.contact}
                    label="Contact"
                    name="contact"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.contact}
                    variant="outlined"
                  />
                </Grid>
                {/* End Contact */}
                {/* Province/City */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.province && errors.province)} id="province-select-label">Province/City</InputLabel>
                  <Select
                    labelId="province-select-label"
                    id="province_select_id"
                    name="province"
                    error={Boolean(touched.province && errors.province)}
                    helperText={touched.province && errors.province}
                    fullWidth
                    required
                    value={values.short_id}
                    onChange={async (e) => {
                      const { value } = e.target;
                      const _district = await getDistricts(value);
                      console.log('city_id', _district);
                      setFieldValue('province', value);
                      setFieldValue('district', '');
                      setFieldValue('districts', _district);
                    }}
                  >
                    {
                        cities && cities.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                      }
                  </Select>
                  <FormHelperText error>{touched.province && errors.province}</FormHelperText>
                </Grid>
                {/* End Province/City */}
                {/* Email */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.contact_email && errors.contact_email)}
                    fullWidth
                    helperText={touched.contact_email && errors.contact_email}
                    label="Email"
                    name="contact_email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.contact_email}
                    variant="outlined"
                  />
                </Grid>
                {/* End Email */}
                {/* District */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.district && errors.district)} id="action-select-label">District</InputLabel>
                  <Select
                    labelId="district-select-label"
                    id="districtselect_id"
                    name="district"
                    error={Boolean(touched.district && errors.district)}
                    helperText={touched.district && errors.district}
                    fullWidth
                    required
                    value={values.short_id}
                    onChange={async (e) => {
                      const { value } = e.target;
                      const _wards = await getWards(value);
                      console.log('wards', _wards);
                      setFieldValue('district', value);
                      setFieldValue('ward', '');
                      setFieldValue('wards', _wards);
                    }}
                  >
                    {
                        values.districts && values.districts.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                      }
                  </Select>
                  <FormHelperText error>{touched.district && errors.district}</FormHelperText>
                </Grid>
                {/* End District */}
                {/* Function */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel shrink id="list_function">
                    Function
                  </InputLabel>
                  <Select
                    fullWidth
                    multiple
                    labelId="Function"
                    id="Function"
                    label="Function"
                    name="functions_key"
                    value={values.functions_key}
                    open={openStationFunctionLists}
                    onClose={() => setStationFunctions(false)}
                    IconComponent={() => (
                      <Button
                        className={classes.buttonArrowDropDown}
                        onClick={() => setStationFunctions(true)}
                      >
                        <ArrowDropDownIcon />
                      </Button>
                    )}
                    onChange={handleChange}
                    renderValue={(selected) => (
                      <div className={classes.chips}>
                        {selected.map((item) => (
                          <Chip
                            key={item}
                            label={item}
                            onDelete={() => setFieldValue('functions_key', _.reject(values.functions_key, (i) => i === item))}
                            className={classes.chip}
                          />
                        ))}
                      </div>
                    )}
                    className={clsx(classes.selectEmpty)}
                  >
                    {
                        stationFunctions && stationFunctions.map((item) => (
                          <MenuItem key={item.id} value={item.key}>
                            <Checkbox checked={values.functions_key.indexOf(item.key) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))
                      }
                  </Select>
                </Grid>
                {/* end Function */}
                {/* ward */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <InputLabel error={Boolean(touched.ward && errors.ward)} id="action-select-label">Ward</InputLabel>
                  <Select
                    labelId="ward-select-label"
                    id="ward_select_id"
                    name="ward"
                    error={Boolean(touched.ward && errors.ward)}
                    helperText={touched.ward && errors.ward}
                    fullWidth
                    required
                    value={values.short_id}
                    onChange={handleChange}
                  >
                    {
                        values.wards && values.wards.map((item) => (
                          <MenuItem key={item.id} value={item.short_id}>{item.name}</MenuItem>
                        ))
                      }
                  </Select>
                  <FormHelperText error>{touched.ward && errors.ward}</FormHelperText>
                </Grid>
                {/* End Ward */}
                <Grid item lg={6} md={12} xs={12}>
                  <InputLabel shrink id="platform_key">Platform</InputLabel>
                  <Select
                    labelId="platform_key"
                    fullWidth
                    id="platform_key"
                    label="platform_key"
                    name="platform_key"
                    value={values.platform_key}
                    onChange={handleChange}
                    displayEmpty
                    className={classes.selectEmpty}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {
                      platforms && platforms.map((item) => (
                        <MenuItem key={item.id} value={item.key}>{item.name}</MenuItem>
                      ))
                    }
                  </Select>
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Create Station
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

StationCreateForm.propTypes = {
  className: PropTypes.string,
  stationFunctions: PropTypes.array,
  platforms: PropTypes.array
};

StationCreateForm.defaultProps = {
  stationFunctions: []
};

export default StationCreateForm;
