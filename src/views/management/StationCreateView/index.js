import React, { useEffect } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getListStationFunctions } from 'src/actions/stationActions';
import { getListPlatform } from 'src/actions/platformActions';
import Header from './Header';
import StationCreateForm from './StationCreateForm';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationCreateView() {
  const dispatch = useDispatch();
  const { stationFunctions } = useSelector((state) => state.station);
  const { platforms } = useSelector((state) => state.platform);

  useEffect(() => {
    dispatch(getListStationFunctions());
    dispatch(getListPlatform());
  }, [dispatch]);

  const classes = useStyles();
  return (
    <Page
      className={classes.rootPageCreateView}
      title="Platform Create"
    >
      <Container maxWidth={false}>
        <Header />
        <StationCreateForm stationFunctions={stationFunctions} platforms={platforms} />
      </Container>
    </Page>
  );
}

export default StationCreateView;
