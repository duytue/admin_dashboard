import React from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import LanguageSettingCreateForm from './LanguageSettingCreateForm';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function LanguageSettingCreateView() {
  const classes = useStyles();
  return (
    <Page
      className={classes.rootPageCreateView}
      title="Language Setting Create"
    >
      <Container maxWidth={false}>
        <Header />
        <LanguageSettingCreateForm />
      </Container>
    </Page>
  );
}

export default LanguageSettingCreateView;
