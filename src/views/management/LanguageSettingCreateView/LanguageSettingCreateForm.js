import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
  makeStyles,
  FormHelperText
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useHistory } from "react-router-dom";
import { createLanguageSetting } from 'src/actions/languageSettingActions';
import { getListPlatform } from 'src/actions/platformActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectBirthDay: {
    width: "100%",
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  buttonArrowDropDown: {
    minWidth: 0,
  }
}));

function LanguageSettingCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const { loadingCreateLanguageSetting } = useSelector((state) => {
    return state.language_setting
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  })

  useEffect(() => {
    dispatch(getListPlatform())
  }, [dispatch])

  return (
    <Formik
      initialValues={{
        key: '',
        value: [],
        description: '',
        language_key: [],
        platform_key: ''
      }}
      validationSchema={Yup.object().shape({
        key: Yup.string().max(255).required('Key is required'),
        value: Yup.string().required('Value is required'),
        description: Yup.string().max(255),
        language_key: Yup.string().required('Language Key is required'),
        platform_key: Yup.string().max(255).required('Platform Code is required'),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(createLanguageSetting(values)).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Language Setting created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/language-setting"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>
                <Grid
                  container
                  spacing={3}
                >
                  {/* Key */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.key && errors.key)}
                      fullWidth
                      helperText={touched.key && errors.key}
                      label="Key"
                      name="key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.key}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Key */}
                  {/* Value */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.value && errors.value)}
                      fullWidth
                      helperText={touched.value && errors.value}
                      label="Value"
                      name="value"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.value}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Value */}
                  {/* Language_key */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.language_key && errors.language_key)}
                      fullWidth
                      helperText={touched.language_key && errors.language_key}
                      label="Language Key"
                      name="language_key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.language_key}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Language_key */}
                  {/* Platform_key */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.platform_key && errors.platform_key)} id="platform-select-label">Platform</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="platform_key"
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      required
                      value={values.platform_key}
                      onChange={handleChange}
                    >
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.key} value={item.key}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.platform_key && errors.platform_key}</FormHelperText>
                    {/* <TextField
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      helperText={touched.platform_key && errors.platform_key}
                      label="Platform code"
                      name="platform_key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.platform_key}
                      variant="outlined"
                    /> */}
                  </Grid>
                  {/* End Platform_key */}
                  {/* Description */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Description */}
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Language Setting
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

LanguageSettingCreateForm.propTypes = {
  className: PropTypes.string,
};

export default LanguageSettingCreateForm;
