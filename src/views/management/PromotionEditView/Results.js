import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import _ from 'lodash';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
  Radio,
  FormControlLabel,
  Switch
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { getPromotionById, updatePromotion } from 'src/actions/promotionActions';
import { KeyboardDatePicker } from '@material-ui/pickers';
import userService from 'src/services/userService';
import { getOne, update } from '../../../actions/reviewActions';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  datePicker: {
    marginLeft: theme.spacing(2)
  }
}));

function Results({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const objId = _.trim(window.location.pathname.slice(26, -4), '/');

  const dispatch = useDispatch();
  const { promotion } = useSelector((state) => {
    return state.promotion
  });
  const { list_platform } = useSelector((state) => {
    return state.account.user;
  });
  const [isApplyForAll, setIsApplyForAll] = useState(true);
  const [isActive, setIsActive] = useState(false);

  const history = useHistory();

  useEffect(() => {
    dispatch(getPromotionById(objId));
  }, [dispatch, objId]);

  if (_.size(promotion) === 0) {
    return null;
  }

  const promotion_type_temp = [
    { id: 'pt1', name: 'Percent' },
    { id: 'pt2', name: 'Cash' }
  ];

  return (
    <Formik
      initialValues={{
        name: promotion.name || '1',
        max_size: promotion.max_size || 0,
        customer_size: promotion.customer_size || 0,
        coupon_code: promotion.coupon_code || '1',
        type: promotion.type || '',
        value: promotion.value || 0,
        platform_key: promotion.platform_key || '',
        description: promotion.description || ''
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().max(255).required('Name is required'),
        max_size: Yup.number()
          .moreThan(-1, 'Total Capacity must be larger than -1')
          .integer('Please provide integer')
          .required('Total capacity is required'),
        customer_size: Yup.number()
          .moreThan(-1, 'Max per customer must be larger than -1')
          .integer('Please provide integer')
          .required('Max per customer is required'),
        coupon_code: Yup.string().required('Coupon code is required'),
        type: Yup.string().required('Promotion Type is required'),
        value: Yup.number()
          .moreThan(-1, 'Promotion Value must be larger than -1')
          .integer('Please provide integer')
          .required('Promotion Value is required'),
        platform_key: Yup.string().required('Platform is required')
      })}
      onSubmit={(
        values,
        { resetForm, setErrors, setStatus, setSubmitting }
      ) => {
        try {
          // Make API request

          dispatch(updatePromotion({ ...values }, objId)).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Promotion updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push('/app/management/promotion');
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form
          className={clsx(classes.rootPageCreateView, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid container spacing={3}>
                <Grid item md={4} xs={12}>
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.name}
                    variant="outlined"
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <TextField
                    error={Boolean(touched.max_size && errors.max_size)}
                    fullWidth
                    helperText={touched.max_size && errors.max_size}
                    label="Total Capacity"
                    name="max_size"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    value={values.max_size}
                    variant="outlined"
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <TextField
                    error={Boolean(
                      touched.customer_size && errors.customer_size
                    )}
                    fullWidth
                    helperText={touched.customer_size && errors.customer_size}
                    label="Max Per Customer"
                    name="customer_size"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    value={values.customer_size}
                    variant="outlined"
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <TextField
                    error={Boolean(touched.coupon_code && errors.coupon_code)}
                    fullWidth
                    helperText={touched.coupon_code && errors.coupon_code}
                    label="Coupon Code"
                    name="coupon_code"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    value={values.coupon_code}
                    variant="outlined"
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <KeyboardDatePicker
                    fullWidth
                    disableToolbar
                    inputVariant="outlined"
                    format="DD/MM/YYYY"
                    label="Start Time"
                    value={values.start_time}
                    onChange={(value) => setFieldValue('start_time', value)}
                    KeyboardButtonProps={{
                      'aria-label': 'start date filter'
                    }}
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <KeyboardDatePicker
                    fullWidth
                    disableToolbar
                    inputVariant="outlined"
                    format="DD/MM/YYYY"
                    label="End Time"
                    value={values.end_time}
                    onChange={(value) => setFieldValue('end_time', value)}
                    KeyboardButtonProps={{
                      'aria-label': 'end date filter'
                    }}
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <InputLabel
                    error={Boolean(
                      touched.type && errors.type
                    )}
                    id="promotion-type-select-label"
                  >
                    Promotion Type
                  </InputLabel>
                  <Select
                    labelId="promotion-type-select-label"
                    id="promotion_type_select_id"
                    name="type"
                    error={Boolean(
                      touched.type && errors.type
                    )}
                    fullWidth
                    value={values.type}
                    onChange={handleChange}
                  >
                    {promotion_type_temp &&
                      promotion_type_temp.map((item) => (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText error>
                    {touched.type && errors.type}
                  </FormHelperText>
                </Grid>

                <Grid item md={4} xs={12}>
                  <TextField
                    error={Boolean(
                      touched.value && errors.value
                    )}
                    fullWidth
                    helperText={
                      touched.value && errors.value
                    }
                    label="Promotion Value"
                    name="value"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    value={values.value}
                    variant="outlined"
                  />
                </Grid>

                <Grid item md={4} xs={12}>
                  <InputLabel
                    error={Boolean(touched.platform_key && errors.platform_key)}
                    id="platform-select-label"
                  >
                    Platform
                  </InputLabel>
                  <Select
                    labelId="platform-select-label"
                    id="platform_select_id"
                    name="platform_key"
                    error={Boolean(touched.platform_key && errors.platform_key)}
                    fullWidth
                    value={values.platform_key}
                    onChange={handleChange}
                  >
                    {list_platform &&
                      list_platform.map((item) => (
                        <MenuItem key={item.id} value={item.id}>
                          {item.name}
                        </MenuItem>
                      ))}
                  </Select>
                  <FormHelperText error>
                    {touched.platform_key && errors.platform_key}
                  </FormHelperText>
                </Grid>

                <Grid item md={4} xs={12} align="center">
                  <FormControlLabel
                    control={
                      <Radio
                        checked={isApplyForAll}
                        value={isApplyForAll}
                        onChange={() => {
                          setIsApplyForAll(true);
                        }}
                      />
                    }
                    label="All Products"
                  />
                  <FormControlLabel
                    control={
                      <Radio
                        checked={!isApplyForAll}
                        value={!isApplyForAll}
                        disabled
                        onChange={() => {
                          setIsApplyForAll(false);
                        }}
                      />
                    }
                    label="On Some Products"
                  />
                  {/*<FormControlLabel*/}
                    {/*control={*/}
                      {/*<Switch*/}
                        {/*checked={isActive}*/}
                        {/*value={isActive}*/}
                        {/*onChange={() => {*/}
                          {/*setIsActive(!isActive);*/}
                        {/*}}*/}
                      {/*/>*/}
                    {/*}*/}
                    {/*label="Active"*/}
                  {/*/>*/}
                </Grid>

                <Grid item md={12} xs={12}>
                  <TextField
                    error={Boolean(touched.description && errors.description)}
                    fullWidth
                    helperText={touched.description && errors.description}
                    label="Description"
                    name="description"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    value={values.description}
                    variant="outlined"
                    rows={5}
                  />
                </Grid>
              </Grid>

              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Update Promotion
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

Results.propTypes = {
  className: PropTypes.string
};

export default Results;
