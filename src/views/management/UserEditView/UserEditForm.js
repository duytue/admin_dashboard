import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
  Select,
  Switch,
  makeStyles,
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import {
  getOneUser,
  updateUser,
  updateRoleOfUser,
  updatePermissionOfUser,
} from 'src/actions/userActions';
import { getListPlatform } from 'src/actions/platformActions';
import { getListPermissions, getListPermissionsByUser } from 'src/actions/permissionActions';
import { getRoles } from 'src/actions/roleActions';

const useStyles = makeStyles(() => ({
  root: {
    '.MuiSelect-icon': {
      display: 'none',
    },
  },
  selectBirthDay: {
    width: '100%',
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  cardPermission: {
    marginTop: '16px',
  },
  buttonArrowDropDown: {
    minWidth: 0,
  },
}));

function UserEditForm({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const userId = _.trim(window.location.pathname.slice(22, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const [openPlatformSelect, setOpenPlatformSelect] = useState(false);
  const [openPermissionSelect, setOpenPermissionSelect] = useState(false);
  const [openRoleSelect, setOpenRoleSelect] = useState(false);
  const {
    user,
    loadingUpdateRoleOfUser,
    loadingUpdateUser,
    loadingUpdatePermissionOfUser,
  } = useSelector(state => {
    return state.user;
  });
  const { roles } = useSelector(state => {
    return state.role;
  });
  const { permissions, permissionsByUser } = useSelector(state => {
    return state.permission;
  });
  const { platforms } = useSelector(state => {
    return state.platform;
  });

  useEffect(() => {
    dispatch(getOneUser(userId));
    dispatch(getListPermissionsByUser(userId));
  }, [dispatch, userId]);

  useEffect(() => {
    dispatch(getRoles());
    dispatch(getListPermissions());
    dispatch(getListPlatform());
  }, [dispatch]);

  const handleAddRole = valueRoles => {
    try {
      dispatch(updateRoleOfUser({ user_id: userId, roles_ids: valueRoles })).then(() => {
        enqueueSnackbar('User updated Role', {
          variant: 'success',
          action: <Button>See all</Button>,
        });
      });
    } catch (error) {
      enqueueSnackbar('User updated Failed', {
        variant: 'error',
        action: <Button>See all</Button>,
      });
    }
  };

  const handleAddPermission = valuePermissions => {
    try {
      dispatch(
        updatePermissionOfUser({
          user_id: userId,
          permissions_ids: valuePermissions,
        }),
      ).then(() => {
        enqueueSnackbar('User updated Role', {
          variant: 'success',
          action: <Button>See all</Button>,
        });
      });
    } catch (error) {
      enqueueSnackbar('User updated Failed', {
        variant: 'error',
        action: <Button>See all</Button>,
      });
    }
  };

  if (_.size(user) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        email: user.email || '',
        first_name: user.first_name || '',
        last_name: user.last_name || '',
        phone: user.phone || '',
        platform_keys: _.map(user.list_platform, 'key') || [],
        active: user.active || false,
        birthday: new Date(user.birthday) || new Date(),
        rolesUser: _.map(user.roles.data, 'id') || [],
        permissionsUser: _.map(user.permissions.data, 'id') || [''],
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email('Must be a valid email')
          .max(255)
          .required('Email is required'),
        first_name: Yup.string()
          .max(255)
          .required('First name is required'),
        last_name: Yup.string()
          .max(255)
          .required('Last name is required'),
        phone: Yup.string().max(15),
        platform_keys: Yup.array(),
        birthday: Yup.date(),
        active: Yup.bool(),
        rolesUser: Yup.array(),
        permissionsUser: Yup.array(),
      })}
      onSubmit={async (values, { resetForm, setErrors, setStatus, setSubmitting }) => {
        try {
          // Make API request
          const omitValue = _.omit(values, ['permissionsUser', 'rolesUser']);
          dispatch(updateUser({ ...omitValue, id: userId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('User updated', {
              variant: 'success',
              action: <Button>See all</Button>,
            });
            history.push('/app/management/users');
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values,
      }) => (
          <form className={clsx(classes.root, className)} onSubmit={handleSubmit} {...rest}>
            <Grid container spacing={3}>
              <Grid item xs={8}>
                <Card>
                  <CardContent>
                    <Grid container spacing={3}>
                      <Grid item md={6} xs={12}>
                        <TextField
                          error={Boolean(touched.email && errors.email)}
                          fullWidth
                          helperText={touched.email && errors.email}
                          label="Email address"
                          name="email"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.email}
                          variant="outlined"
                          disabled
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          error={Boolean(touched.first_name && errors.first_name)}
                          fullWidth
                          helperText={touched.first_name && errors.first_name}
                          label="First name"
                          name="first_name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.first_name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          error={Boolean(touched.last_name && errors.last_name)}
                          fullWidth
                          helperText={touched.last_name && errors.last_name}
                          label="Last name"
                          name="last_name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.last_name}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          error={Boolean(touched.phone && errors.phone)}
                          fullWidth
                          helperText={touched.phone && errors.phone}
                          label="Phone number"
                          name="phone"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.phone}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <InputLabel shrink id="Platform_keys_label">
                          Platform Name
                      </InputLabel>
                        <Select
                          multiple
                          fullWidth
                          onFocus={() => false}
                          labelId="Platform_keys_label"
                          id={classes.Platform_keys}
                          label="Platform Name"
                          name="platform_keys"
                          open={openPlatformSelect}
                          onClose={() => setOpenPlatformSelect(false)}
                          IconComponent={() => (
                            <Button
                              className={classes.buttonArrowDropDown}
                              onClick={() => setOpenPlatformSelect(true)}
                            >
                              <ArrowDropDownIcon />
                            </Button>
                          )}
                          value={values.platform_keys}
                          onChange={handleChange}
                          renderValue={selected => (
                            <div className={classes.chips}>
                              {selected.map(item => (
                                <Chip
                                  key={item}
                                  label={item}
                                  onDelete={() =>
                                    setFieldValue(
                                      'platform_keys',
                                      _.reject(values.platform_keys, i => i === item),
                                    )
                                  }
                                  className={classes.chip}
                                />
                              ))}
                            </div>
                          )}
                        >
                          {platforms &&
                            platforms.map(item => (
                              <MenuItem key={item.id} value={item.key}>
                                <Checkbox checked={values.platform_keys.indexOf(item.key) > -1} />
                                <ListItemText primary={item.key} />
                              </MenuItem>
                            ))}
                        </Select>
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <KeyboardDatePicker
                          className={clsx(classes.selectBirthDay)}
                          disableToolbar
                          variant="inline"
                          format="DD/MM/YYYY"
                          name="birthday"
                          margin="normal"
                          id="Date Of Birth"
                          label="Date Of Birth"
                          value={values.birthday}
                          onChange={val => {
                            setFieldValue('birthday', val);
                          }}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <Typography variant="h5" color="textPrimary">
                          User Active
                      </Typography>
                        <Typography variant="body2" color="textSecondary">
                          This will give the user is active to work
                      </Typography>
                        <Switch
                          checked={values.active}
                          color="secondary"
                          edge="start"
                          name="active"
                          onChange={handleChange}
                          value={values.active}
                        />
                      </Grid>
                    </Grid>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        type="submit"
                        disabled={loadingUpdateUser}
                      >
                        Update user
                    </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item xs={4}>
                <Card>
                  <CardContent>
                    <InputLabel shrink id="Roles_User">
                      Roles User
                  </InputLabel>
                    <Select
                      multiple
                      fullWidth
                      labelId="Roles_User"
                      id="Roles_User"
                      label="Roles User"
                      name="rolesUser"
                      open={openRoleSelect}
                      onClose={() => setOpenRoleSelect(false)}
                      IconComponent={() => (
                        <Button
                          className={classes.buttonArrowDropDown}
                          onClick={() => setOpenRoleSelect(true)}
                        >
                          <ArrowDropDownIcon />
                        </Button>
                      )}
                      value={values.rolesUser}
                      onChange={handleChange}
                      renderValue={selected => (
                        <div className={classes.chips}>
                          {selected.map(item => (
                            <Chip
                              key={item}
                              label={_.get(
                                _.find(roles, i => i.id === item),
                                'name',
                              )}
                              onDelete={() =>
                                setFieldValue(
                                  'rolesUser',
                                  _.reject(values.rolesUser, i => i === item),
                                )
                              }
                              className={classes.chip}
                            />
                          ))}
                        </div>
                      )}
                    >
                      {roles &&
                        roles.map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            <Checkbox checked={values.rolesUser.indexOf(item.id) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))}
                    </Select>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => handleAddRole(values.rolesUser)}
                        disabled={loadingUpdateRoleOfUser}
                      >
                        Update Role
                    </Button>
                    </Box>
                  </CardContent>
                </Card>
                <Card className={classes.cardPermission}>
                  <CardContent>
                    <InputLabel shrink id="Roles_User">
                      Permissions User
                  </InputLabel>
                    <Select
                      multiple
                      fullWidth
                      displayEmpty
                      labelId="Permissions_User"
                      id="Permissions_User"
                      label="Permissions User"
                      name="permissionsUser"
                      value={values.permissionsUser}
                      open={openPermissionSelect}
                      onClose={() => setOpenPermissionSelect(false)}
                      IconComponent={() => (
                        <Button
                          className={classes.buttonArrowDropDown}
                          onClick={() => setOpenPermissionSelect(true)}
                        >
                          <ArrowDropDownIcon />
                        </Button>
                      )}
                      onChange={handleChange}
                      renderValue={selected => (
                        <div className={classes.chips}>
                          {selected.map(item => (
                            <Chip
                              key={item}
                              label={_.get(
                                _.find(permissions, i => i.id === item),
                                'name',
                              )}
                              onDelete={() =>
                                setFieldValue(
                                  'permissionsUser',
                                  _.reject(values.permissionsUser, i => i === item),
                                )
                              }
                              className={classes.chip}
                            />
                          ))}
                          {_.xor(
                            _.map(_.intersectionBy(permissions, permissionsByUser, 'id'), 'id'),
                            selected,
                          ).map(item => (
                            <Chip
                              key={item}
                              disabled
                              label={_.get(
                                _.find(permissions, i => i.id === item),
                                'name',
                              )}
                              className={classes.chip}
                            />
                          ))}
                        </div>
                      )}
                    >
                      {permissions && permissionsByUser
                        ? _.xorBy(permissions, permissionsByUser, 'id').map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            <Checkbox checked={values.permissionsUser.indexOf(item.id) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))
                        : permissions.map(item => (
                          <MenuItem key={item.id} value={item.id}>
                            <Checkbox checked={values.permissions.indexOf(item.id) > -1} />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))}
                      {permissionsByUser &&
                        permissionsByUser.map(item => (
                          <MenuItem key={item.id} value={item.id} disabled>
                            <Checkbox checked />
                            <ListItemText primary={item.name} />
                          </MenuItem>
                        ))}
                    </Select>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => handleAddPermission(values.permissionsUser)}
                        disabled={loadingUpdatePermissionOfUser}
                      >
                        Update Permission
                    </Button>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </form>
        )}
    </Formik>
  );
}

UserEditForm.propTypes = {
  className: PropTypes.string,
};

export default UserEditForm;
