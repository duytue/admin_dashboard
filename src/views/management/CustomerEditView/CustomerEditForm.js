import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Switch,
  Typography,
  TextField,
  FormHelperText,
  makeStyles,
  Select,
  MenuItem,
  InputLabel,
  Paper
} from '@material-ui/core';
import {
  Upload as UploadIcon,
  XCircle as DeleteIcon,
  AlertCircle
} from 'react-feather';
import {
  ToggleButtonGroup,
  ToggleButton
} from '@material-ui/lab';
import { KeyboardDatePicker } from '@material-ui/pickers';
import customerService from 'src/services/customerService';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import _ from 'lodash';
import { useHistory } from "react-router-dom";
import { getOneCustomer, updateCustomer, getListCustomer, getListMetaData } from 'src/actions/customerAction';
import { getListPlatform } from 'src/actions/platformActions';


const genders = [
  {"value": "male", "name":"male"},
  {"value": "female", "name":"female"}
]
const types = [
  {"value": "enterprise", "name":"enterprise"},
  {"value": "individual", "name":"individual"}
]


const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerEditForm({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const history = useHistory();
  const customerId = _.trim(window.location.pathname.slice(25, -4), '/');
  const [avatar, setImageUrl] = useState(null);
  const [new_image, setNewImage] = useState(null);
  const [language, setLanguage] = useState('en');
  const [isShowCurrentImage, setIsShowCurrentImage] = useState(true);

  const { user } = useSelector((state) => state.account);
  const { customer } = useSelector((state) => {
    return state.customer
  });
  const { metadatas } = useSelector((state) => {
    return state.customer
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  })
  const categories = useSelector((state) => {
    return state.customer.customers.filter(item => item.level < 3);
  })

  useEffect(() => {
    dispatch(getListCustomer());
    dispatch(getListPlatform());
    dispatch(getListMetaData());
    dispatch(getOneCustomer(customerId));
  }, [dispatch, customerId]);


  if (_.size(customer) === 0) {
    return null;
  }
  const handleChangeImage = (event) => {
    if (event.target.files.length !== 0) {
      setIsShowCurrentImage(false);
      let file = event.target.files[0];
      let type = file.type;
      if (type.indexOf('image/') < 0) {
        enqueueSnackbar('Only image files are allowed', {
          variant: 'error'
        });
      } else {
        setNewImage(file);
        fileToBase64(file);
      }

    }
  }

  const fileToBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setImageUrl(reader.result);
    };
  };

  const onClearImage = () => {
    setIsShowCurrentImage(false);
    setImageUrl(null);
    setNewImage(null);
  }

  const onChangeLanguage = (event, value) => {
    setLanguage(value[0]);
  }

  const handleSeeAll = () => {
    history.push(("/app/management/customers"))
  }

  return (
    <Formik
      initialValues={{
        // parent_id: customer.parent_id ? customer.parent_id : '',
        email: customer.email || '',
        customer_type: customer.customer_type || '',
        first_name: customer.first_name || '',
        last_name: customer.last_name || '',
        phone_number: customer.phone_number || '',
        platform_key: customer.platform_key || '',
        is_active: customer.is_active || false,
        birthday: new Date(customer.birthday) || new Date(),
        gender: customer.gender || '',
        customer_group: customer.customer_group || '',
        // keyword: convertToString(customer.keywords)
      }}
      validationSchema={Yup.object().shape({
        // parent_id: Yup.string().max(255),
        email: Yup.string()
          .email('Must be a valid email')
          .max(255)
          .required('Email is required'),
        first_name: Yup.string()
          .max(255)
          .required('First name is required'),
        last_name: Yup.string()
          .max(255)
          .required('Last name is required'),
        phone_number: Yup.string().max(15),
        customer_type: Yup.string().max(255),
        platform_keys: Yup.array(),
        gender: Yup.string().max(15),
        birthday: Yup.date(),
        is_active: Yup.bool(),
        customer_group: Yup.string(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          let param = {
            "id": customer.id,
            // "parent_id": values.parent_id === '' ? null : values.parent_id,
            "platform_key": values.platform_key,
            "gender": values.gender,
            "email": values.email,
            "customer_type": values.customer_type,
            "first_name": values.first_name,
            "last_name": values.last_name,
            "phone_number": values.phone_number,
            "birthday": values.birthday,
            "is_active": values.is_active,
            "customer_group": values.customer_group

          }
          if (new_image !== null) {
            let media_code = new_image.type.replace('image/', '').toUpperCase();

            let preUploadParam = {
              "creator_by": user.id,
              "name": new_image.name,
              "media_code": media_code,
              "platform_key": values.platform_key
            }

            let responsePre = await customerService.preUpload(preUploadParam);
            if (responsePre) {
              let responseUpload = await customerService.uploadImage(responsePre, new_image);
              if (responseUpload) {
                let responseCreateUpload = await customerService.createUpload(preUploadParam);
                if (responseCreateUpload) {
                  param['image_url'] = responseCreateUpload.url;
                  try {
                    await dispatch(updateCustomer(param));
                    setStatus({ success: true });
                    enqueueSnackbar('Category updated', {
                      variant: 'success',
                      action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
                    });
                  } catch (error) {
                    setStatus({ success: false });
                    setErrors({ submit: error.message });
                    enqueueSnackbar('Update customer failure', {
                      variant: 'error'
                    });
                  } finally {
                    setSubmitting(false);
                  }
                }
              }
            }
          } else {
            param['image_url'] = isShowCurrentImage ? customer.image_url : null;
            try {
              await dispatch(updateCustomer(param));
              setStatus({ success: true });
              enqueueSnackbar('Customer updated', {
                variant: 'success',
                action: (<Button variant="contained" onClick={handleSeeAll}>See all</Button>)
              });
            } catch (error) {
              setStatus({ success: false });
              setErrors({ submit: error.message });
              enqueueSnackbar('Update customer failure', {
                variant: 'error'
              });
            } finally {
              setSubmitting(false);
            }
          }
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
        touched,
        values
      }) => (
          <form className={clsx(classes.rootPageCreateView, className)} onSubmit={handleSubmit} {...rest}>

            <Card>
              <CardContent>

                <Grid container spacing={3}>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.platform_key && errors.platform_key)} id="platform-select-label">Platform</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="platform_key"
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      required
                      value={values.platform_key}
                      onChange={handleChange}
                    >
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.id} value={item.key}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.platform_key && errors.platform_key}</FormHelperText>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="platform-select-label">
                      customer group
                    </InputLabel>
                    <Select labelId="customer_group" fullWidth id="customer_group" label="customer_group" name="customer_group"
                      value={values.customer_group} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {
                        metadatas && metadatas.map((item) => (
                          <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email}
                      label="Email"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.first_name && errors.first_name)}
                      fullWidth
                      helperText={touched.first_name && errors.first_name}
                      label="First Name"
                      name="first_name"
                      type="first_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.first_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.last_name && errors.last_name)}
                      fullWidth
                      helperText={touched.last_name && errors.last_name}
                      label="Last Name"
                      name="last_name"
                      type="last_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.last_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <TextField
                      error={Boolean(touched.phone_number && errors.phone_number)}
                      fullWidth
                      helperText={touched.phone_number && errors.phone_number}
                      label="Phone"
                      name="phone_number"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.phone_number}
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="platform-select-label">
                      Gender
                    </InputLabel>
                    <Select labelId="gender" fullWidth id="gender" label="gender" name="gender"
                      value={values.gender} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                      {
                        genders && genders.map((item) => (
                          <MenuItem value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <InputLabel shrink id="platform-select-label">
                      Customer Type
                    </InputLabel>
                    <Select labelId="customer_type" fullWidth id="customer_type" label="customer_type" name="customer_type"
                      value={values.customer_type} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                      {
                        types && types.map((item) => (
                          <MenuItem value={item.value}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid item  lg={6} md={12} xs={12}>
                    <KeyboardDatePicker
                      className={clsx(classes.selectBirthDay)}
                      disableToolbar
                      fullWidth
                      variant="inline"
                      format="DD/MM/YYYY"
                      name="birthday"
                      margin="normal"
                      id="Date Of Birth"
                      label="Date Of Birth"
                      value={values.birthday}
                      onChange={val => {
                        setFieldValue('birthday', val);
                      }}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </Grid>
                
                  <Grid item lg={6} md={12}>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="contained-button-file"
                      type="file"
                      style={{ display: 'none' }}
                      onChange={handleChangeImage}
                    />
                    {isShowCurrentImage && <>
                      {customer && customer.avatar && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                        <label htmlFor="contained-button-file">
                          <img className={classes.imageStyle} src={customer?.avatar} style={{ maxWidth: '150px', border: '1px solid' }} />
                        </label>
                        <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }} onClick={onClearImage} />
                      </Paper>}

                      {customer && !customer.avatar && <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                          {language === 'en' ? 'Choose image' : 'Chọn ảnh'}
                        </Button>
                      </label>}
                    </>}
                    {!isShowCurrentImage && <>
                      {avatar && <Paper className={classes.paper} elevation={0} style={{ position: 'relative', display: 'block' }}>
                        <label htmlFor="contained-button-file">
                          <img className={classes.imageStyle} src={avatar} style={{ maxWidth: '150px', border: '1px solid' }} />
                        </label>
                        <DeleteIcon style={{ color: '#ff0000a6', top: 3, marginLeft: -25, position: 'absolute', height: 20 }} onClick={onClearImage} />
                      </Paper>}

                      {!avatar && <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span" className={classes.button, classes.margin} startIcon={<UploadIcon />}>
                          {language === 'en' ? 'Choose image' : 'Chọn ảnh'}
                        </Button>
                      </label>}
                    </>}
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Typography
                      variant="h5"
                      color="textPrimary"
                    >
                      Customer Active
                  </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                    >
                      This will give the customer is active to work
                  </Typography>
                    <Switch
                      checked={values.is_active}
                      color="secondary"
                      edge="start"
                      name="is_active"
                      onChange={handleChange}
                      value={values.is_active}
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button variant="contained" color="secondary" type="submit" disabled={isSubmitting}>
                    Update customer
                  </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

CustomerEditForm.propTypes = {
  className: PropTypes.string,
  customer: PropTypes.object.isRequired
};

export default CustomerEditForm;
