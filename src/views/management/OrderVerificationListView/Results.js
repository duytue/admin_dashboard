/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Divider,
  Tab,
  Tabs,
  makeStyles
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from 'src/actions/userActions';
import Rules from './Rules';
import Reviewed from './Reviewed';
const tabs = [
  {
    value: 'rules',
    label: 'Rules'
  },
  {
    value: 'need-review',
    label: 'Need reviewed'
  }
];


const useStyles = makeStyles((theme) => ({
  root: {
    height: '79vh',
    position: 'relative'
  }
}));

function Results({ className, customerFees, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.user);
  const [currentTab, setCurrentTab] = useState('rules');

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    console.log(value);
    setCurrentTab(value);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <PerfectScrollbar>
        <Box>
          { currentTab && currentTab === 'rules' && <Rules />}
          { currentTab && currentTab === 'need-review' && <Reviewed />}
        </Box>
      </PerfectScrollbar>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  customerFees: PropTypes.array
};

Results.defaultProps = {
  customerFees: []
};

export default Results;
