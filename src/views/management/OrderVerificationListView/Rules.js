import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    Box,
    makeStyles,
    Grid,
    Typography,
    OutlinedInput,
    InputAdornment,
    Switch,
    Button,
    SvgIcon,
    TextField,
    MenuItem,
    IconButton
} from '@material-ui/core';
import {
    PlusCircle as PlusCircleIcon,
    Save as SaveIcon,
    X as DeleteIcon
} from 'react-feather';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '79vh',
        position: 'relative'
    },
    borderBottom: {
        borderBottom: '1px solid #ccc'
    },
    container: {
        padding: '20px',
        margin: '0'
    },
    danger: {
        color: theme.palette.error.main
    },
    success: {
        color: theme.palette.success.main
    }
}));

const failedRules = [
    {
        id: 1,
        key: 'maximum',
        name: 'Failed delivery maximum',
        times: 3,
        duration: 5,
        active: true
    },
    {
        id: 2,
        key: 'minimum',
        name: 'Failed minimum',
        times: 3,
        duration: 5,
        active: true
    }
];

const duplicationRules = [
    {
        id: 1,
        key: 'similarity',
        name: 'Similarity Threshold',
        percent: 3,
        duration: 5,
        active: true
    }
];

const SKUValues = [
    {
        id: 1,
        key: 'maximum',
        name: 'Maximum',
        value: 100000,
        method: 0,
        active: true
    }
];

const customerProfile = true;
function Rules({ className, ...rest }) {
    const classes = useStyles();
    const [lstFailedRules, setLstFailedRules] = useState(failedRules);
    const [lstDuplicationRules, setLstDuplicationRules] = useState(duplicationRules);
    const [lstSKUValues, setLstSKUValues] = useState(SKUValues);
    const [isCustomerProfile, setIsCustomerProfile] = useState(customerProfile);

    const handleChange = (e, item) => {
        console.log(e);
        console.log(item);
    }

    const addNewFaileRules = () => {
        let array = [...lstFailedRules];
        array.push({
            id: null,
            key: '',
            name: '',
            times: null,
            duration: null,
            active: false
        });
        setLstFailedRules(array);
    }

    const removeFailedRules = (e, index) => {
        let array = [...lstFailedRules];
        array.splice(index, 1);
        setLstFailedRules(array);
    }

    const addNewDuplicationRules = () => {
        let array = [...lstDuplicationRules];
        array.push({
            id: null,
            key: '',
            name: '',
            percent: null,
            duration: null,
            active: false
        });
        setLstDuplicationRules(array);
    }

    const removeDuplicationRules = (e, index) => {
        let array = [...lstDuplicationRules];
        array.splice(index, 1);
        setLstDuplicationRules(array);
    }

    const addNewSKUValues = () => {
        let array = [...lstSKUValues];
        array.push({
            id: null,
            key: '',
            name: '',
            value: null,
            method: 0,
            active: false
        });
        setLstSKUValues(array);
    }

    const removeSKUValues = (e, index) => {
        let array = [...lstSKUValues];
        array.splice(index, 1);
        setLstSKUValues(array);
    }

    return (
        <Grid container spacing={3} className={classes.container}>
            <Grid item xs={12} className={classes.borderBottom}>
                <Typography variant="h4" color="textPrimary">
                    Failed delivery rules
              </Typography>
                {lstFailedRules && lstFailedRules.map((item, index) => (<Grid container spacing={2} key={index}>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            {item.id !== null && <label color="textPrimary">{item.name}</label>}
                            {item.id === null && <OutlinedInput
                                id="outlined-adornment-name"
                                value={item.name}
                                onChange={(e) => handleChange(e, item)}
                                startAdornment={<InputAdornment position="start">Name:</InputAdornment>}
                                labelWidth={0}
                            />}
                            <OutlinedInput
                                id="outlined-adornment-times"
                                type={'number'}
                                value={item.times}
                                onChange={(e) => handleChange(e, item)}
                                endAdornment={<InputAdornment position="end">times</InputAdornment>}
                                style={{ maxWidth: '130px' }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <label color="textPrimary">duration</label>&nbsp;&nbsp;
                    <OutlinedInput
                                id="outlined-adornment-duration"
                                type={'number'}
                                value={item.duration}
                                onChange={(e) => handleChange(e, item)}
                                endAdornment={<InputAdornment position="end">days</InputAdornment>}
                                labelWidth={0}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <Switch
                                checked={item.active}
                                color="secondary"
                                edge="start"
                                name="is_active"
                                onChange={(e) => handleChange(e, item)}
                                value={item.active}
                            />
                            {item.id === null &&
                                <IconButton onClick={(e) => removeFailedRules(e, index)} className={classes.danger}>
                                    <SvgIcon fontSize="small">
                                        <DeleteIcon />
                                    </SvgIcon>
                                </IconButton>}
                            {item.id === null && <IconButton className={classes.success}>
                                <SvgIcon fontSize="small">
                                    <SaveIcon />
                                </SvgIcon>
                            </IconButton>}
                        </Grid>
                    </Grid>
                </Grid>))}
                <Box mt={2}>
                    <IconButton onClick={addNewFaileRules}>
                        <SvgIcon fontSize="small">
                            <PlusCircleIcon />
                        </SvgIcon>
                    </IconButton>
                </Box>
            </Grid>
            <Grid item xs={12} className={classes.borderBottom}>
                <Typography variant="h4" color="textPrimary">
                    Duplication rules
              </Typography>
                {lstDuplicationRules && lstDuplicationRules.map((item, index) => (<Grid container spacing={2} key={index}>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            {item.id !== null && <label color="textPrimary">{item.name}</label>}
                            {item.id === null && <OutlinedInput
                                id="outlined-adornment-name"
                                value={item.name}
                                onChange={(e) => handleChange(e, item)}
                                startAdornment={<InputAdornment position="start">Name:</InputAdornment>}
                                labelWidth={0}
                            />}
                            <OutlinedInput
                                id="outlined-adornment-percent"
                                type={'number'}
                                value={item.percent}
                                onChange={(e) => handleChange(e, item)}
                                endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                style={{ maxWidth: '130px' }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <label color="textPrimary">duration</label>&nbsp;&nbsp;
                    <OutlinedInput
                                id="outlined-adornment-duration"
                                type={'number'}
                                value={item.duration}
                                onChange={(e) => handleChange(e, item)}
                                endAdornment={<InputAdornment position="end">min</InputAdornment>}
                                labelWidth={0}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <Switch
                                checked={item.active}
                                color="secondary"
                                edge="start"
                                name="is_active"
                                onChange={(e) => handleChange(e, item)}
                                value={item.active}
                            />
                            {item.id === null &&
                                <IconButton onClick={(e) => removeDuplicationRules(e, index)} className={classes.danger}>
                                    <SvgIcon fontSize="small">
                                        <DeleteIcon />
                                    </SvgIcon>
                                </IconButton>}
                            {item.id === null && <IconButton className={classes.success}>
                                <SvgIcon fontSize="small">
                                    <SaveIcon />
                                </SvgIcon>
                            </IconButton>}
                        </Grid>
                    </Grid>
                </Grid>))}
                <Box mt={2}>
                    <IconButton onClick={addNewDuplicationRules}>
                        <SvgIcon fontSize="small">
                            <PlusCircleIcon />
                        </SvgIcon>
                    </IconButton>
                </Box>
            </Grid>
            <Grid item xs={12} className={classes.borderBottom}>
                <Typography variant="h4" color="textPrimary">
                    SKU values
              </Typography>
                {lstSKUValues && lstSKUValues.map((item, index) => (<Grid container spacing={2} key={index}>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            {item.id !== null && <label color="textPrimary">{item.name}</label>}
                            {item.id === null && <OutlinedInput
                                id="outlined-adornment-name"
                                value={item.name}
                                onChange={(e) => handleChange(e, item)}
                                startAdornment={<InputAdornment position="start">Name:</InputAdornment>}
                                labelWidth={0}
                            />}
                            <OutlinedInput
                                id="outlined-adornment-percent"
                                type={'number'}
                                value={item.percent}
                                onChange={(e) => handleChange(e, item)}
                                endAdornment={<InputAdornment position="end">VNĐ</InputAdornment>}
                                style={{ maxWidth: '130px' }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <label color="textPrimary">method</label>&nbsp;&nbsp;
                    <TextField
                                id="outlined-select-currency"
                                select
                                value={item.method}
                                onChange={handleChange}
                                variant="outlined"
                                style={{ minWidth: '250px' }}
                            >
                                <MenuItem key={0} value={0}>
                                    COD
                      </MenuItem>
                                <MenuItem key={1} value={1}>
                                    NON-COD
                      </MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid item md={4} xs={12}>
                        <Grid container direction="row" alignItems="center">
                            <Switch
                                checked={item.active}
                                color="secondary"
                                edge="start"
                                name="is_active"
                                onChange={(e) => handleChange(e, item)}
                                value={item.active}
                            />
                            {item.id === null &&
                                <IconButton onClick={(e) => removeSKUValues(e, index)} className={classes.danger}>
                                    <SvgIcon fontSize="small">
                                        <DeleteIcon />
                                    </SvgIcon>
                                </IconButton>}
                            {item.id === null && <IconButton className={classes.success}>
                                <SvgIcon fontSize="small">
                                    <SaveIcon />
                                </SvgIcon>
                            </IconButton>}
                        </Grid>
                    </Grid>
                </Grid>))}
                <Box mt={2}>
                    <IconButton onClick={addNewSKUValues}>
                        <SvgIcon fontSize="small">
                            <PlusCircleIcon />
                        </SvgIcon>
                    </IconButton>
                </Box>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h4" color="textPrimary">
                    Customer profile
              </Typography>
                <Switch
                    checked={isCustomerProfile}
                    color="secondary"
                    edge="start"
                    name="is_active"
                    value={isCustomerProfile}
                />
            </Grid>
        </Grid>
    );
}

Rules.propTypes = {
    className: PropTypes.string
};

export default Rules;
