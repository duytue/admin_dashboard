import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    makeStyles,
    Grid,
    SvgIcon,
    IconButton,
    Table,
    TableCell,
    TableHead,
    TableRow,
    TableBody
} from '@material-ui/core';
import {
    X as DeleteIcon,
    Check as AproveIcon
} from 'react-feather';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '79vh',
        position: 'relative'
    },
    borderBottom: {
        borderBottom: '1px solid #ccc'
    },
    container: {
        padding: '20px',
        margin: '0'
    },
    danger: {
        color: theme.palette.error.main
    },
    success: {
        color: theme.palette.success.main
    }
}));

function Rules({ className, ...rest }) {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell align="center">
                            Order number
                    </TableCell>
                        <TableCell align="center">
                            Create at
                    </TableCell>
                        <TableCell>
                            Pending duration
                    </TableCell>
                        <TableCell>
                            Method of payment
                    </TableCell>
                        <TableCell>
                            Total value
                    </TableCell>
                        <TableCell>
                            Status
                    </TableCell>
                        <TableCell>
                            Reson
                    </TableCell>
                        <TableCell align="right">
                            Actions
                    </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow
                        hover
                    >
                        <TableCell align="center">
                            1
                    </TableCell>
                        <TableCell align="center">
                            17-08-2020 15:30
                    </TableCell>
                        <TableCell>
                            2hour:15min
                    </TableCell>
                        <TableCell>
                            COD
                    </TableCell>
                        <TableCell>
                            200.000 VND
                    </TableCell>
                        <TableCell>
                            Need reviewed
                    </TableCell>
                        <TableCell>
                            Success
                    </TableCell>
                        <TableCell align="right">
                            <IconButton className={classes.success}>
                                <SvgIcon fontSize="small">
                                    <AproveIcon />
                                </SvgIcon>
                            </IconButton>
                            <IconButton className={classes.danger}>
                                <SvgIcon fontSize="small">
                                    <DeleteIcon />
                                </SvgIcon>
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </Grid>
    );
}

Rules.propTypes = {
    className: PropTypes.string
};

export default Rules;
