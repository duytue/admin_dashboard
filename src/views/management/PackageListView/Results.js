/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import LinkIcon from '@material-ui/icons/Link';
import {
  Edit as EditIcon,
  Search as SearchIcon,
  Check as CheckIcon,
  Printer as PrinterIcon,
  Codepen as CodepenIcon
} from 'react-feather';
import {
  deletePackage,
  updatePackageIsPrinted
} from 'src/actions/packageActions';
import { useDispatch } from 'react-redux';
import { useSnackbar } from 'notistack';
import printer from './printer';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'created_at|desc',
    label: 'Last create (newest first)'
  },
  {
    value: 'created_at|asc',
    label: 'Last create (oldest first)'
  }
];

function applyFilters(packages, query, filters) {
  return packages.filter((pk) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (pk[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && pk[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(packages, page, limit) {
  return packages.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(packages, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = packages.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, packages, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const { enqueueSnackbar } = useSnackbar();

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelected([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAll = (event) => {
    setSelected(event.target.checked ? packages.map((obj) => obj.id) : []);
  };

  const handleDeleteAll = () => {
    if (window.confirm('Are you sure delete the item?')) {
      selected.map((obj) => {
        // obj is id
        dispatch(deletePackage(obj));
      });
    }
  };

  const handleSelectOne = (event, objId) => {
    if (!selected.includes(objId)) {
      setSelected((prevSelected) => [...prevSelected, objId]);
    } else {
      setSelected((prevSelected) => prevSelected.filter((id) => id !== objId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleDelete = (obj) => {
    if (window.confirm('Are you sure delete the item?')) {
      dispatch(deletePackage(obj.id));
    }
  };

  const handleCopy = (obj) => {
    navigator.clipboard.writeText(obj.url);
    enqueueSnackbar('Link has been copied.', {
      variant: 'success'
    });
  };

  const handlePrint = (obj) => {
    printer(obj);
  };

  const handleComplete = (obj) => {
    let paginatedTemp = [];
    paginated.forEach((packageItem) => {
      if (packageItem.id === obj.id) {
        packageItem.state = 'Consolidated';
      }
      paginatedTemp.push(packageItem);
    });
    setPaginated(paginatedTemp);
  };

  const updateIsPrinted = (obj) => {
    console.log(obj);
    dispatch(updatePackageIsPrinted(obj.id));
  };

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(packages, query, filters);
  const sorted = applySort(filtered, sort);
  const [paginated, setPaginated] = useState([]);
  // const paginated = applyPagination(sorted, page, limit);
  const enableBulkOperations = selected.length > 0;
  const selectedSome = selected.length > 0 && selected.length < packages.length;
  const selectedAll = selected.length === packages.length;

  useEffect(() => {
    setPaginated(applyPagination(sorted, page, limit));
  }, []);

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search packages"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAll}
              indeterminate={selectedSome}
              onChange={handleSelectAll}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAll}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAll}
                    indeterminate={selectedSome}
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>Picklist ID</TableCell>
                <TableCell>State</TableCell>
                <TableCell align="center">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginated.map((obj) => {
                const isSelected = selected.includes(obj.id);
                return (
                  <TableRow hover key={obj.id} selected={isSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isSelected}
                        onChange={(event) => handleSelectOne(event, obj.id)}
                        value={isSelected}
                      />
                    </TableCell>
                    <TableCell>{obj.picklist_id}</TableCell>
                    <TableCell>{obj.state}</TableCell>
                    <TableCell align="center">
                      <IconButton onClick={() => handlePrint(obj)}>
                        <SvgIcon fontSize="small">
                          <PrinterIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton onClick={() => handleComplete(obj)}>
                        <SvgIcon fontSize="small">
                          <CheckIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  packages: PropTypes.array
};

Results.defaultProps = {
  packages: []
};

export default Results;
