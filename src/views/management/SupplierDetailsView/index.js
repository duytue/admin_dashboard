import React, {
    // useState,
    // useCallback,
    // useEffect,
  } from 'react';
  import {
    Box,
    Container,
    makeStyles
  } from '@material-ui/core';
  import Page from 'src/components/Page';
  import Header from './Header';
  import SupplierDetailsForm from './SupplierDetailsForm';
  
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3)
    }
  }));
  
  function SupplierDetailsView() {
    const classes = useStyles();
  
    return (
      <Page
        className={classes.root}
        title="Supplier Details"
      >
        <Container maxWidth="lg">
          <Header />
          <Box mt={3}>
            <SupplierDetailsForm />
          </Box>
        </Container>
      </Page>
    );
  }
  
  export default SupplierDetailsView;
  