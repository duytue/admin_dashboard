import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
  makeStyles,
  FormHelperText
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useHistory } from "react-router-dom";
import { createLanguage } from 'src/actions/languageActions';
import { getListPlatform } from 'src/actions/platformActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectBirthDay: {
    width: "100%",
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  buttonArrowDropDown: {
    minWidth: 0,
  }
}));

function LanguageCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const { loadingCreateLanguage } = useSelector((state) => {
    return state.language
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  })

  useEffect(() => {
    dispatch(getListPlatform())
  }, [dispatch])

  return (
    <Formik
      initialValues={{
        key: '',
        name: '',
        description: '',
        platform_key: ''
      }}
      validationSchema={Yup.object().shape({
        key: Yup.string().max(255).required('Key is required'),
        name: Yup.string().max(255).required('Name is required'),
        platform_key: Yup.string().max(255)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(createLanguage(values)).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Language created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/languages"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>
                {/* Key */}
                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.key && errors.key)}
                      fullWidth
                      helperText={touched.key && errors.key}
                      label="Key"
                      name="key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.key}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Key */}
                  {/* Name */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name}
                      variant="outlined"
                    />
                  </Grid>
                  {/* End Name */}
                  {/* Description */}
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      helperText={touched.platform_key && errors.platform_key}
                      label="Platform Key"
                      name="platform_key"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.platform_key}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Language
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

LanguageCreateForm.propTypes = {
  className: PropTypes.string,
};

export default LanguageCreateForm;
