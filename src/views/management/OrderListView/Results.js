import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Box,
  Card,
  Tabs,
  Tab,
  Checkbox,
  Divider,
  InputAdornment,
  Link,
  TextField,
  IconButton,
  Button,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  makeStyles
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Search as SearchIcon
} from 'react-feather';
import { deleteOrder } from 'src/actions/orderActions';
// import Label from 'src/components/Label';
// import GenericMoreButton from 'src/components/GenericMoreButton';
// import BulkOperations from './BulkOperations';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const statusOptions = [
  {
    value: 'all',
    label: 'All'
  },
  {
    value: 'created',
    label: 'created'
  },
];

const dateOptions = [
  {
    value: 'today',
    label: 'Today'
  },
  {
    value: 'selectDate',
    label: 'Select Date'
  },
];

function applyFilters(orders, query, filters) {
  return orders.filter((order) => {
    let matches = true;

    if (query) {
      const properties = ['order_number', 'station_name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (order[property] && order[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && order[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(orders, page, limit) {
  return orders.slice(page * limit, page * limit + limit);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  datepicker: {
    marginLeft: '16px'
  },
}));

function Results({
  className, orders, ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedOrders, setSelectedOrders] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  // const [isDeletedItem, setIsDeletedItem] = useState(false);
  const [status, setStatus] = useState(statusOptions[0].value);
  const [dateFilter, setDateFilter] = useState(dateOptions[0].value);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedOrders([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleStatusChange = (event) => {
    event.persist();
    setStatus(event.target.value);
  };

  const handleDateFilterChange = (event) => {
    event.persist();
    setDateFilter(event.target.value);
  };

  const handleSelectAllOrders = (event) => {
    setSelectedOrders(
      event.target.checked ? orders.map((order) => order.id) : []
    );
  };

  const handleDeleteAllOrders = () => {
    // handle order as string not object
    selectedOrders.map((order) => dispatch(deleteOrder(order)));
  };

  const handleSelectOneOrder = (event, orderId) => {
    if (!selectedOrders.includes(orderId)) {
      setSelectedOrders((prevSelected) => [...prevSelected, orderId]);
    } else {
      setSelectedOrders((prevSelected) => prevSelected.filter((id) => id !== orderId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const filteredOrders = applyFilters(orders, query, filters);
  const paginatedOrders = applyPagination(filteredOrders, page, limit);
  const enableBulkOperations = selectedOrders.length > 0;
  const selectedSomeOrders = selectedOrders.length > 0 && selectedOrders.length < orders.length;
  const selectedAllUsers = selectedOrders.length === orders.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search orders"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Status"
          name="status"
          onChange={handleStatusChange}
          select
          SelectProps={{ native: true }}
          value={status}
          variant="outlined"
        >
          {statusOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
        <TextField
          className={classes.datepicker}
          label="Date"
          name="date"
          onChange={handleDateFilterChange}
          select
          SelectProps={{ native: true }}
          value={dateFilter}
          variant="outlined"
        >
          {dateOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredOrders.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeOrders}
              onChange={handleSelectAllOrders}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllOrders}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeOrders}
                    onChange={handleSelectAllOrders}
                  />
                </TableCell>
                <TableCell>Order Number</TableCell>
                <TableCell>Order Time</TableCell>
                <TableCell>Time slot</TableCell>
                <TableCell>Pickup point</TableCell>
                <TableCell>Grand total</TableCell>
                <TableCell>State</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedOrders.map((order) => {
                const isOrderSelected = selectedOrders.includes(order.id);

                return (
                  <TableRow hover key={order.id} selected={isOrderSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isOrderSelected}
                        onChange={(event) => handleSelectOneOrder(event, order.id)}
                        value={isOrderSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/management/orders/${order.id}`}
                        variant="h6"
                      >
                        {order.order_number}
                      </Link>
                    </TableCell>
                    <TableCell>
                      {moment(order.created_at).format(
                        'DD MMM YYYY | hh:mm'
                      )}
                    </TableCell>
                    <TableCell>
                      {moment(order.min_promise).format(
                        'DD MMM YYYY | hh:mm'
                      )}
                      <span> - </span>
                      {moment(order.max_promise).format(
                        'DD MMM YYYY | hh:mm'
                      )}
                    </TableCell>
                    <TableCell>{order.station_name}</TableCell>
                    <TableCell>{order.grand_total}</TableCell>
                    <TableCell>{order.state}</TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/order/${order.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>

                      <IconButton component={RouterLink} to="/app/management/order" onClick={() => { dispatch(deleteOrder(order)); }}>
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>

                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredOrders.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  orders: PropTypes.array
};

Results.defaultProps = {
  orders: []
};

export default Results;
