import React from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import Page from 'src/components/Page';
import CustomerFeeForm from './CustomerFeeForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerFeeFormView() {
  const classes = useStyles();
  const { feeId } = useParams();

  return (
    <Page
      className={classes.root}
      title={feeId ? 'Edit Customer Fee' : 'Create Customer Fee'}
    >
      <Container maxWidth="lg">
        <Header isEdit={feeId} />
        <Box mt={3}>
          <CustomerFeeForm />
        </Box>
      </Container>
    </Page>
  );
}

export default CustomerFeeFormView;
