/* eslint-disable max-len */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
// import moment from 'moment';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Button,
  Card,
  Tabs,
  Tab,
  CardContent,
  CardActions,
  Grid,
  TextField,
  makeStyles,
  MenuItem,
  Switch,
  Divider,
  SvgIcon,
  Typography,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  IconButton,
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import {
  DateTimePicker,
} from '@material-ui/pickers';
import { useHistory, useParams } from 'react-router-dom';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Save as SaveIcon,
  PlusCircle as PlusCircleIcon,
  // Download as DownloadIcon,
  // Upload as UploadIcon
} from 'react-feather';
import _ from 'lodash';
import {
  getCustomerFeeById,
  updateCustomerFee,
  addCustomerFee,
  clearCustomerFee
} from 'src/actions/customerFeeActions';
// import { getListStations } from 'src/actions/stationActions';

const typeList = [
  { value: 'fulfillment_surcharge', label: 'Fulfillment Surcharge' },
  { value: 'small_order_fee', label: 'Small Order Fee' },
  { value: 'cancellation_fee', label: 'Cancelation Fee' },
  { value: 'update_location_reschedule_fee', label: 'Update location & Reschedule fee' },
  { value: 'update_order_detail_fee', label: 'Update order detail fee' },
];

const tabs = [
  {
    value: 'general',
    label: 'General'
  },
  {
    value: 'conditions',
    label: 'Conditions'
  },
];

const useStyles = makeStyles((theme) => ({
  root: {},
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  btnSave: {
    margin: theme.spacing(1)
  },
  activeLabel: {
    marginRight: theme.spacing(2)
  },
}));

function CustomerFeeForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('general');
  const { enqueueSnackbar } = useSnackbar();
  const { feeId } = useParams();
  const dispatch = useDispatch();
  const { fee } = useSelector((state) => state.customer_fee);
  // const { stations } = useSelector((state) => state.station);
  const history = useHistory();

  const [curSurchFromValue, setCurSurchFromValue] = useState('');
  const [curSurchToValue, setCurSurchToValue] = useState('');
  const [curSurchFeeValue, setCurSurchFeeValue] = useState('');
  const [isSubmitSurch, setIsSubmitSurch] = useState(false);

  useEffect(() => {
    if (feeId) {
      dispatch(getCustomerFeeById(feeId));
    }
    // dispatch(getListStations());
  }, [dispatch]);

  useEffect(() => () => dispatch(clearCustomerFee()), []);

  if (feeId && _.size(fee) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        name: fee.name || '',
        is_active: fee.is_active || true,
        start_time: fee.start_time || null,
        end_time: fee.end_time || null,
        description: fee.description || '',
        type: fee.type || 'fulfillment_surcharge',
        value1: (fee.type === 'fulfillment_surcharge' && fee.value) || [],
        value2: (fee.type === 'small_order_fee' && fee.value) || [],
        value3: (fee.type === 'cancellation_fee' && fee.value) || [],
        value4: (fee.type === 'update_location_reschedule_fee' && fee.value) || [],
        value5: (fee.type === 'update_order_detail_fee' && fee.value) || [],
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required(),
        start_time: Yup.string().required(),
        end_time: Yup.string().required(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Format time
          const submitObj = { ...values };
          if (submitObj.type === 'fulfillment_surcharge') {
            submitObj.value = submitObj.value1;
          }
          if (submitObj.type === 'small_order_fee') {
            submitObj.value = submitObj.value2;
          }
          if (submitObj.type === 'cancellation_fee') {
            submitObj.value = submitObj.value3;
          }
          if (submitObj.type === 'update_location_reschedule_fee') {
            submitObj.value = submitObj.value4;
          }
          if (submitObj.type === 'update_order_detail_fee') {
            submitObj.value = submitObj.value5;
          }

          if (submitObj.value.length === 0) {
            throw new Error();
          }

          // Make API request
          dispatch(feeId
            ? updateCustomerFee({ ...submitObj, id: feeId }) : addCustomerFee(submitObj))
            .then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar(feeId ? 'Customer Fee updated' : 'Customer Fee created', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(('/app/management/customer-fee'));
            });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
          enqueueSnackbar(feeId ? 'Update failed, Please input conditions values' : 'Create Failed, Please input conditions values', {
            variant: 'error',
          });
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <Tabs
              onChange={(event, value) => setCurrentTab(value)}
              scrollButtons="auto"
              textColor="secondary"
              value={currentTab}
              variant="scrollable"
            >
              {tabs.map((tab) => (
                <Tab key={tab.value} value={tab.value} label={tab.label} />
              ))}
            </Tabs>
            <Divider />
            <CardContent>
              {currentTab === 'general' && (
              <Grid container spacing={3}>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={12}
                >
                  <Grid container spacing={3}>
                    <Grid
                      item
                      lg={12}
                    >
                      <TextField
                        error={Boolean(touched.name && errors.name)}
                        fullWidth
                        helperText={touched.name && errors.name}
                        label="Name"
                        name="name"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                        value={values.name}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                    >
                      <Typography className={classes.activeLabel}>Active</Typography>
                      <Switch
                        checked={values.is_active}
                        onChange={() => setFieldValue('is_active', !values.is_active)}
                        name="active"
                        inputProps={{ 'aria-label': 'checkbox' }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={12}
                >
                  <Grid
                    container
                    spacing={3}
                  >
                    <Grid
                      item
                      lg={6}
                      md={12}
                    >
                      <DateTimePicker
                        fullWidth
                        disableToolbar
                        inputVariant="outlined"
                        format="DD/MM/YYYY hh:mm A"
                        ampm={false}
                        label="Start"
                        value={values.start_time}
                        onChange={(value) => setFieldValue('start_time', value)}
                        KeyboardButtonProps={{
                          'aria-label': 'Start time',
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      md={12}
                    >
                      <DateTimePicker
                        fullWidth
                        disableToolbar
                        inputVariant="outlined"
                        format="DD/MM/YYYY hh:mm A"
                        ampm={false}
                        label="End"
                        value={values.end_time}
                        onChange={(value) => setFieldValue('end_time', value)}
                        KeyboardButtonProps={{
                          'aria-label': 'End time',
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                    >
                      <TextField
                        multiline
                        error={Boolean(touched.description && errors.description)}
                        fullWidth
                        helperText={touched.description && errors.description}
                        label="Description"
                        name="description"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.description}
                        variant="outlined"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              )}
              {currentTab === 'conditions' && (
                <Grid container spacing={3}>
                  <Grid item lg={12}>
                    <Typography>Conditions specify rules which must be met before a customer fee will be valid.</Typography>
                  </Grid>
                  <Grid item lg={12}>
                    <TextField
                      error={Boolean(touched.type && errors.type)}
                      fullWidth
                      select
                      helperText={touched.type && errors.type}
                      label="Type"
                      name="type"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.type}
                      variant="outlined"
                    >
                      {typeList.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item lg={12}>
                    {/* TYPE IS fulfillment_surcharge */}
                    {values.type === 'fulfillment_surcharge' && (
                      <Grid container spacing={3}>
                        <Grid item lg={3} md={3} sm={6} xs={12}>
                          <TextField
                            error={(Number.isNaN(Number(curSurchFromValue)) || curSurchFromValue === '') && isSubmitSurch}
                            helperText={isSubmitSurch
                              ? ((curSurchFromValue !== '' && Number.isNaN(Number(curSurchFromValue)) && 'From must be a number')
                                || (curSurchFromValue === '' && 'From is required'))
                              : false}
                            fullWidth
                            label="From"
                            name="from"
                            onChange={(value) => setCurSurchFromValue(value.target.value)}
                            value={curSurchFromValue}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item lg={3} md={3} sm={6} xs={12}>
                          <TextField
                            error={Number.isNaN(Number(curSurchToValue)) && isSubmitSurch}
                            helperText={isSubmitSurch && Number.isNaN(Number(curSurchToValue)) ? 'To must be a number' : false}
                            fullWidth
                            label="To"
                            name="to"
                            onChange={(value) => setCurSurchToValue(value.target.value)}
                            value={curSurchToValue}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item lg={3} md={3} sm={6} xs={12}>
                          <TextField
                            error={Number.isNaN(Number(curSurchFeeValue)) && isSubmitSurch}
                            helperText={isSubmitSurch && Number.isNaN(Number(curSurchFeeValue)) ? 'Fee must be a number' : false}
                            fullWidth
                            label="Fee"
                            name="fee"
                            onChange={(value) => setCurSurchFeeValue(value.target.value)}
                            value={curSurchFeeValue}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid container item lg={3} md={3} sm={6} xs={12} direction="row" justify="center" alignItems="center">
                          <Button
                            color="secondary"
                            variant="contained"
                            onClick={() => {
                              setIsSubmitSurch(true);
                              if (
                                curSurchFromValue !== ''
                                && !Number.isNaN(Number(curSurchFromValue))
                                && !Number.isNaN(Number(curSurchToValue))
                                && !Number.isNaN(Number(curSurchFeeValue))
                              ) {
                                setFieldValue('value1', [...values.value1, {
                                  from: Number(curSurchFromValue),
                                  to: Number(curSurchToValue),
                                  fee: Number(curSurchFeeValue),
                                }]);
                                setCurSurchFromValue('');
                                setCurSurchToValue('');
                                setCurSurchFeeValue('');
                                setIsSubmitSurch(false);
                              }
                            }}
                          >
                            <SvgIcon
                              fontSize="small"
                              className={classes.actionIcon}
                            >
                              <PlusCircleIcon />
                            </SvgIcon>
                            Add
                          </Button>
                        </Grid>
                        <Grid item lg={12}>
                          <Table>
                            <TableHead>
                              <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>From(minutes)</TableCell>
                                <TableCell>To(minutes)</TableCell>
                                <TableCell>Fee(VND)</TableCell>
                                <TableCell align="right">Actions</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {values.value1.map((val, i) => (
                                <TableRow>
                                  <TableCell>{i + 1}</TableCell>
                                  <TableCell>{val.from}</TableCell>
                                  <TableCell>{val.to || '-'}</TableCell>
                                  <TableCell>{val.fee || '-'}</TableCell>
                                  <TableCell align="right">
                                    <IconButton
                                      onClick={() => {
                                        const copyArr = [...values.value1];
                                        copyArr.splice(i, 1);
                                        setFieldValue('value1', copyArr);
                                      }}
                                    >
                                      <SvgIcon fontSize="small">
                                        <DeleteOutlineIcon />
                                      </SvgIcon>
                                    </IconButton>
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </Grid>
                      </Grid>
                    )}
                    {/* TYPE IS SMALL ORDER FEE */}
                    {values.type === 'small_order_fee' && (
                      <Grid container spacing={3}>
                        <Grid item lg={6} sm={12}>
                          <TextField
                            error={Boolean(touched.order_fee && errors.order_fee)}
                            fullWidth
                            helperText={touched.order_fee && errors.order_fee}
                            label="Fee"
                            name="fee"
                            onBlur={handleBlur}
                            onChange={(value) => {
                              if (!Number.isNaN(Number(value.target.value))) {
                                const newVal = values.value2.length === 0
                                  ? [{ fee: Number(value.target.value) }]
                                  : [{ ...values.value2[0], fee: Number(value.target.value) }];
                                setFieldValue('value2', newVal);
                              }
                            }}
                            required={values.type === 'small_order_fee'}
                            value={values.value2[0] && values.value2[0].fee}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item lg={6} sm={12}>
                          <TextField
                            error={Boolean(touched.order_minimum_value && errors.order_minimum_value)}
                            fullWidth
                            helperText={touched.order_minimum_value && errors.order_minimum_value}
                            label="Minimum order value"
                            name="minimum_order_value"
                            onBlur={handleBlur}
                            onChange={(value) => {
                              if (!Number.isNaN(Number(value.target.value))) {
                                const newVal = values.value2.length === 0
                                  ? [{ minimum_order_value: Number(value.target.value) }]
                                  : [{ ...values.value2[0], minimum_order_value: Number(value.target.value) }];
                                setFieldValue('value2', newVal);
                              }
                            }}
                            required={values.type === 'small_order_fee'}
                            value={values.value2[0] && values.value2[0].minimum_order_value}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                    )}
                    {/* TYPE CANCELATION FEE */}
                    {values.type === 'cancellation_fee' && (
                      <Grid container spacing={3}>
                        <Grid item lg={6} sm={12}>
                          <TextField
                            error={Boolean(touched.order_fee && errors.order_fee)}
                            fullWidth
                            helperText={touched.order_fee && errors.order_fee}
                            label="Fee"
                            name="fee"
                            onBlur={handleBlur}
                            onChange={(value) => {
                              if (!Number.isNaN(Number(value.target.value))) {
                                const newVal = values.value3.length === 0
                                  ? [{ fee: Number(value.target.value) }]
                                  : [{ ...values.value3[0], fee: Number(value.target.value) }];
                                setFieldValue('value3', newVal);
                              }
                            }}
                            required={values.type === 'cancellation_fee'}
                            value={values.value3[0] && values.value3[0].fee}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                    )}
                    {/* TYPE LOCATION RESCHEDULE FEE */}
                    {values.type === 'update_location_reschedule_fee' && (
                      <Grid container spacing={3}>
                        <Grid item lg={6} sm={12}>
                          <TextField
                            error={Boolean(touched.order_fee && errors.order_fee)}
                            fullWidth
                            helperText={touched.order_fee && errors.order_fee}
                            label="Fee"
                            name="fee"
                            onBlur={handleBlur}
                            onChange={(value) => {
                              if (!Number.isNaN(Number(value.target.value))) {
                                const newVal = values.value4.length === 0
                                  ? [{ fee: Number(value.target.value) }]
                                  : [{ ...values.value4[0], fee: Number(value.target.value) }];
                                setFieldValue('value4', newVal);
                              }
                            }}
                            required={values.type === 'update_location_reschedule_fee'}
                            value={values.value4[0] && values.value4[0].fee}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                    )}
                    {/* TYPE UPDATE ORDER DETAIL FEE */}
                    {values.type === 'update_order_detail_fee' && (
                      <Grid container spacing={3}>
                        <Grid item lg={6} sm={12}>
                          <TextField
                            error={Boolean(touched.order_fee && errors.order_fee)}
                            fullWidth
                            helperText={touched.order_fee && errors.order_fee}
                            label="Fee"
                            name="fee"
                            onBlur={handleBlur}
                            onChange={(value) => {
                              if (!Number.isNaN(Number(value.target.value))) {
                                const newVal = values.value5.length === 0
                                  ? [{ fee: Number(value.target.value) }]
                                  : [{ ...values.value5[0], fee: Number(value.target.value) }];
                                setFieldValue('value5', newVal);
                              }
                            }}
                            required={values.type === 'update_order_detail_fee'}
                            value={values.value5[0] && values.value5[0].fee}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              )}
            </CardContent>
            <Divider />
            <CardActions>
              <Button
                variant="contained"
                color="secondary"
                type="submit"
                disabled={isSubmitting}
                className={classes.btnSave}
              >
                <SvgIcon fontSize="small" className={classes.actionIcon}>
                  <SaveIcon />
                </SvgIcon>
                Save
              </Button>
            </CardActions>
          </Card>
        </form>
      )}
    </Formik>
  );
}

CustomerFeeForm.propTypes = {
  className: PropTypes.string,
};

export default CustomerFeeForm;
