import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import _ from 'lodash';
import { getOrder, updateOrder } from 'src/actions/orderActions';

const useStyles = makeStyles(() => ({
  root: {}
}));

function OrderEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { orderId } = useParams();
  const dispatch = useDispatch();
  const { order } = useSelector((state) => state.order);
  const history = useHistory();

  useEffect(() => {
    dispatch(getOrder(orderId));
  }, [dispatch]);


  if (_.size(order) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        order: order.id || '',
        min_promise: moment(order.min_promise).format('ddd, MMM DD, YYYY hh:mm A') || '',
        max_promise: moment(order.max_promise).format('ddd, MMM DD, YYYY hh:mm A') || '',
        customer_pickup_station_id: order.customer_pickup_station_id || '',
        state: order.state || '',
      }}
      validationSchema={Yup.object().shape({
        order: Yup.string().max(255)
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(updateOrder({ ...values, id: orderId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Order updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(('/app/management/orders'));
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={12}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.min_promise && errors.min_promise)}
                    fullWidth
                    helperText={touched.min_promise && errors.min_promise}
                    label="Pick up min time"
                    name="min_promise"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.min_promise}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={12}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.max_promise && errors.max_promise)}
                    fullWidth
                    helperText={touched.max_promise && errors.max_promise}
                    label="Pick up max time"
                    name="max_promise"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.max_promise}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={12}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.customer_pickup_station_id
                      && errors.customer_pickup_station_id)}
                    fullWidth
                    helperText={touched.customer_pickup_station_id
                      && errors.customer_pickup_station_id}
                    label="Station"
                    name="customer_pickup_station_id"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.customer_pickup_station_id}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={12}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.state && errors.state)}
                    fullWidth
                    helperText={touched.state && errors.state}
                    label="State"
                    name="state"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.state}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Update order
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

OrderEditForm.propTypes = {
  className: PropTypes.string
};

export default OrderEditForm;
