import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  TextField,
  Typography,
  Select,
  Switch,
  makeStyles
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import { getLanguage, updateLanguage } from '../../../actions/languageActions';
const useStyles = makeStyles(() => ({
  root: {
    '.MuiSelect-icon': {
      display: 'none'
    }
  },
  selectBirthDay: {
    width: '100%',
    marginTop: 0
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  cardPermission: {
    marginTop: '16px'
  },
  buttonArrowDropDown: {
    minWidth: 0
  }
}));

function LanguageEditForm({ className, ...rest }) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  // TODO
  const languageId = _.trim(window.location.pathname.slice(26, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const [openPlatformSelect, setOpenPlatformSelect] = useState(false);
  const {
    language
  } = useSelector(state => {
    return state.language;
  });


  useEffect(() => {
    dispatch(getLanguage(languageId));
  }, [dispatch, languageId]);

  if (_.size(language) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        key: language.key || '',
        name: language.name || '',
        description: language.description || '',
        platform_key: language.platform_key || ''
      }}
      validationSchema={Yup.object().shape({
        key: Yup.string().max(255).required('Key is required'),
        name: Yup.string().max(255).required('Name is required'),
        platform_key: Yup.string().max(255)
      })}
      onSubmit={async (
        values,
        { resetForm, setErrors, setStatus, setSubmitting }
      ) => {
        try {
          // Make API request
          console.log(values, 'values')
          dispatch(updateLanguage({ ...values, id: languageId })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Language updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push('/app/management/languages');
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
          isSubmitting,
          touched,
          values
        }) => (
        <form
          className={clsx(classes.root, className)}
          onSubmit={handleSubmit}
          {...rest}
        >
          <Card>
            <CardContent>
              {/* Key */}
              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.key && errors.key)}
                    fullWidth
                    helperText={touched.key && errors.key}
                    label="Key"
                    name="key"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.key}
                    variant="outlined"
                  />
                </Grid>
                {/* End Key */}
                {/* Name */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.name && errors.name)}
                    fullWidth
                    helperText={touched.name && errors.name}
                    label="Name"
                    name="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.name}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.description && errors.description)}
                    fullWidth
                    helperText={touched.description && errors.description}
                    label="Description"
                    name="description"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    multiline
                    rows={4}
                    value={values.description}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <TextField
                    error={Boolean(touched.platform_key && errors.platform_key)}
                    fullWidth
                    helperText={touched.platform_key && errors.platform_key}
                    label="Platform Key"
                    name="platform_key"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    required
                    value={values.platform_key}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Edit Language
                </Button>
              </Box>
            </CardContent>
          </Card>
        </form>
      )}
    </Formik>
  );
}

LanguageEditForm.propTypes = {
  className: PropTypes.string
};

export default LanguageEditForm;
