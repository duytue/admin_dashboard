/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  TextField,
  makeStyles,
  Button,
  Checkbox,
  Box,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  IconButton,
  SvgIcon,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import moment from 'moment';
import {
  getList, deletePicklist, checkConsolidation, updateByTradeOrderNumber, clearConsolidation
} from 'src/actions/picklistActions';
import { getListStations } from 'src/actions/stationActions';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
// import printer from './printer';
import ScreenShareIcon from '@material-ui/icons/ScreenShare';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { STATES } from 'src/constants';
import QrReader from 'react-qr-reader';
import PerfectScrollbar from 'react-perfect-scrollbar';
import _ from 'lodash';

// const tabs = [
//   {
//     value: 'all',
//     label: 'All'
//   }
// ];

function applyFilters(picklists, query, filters) {
  return picklists.filter((picklist) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (picklist[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && picklist[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(picklists, page, limit) {
  return picklists.slice(page * limit, page * limit + limit);
}

// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }

//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }

//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// function applySort(picklists, sort) {
//   const [orderBy, order] = sort.split('|');
//   const comparator = getComparator(order, orderBy);
//   const stabilizedThis = picklists.map((el, index) => [el, index]);

//   stabilizedThis.sort((a, b) => {
//     // eslint-disable-next-line no-shadow
//     const order = comparator(a[0], b[0]);

//     if (order !== 0) return order;

//     return a[1] - b[1];
//   });

//   return stabilizedThis.map((el) => el[0]);
// }

const useStyles = makeStyles(() => ({
  root: {
    padding: '30px'
  },
  marginLeft: {
    marginLeft: '75px'
  },
  qrScanner: {
    width: '500px'
  },
  bigFontSize: {
    fontSize: '3em'
  },
  dFlex: {
    display: 'flex'
  },
  flexColumn: {
    flexDirection: 'column'
  },
  searchContainer: {
    height: '56px'
  },
  alignCenter: {
    alignItems: 'center'
  },
  fullHeight: {
    height: '100%'
  },
  searchButtons: {
    border: '1px solid lightgrey'
  },
  hide: {
    display: 'none'
  },
  show: {
    display: 'block'
  },
  dialogContainer: {
    padding: '20px 40px'
  },
  flexText: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '15px'
  },
  smallFlex: {
    display: 'flex',
    flexDirection: 'column'
  },
  smallFlexInvi: {
    display: 'flex',
    flexDirection: 'column',
    color: 'transparent'
  },
  smallMarginBottom: {
    marginBottom: '15px'
  },
  bigMarginBottom: {
    marginBottom: '25px'
  },
  smallTitle: {
    fontWeight: 'bold',
    fontSize: '1.3em'
  },
  pictureFrame: {
    width: '250px',
    height: '150px'
  },
  smallPictureFrame: {
    width: '180px',
    height: '120px'
  },
  justifyCenter: {
    justifyContent: 'center'
  },
  imageStyle: {
    width: '100%',
    height: '100%'
  },
  bigIcon: {
    justifyContent: 'center',
    fontSize: '8em'
  },
  displayFlex: {
    display: 'flex'
  },
  textFieldWidth: {
    width: '30vw'
  },
  bigMarginRight: {
    marginRight: '5vw'
  },
  redBackground: {
    backgroundColor: '#F44336'
  },
  blurr: {
    opacity: 0.3
  },
  noBlurr: {
    opacity: 1
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, ...rest }) {
  const textFieldRef = React.useRef();
  const classes = useStyles();
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [scannerIsVisible, setScannerIsVisible] = useState(false);
  const dispatch = useDispatch();
  const { picklists, consolidation } = useSelector((state) => state.picklist);
  const { stations } = useSelector((state) => state.station);
  const [confirmDialogVisible, setConfirmDialogVisible] = useState(false);
  const [scannedList, setScannedList] = useState([]);
  const filters = {
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null,
  };
  const { enqueueSnackbar } = useSnackbar();

  const handleSelectAll = (event) => {
    setSelected(event.target.checked ? picklists.map((obj) => obj.id) : []);
  };

  const handleDeleteAll = () => {
    if (window.confirm('Are you sure delete the item?')) {
      selected.map((obj) => dispatch(deletePicklist(obj)));
    }
  };

  const handleSelectOne = (event, objId) => {
    if (!selected.includes(objId)) {
      setSelected((prevSelected) => [...prevSelected, objId]);
    } else {
      setSelected((prevSelected) => prevSelected.filter((id) => id !== objId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleScanCart = (enteredPicklistNumber) => {
    if (enteredPicklistNumber.length == 0) {
      enqueueSnackbar("Please enter valid information!", { variant: 'error' });
      return;
    }

    // If consolidation is empty
    if (_.isEmpty(consolidation)) {
      // Load consolidation
      dispatch(checkConsolidation(enteredPicklistNumber, [STATES.NEED_TO_TRANSIT_FC, STATES.NEED_TO_CONSOLIDATE, STATES.READY_TO_CONSOLIDATE]))
        .then(() => {
        })
        .catch((error) => {
          enqueueSnackbar(error.response.data.message, { variant: 'error' });
        });
      // Add scanned picklist to scannedList
      setScannedList([enteredPicklistNumber]);
    } else {
      // If consolidation is not empty, check if scanned picklist
      // belongs to this consolidation or not.
      let isSameConsolidationList = false;
      for (let i = 0; i < consolidation.pick_lists.length; i++) {
        if (consolidation.pick_lists[i].picklist_number == enteredPicklistNumber) {
          isSameConsolidationList = true;
          break;
        }
      }
      // If scanned picklist belongs to a different consolidation
      if (!isSameConsolidationList) {
        // Reload consolidation
        dispatch(checkConsolidation(enteredPicklistNumber, [STATES.NEED_TO_TRANSIT_FC, STATES.NEED_TO_CONSOLIDATE, STATES.READY_TO_CONSOLIDATE]))
          .then(() => {
          })
          .catch((error) => {
            enqueueSnackbar(error.response.data.message, { variant: 'error' });
          });
        // Reload scannedList and add scanned picklist
        setScannedList([enteredPicklistNumber]);
      } else {
        // If scanned picklist belongs to current consolidation
        // Add scanned picklist to scannedList
        setScannedList([...scannedList, enteredPicklistNumber]);
      }
    }
  };

  const callConsolidate = async (tradeOrderNumber) => {
    //updateByTradeOrderNumber
    let data = { state: STATES.CONSOLIDATED };
    await dispatch(updateByTradeOrderNumber(tradeOrderNumber, data));
    enqueueSnackbar("Consolidate successfully!", { variant: 'success' });
    setScannedList([]);

  };

  const handleConsolidate = () => {
    // Check if all picklists in consolidation are in scannedList
    let isScannedAll = true;
    for (let i = 0; i < consolidation.pick_lists.length; i++) {
      let currentPicklistIsScanned = false;
      for (let e = 0; e < scannedList.length; e++) {
        if (consolidation.pick_lists[i].picklist_number == scannedList[e]) {
          currentPicklistIsScanned = true;
        }
      }
      if (!currentPicklistIsScanned) {
        isScannedAll = false;
      }
    }
    // If consolidation are not all scanned, show popup
    if (!isScannedAll) {
      setConfirmDialogVisible(true);
    } else {
      callConsolidate(consolidation.pick_lists[0].trade_order_number);
    }
  };

  const handleCancelConsolidate = () => {
    setScannedList([]);
    dispatch(clearConsolidation());
  }

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(picklists, '', filters);
  const paginated = applyPagination(filtered, page, limit);
  // const [paginated, setPaginated] = useState([]);
  const enableBulkOperations = selected.length > 0;
  const selectedSome = selected.length > 0 && selected.length < picklists.length;
  const selectedAll = selected.length === picklists.length;

  useEffect(() => {
    dispatch(getListStations());
    dispatch(getList([STATES.NEED_TO_TRANSIT_FC, STATES.NEED_TO_CONSOLIDATE, STATES.READY_TO_CONSOLIDATE], ''));
  }, [dispatch]);

  useEffect(() => {

  }, [consolidation]);

  // const [qrContent, setQrContent] = useState(null);

  const qrScannerErrorHandler = (err) => {
    throw err;
  };

  const qrScannerScanHandler = (qrContent) => {
    // if (data == 'http://zh.wikipedia.org/') {
    if (qrContent != null) {
      handleScanCart(qrContent);
    }
  };

  const toggleScanner = () => {
    setScannerIsVisible(!scannerIsVisible);
  };

  const handleSearch = () => {
    handleScanCart(textFieldRef.current.value);
  };

  return (
    <>
      <Card className={clsx(classes.root, className)} {...rest}>
        <div
          className={`${classes.dFlex} ${classes.alignCenter} ${classes.searchContainer} ${classes.bigMarginBottom}`}
        >
          <TextField
            className={`${classes.fullHeight} ${classes.textFieldWidth}`}
            label="Scan or enter picklist number here"
            variant="outlined"
            inputRef={textFieldRef}
          />
          <Button
            className={`${classes.fullHeight} ${classes.searchButtons}`}
            onClick={toggleScanner}
          >
            <ScreenShareIcon className={classes.bigFontSize} />
          </Button>
          <Button
            className={`${classes.fullHeight} ${classes.searchButtons}`}
            onClick={handleSearch}
          >
            Search
          </Button>
        </div>

        {/* ////////////////////////////////// */}
        <TablePagination
          className={classes.header_pagination}
          component="div"
          count={filtered.length}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10, 25]}
        />
        {enableBulkOperations && (
          <div className={classes.bulkOperations}>
            <div className={classes.bulkActions}>
              <Checkbox
                checked={selectedAll}
                indeterminate={selectedSome}
                onChange={handleSelectAll}
              />
              <Button
                variant="outlined"
                className={classes.bulkAction}
                onClick={handleDeleteAll}
              >
                Delete
              </Button>
            </div>
          </div>
        )}
        <PerfectScrollbar>
          <Box minWidth={700}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedAll}
                      indeterminate={selectedSome}
                      onChange={handleSelectAll}
                    />
                  </TableCell>
                  <TableCell>Picklist ID</TableCell>
                  <TableCell>Created at</TableCell>
                  <TableCell>State</TableCell>
                  <TableCell align="center">Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {paginated.map((obj) => {
                  const isSelected = selected.includes(obj.id);
                  return (
                    <TableRow hover key={obj.id} selected={isSelected}>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isSelected}
                          onChange={(event) => handleSelectOne(event, obj.id)}
                          value={isSelected}
                        />
                      </TableCell>
                      <TableCell>{obj.picklist_number}</TableCell>
                      <TableCell>
                        {moment(obj.created_at).format(
                          'DD MMM YYYY | hh:mm'
                        )}
                      </TableCell>
                      <TableCell>{obj.state}</TableCell>
                      <TableCell align="right">
                        <IconButton
                          component={RouterLink}
                          onClick={() => {
                            dispatch(deletePicklist(obj));
                          }}
                        >
                          <SvgIcon fontSize="small">
                            <DeleteOutlineIcon />
                          </SvgIcon>
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </PerfectScrollbar>
        <TablePagination
          component="div"
          count={filtered.length}
          onChangePage={handlePageChange}
          onChangeRowsPerPage={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10, 25]}
        />
        {/* ////////////////////////////////// */}

        {scannerIsVisible && (
          <QrReader
            delay={1000}
            onError={qrScannerErrorHandler}
            onScan={qrScannerScanHandler}
            className={`${classes.qrScanner} ${classes.bigMarginBottom}`}
          />
        )}
        {('pick_lists' in consolidation) && (
          <div className={`${classes.dFlex}`}>
            {consolidation.pick_lists.map((item, index) => {
              const station = stations.find((st) => st.id === item.station_final);
              return (
                <div
                  key={item.id}
                  className={`${classes.dFlex} ${classes.flexColumn} ${classes.bigMarginRight}
                    ${scannedList.includes(item.picklist_number) ? classes.noBlurr : classes.blurr}`}
                >
                  <Typography variant="h2" className={`${classes.bigMarginBottom}`}>
                    {item.cart_number || '\u00a0'}
                  </Typography>
                  <div
                    className={`${classes.bigMarginBottom} ${classes.pictureFrame}`}
                  >
                    <img
                      className={`${classes.imageStyle}`}
                      alt="barcode"
                      src="https://p.kindpng.com/picc/s/77-771409_barcode-png-images-transparent-background-barcode-png-png.png"
                    />
                  </div>
                  <Typography className={`${classes.smallMarginBottom}`}>
                    {`Mã đơn hàng: ${item.trade_order_number}`}
                  </Typography>
                  <Typography className={`${classes.smallMarginBottom}`}>
                    {`Danh sách pick hàng: ${item.picklist_number}`}
                  </Typography>
                  <Typography className={`${classes.smallMarginBottom}`}>
                    {`Số thứ tự: ${index + 1}/${consolidation.pick_lists.length}`}
                  </Typography>
                  <Typography>
                    {`Gộp kiện tại: ${(station && station.name) || ''}`}
                  </Typography>
                </div>
              );
            })}
            <div className={`${classes.dFlex} ${classes.flexColumn}`}>
              <Button
                variant="contained"
                color="primary"
                className={`${classes.smallMarginBottom}`}
                onClick={handleConsolidate}
              >
                Confirm
              </Button>
              <Button
                variant="contained"
                className={classes.redBackground}
                onClick={handleCancelConsolidate}
              >
                Cancel
              </Button>
            </div>
          </div>
        )}
      </Card>
      <Dialog
        open={confirmDialogVisible}
        // onClose={callConsolidate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Save Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            All items are not fully scanned? Do you still want to save?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => { setConfirmDialogVisible(false); }} color="primary">
            No
          </Button>
          <Button onClick={() => { callConsolidate(consolidation.pick_lists[0].trade_order_number); }} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

Results.propTypes = {
  className: PropTypes.string
};

export default Results;
