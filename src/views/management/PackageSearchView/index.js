import React from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
// import useIsMountedRef from 'src/hooks/useIsMountedRef';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PicklistSearchView() {
  const classes = useStyles();
  // const isMountedRef = useIsMountedRef();
  // const [packages, setPackages] = useState([
  //   {
  //     id: 'id_6',
  //     picklist_id: 'picklist id 6',
  //     order_number: 'order number 6',
  //     pickup_at: 'station name 6',
  //     bags_count: 1,
  //     refund_amount: 0,
  //     charged_amount: '30.000',
  //     state: 'Awaiting consolidation',
  //     is_printed: false
  //   },
  //   {
  //     id: 'id_7',
  //     picklist_id: 'picklist id 7',
  //     order_number: 'order number 7',
  //     pickup_at: 'station name 7',
  //     bags_count: 1,
  //     refund_amount: 0,
  //     charged_amount: '40.000',
  //     state: 'Awaiting consolidation',
  //     is_printed: false
  //   },
  //   {
  //     id: 'id_8',
  //     picklist_id: 'picklist id 8',
  //     order_number: 'order number 8',
  //     pickup_at: 'station name 8',
  //     bags_count: 1,
  //     refund_amount: '10.000',
  //     charged_amount: 0,
  //     state: 'Awaiting consolidation',
  //     is_printed: false
  //   }
  // ]);

  // if (!packages) {
  //   return null;
  // }

  return (
    <Page className={classes.root} title="Picklist List">
      <Container maxWidth={false}>
        <Header />
        {/* {packages && ( */}
        <Box mt={3}>
          <Results />
        </Box>
        {/* )} */}
      </Container>
    </Page>
  );
}

export default PicklistSearchView;
