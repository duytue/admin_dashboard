import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getListCapacities } from 'src/actions/capacityTimeSlotActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { capacities } = useSelector((state) => state.capacity_timeslot);

  useEffect(() => {
    dispatch(getListCapacities());
  }, [dispatch]);

  if (!capacities) {
    return null;
  }

  return (
    <Page className={classes.root} title="Capacity & Time-slot Management">
      <Container maxWidth={false}>
        <Header />
        {capacities && (
          <Box mt={3}>
            <Results capacities={capacities} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default StationListView;
