/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Switch,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Edit as EditIcon,
  // ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
// import Axios from 'axios';
import { useDispatch } from 'react-redux';
import { deleteCapacity, updateCapacity } from 'src/actions/capacityTimeSlotActions';
// import { API_BASE_URL_MSSTATION } from 'src/config';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  }
];

function applyFilters(capacities, query, filters) {
  return capacities.filter((capacity) => {
    let matches = true;

    if (query) {
      const properties = ['station_name', 'payment_method'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          capacity[property] && capacity[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && capacity[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(capacities, page, limit) {
  return capacities.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(capacities, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = capacities.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  mL16: {
    marginLeft: '16px'
  }
}));

function Results({ className, capacities, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedCapacities, setSelectedCapacities] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  // const [isDeletedItem, setIsDeletedItem] = useState(false);
  const [sort, setSort] = useState(sortOptions[0].value);
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const toggleActiveCapacity = (capacity) => {
    dispatch(updateCapacity({ ...capacity, active: !capacity.active }));
  };

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedCapacities([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllCapacities = (event) => {
    setSelectedCapacities(
      event.target.checked ? capacities.map((capacity) => capacity.id) : []
    );
  };

  const handleDeleteAllCapacity = () => {
    // handle capacity as string not object
    selectedCapacities.map((capacity) => dispatch(deleteCapacity(capacity)));
  };

  const handleSelectOneCapacity = (event, capacityId) => {
    if (!selectedCapacities.includes(capacityId)) {
      setSelectedCapacities((prevSelected) => [...prevSelected, capacityId]);
    } else {
      setSelectedCapacities((prevSelected) => prevSelected.filter((id) => id !== capacityId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const filteredCapacities = applyFilters(capacities, query, filters);
  const sortedCapacities = applySort(filteredCapacities, sort);
  const paginatedCapacities = applyPagination(sortedCapacities, page, limit);
  const enableBulkOperations = selectedCapacities.length > 0;
  const selectedSomeCapacities = selectedCapacities.length > 0 && selectedCapacities.length < capacities.length;
  const selectedAllUsers = selectedCapacities.length === capacities.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search capacities"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          className={classes.mL16}
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredCapacities.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeCapacities}
              onChange={handleSelectAllCapacities}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllCapacity}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeCapacities}
                    onChange={handleSelectAllCapacities}
                  />
                </TableCell>
                <TableCell>Position</TableCell>
                <TableCell>Station</TableCell>
                <TableCell>Payment Method</TableCell>
                <TableCell>From</TableCell>
                <TableCell>To</TableCell>
                <TableCell>Active</TableCell>
                <TableCell>Capacity</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedCapacities.map((capacity) => {
                const isCapacitySelected = selectedCapacities.includes(capacity.id);

                return (
                  <TableRow hover key={capacity.id} selected={isCapacitySelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isCapacitySelected}
                        onChange={(event) => handleSelectOneCapacity(event, capacity.id)}
                        value={isCapacitySelected}
                      />
                    </TableCell>
                    <TableCell>{capacity.position}</TableCell>
                    <TableCell>{capacity.station_name}</TableCell>
                    <TableCell>{capacity.payment_method}</TableCell>
                    <TableCell>{capacity.from}</TableCell>
                    <TableCell>{capacity.to}</TableCell>
                    <TableCell>
                      <Switch
                        checked={capacity.active}
                        onChange={() => toggleActiveCapacity(capacity)}
                        name="capacity_active"
                        inputProps={{ 'aria-label': 'checkbox' }}
                      />
                    </TableCell>
                    <TableCell>{capacity.capacity}</TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/time-slot-capacity/edit/${capacity.id}`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>

                      <IconButton
                        component={RouterLink}
                        onClick={() => {
                          dispatch(deleteCapacity(capacity));
                        }}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredCapacities.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  capacities: PropTypes.array
};

Results.defaultProps = {
  capacities: []
};

export default Results;
