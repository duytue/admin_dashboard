/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import moment from 'moment';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Switch,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import { Edit as EditIcon, Search as SearchIcon } from 'react-feather';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCustomerFee } from 'src/actions/customerFeeActions';
import { getUsers } from 'src/actions/userActions';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

// const sortOptions = [
//   {
//     value: 'updated_at|desc',
//     label: 'Last update (newest first)'
//   },
//   {
//     value: 'updated_at|asc',
//     label: 'Last update (oldest first)'
//   },
//   {
//     value: 'orders|desc',
//     label: 'Total orders (high to low)'
//   },
//   {
//     value: 'orders|asc',
//     label: 'Total orders (low to high)'
//   }
// ];

function applyFilters(customerFees, query, filters, dates) {
  return customerFees.filter((fee) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          fee[property] &&
          fee[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    if (dates.filterStartDate && !dates.filterEndDate) {
      if (
        moment(fee.start_time).format('DD/MM/YYY') >=
        moment(dates.filterStartDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (!dates.filterStartDate && dates.filterEndDate) {
      if (
        moment(fee.end_time).format('DD/MM/YYY') <=
        moment(dates.filterEndDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (dates.filterStartDate && dates.filterEndDate) {
      if (
        moment(fee.start_time).format('DD/MM/YYY') >=
          moment(dates.filterStartDate).format('DD/MM/YYY') &&
        moment(fee.end_time).format('DD/MM/YYY') <=
          moment(dates.filterEndDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && fee[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(customerFees, page, limit) {
  return customerFees.slice(page * limit, page * limit + limit);
}

// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }

//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }

//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// function applySort(customerFees, sort) {
//   const [orderBy, order] = sort.split('|');
//   const comparator = getComparator(order, orderBy);
//   const stabilizedThis = customerFees.map((el, index) => [el, index]);

//   stabilizedThis.sort((a, b) => {
//     // eslint-disable-next-line no-shadow
//     const order = comparator(a[0], b[0]);

//     if (order !== 0) return order;

//     return a[1] - b[1];
//   });

//   return stabilizedThis.map((el) => el[0]);
// }

const useStyles = makeStyles((theme) => ({
  root: {
    height: '79vh',
    position: 'relative'
  },
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  datePicker: {
    marginLeft: theme.spacing(2)
  },
  redColor: {
    color: 'red',
    fontWeight: 'bold'
  },
  greenColor: {
    color: 'green',
    fontWeight: 'bold'
  },
  inProgressBanner: {
    width: '70%',
    height: '70%',
    position: 'absolute',
    top: '53%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  }
}));

function Results({ className, customerFees, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.user);

  const [filterStartDate, setFilterStartDate] = useState(null);
  const [filterEndDate, setFilterEndDate] = useState(null);
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedFees, setselectedFees] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setselectedFees([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSelectAllFees = (event) => {
    setselectedFees(
      event.target.checked ? customerFees.map((fee) => fee.id) : []
    );
  };

  const handleDeleteAllFee = () => {
    selectedFees.map((fee) => dispatch(deleteCustomerFee(fee)));
  };

  const handleSelectOneFee = (event, feeId) => {
    if (!selectedFees.includes(feeId)) {
      setselectedFees((prevSelected) => [...prevSelected, feeId]);
    } else {
      setselectedFees((prevSelected) =>
        prevSelected.filter((id) => id !== feeId)
      );
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const filteredFees = applyFilters(customerFees, query, filters, {
    filterStartDate,
    filterEndDate
  });
  // const sortedFees = applySort(filteredFees, sort);
  const paginatedFees = applyPagination(filteredFees, page, limit);
  const enableBulkOperations = selectedFees.length > 0;
  const selectedSomeFees =
    selectedFees.length > 0 && selectedFees.length < customerFees.length;
  const selectedAllUsers = selectedFees.length === customerFees.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <img
        className={classes.inProgressBanner}
        src="/static/images/covers/work_in_progress.png"
      ></img>
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  customerFees: PropTypes.array
};

Results.defaultProps = {
  customerFees: []
};

export default Results;
