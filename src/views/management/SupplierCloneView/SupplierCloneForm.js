import React, {useEffect} from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { getOneSupplier, createSupplier } from 'src/actions/supplierActions';
import _ from 'lodash';


const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function SupplierCloneForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const supplierId = _.trim(window.location.pathname.slice(25, -5), '/');
  const { list_platform } = useSelector((state) => {
    return state.account.user;
  });

  const { supplier } = useSelector((state) => {
    return state.supplier
  });

  useEffect(() => {
    dispatch(getOneSupplier(supplierId));
  }, [dispatch, supplierId]);
  
  if (_.size(supplier) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        name: supplier.name || '',
        contact: supplier.contact || '',
        phone: supplier.phone || '',
        email: supplier.email || '',
        platform_key: supplier.platform_key || '',
      }
    }
      validationSchema={Yup.object().shape({
        name: Yup.string()
          .max(255)
          .required('Name is required'),
        contact: Yup.string()
          .max(255)
          .required('Contact is required'),
        phone: Yup.string()
          .required('Phone is required'),
        email: Yup.string()
          .email('Must be a valid email')
          .max(255)
          .required('Email is required')
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(createSupplier(values)).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Supplier created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/suppliers"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      fullWidth
                      helperText={touched.name && errors.name}
                      label="Name"
                      name="name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.contact && errors.contact)}
                      fullWidth
                      helperText={touched.contact && errors.contact}
                      label="Contact"
                      name="contact"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.contact}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.phone && errors.phone)}
                      fullWidth
                      helperText={touched.phone && errors.phone}
                      label="Phone"
                      name="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.phone}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email}
                      label="Email"
                      name="email"
                      type="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      multiline
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.platform_key && errors.platform_key)} id="platform-select-label">Platform</InputLabel>
                    <Select
                      labelId="platform-select-label"
                      id="platform_select_id"
                      name="platform_key"
                      error={Boolean(touched.platform_key && errors.platform_key)}
                      fullWidth
                      required
                      value={values.platform_key}
                      onChange={handleChange}
                    >
                      {
                        list_platform && list_platform.map((item) => (
                          <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.platform_key && errors.platform_key}</FormHelperText>
                  </Grid>
                </Grid>
                
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Supplier
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

SupplierCloneForm.propTypes = {
  className: PropTypes.string,
};

export default SupplierCloneForm;
