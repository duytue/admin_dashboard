import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getCustomerFees } from 'src/actions/customerFeeActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function CustomerFeeListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { customerFees } = useSelector((state) => state.customer_fee);
  const promotions_temp = [
    {
      id: '1',
      name: 'promotion 1',
      type: 'Order Coupon',
      code: 'BIPBIPBIP',
      max_size: 100,
      current_size: 20,
      start_time: '2020/01/01',
      end_time: '2020/02/02',
      is_active: true,
      apply_for: 'ALL PRODUCT'
    },
    {
      id: '2',
      name: 'promotion 2',
      type: 'Order Coupon',
      code: 'CENTRALCENTRAL',
      max_size: 230,
      current_size: 230,
      start_time: '2020/02/03',
      end_time: '2020/03/03',
      is_active: true,
      apply_for: ['a', 'b', 'c', 'd']
    },
    {
      id: '3',
      name: 'promotion 3',
      type: 'Order Coupon',
      code: 'JMLJML',
      max_size: 390,
      current_size: 67,
      start_time: '2020/03/04',
      end_time: '2020/04/04',
      is_active: true,
      apply_for: 'ALL PRODUCT'
    }
  ];

  useEffect(() => {
    dispatch(getCustomerFees());
  }, [dispatch]);

  if (!customerFees) {
    return null;
  }

  return (
    <Page className={classes.root} title="Promotion Management">
      <Container maxWidth={false}>
        <Header />
        {customerFees && (
          <Box mt={3}>
            <Results customerFees={promotions_temp} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default CustomerFeeListView;
