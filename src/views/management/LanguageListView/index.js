import React, {
    // useState,
    useEffect,
  } from 'react';
  import {
    Box,
    Container,
    makeStyles
  } from '@material-ui/core';
  import {
    useDispatch,
    useSelector
  } from 'react-redux';
  import Page from 'src/components/Page';
  import Header from './Header';
  import Results from './Results';
  import { getLanguages } from 'src/actions/languageActions';
  
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3)
    }
  }));
  
function LanguageListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { languages } = useSelector((state) => {
    return state.language
});
  
  useEffect(() => {
    dispatch(getLanguages());
  }, [dispatch]);
  
  if (!languages) {
    return null;
  }
  
  return (
    <Page
    className={classes.root}
    title="Language List"
    >
      <Container maxWidth={false}>
        <Header />
        {languages && (
          <Box mt={3}>
            <Results languages={languages} />
          </Box>
        )}
      </Container>
    </Page>
  );
}
  
  export default LanguageListView;
