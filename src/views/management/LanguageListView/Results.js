/* eslint-disable max-len */
import React, { useState, useCallback } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  SvgIcon,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import getInitials from 'src/utils/getInitials';
import {
  useDispatch, useSelector
} from 'react-redux';
import Axios from 'axios';
import moment from 'moment';
import _ from 'lodash';
import { getLanguages, deleteLanguage } from 'src/actions/languageActions';
import { useSnackbar } from 'notistack';

const tabs = [
  {
    value: 'all',
    label: 'All'
  },
  {
    value: 'acceptsMarketing',
    label: 'Accepts Marketing'
  },
  {
    value: 'isProspect',
    label: 'Prospect'
  },
  {
    value: 'isReturning',
    label: 'Returning'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  },
  {
    value: 'platform|desc',
    label: 'Platform'
  }
];

function applyFilters(languages, query, filters) {
  return languages.filter((language) => {
    let matches = true;

    if (query) {
      const properties = ['key', 'name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (language[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && language[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(languages, page, limit) {
  return languages.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(languages, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = languages.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px',
  },
  // tableWidth: {
  //   width: '1000px',
  // },
}));

function Results({ className, languages, ...rest }) {
  const classes = useStyles();
  const [selectedLanguages, setSelectedLanguages] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();

  const { deletedSuccess } = useSelector((state) => {
    return state.language
  });

  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelectedLanguages([]);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const deleteLanguageById = (id) => {
    dispatch(deleteLanguage(id))
    enqueueSnackbar('Language deleted', {
      variant: 'success',
      action: <Button>See all</Button>
    });
    // }
  }

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllLanguages = (event) => {
    setSelectedLanguages(event.target.checked
      ? languages.map((language) => language.id)
      : []);
  };

  const handleDeleteAllLanguage = () => {
    selectedLanguages.map((language) => {
      return Axios.delete(`/api/language/${language}`)
        .then((response) => {
        }).catch((error) => {
        })
    })
  }

  const handleSelectOneLanguage = (event, languageId) => {
    if (!selectedLanguages.includes(languageId)) {
      setSelectedLanguages((prevSelected) => [...prevSelected, languageId]);
    } else {
      setSelectedLanguages((prevSelected) => prevSelected.filter((id) => id !== languageId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const getLanguageAfterChangeActive = useCallback(() => {
    dispatch(getLanguages())
  }, [languages])


  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  // const handleChangeActiveLanguage = language => {
  //   dispatch(updateActiveLanguage(language))
  // }

  // Usually query is done on backend with indexing solutions
  const filteredLanguages = applyFilters(languages, query, filters);
  const sortedLanguages = applySort(filteredLanguages, sort);
  const paginatedLanguages = applyPagination(sortedLanguages, page, limit);
  const enableBulkOperations = selectedLanguages.length > 0;
  const selectedSomeLanguages = selectedLanguages.length > 0 && selectedLanguages.length < languages.length;
  const selectedAllLanguages = selectedLanguages.length === languages.length;

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Divider />
      <Box
        p={2}
        minHeight={56}
        display="flex"
        alignItems="center"
      >
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon
                  fontSize="small"
                  color="action"
                >
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search languages"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option
              key={option.value}
              value={option.value}
            >
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredLanguages.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllLanguages}
              indeterminate={selectedSomeLanguages}
              onChange={handleSelectAllLanguages}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllLanguage}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table stickyHeader className={classes.tableWidth}>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllLanguages}
                    indeterminate={selectedSomeLanguages}
                    onChange={handleSelectAllLanguages}
                  />
                </TableCell>
                <TableCell>
                  Key
                </TableCell>
                <TableCell>
                  Name
                </TableCell>
                <TableCell>
                  Description
                </TableCell>
                <TableCell align="right">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedLanguages.map((language) => {
                const isLanguageSelected = selectedLanguages.includes(language.id);
                return (
                  <TableRow
                    hover
                    key={language.id}
                    selected={isLanguageSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isLanguageSelected}
                        onChange={(event) => handleSelectOneLanguage(event, language.id)}
                        value={isLanguageSelected}
                      />
                    </TableCell>
                    <TableCell>
                      {language.key}
                    </TableCell>
                    <TableCell>
                      {language.name}
                    </TableCell>
                    <TableCell>
                      {language.description}
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/languages/${language.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/languages/${language.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <ArrowRightIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => {deleteLanguageById(language.id)}}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredLanguages.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  languages: PropTypes.array
};

Results.defaultProps = {
  languages: []
};

export default Results;
