import React, {
  // useState,
  useEffect
} from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getListSupplier } from 'src/actions/supplierActions';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function SupplierListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { suppliers } = useSelector(state => {
    return state.supplier;
  });

  useEffect(() => {
    dispatch(getListSupplier());
  }, [dispatch]);

  if (!suppliers) {
    return null;
  }

  return (
    <Page className={classes.root} title="Supplier List">
      <Container maxWidth={false}>
        <Header />
        {suppliers && (
          <Box mt={3}>
            <Results suppliers={suppliers} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default SupplierListView;
