/* eslint-disable max-len */
import React, { useState, useEffect, useCallback } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Button,
  Card,
  Checkbox,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  Modal,
  SvgIcon,
  Switch,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import LinkIcon from '@material-ui/icons/Link';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import {
  Edit as EditIcon,
  Search as SearchIcon,
  Check as CheckIcon,
  Printer as PrinterIcon,
  Codepen as CodepenIcon
} from 'react-feather';
import {
  deleteOrderProcess,
  updateOrderProcessIsPrinted
} from 'src/actions/orderProcessActions';
import { useDispatch } from 'react-redux';
import { useSnackbar } from 'notistack';
import printer from './printer';
import Webcam from 'react-webcam';
import QrReader from 'react-qr-reader';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'created_at|desc',
    label: 'Last create (newest first)'
  },
  {
    value: 'created_at|asc',
    label: 'Last create (oldest first)'
  }
];

function applyFilters(orderProcesses, query, filters) {
  return orderProcesses.filter((orderProcesses) => {
    let matches = true;

    if (query) {
      const properties = ['state'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          orderProcesses[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && orderProcesses[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(orderProcesses, page, limit) {
  return orderProcesses.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(orderProcesses, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = orderProcesses.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  flexText: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '15px'
  },
  smallFlex: {
    display: 'flex',
    flexDirection: 'column'
  },
  smallFlexInvi: {
    display: 'flex',
    flexDirection: 'column',
    color: 'transparent'
  },
  outsideDialog: {
    // width:
  },
  dialogContainer: {
    padding: '20px 40px'
  },
  smallMargin: {
    marginBottom: '15px'
  },
  bigTitle: {
    marginBottom: '20px'
  },
  smallTitle: {
    fontWeight: 'bold',
    fontSize: '1.3em'
  },
  pictureFrame: {
    width: '250px',
    height: '150px',
    border: '1px solid grey'
  },
  smallPictureFrame: {
    width: '180px',
    height: '120px'
  },
  alignCenter: {
    alignItems: 'center'
  },
  justifyCenter: {
    justifyContent: 'center'
  },
  imageStyle: {
    width: '100%',
    height: '100%'
  },
  bigIcon: {
    justifyContent: 'center',
    fontSize: '8em'
  },
  displayFlex: {
    display: 'flex'
  },
  marginLeft: {
    marginLeft: '75px'
  },
  qrScanner: {
    width: '500px'
  }
}));

function Results({ className, orderProcesses, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selected, setSelected] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const [showBagModal, setShowBagModal] = useState(false);
  const [currentOrderProcessObject, setCurrentOrderProcessObject] = useState(
    {}
  );
  const dispatch = useDispatch();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });
  const { enqueueSnackbar } = useSnackbar();

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setSelected([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAll = (event) => {
    setSelected(
      event.target.checked ? orderProcesses.map((obj) => obj.id) : []
    );
  };

  const handleDeleteAll = () => {
    if (window.confirm('Are you sure delete the item?')) {
      selected.map((obj) => {
        // obj is id
        dispatch(deleteOrderProcess(obj));
      });
    }
  };

  const handleSelectOne = (event, objId) => {
    if (!selected.includes(objId)) {
      setSelected((prevSelected) => [...prevSelected, objId]);
    } else {
      setSelected((prevSelected) => prevSelected.filter((id) => id !== objId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleDelete = (obj) => {
    if (window.confirm('Are you sure delete the item?')) {
      dispatch(deleteOrderProcess(obj.id));
    }
  };

  const handleCopy = (obj) => {
    navigator.clipboard.writeText(obj.url);
    enqueueSnackbar('Link has been copied.', {
      variant: 'success'
    });
  };

  const updateIsPrinted = (obj) => {
    if (window.confirm('Are you sure to mark this order process printed?')) {
      // dispatch(updatePicklistIsPrinted(obj.id));
    }
  };

  const handlePrint = (obj) => {
    setCurrentOrderProcessObject(obj);
    setShowBagModal(true);
  };

  const callPrinter = () => {
    printer(currentOrderProcessObject);
  };

  const handleClose = () => {
    setShowBagModal(false);
  };

  const handleComplete = (obj) => {
    let paginatedTemp = [];
    paginated.forEach((orderProcessItem) => {
      if (orderProcessItem.id === obj.id) {
        orderProcessItem.state = 'Packed';
      }
      paginatedTemp.push(orderProcessItem);
    });
    setPaginated(paginatedTemp);
  };

  // Usually query is done on backend with indexing solutions
  const filtered = applyFilters(orderProcesses, query, filters);
  const sorted = applySort(filtered, sort);
  // const paginated = applyPagination(sorted, page, limit);
  const [paginated, setPaginated] = useState([]);
  const enableBulkOperations = selected.length > 0;
  const selectedSome =
    selected.length > 0 && selected.length < orderProcesses.length;
  const selectedAll = selected.length === orderProcesses.length;

  useEffect(() => {
    setPaginated(applyPagination(sorted, page, limit));
  }, []);

  // const captureWebcamRef = React.useRef(null);
  // const [captureImageSrc, setCaptureImageSrc] = useState(null);

  // const captureImage = useCallback(() => {
  //   const imgSrc = captureWebcamRef.current.getScreenshot();
  //   setCaptureImageSrc(imgSrc);
  // }, [captureWebcamRef, setCaptureImageSrc]);

  const [qrContent, setQrContent] = useState(null);

  const qrScannerErrorHandler = (err) => {
    console.log(err);
  };

  const qrScannerScanHandler = (data) => {
    if (data) {
      setQrContent(data);
    }
  };

  useEffect(() => {
    console.log(qrContent);
  }, [qrContent]);

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search order process"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAll}
              indeterminate={selectedSome}
              onChange={handleSelectAll}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAll}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAll}
                    indeterminate={selectedSome}
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell>Trade Order ID</TableCell>
                <TableCell>State</TableCell>
                <TableCell>Rack</TableCell>
                <TableCell>Create Time</TableCell>
                <TableCell align="center">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginated.map((obj) => {
                const isSelected = selected.includes(obj.id);

                return (
                  <TableRow hover key={obj.id} selected={isSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isSelected}
                        onChange={(event) => handleSelectOne(event, obj.id)}
                        value={isSelected}
                      />
                    </TableCell>
                    <TableCell>{obj.trade_order_id}</TableCell>
                    <TableCell>{obj.state}</TableCell>
                    <TableCell>{obj.rack}</TableCell>
                    <TableCell>{obj.created_at}</TableCell>
                    <TableCell align="center">
                      <IconButton onClick={() => handlePrint(obj)}>
                        <SvgIcon fontSize="small">
                          <PrinterIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton onClick={() => handleComplete(obj)}>
                        {/* <RouterLink to="/app/management/package"> */}
                        <SvgIcon fontSize="small">
                          <CheckIcon />
                        </SvgIcon>
                        {/* </RouterLink> */}
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filtered.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  orderProcesses: PropTypes.array
};

Results.defaultProps = {
  orderProcesses: []
};

export default Results;
