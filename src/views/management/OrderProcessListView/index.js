import React, { useState, useEffect, useCallback } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';
import { getList } from 'src/actions/orderProcessActions';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import axios from 'src/utils/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function OrderProcessListView() {
  const classes = useStyles();
  const isMountedRef = useIsMountedRef();
  const [orderProcesses, setOrderProcesses] = useState([
    {
      id: '1',
      state: 'Created',
      trade_order_id: 'TD00001',
      rack: 'AB07',
      created_at: '20/07/2020'
    },
    {
      id: '2',
      state: 'Picking',
      trade_order_id: 'TD00002',
      rack: 'AB07',
      created_at: '20/07/2020'
    },
    {
      id: '3',
      state: 'Packing',
      trade_order_id: 'TD00003',
      rack: 'AB07',
      created_at: '20/07/2020'
    },
    {
      id: '4',
      state: 'Created',
      trade_order_id: 'TD00004',
      rack: 'AB07',
      created_at: '20/07/2020'
    },
    {
      id: '5',
      state: 'Packing',
      trade_order_id: 'TD00005',
      rack: 'AB07',
      created_at: '20/07/2020'
    }
  ]);

  if (!orderProcesses) {
    return null;
  }

  return (
    <Page className={classes.root} title="Order Process List">
      <Container maxWidth={false}>
        <Header />
        {orderProcesses && (
          <Box mt={3}>
            <Results orderProcesses={orderProcesses} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default OrderProcessListView;
