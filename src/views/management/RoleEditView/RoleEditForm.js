import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  Grid,
  TextField,
  InputLabel,
  MenuItem,
  Select,
  Checkbox,
  ListItemText,
  makeStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { useHistory } from "react-router-dom";
import { getOneRole, updateRole, updatePermissionOfRole } from 'src/actions/roleActions';
import { getListPermissions } from 'src/actions/permissionActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageUpdateRole: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectEmpty: {
    width: "100%",
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  buttonArrowDropDown: {
    minWidth: 0
  }
}));

function RoleEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [saveCallback, setSaveCallback] = useState();
  const { enqueueSnackbar } = useSnackbar();
  const roleId = _.trim(window.location.pathname.slice(22, -4), '/');
  const dispatch = useDispatch();
  const history = useHistory();
  const [openPermissionSelect, setOpenPermissionSelect] = useState(false);
  const { role, loadingUpdateRole, loadingUpdatePermissionOfRole } = useSelector((state) => {
    return state.role
  });
  const { permissions } = useSelector((state) => {
    return state.permission
  })

  useEffect(() => {
    dispatch(getOneRole(roleId));
  }, [dispatch, roleId]);

  useEffect(() => {
    dispatch(getListPermissions());
  }, [dispatch])

  const handleAddPermission = (valuePermissions) => {
    try {
      dispatch(updatePermissionOfRole({ role_id: roleId, permissions_ids: valuePermissions })).then(() => {
        enqueueSnackbar('Role updated Permission', {
          variant: 'success',
          action: <Button>See all</Button>
        });
      })
    } catch (error) {
      enqueueSnackbar('Role update failed', {
        variant: 'error',
        action: <Button>See all</Button>
      });
    }
  }

  const handleSave = (callback) => {
    setOpen(true);
    setSaveCallback(callback);
  };

  const handleClose = (action) => {
    setOpen(false);
    if (action) {
      saveCallback();
    }
  };

  if (_.size(role) === 0) {
    return null;
  }

  return (
    <>
      <Formik
        initialValues={{
          name: role.name || '',
          description: role.description || '',
          permissionsRole: _.map(role.permissions.data, 'id') || [],
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string().max(255).required('Name is required'),
          description: Yup.string(),
          permissionsRole: Yup.array(),
        })}
        onSubmit={async (values, {
          resetForm,
          setErrors,
          setStatus,
          setSubmitting
        }) => {
          // Make API request
          const updateValues = _.omit(values, ['name', 'permissionsRole']);
          const callback = () => () => dispatch(updateRole({
            ...updateValues,
            name: _.startCase(values.name),
            key: `${_.kebabCase(_.join(_.slice(_.words(values.name), 0, 2)))}${_.lowerCase(_.join(_.slice(_.words(values.name), 2)))}`,
            id: roleId,
          })).then(() => {
            resetForm();
            setStatus({ success: true });
            setSubmitting(false);
            enqueueSnackbar('Role updated', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push("/app/management/roles")
          }).catch((error) => {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            setSubmitting(false);
            enqueueSnackbar(error.response.data.message, {
              variant: 'error',
            });
          });

          handleSave(callback);
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
          isSubmitting,
          touched,
          values
        }) => (
            <form
              className={clsx(classes.rootPageUpdateRole, className)}
              onSubmit={handleSubmit}
              {...rest}
            >
              <Grid container spacing={3}>
                <Grid item xs={8}>
                  <Card>
                    <CardContent>
                      <Grid
                        container
                        spacing={3}
                      >
                        <Grid
                          item
                          md={12}
                          xs={24}
                        >
                          <TextField
                            error={Boolean(touched.name && errors.name)}
                            fullWidth
                            helperText={touched.name && errors.name}
                            label="Name"
                            name="name"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            required
                            value={values.name}
                            variant="outlined"
                          />
                        </Grid>
                        <Grid
                          item
                          md={12}
                          xs={24}
                        >
                          <TextField
                            error={Boolean(touched.description && errors.description)}
                            fullWidth
                            helperText={touched.description && errors.description}
                            label="Description"
                            name="description"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            multiline
                            rows={4}
                            value={values.description}
                            variant="outlined"
                          />
                        </Grid>
                      </Grid>
                      <Box mt={2}>
                        <Button
                          variant="contained"
                          color="secondary"
                          type="submit"
                          disabled={loadingUpdateRole}
                        >
                          Update role
                  </Button>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={4}>
                  <Card>
                    <CardContent>
                      <InputLabel shrink id="Permissions_Role">
                        Permission Role
                      </InputLabel>
                      <Select
                        labelId="Permissions_Role"
                        id="Permissions_Role"
                        label="Permission Role"
                        name="permissionsRole"
                        multiple
                        value={values.permissionsRole}
                        onChange={handleChange}
                        open={openPermissionSelect}
                        onClose={() => setOpenPermissionSelect(false)}
                        IconComponent={() => (
                          <Button className={classes.buttonArrowDropDown} onClick={() => setOpenPermissionSelect(true)}>
                            <ArrowDropDownIcon />
                          </Button>
                        )}
                        renderValue={(selected) => (
                          <div className={classes.chips}>
                            {selected.map((item) => (
                              <Chip
                                key={item}
                                label={_.get(_.find(permissions, i => i.id === item), "name")}
                                onDelete={() => setFieldValue("permissionsRole", _.reject(values.permissionsRole, i => i === item))}
                                className={classes.chip}
                              />
                            ))}
                          </div>
                        )}
                        className={clsx(classes.selectEmpty)}
                      >
                        {
                          permissions && permissions.map((item) => (
                            <MenuItem key={item.id} value={item.id}>
                              <Checkbox checked={values.permissionsRole.indexOf(item.id) > -1} />
                              <ListItemText primary={item.name} />
                            </MenuItem>
                          ))
                        }
                      </Select>
                      <Box mt={2}>
                        <Button
                          variant="contained"
                          color="secondary"
                          onClick={() => handleAddPermission(values.permissionsRole)}
                          disabled={loadingUpdatePermissionOfRole}
                        >
                          Update Permission
                        </Button>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
              </Grid>
            </form>
          )}
      </Formik>
      <Dialog
        open={open}
        onClose={() => handleClose(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Save Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Do you want to save the changes?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose(false)} color="primary">
            No
          </Button>
          <Button onClick={() => handleClose(true)} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

RoleEditForm.propTypes = {
  className: PropTypes.string,
};

export default RoleEditForm;
