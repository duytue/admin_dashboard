import React, {
  // useState,
  useEffect,
} from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import { getListPermissions } from 'src/actions/permissionActions';
import Page from 'src/components/Page';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PermissionListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { permissions } = useSelector((state) => state.permission);

  useEffect(() => {
    dispatch(getListPermissions());
  }, [dispatch]);

  if (!permissions) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Permission List"
    >
      <Container maxWidth={false}>
        <Header />
        {permissions && (
          <Box mt={3}>
            <Results users={permissions} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default PermissionListView;
