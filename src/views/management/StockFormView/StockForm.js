import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Button,
  Card,
  CardHeader,
  Table,
  TableBody,
  TableRow,
  TableCell,
  CardContent,
  Grid,
  TextField,
  makeStyles,
  MenuItem,
  FormGroup,
  FormControlLabel,
  Checkbox,
  SvgIcon,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import {
  KeyboardDatePicker,
} from '@material-ui/pickers';
import {
  Save as SaveIcon
} from 'react-feather';
import { useHistory, useParams } from 'react-router-dom';
import _ from 'lodash';
import {
  getStock,
  updateStock,
  addStock,
  clearStock
} from 'src/actions/stockActions';
import { getListItems } from 'src/actions/itemManagementAction';
import { getListStations } from 'src/actions/stationActions';
import { getListSkus } from 'src/actions/skuActions';
import { getListSupplier } from 'src/actions/supplierActions';

const useStyles = makeStyles((theme) => ({
  root: {},
  actionIcon: {
    marginRight: theme.spacing(1)
  },
  fHeight: {
    height: '100%'
  }
}));

function StockForm({
  isEdit,
  className,
  ...rest
}) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { stockId } = useParams();
  const dispatch = useDispatch();
  const { stock } = useSelector((state) => state.stock);
  const { stations } = useSelector((state) => state.station);
  const { items, loadingGetItems } = useSelector((state) => state.item);
  const { skus, loading } = useSelector((state) => state.sku);
  const { suppliers } = useSelector((state) => state.supplier);
  const history = useHistory();
  const [currentItem, setCurrentItem] = useState(null);

  const [openItem, setOpenItem] = React.useState(false);
  const [openSku, setOpenSku] = React.useState(false);
  const [listSkuByItem, setListSkuByItem] = React.useState([]);

  useEffect(() => {
    if (stockId) {
      dispatch(getStock(stockId));
    }
    dispatch(getListStations());
    dispatch(getListItems());
    dispatch(getListSkus());
    dispatch(getListSupplier());
  }, [dispatch, stockId]);

  useEffect(() => {
    if (openItem) {
      dispatch(getListItems());
    }
  }, [dispatch, openItem]);

  useEffect(() => {
    if (openSku) {
      dispatch(getListSkus());
    }
  }, [dispatch, openSku]);

  useEffect(() => {
    if (stockId) {
      const curItem = items.find((item) => item.id === stock.item_id);
      setCurrentItem(curItem);
    }
  }, [items, stock]);

  useEffect(() => {
    if (currentItem && skus.length > 0) {
      setListSkuByItem(skus.filter((sku) => sku.item_id === currentItem.id));
    }
  }, [currentItem, skus]);

  useEffect(() => () => dispatch(clearStock()), []);

  if (stockId && _.size(stock) === 0) {
    return null;
  }

  return (
    <Formik
      initialValues={{
        item_id: stock.item_id || '',
        selling_quantity: stock.selling_quantity || '',
        reserved_quantity: stock.reserved_quantity || '',
        batch_code: stock.batch_code || '',
        station_id: stock.station_id || '',
        sku_id: stock.sku_id || '',
        bin_number: stock.bin_number || '',
        expiry_date: stock.expiry_date || moment(),
        variant: stock.variant || '',
        item_names: stock.item_names || { vn: '' },
        source_sku: stock.source_sku || '',
        supplier_id: stock.supplier_id || '',
      }}
      validationSchema={Yup.object().shape({
        item_id: Yup.string().required(),
        selling_quantity: Yup.number().required(),
        reserved_quantity: Yup.number().required(),
        batch_code: Yup.number().required(),
        sku_id: Yup.string().required(),
        station_id: Yup.string().max(255).required(),
        supplier_id: Yup.string().max(255).required(),
        bin_number: Yup.string().required(),
        expiry_date: Yup.date().required(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          dispatch(stockId
            ? updateStock({ ...values, id: stockId })
            : addStock(values)).then(() => {
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              enqueueSnackbar(stockId ? 'Stock updated' : 'Stock Created', {
                variant: 'success',
                action: <Button>See all</Button>
              });
              history.push(('/app/management/stocks'));
            });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values,
        setFieldValue
      }) => (
          <form
            className={clsx(classes.root, className)}
            onSubmit={handleSubmit}
            {...rest}
          >
            <Grid
              container
              spacing={3}
            >
              <Grid item lg={8} md={8} sm={12}>
                <Card>
                  <CardHeader title="General" />
                  <CardContent>
                    <Grid container spacing={3}>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        {stockId ? (
                          <TextField
                            disabled
                            fullWidth
                            label="Product"
                            name="item"
                            value={values.item_names.vn}
                            variant="outlined"
                          />
                        ) : (
                            <Autocomplete
                              open={openItem}
                              onOpen={() => {
                                setOpenItem(true);
                              }}
                              onClose={() => {
                                setOpenItem(false);
                              }}
                              getOptionLabel={(option) => {
                                const retVal = option.names.vn || option.names.vi || option.names.en;
                                return retVal;
                              }}
                              options={items}
                              loading={openItem && loadingGetItems}
                              onChange={(event, newValue) => {
                                if (newValue) {
                                  setFieldValue('item_id', newValue.id);
                                  setFieldValue('item_names', newValue.names);
                                  setCurrentItem(newValue);
                                }
                              }}
                              onInputChange={(event) => {
                                dispatch(getListItems({ name: event.target.value }));
                              }}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  error={Boolean(touched.item_id && errors.item_id)}
                                  helperText={touched.item_id && errors.item_id}
                                  fullWidth
                                  label="Product"
                                  name="item"
                                  onBlur={handleBlur}
                                  value={values.item_names.vn}
                                  required
                                  variant="outlined"
                                />
                              )}
                            />
                          )}
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled
                          fullWidth
                          label="Brand"
                          name="product_brand"
                          value={(currentItem && currentItem.brand) || ''}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled={stockId}
                          error={Boolean(touched.supplier_id && errors.supplier_id)}
                          fullWidth
                          select
                          helperText={touched.supplier_id && errors.supplier_id}
                          label="Supplier"
                          name="supplier_id"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.supplier_id}
                          variant="outlined"
                        >
                          {suppliers.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                              {option.name}
                            </MenuItem>
                          ))}
                        </TextField>
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        {stockId ? (
                          <TextField
                            disabled
                            fullWidth
                            label="SKU"
                            name="item"
                            value={values.source_sku}
                            variant="outlined"
                          />
                        ) : (
                            <Autocomplete
                              disabled={!currentItem}
                              open={openSku}
                              onOpen={() => {
                                setOpenSku(true);
                              }}
                              onClose={() => {
                                setOpenSku(false);
                              }}
                              getOptionLabel={(option) => option.sku || option.source_sku}
                              options={listSkuByItem}
                              loading={openSku && loading}
                              onChange={(event, newValue) => {
                                if (newValue) {
                                  setFieldValue('sku_id', newValue.id);
                                  setFieldValue('source_sku', newValue.source_sku);
                                  let variant = '';
                                  if (Array.isArray(newValue.variant_values)) {
                                    newValue.variant_values.forEach((i, index) => {
                                      variant += `${i.name}: ${i.value}`;
                                      if (newValue.variant_values.length - 1 !== index) {
                                        variant += ', ';
                                      }
                                    });
                                  }
                                  setFieldValue('variant', variant);
                                }
                              }}
                              onInputChange={(event) => {
                                dispatch(getListSkus({
                                  sku: event.target.value,
                                  sku_source: event.target.value,
                                }));
                              }}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  disabled={stockId}
                                  error={Boolean(touched.sku_id && errors.sku_id)}
                                  helperText={touched.sku_id && errors.sku_id}
                                  fullWidth
                                  label="SKU"
                                  name="sku"
                                  onBlur={handleBlur}
                                  value={values.source_sku}
                                  required
                                  variant="outlined"
                                />
                              )}
                            />
                          )}
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled={stockId && !isEdit}
                          error={Boolean(touched.selling_quantity && errors.selling_quantity)}
                          fullWidth
                          helperText={touched.selling_quantity && errors.selling_quantity}
                          label="Selling Quantity"
                          name="selling_quantity"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.selling_quantity}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled={stockId && !isEdit}
                          error={Boolean(touched.reserved_quantity && errors.reserved_quantity)}
                          fullWidth
                          helperText={touched.reserved_quantity && errors.reserved_quantity}
                          label="Reserved Quantity"
                          name="reserved_quantity"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.reserved_quantity}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled
                          fullWidth
                          label="Barcode"
                          name="barcode"
                          value="N/A"
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        md={12}
                      >
                        <TextField
                          disabled
                          fullWidth
                          label="Variants"
                          name="variant"
                          value={values.variant}
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={4} md={4} sm={12}>
                <Card className={classes.fHeight}>
                  <CardHeader title="Product Attributes" />
                  <CardContent>
                    <Table>
                      <TableBody>
                        <TableRow>
                          <TableCell>Attribute 1</TableCell>
                          <TableCell>N/A</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Attribute 2</TableCell>
                          <TableCell>N/A</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Attribute 3</TableCell>
                          <TableCell>N/A</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Attribute 4</TableCell>
                          <TableCell>N/A</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Attribute 5</TableCell>
                          <TableCell>N/A</TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={12}>
                <Card>
                  <CardHeader title="Item Details" />
                  <CardContent>
                    <Grid container spacing={3}>
                      <Grid
                        item
                        lg={3}
                        md={6}
                        sm={12}
                      >
                        <TextField
                          disabled={stockId && !isEdit}
                          error={Boolean(touched.batch_code && errors.batch_code)}
                          fullWidth
                          helperText={touched.batch_code && errors.batch_code}
                          label="Batch Code"
                          name="batch_code"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.batch_code}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid
                        item
                        lg={3}
                        md={6}
                        sm={12}
                      >
                        <KeyboardDatePicker
                          disabled={stockId && !isEdit}
                          disableToolbar
                          inputVariant="outlined"
                          format="DD/MM/YYYY"
                          label="Expire Date"
                          value={values.expiry_date}
                          onChange={(value) => setFieldValue('expiry_date', value)}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </Grid>
                      <Grid
                        item
                        lg={3}
                        md={6}
                        sm={12}
                      >
                        <TextField
                          disabled={stockId && !isEdit}
                          error={Boolean(touched.station_id && errors.station_id)}
                          fullWidth
                          select
                          helperText={touched.station_id && errors.station_id}
                          label="Station"
                          name="station_id"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.station_id}
                          variant="outlined"
                        >
                          {stations.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                              {option.name}
                            </MenuItem>
                          ))}
                        </TextField>
                      </Grid>
                      <Grid
                        item
                        lg={3}
                        md={6}
                        sm={12}
                      >
                        <TextField
                          disabled={stockId && !isEdit}
                          error={Boolean(touched.bin_number && errors.bin_number)}
                          fullWidth
                          helperText={touched.bin_number && errors.bin_number}
                          label="Bin Number"
                          name="bin_number"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          required
                          value={values.bin_number}
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={12}>
                <Card>
                  <CardHeader title="Storage and Transportation attributes" />
                  <CardContent>
                    <FormGroup row>
                      {currentItem
                        && currentItem.storage_and_transport_attributes
                        && currentItem.storage_and_transport_attributes.map((attr) => (
                          <FormControlLabel disabled control={<Checkbox checked name="attribute" />} label={attr} />
                        ))}
                    </FormGroup>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={12}>
                <Button
                  variant="contained"
                  color="secondary"
                  type="submit"
                  disabled={isSubmitting}
                >
                  <SvgIcon fontSize="small" className={classes.actionIcon}>
                    <SaveIcon />
                  </SvgIcon>
                Save
              </Button>
              </Grid>
            </Grid>
          </form>
        )}
    </Formik>
  );
}

StockForm.propTypes = {
  className: PropTypes.string,
  isEdit: PropTypes.bool,
};

export default StockForm;
