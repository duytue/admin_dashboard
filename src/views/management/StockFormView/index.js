import React, { useState } from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import Page from 'src/components/Page';
import StockForm from './StockForm';
import Header from './Header';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StockFormView() {
  const classes = useStyles();
  const { stockId } = useParams();
  const [isEdit, setIsEdit] = useState(false);

  const editStock = () => {
    setIsEdit(true);
  };

  return (
    <Page
      className={classes.root}
      title={stockId ? 'Stock Edit' : 'Stock Create'}
    >
      <Container maxWidth="lg">
        <Header editStock={editStock} stockId={stockId} />
        <Box mt={3}>
          <StockForm isEdit={isEdit} />
        </Box>
      </Container>
    </Page>
  );
}

export default StockFormView;
