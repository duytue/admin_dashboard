/* eslint-disable max-len */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Link,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles
} from '@material-ui/core';
import {
  Edit as EditIcon,
  ArrowRight as ArrowRightIcon,
  Search as SearchIcon
} from 'react-feather';
import Axios from 'axios';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { getLanguageSettings, deleteLanguageSetting } from 'src/actions/languageSettingActions';
import { useDispatch } from 'react-redux';
import { useSnackbar } from 'notistack';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

const sortOptions = [
  {
    value: 'updated_at|desc',
    label: 'Last update (newest first)'
  },
  {
    value: 'updated_at|asc',
    label: 'Last update (oldest first)'
  },
  {
    value: 'orders|desc',
    label: 'Total orders (high to low)'
  },
  {
    value: 'orders|asc',
    label: 'Total orders (low to high)'
  }
];

function applyFilters(language_settings, query, filters) {
  return language_settings.filter((language_setting) => {
    let matches = true;

    if (query) {
      const properties = ['key', 'value'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (language_setting[property].toLowerCase().includes(query.toLowerCase())) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && language_setting[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(language_settings, page, limit) {
  return language_settings.slice(page * limit, page * limit + limit);
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  if (b[orderBy] > a[orderBy]) {
    return 1;
  }

  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySort(language_settings, sort) {
  const [orderBy, order] = sort.split('|');
  const comparator = getComparator(order, orderBy);
  const stabilizedThis = language_settings.map((el, index) => [el, index]);

  stabilizedThis.sort((a, b) => {
    // eslint-disable-next-line no-shadow
    const order = comparator(a[0], b[0]);

    if (order !== 0) return order;

    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  }
}));

function Results({ className, language_settings, ...rest }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedLanguageSettings, setselectedLanguageSettings] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [sort, setSort] = useState(sortOptions[0].value);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setselectedLanguageSettings([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSortChange = (event) => {
    event.persist();
    setSort(event.target.value);
  };

  const handleSelectAllUsers = (event) => {
    setselectedLanguageSettings(
      event.target.checked ? language_settings.map((language_setting) => language_setting.id) : []
    );
  };

  const handleDeleteAllLanguageSetting = () => {
    selectedLanguageSettings.map((language_setting) => Axios.delete(`/api/language-info/${language_setting.key}`)
      .then((response) => {
      })
      .catch((error) => {
      }));
  };

  const handleSelectOneUser = (event, userId) => {
    if (!selectedLanguageSettings.includes(userId)) {
      setselectedLanguageSettings((prevSelected) => [...prevSelected, userId]);
    } else {
      setselectedLanguageSettings((prevSelected) => prevSelected.filter((id) => id !== userId));
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const deleteLanguageSettingByKey = (key, platform_key) => {
    dispatch(deleteLanguageSetting(key, platform_key));
    enqueueSnackbar('Language setting deleted!', {
      variant: 'success',
    });
  }

  const filteredLanguageSettings = applyFilters(language_settings, query, filters);
  const sortedLanguageSettings = applySort(filteredLanguageSettings, sort);
  const paginatedLanguageSettings = applyPagination(sortedLanguageSettings, page, limit);
  const enableBulkOperations = selectedLanguageSettings.length > 0;
  const selectedSomeLanguageSettings = selectedLanguageSettings.length > 0 && selectedLanguageSettings.length < language_settings.length;
  const selectedAllUsers = selectedLanguageSettings.length === language_settings.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search language settings"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        <TextField
          label="Sort By"
          name="sort"
          onChange={handleSortChange}
          select
          SelectProps={{ native: true }}
          value={sort}
          variant="outlined"
        >
          {sortOptions.map((option) => (
            <option key={option.key} value={option.key}>
              {option.label}
            </option>
          ))}
        </TextField>
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredLanguageSettings.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeLanguageSettings}
              onChange={handleSelectAllUsers}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllLanguageSetting}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeLanguageSettings}
                    onChange={handleSelectAllUsers}
                  />
                </TableCell>
                <TableCell>Key</TableCell>
                <TableCell>Value</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Icon</TableCell>
                <TableCell>Platform</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedLanguageSettings.map((language_setting) => {
                const isLanguageSettingSelected = selectedLanguageSettings.includes(language_setting.id);

                return (
                  <TableRow hover key={language_setting.id} selected={isLanguageSettingSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isLanguageSettingSelected}
                        onChange={(event) => handleSelectOneUser(event, language_setting.id)}
                        value={isLanguageSettingSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/management/language-setting/${language_setting.key}/edit`}
                        variant="h6"
                      >
                        {language_setting.key}
                      </Link>
                    </TableCell>
                    <TableCell>{language_setting.value}</TableCell>
                    <TableCell>{language_setting.description}</TableCell>
                    <TableCell>{language_setting.icon}</TableCell>
                    <TableCell>{language_setting.platform}</TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/language-setting/${language_setting.key}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/language-setting/${language_setting.key}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <ArrowRightIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        component={RouterLink}
                        to="/app/management/language-setting"
                        // onClick={() => { deleteLanguageSettingByKey(language_setting.key, language_setting.platform_key) }}
                        onClick={() => { dispatch(deleteLanguageSetting(language_setting.key, language_setting.platform_key)); }}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredLanguageSettings.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  language_settings: PropTypes.array
};

Results.defaultProps = {
  language_settings: []
};

export default Results;
