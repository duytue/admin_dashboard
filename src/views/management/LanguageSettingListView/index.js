import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getLanguageSettings } from 'src/actions/languageSettingActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function LanguageSettingListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { language_settings } = useSelector((state) => state.language_setting);

  useEffect(() => {
    dispatch(getLanguageSettings());
  }, [dispatch]);

  if (!language_settings) {
    return null;
  }

  return (
    <Page className={classes.root} title="Langage Setting List">
      <Container maxWidth={false}>
        <Header />
        {language_settings && (
          <Box mt={3}>
            <Results language_settings={language_settings} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default LanguageSettingListView;
