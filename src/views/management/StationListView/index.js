import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getListStations, getListStationFunctions } from 'src/actions/stationActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function StationListView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { stations, stationFunctions } = useSelector((state) => state.station);

  useEffect(() => {
    dispatch(getListStations());
    dispatch(getListStationFunctions());
  }, [dispatch]);

  if (!stations) {
    return null;
  }

  return (
    console.log('station function create view', stationFunctions),
    <Page className={classes.root} title="Station List">
      <Container maxWidth={false}>
        <Header />
        {stations && (
          <Box mt={3}>
            <Results stations={stations} stationFunctions={stationFunctions} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default StationListView;
