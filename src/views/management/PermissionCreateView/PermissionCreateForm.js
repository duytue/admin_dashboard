import React, { useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  Card,
  CardContent,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  makeStyles
} from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { createPermission, getListObjectPermissions, getListActionPermissions } from 'src/actions/permissionActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
}));

function CustomerEditForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const { objects, actions } = useSelector((state) => {
    return state.permission
  });

  useEffect(() => {
    dispatch(getListObjectPermissions());
    dispatch(getListActionPermissions());
  }, [dispatch])

  return (
    <Formik
      initialValues={{
        object: '',
        action: '',
        description: '',
      }}
      validationSchema={Yup.object().shape({
        object: Yup.string().max(255).required('Object is required'),
        action: Yup.string().max(255).required('Action is required'),
        description: Yup.string(),
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        try {
          // Make API request
          const newValues = _.omit(values, ['object', 'action'])
          const detailObjectId = _.find(objects, i => i.id === values.object)
          const detailActiontId = _.find(actions, i => i.id === values.action)
          dispatch(createPermission({
            ...newValues,
            name: `${detailActiontId.name} ${detailObjectId.name}`,
            key: `${detailActiontId.key}_${detailObjectId.key}`,
            platform_key: "centralweb",
          })).then(() => {
            resetForm();
            setStatus({ success: true }); setSubmitting(false);
            enqueueSnackbar('Permission created', {
              variant: 'success',
              action: <Button>See all</Button>
            });
            history.push(("/app/management/permissions"))
          });
        } catch (error) {
          setStatus({ success: false });
          setErrors({ submit: error.message });
          setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.action && errors.action)} id="action-select-label">Action</InputLabel>
                    <Select
                      labelId="action-select-label"
                      id="action_select_id"
                      name="action"
                      error={Boolean(touched.action && errors.action)}
                      helperText={touched.action && errors.action}
                      fullWidth
                      required
                      value={values.action}
                      onChange={handleChange}
                    >
                      {
                        actions && actions.map((item) => (
                          <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.action && errors.action}</FormHelperText>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel error={Boolean(touched.object && errors.object)} id="object-select-label">Object</InputLabel>
                    <Select
                      labelId="object-select-label"
                      id="object_select_id"
                      name="object"
                      error={Boolean(touched.object && errors.object)}
                      fullWidth
                      required
                      value={values.object}
                      onChange={handleChange}
                    >
                      {
                        objects && objects.map((item) => (
                          <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                        ))
                      }
                    </Select>
                    <FormHelperText error>{touched.object && errors.object}</FormHelperText>
                  </Grid>
                  <Grid
                    item
                    md={12}
                    xs={24}
                  >
                    <TextField
                      error={Boolean(touched.description && errors.description)}
                      fullWidth
                      helperText={touched.description && errors.description}
                      label="Description"
                      name="description"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      multiline
                      rows={4}
                      value={values.description}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Create Permission
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )
      }
    </Formik >
  );
}

CustomerEditForm.propTypes = {
  className: PropTypes.string,
};

export default CustomerEditForm;
