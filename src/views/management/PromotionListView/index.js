import React, { useEffect } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import { getPromotions } from 'src/actions/promotionActions';
import Header from './Header';
import Results from './Results';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  }
}));

function PromotionListView() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { promotions } = useSelector((state) => state.promotion);

  useEffect(() => {
    dispatch(getPromotions());
  }, [dispatch]);

  if (!promotions) {
    return null;
  }

  return (
    <Page className={classes.root} title="Promotion Management">
      <Container maxWidth={false}>
        <Header />
        {promotions && (
          <Box mt={3}>
            <Results promotions={promotions} />
          </Box>
        )}
      </Container>
    </Page>
  );
}

export default PromotionListView;
