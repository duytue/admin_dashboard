/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import moment from 'moment';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  InputAdornment,
  Switch,
  SvgIcon,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tabs,
  TextField,
  makeStyles,
  Link
} from '@material-ui/core';
import { Edit as EditIcon, Search as SearchIcon } from 'react-feather';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { getPromotions, deletePromotion, updatePromotion } from 'src/actions/promotionActions';
import { deleteComment } from '../../../actions/reviewActions';
import { updateComment } from '../../../actions/commentActions';
// import { getUsers } from 'src/actions/userActions';

const tabs = [
  {
    value: 'all',
    label: 'All'
  }
];

// const sortOptions = [
//   {
//     value: 'updated_at|desc',
//     label: 'Last update (newest first)'
//   },
//   {
//     value: 'updated_at|asc',
//     label: 'Last update (oldest first)'
//   },
//   {
//     value: 'orders|desc',
//     label: 'Total orders (high to low)'
//   },
//   {
//     value: 'orders|asc',
//     label: 'Total orders (low to high)'
//   }
// ];

function applyFilters(promotions, query, filters, dates) {
  return promotions.filter((fee) => {
    let matches = true;

    if (query) {
      const properties = ['name'];
      let containsQuery = false;

      properties.forEach((property) => {
        if (
          fee[property] &&
          fee[property].toLowerCase().includes(query.toLowerCase())
        ) {
          containsQuery = true;
        }
      });

      if (!containsQuery) {
        matches = false;
      }
    }

    if (dates.filterStartDate && !dates.filterEndDate) {
      if (
        moment(fee.start_time).format('DD/MM/YYY') >=
        moment(dates.filterStartDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (!dates.filterStartDate && dates.filterEndDate) {
      if (
        moment(fee.end_time).format('DD/MM/YYY') <=
        moment(dates.filterEndDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    if (dates.filterStartDate && dates.filterEndDate) {
      if (
        moment(fee.start_time).format('DD/MM/YYY') >=
          moment(dates.filterStartDate).format('DD/MM/YYY') &&
        moment(fee.end_time).format('DD/MM/YYY') <=
          moment(dates.filterEndDate).format('DD/MM/YYY')
      ) {
        matches = true;
      } else {
        matches = false;
      }
    }

    Object.keys(filters).forEach((key) => {
      const value = filters[key];

      if (value && fee[key] !== value) {
        matches = false;
      }
    });

    return matches;
  });
}

function applyPagination(promotions, page, limit) {
  return promotions.slice(page * limit, page * limit + limit);
}

// function descendingComparator(a, b, orderBy) {
//   if (b[orderBy] < a[orderBy]) {
//     return -1;
//   }

//   if (b[orderBy] > a[orderBy]) {
//     return 1;
//   }

//   return 0;
// }

// function getComparator(order, orderBy) {
//   return order === 'desc'
//     ? (a, b) => descendingComparator(a, b, orderBy)
//     : (a, b) => -descendingComparator(a, b, orderBy);
// }

// function applySort(promotions, sort) {
//   const [orderBy, order] = sort.split('|');
//   const comparator = getComparator(order, orderBy);
//   const stabilizedThis = promotions.map((el, index) => [el, index]);

//   stabilizedThis.sort((a, b) => {
//     // eslint-disable-next-line no-shadow
//     const order = comparator(a[0], b[0]);

//     if (order !== 0) return order;

//     return a[1] - b[1];
//   });

//   return stabilizedThis.map((el) => el[0]);
// }

const useStyles = makeStyles((theme) => ({
  root: {},
  queryField: {
    width: 500
  },
  bulkOperations: {
    position: 'relative'
  },
  bulkActions: {
    paddingLeft: 4,
    paddingRight: 4,
    marginTop: 6,
    position: 'absolute',
    width: '100%',
    zIndex: 2,
    backgroundColor: theme.palette.background.default
  },
  bulkAction: {
    marginLeft: theme.spacing(2)
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  header_pagination: {
    display: 'flex',
    marginLeft: '-10px'
  },
  datePicker: {
    marginLeft: theme.spacing(2)
  },
  redColor: {
    color: 'red',
    fontWeight: 'bold'
  },
  greenColor: {
    color: 'green',
    fontWeight: 'bold'
  }
}));

function Results({ className, promotions, ...rest }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const [filterStartDate, setFilterStartDate] = useState(null);
  const [filterEndDate, setFilterEndDate] = useState(null);
  const [currentTab, setCurrentTab] = useState('all');
  const [selectedFees, setselectedFees] = useState([]);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [query, setQuery] = useState('');
  const [filters, setFilters] = useState({
    isProspect: null,
    isReturning: null,
    acceptsMarketing: null
  });

  const handleTabsChange = (event, value) => {
    const updatedFilters = {
      ...filters,
      isProspect: null,
      isReturning: null,
      acceptsMarketing: null
    };

    if (value !== 'all') {
      updatedFilters[value] = true;
    }

    setFilters(updatedFilters);
    setselectedFees([]);
    setCurrentTab(value);
  };

  const handleQueryChange = (event) => {
    event.persist();
    setQuery(event.target.value);
  };

  const handleSelectAllFees = (event) => {
    setselectedFees(
      event.target.checked ? promotions.map((fee) => fee.id) : []
    );
  };

  const handleDeleteAllFee = () => {
    if (window.confirm("Are you sure delete the promotion?")) {
      selectedFees.map((fee) => dispatch(deletePromotion(fee)));
    }

  };

  const handleSelectOneFee = (event, feeId) => {
    if (!selectedFees.includes(feeId)) {
      setselectedFees((prevSelected) => [...prevSelected, feeId]);
    } else {
      setselectedFees((prevSelected) =>
        prevSelected.filter((id) => id !== feeId)
      );
    }
  };

  const handleChangeActive = promotion => {
    let promotionObj = {};
    promotionObj.is_visible = !promotion.is_active;
    // dispatch(updateComment(promotionObj));
    try {
      // Make API request
      dispatch(updatePromotion({ ...promotionObj }, promotion.id)).then(() => {
        enqueueSnackbar('Promotion updated', {
          variant: 'success',
        });
      });
    } catch (error) {
      enqueueSnackbar('Promotion Fail', {
        variant: 'error',
      });
    }
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handleDelete = obj => {
    if (window.confirm("Are you sure delete the promotion?")) {
      dispatch(deletePromotion(obj.id));
    }
  };

  const filteredFees = applyFilters(promotions, query, filters, {
    filterStartDate,
    filterEndDate
  });
  // const sortedFees = applySort(filteredFees, sort);
  const paginatedFees = applyPagination(filteredFees, page, limit);
  const enableBulkOperations = selectedFees.length > 0;
  const selectedSomeFees =
    selectedFees.length > 0 && selectedFees.length < promotions.length;
  const selectedAllUsers = selectedFees.length === promotions.length;

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Tabs
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="secondary"
        value={currentTab}
        variant="scrollable"
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} value={tab.value} label={tab.label} />
        ))}
      </Tabs>
      <Divider />
      <Box p={2} minHeight={56} display="flex" alignItems="center">
        <TextField
          className={classes.queryField}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SvgIcon fontSize="small" color="action">
                  <SearchIcon />
                </SvgIcon>
              </InputAdornment>
            )
          }}
          onChange={handleQueryChange}
          placeholder="Search Promotion"
          value={query}
          variant="outlined"
        />
        <Box flexGrow={1} />
        {/* <KeyboardDatePicker
          className={classes.datePicker}
          disableToolbar
          inputVariant="outlined"
          format="DD/MM/YYYY"
          label="Start"
          value={filterStartDate}
          onChange={(value) => setFilterStartDate(value)}
          KeyboardButtonProps={{
            'aria-label': 'start date filter'
          }}
        />
        <KeyboardDatePicker
          className={classes.datePicker}
          disableToolbar
          inputVariant="outlined"
          format="DD/MM/YYYY"
          label="End"
          value={filterEndDate}
          onChange={(value) => setFilterEndDate(value)}
          KeyboardButtonProps={{
            'aria-label': 'end date filter'
          }}
        /> */}
      </Box>
      <TablePagination
        className={classes.header_pagination}
        component="div"
        count={filteredFees.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
      {enableBulkOperations && (
        <div className={classes.bulkOperations}>
          <div className={classes.bulkActions}>
            <Checkbox
              checked={selectedAllUsers}
              indeterminate={selectedSomeFees}
              onChange={handleSelectAllFees}
            />
            <Button
              variant="outlined"
              className={classes.bulkAction}
              onClick={handleDeleteAllFee}
            >
              Delete
            </Button>
          </div>
        </div>
      )}
      <PerfectScrollbar>
        <Box minWidth={700}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAllUsers}
                    indeterminate={selectedSomeFees}
                    onChange={handleSelectAllFees}
                  />
                </TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Code</TableCell>
                <TableCell>Start Time</TableCell>
                <TableCell>End Time</TableCell>
                <TableCell>Active</TableCell>
                <TableCell>Apply For</TableCell>
                <TableCell>Used / Total Amount</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedFees.map((fee) => {
                const isFeeSelected = selectedFees.includes(fee.id);

                return (
                  <TableRow hover key={fee.id} selected={isFeeSelected}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isFeeSelected}
                        onChange={(event) => handleSelectOneFee(event, fee.id)}
                        value={isFeeSelected}
                      />
                    </TableCell>
                    <TableCell>
                      <Link
                        color="inherit"
                        component={RouterLink}
                        to={`/app/management/promotion/${fee.id}/edit`}
                        variant="h6"
                      >
                        {fee.name}
                      </Link>
                    </TableCell>
                    <TableCell>{fee.type}</TableCell>
                    <TableCell>{fee.coupon_code}</TableCell>
                    <TableCell>
                      {moment(fee.start_time).format('hh:mm DD-MMM-YYYY')}
                    </TableCell>
                    <TableCell>
                      {moment(fee.end_time).format('hh:mm DD-MMM-YYYY')}
                    </TableCell>

                    <TableCell>
                      <Switch
                        disableRipple
                        checked={fee.is_active}
                        onChange={() => handleChangeActive(fee)}
                        color="secondary"
                        edge="start"
                      />
                    </TableCell>
                    <TableCell>
                      {fee.apply_for  ? ((fee.apply_for === 'ALL PRODUCT')
                        ? fee.apply_for
                        : fee.apply_for.length + ' SKUs') : 'ALL PRODUCT'}
                    </TableCell>
                    <TableCell>
                      <span
                        className={
                          fee.current_size < fee.max_size
                            ? classes.greenColor
                            : classes.redColor
                        }
                      >{`${fee.current_size} / ${fee.max_size}`}</span>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton
                        component={RouterLink}
                        to={`/app/management/promotion/${fee.id}/edit`}
                      >
                        <SvgIcon fontSize="small">
                          <EditIcon />
                        </SvgIcon>
                      </IconButton>
                      <IconButton
                        onClick={() => handleDelete(fee)}
                      >
                        <SvgIcon fontSize="small">
                          <DeleteOutlineIcon />
                        </SvgIcon>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={filteredFees.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
}

Results.propTypes = {
  className: PropTypes.string,
  promotions: PropTypes.array
};

Results.defaultProps = {
  promotions: []
};

export default Results;
