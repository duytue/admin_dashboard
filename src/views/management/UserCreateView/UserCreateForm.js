import React, { useState, useEffect } from 'react';
import {
  useDispatch,
  useSelector
} from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Chip,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
  makeStyles
} from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { useHistory } from "react-router-dom";
import { createUser } from 'src/actions/userActions';
import { getListPlatform } from 'src/actions/platformActions';
import _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  rootPageCreateView: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3)
  },
  selectBirthDay: {
    width: "100%",
    marginTop: 0,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  buttonArrowDropDown: {
    minWidth: 0,
  }
}));

function UserCreateForm({
  className,
  ...rest
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [selectBirthDay, setSelectBirthDay] = useState(new Date());
  const [openRoleSelect, setOpenRoleSelect] = useState(false);

  const { loadingCreateUser } = useSelector((state) => {
    return state.user
  });
  const { platforms } = useSelector((state) => {
    return state.platform
  })

  useEffect(() => {
    dispatch(getListPlatform())
  }, [dispatch])

  const handleChangeBirthDay = value => {
    setSelectBirthDay(value)
  }

  return (
    <Formik
      initialValues={{
        email: '',
        first_name: '',
        last_name: '',
        password: '',
        platform_keys: [],
        phone: '',
        active: true,
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
        first_name: Yup.string().max(255).required('First name is required'),
        last_name: Yup.string().max(255).required('Last name is required'),
        password: Yup.string().max(255).required('Password is required'),
        platform_keys: Yup.array(),
        phone: Yup.string().max(15),
        active: Yup.bool()
      })}
      onSubmit={async (values, {
        resetForm,
        setErrors,
        setStatus,
        setSubmitting
      }) => {
        // try {
        //   // Make API request
        //   dispatch(createUser({ ...values, birthday: selectBirthDay })).then(() => {
        //     resetForm();
        //     setStatus({ success: true }); setSubmitting(false);
        //     enqueueSnackbar('User created!', {
        //       variant: 'success',
        //       action: <Button>See all</Button>
        //     });
        //     history.push(("/app/management/users"))
        //   });
        // } catch (error) {
        //   console.log(error);
        //   enqueueSnackbar('Create user failure!', {
        //     variant: 'error'
        //   });
        //   setStatus({ success: false });
        //   setErrors({ submit: error.message });
        //   setSubmitting(false);
        // }
        try {
          await dispatch(createUser({ ...values, birthday: selectBirthDay }));
          setStatus({ success: true });
          resetForm();
          enqueueSnackbar('User created!', {
              variant: 'success'
          });
          history.push(("/app/management/users"));
        } catch (error) {
            setStatus({ success: false });
            setErrors({ submit: error.message });
            enqueueSnackbar('Create user failure!', {
                variant: 'error'
            });
        } finally {
            setSubmitting(false);
        }
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        setFieldValue,
        touched,
        values
      }) => (
          <form
            className={clsx(classes.rootPageCreateView, className)}
            onSubmit={handleSubmit}
            {...rest}
          >

            <Card>
              <CardContent>

                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      fullWidth
                      helperText={touched.email && errors.email}
                      label="Email address"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.email}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.password && errors.password)}
                      fullWidth
                      helperText={touched.password && errors.password}
                      label="Password"
                      name="password"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.password}
                      variant="outlined"
                      type="password"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.first_name && errors.first_name)}
                      fullWidth
                      helperText={touched.first_name && errors.first_name}
                      label="First name"
                      name="first_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.first_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.last_name && errors.last_name)}
                      fullWidth
                      helperText={touched.last_name && errors.last_name}
                      label="Last name"
                      name="last_name"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      required
                      value={values.last_name}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <InputLabel shrink id="Roles_User">
                      Platform Name
                    </InputLabel>
                    <Select
                      fullWidth
                      multiple
                      labelId="Platform"
                      id="Platform"
                      label="Platform"
                      name="platform_keys"
                      value={values.platform_keys}
                      open={openRoleSelect}
                      onClose={() => setOpenRoleSelect(false)}
                      IconComponent={() => (
                        <Button className={classes.buttonArrowDropDown} onClick={() => setOpenRoleSelect(true)}>
                          <ArrowDropDownIcon />
                        </Button>
                      )}
                      onChange={handleChange}
                      renderValue={(selected) => (
                        <div className={classes.chips}>
                          {selected.map((item) => (
                            <Chip
                              key={item}
                              label={item}
                              onDelete={() => setFieldValue("platform_keys", _.reject(values.platform_keys, i => i === item))}
                              className={classes.chip}
                            />
                          ))}
                        </div>
                      )}
                      className={clsx(classes.selectEmpty)}
                    >
                      {
                        platforms && platforms.map((item) => (
                          <MenuItem key={item.id} value={item.key}>
                            <Checkbox checked={values.platform_keys.indexOf(item.key) > -1} />
                            <ListItemText primary={item.key} />
                          </MenuItem>
                        ))
                      }
                    </Select>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <TextField
                      error={Boolean(touched.phone && errors.phone)}
                      fullWidth
                      helperText={touched.phone && errors.phone}
                      label="Phone number"
                      name="phone"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.phone}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <KeyboardDatePicker
                      className={clsx(classes.selectBirthDay)}
                      disableToolbar
                      variant="inline"
                      format="DD/MM/YYYY"
                      margin="normal"
                      id="Date Of Birth"
                      label="Date Of Birth"
                      value={selectBirthDay}
                      onChange={handleChangeBirthDay}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Typography
                      variant="h5"
                      color="textPrimary"
                    >
                      User Active
                  </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                    >
                      This will give the user is active to work
                  </Typography>
                    <Switch
                      checked={values.active}
                      color="secondary"
                      edge="start"
                      name="active"
                      onChange={handleChange}
                      value={values.active}
                    />
                  </Grid>
                </Grid>
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="secondary"
                    type="submit"
                    disabled={loadingCreateUser}
                  >
                    Create User
                </Button>
                </Box>
              </CardContent>
            </Card>
          </form>
        )}
    </Formik>
  );
}

UserCreateForm.propTypes = {
  className: PropTypes.string,
};

export default UserCreateForm;
